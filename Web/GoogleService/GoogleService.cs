﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace GoogleService
{
    public partial class GoogleService : ServiceBase
    {
        System.Timers.Timer _GoogleChannelTimer = new System.Timers.Timer();
        public GoogleService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            double _GoogleChannelInterval = 30000;
            try
            {

                GoogleWatchChannel GoogleWatchChannel = new GoogleWatchChannel();
                GoogleWatchChannel.GoogleChannelupdate();
                _GoogleChannelTimer.Elapsed += new System.Timers.ElapsedEventHandler(GoogleChannelInterval_Elapsed);
                _GoogleChannelTimer.Interval = _GoogleChannelInterval;
                _GoogleChannelTimer.AutoReset = true;
                _GoogleChannelTimer.Start();

            }
            catch (Exception)
            {

                throw;
            }
        }

        protected override void OnStop()
        {
            _GoogleChannelTimer.Enabled = false;
            _GoogleChannelTimer.AutoReset = false;
            _GoogleChannelTimer.Stop();
        }
        #region GoogleChannelInterval_Elapsed
        /// <summary>
        /// GoogleChannelInterval_Elapsed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GoogleChannelInterval_Elapsed(object sender, EventArgs e)
        {
            double GoogleChannelInterval = 1 * 60 * 1000;
            try
            {
                _GoogleChannelTimer.Stop();
                GoogleWatchChannel GoogleWatchChannel = new GoogleWatchChannel();
                GoogleWatchChannel.GoogleChannelupdate();

                _GoogleChannelTimer.Interval = GoogleChannelInterval;
                _GoogleChannelTimer.AutoReset = true;
                _GoogleChannelTimer.Start();

            }
            catch (Exception)
            {
            }


        }
        #endregion
    }
}
