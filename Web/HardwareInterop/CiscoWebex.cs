﻿/* Copyright (C) 2014 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/ //ZD 100886
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;


namespace NS_CiscoWebex
{

    class CiscoWebex
    {
        NS_LOGGER.Log _log = null;
        NS_MESSENGER.ConfigParams _configParams;
        private NS_DATABASE.Database _db;

        List<NS_MESSENGER.Conference> _Conf = null;
        NS_MESSENGER.Recurrence _rec = null;

        StringBuilder _inXML = null;
        string _receiveXML = "";
        string _errMsg = "";
        internal bool _isRecurring = false; 

        bool _ret = false;

       
        public CiscoWebex(NS_MESSENGER.ConfigParams _config)
        {
            _configParams = new NS_MESSENGER.ConfigParams();
            _configParams = _config;
            _log = new NS_LOGGER.Log(_configParams);
            _db = new NS_DATABASE.Database(_configParams);
        }

        #region ScheduleMeeting
        /// <summary>
        /// ScheduleMeeting
        /// </summary>
        /// <param name="_tempConf"></param>
        /// <returns></returns>
        public bool ScheduleMeeting(NS_MESSENGER.Conference _TempConf)
        {
            List<NS_MESSENGER.Conference> _confs = null;
            NS_EMAIL.Email sendEmail = null; // ZD 100221 Email
            string meetingkey="",HostURL="",AttendeeURL ="";
            XDocument _xdoc = new XDocument();
            string result = "", reason = "";
            try
            {
                sendEmail = new NS_EMAIL.Email(_configParams); // ZD 100221 Email
                _ret = false; 

                _confs = new List<NS_MESSENGER.Conference>();
                _Conf = new List<NS_MESSENGER.Conference>();
                _Conf.Add(_TempConf);

                _ret = false;
                _ret = _db.FetchConf(ref _Conf, ref _confs,NS_MESSENGER.MCU.eType.Webex, 0); //for new and edit conference need to send delStatus as 0
                if (!_ret)
                {
                    _log.Trace("Error in Fecthing the Conference List");
                    return false;
                }

                if (_TempConf.iRecurring == 1 && _isRecurring)
                {
                    _rec = new NS_MESSENGER.Recurrence();
                    _rec = (NS_MESSENGER.Recurrence)_TempConf.Recurrence.Dequeue();
                    Queue<NS_MESSENGER.Recurrence> _Enque = new Queue<NS_MESSENGER.Recurrence>();
                    _Enque.Enqueue(_rec);
                    _confs.All(Value => { Value.Recurrence.Enqueue(_Enque); return true; }); //To Enque the dequeued item again
                }
                int Distinctmeetkeyvalue = (_confs.Where(Meetid => (Meetid.sWebEXMeetingKey != null && Meetid.sWebEXMeetingKey != "")).Select(Meetid => Meetid.sWebEXMeetingKey).Distinct()).Count(); //If the existing webex followed the Recurrence pattern the Webex meetingkey will be same if not webex meeting key will be differ

                if (_rec != null)
                {
                    if (((_rec.iRecurType == 3 || _rec.iRecurType == 4) && (_rec.iSubType==2)&& (_rec.sDays == "1" || _rec.sDays == "2" || _rec.sDays == "3")) || _rec.iRecurType == 5) //For webEX  Yearly and Monthly pattern doesn't support the Day, weekday and week end option and also custom pattern, so creating conference one by one
                    {
                        if (Distinctmeetkeyvalue == 1 && !string.IsNullOrEmpty(_confs[0].sWebEXMeetingKey))
                        {
                            _confs.All(Value => { Value.iWebexExceptionHandler = 0; Value.sWebEXMeetingKey = ""; return true; }); //To create recurrece conference  as single instances
                            _ret = false;
                            _ret = DeleteSyncList(_confs[0],1); //Second parameter to existing recurrence pattern delete
                            if (!_ret)
                            {
                                _log.Trace("Error in deleting the Webex conferences");
                                return false;
                            }
                            _confs.All(Value => { Value.sWebEXMeetingKey = ""; return true; }); //To create recurrece conference  as single instances
                            Distinctmeetkeyvalue = 0;

                        }

                        _confs.All(Value => { Value.iRecurring = 0; Value.iWebexExceptionHandler = 1; return true; }); //To create recurrece conference  as single instances
                        _isRecurring = false; //To create recurrece conference  as single instances
                        _ret = ScheduleMeetforRecExcep(_confs, Distinctmeetkeyvalue);
                        if (!_ret)
                        {
                            _log.Trace("Error in Generating webEX conference create XML");
                            return false;
                        }
                        else
                            return true;
                    }
                }
                if (Distinctmeetkeyvalue > 1)
                {
                    _confs.All(Value => { Value.iWebexExceptionHandler = 0; Value.sWebEXMeetingKey = ""; return true; }); //To create recurrece conference  as single instances
                    _ret = false;
                    _ret=DeleteSyncList(_confs[0],2); // Second parameter to delete the exceptional cases 1=normal cases 2=Exceptional cases
                    if (!_ret)
                    {
                        _log.Trace("Error in deleting the Webex conferences");
                        return false;
                    }
                    _confs.All(Value => {Value.sWebEXMeetingKey = ""; return true; }); //To create recurrece conference  as single instances

                }

                _ret = false;
                _ret = ConferenceCreateXML(_confs[0], ref _inXML);

                if (!_ret)
                {
                    _log.Trace("Error in Generating webEX conference create XML");
                    return false;
                }


                _ret = false; _receiveXML = "";
                _ret = SendCommand(_confs[0].sWebExURL, _inXML.ToString(), ref _receiveXML);

                if (!string.IsNullOrEmpty(_receiveXML) && _receiveXML.ToString().IndexOf("FAILURE") > 0)
                {
                    if (_receiveXML.Contains('&'))
                        _receiveXML = _receiveXML.Replace('&', 'σ'); //Alt 229
                    if (_receiveXML.Contains("serv:"))
                        _receiveXML = _receiveXML.Replace("serv:", ""); //Alt 229

                    _xdoc = XDocument.Parse(_receiveXML);

                    var _result = _xdoc.Descendants("result").Select(s => s.Value).ToList();
                    var _reason = _xdoc.Descendants("reason").Select(s => s.Value).ToList();
                    result = _result[0].ToString();
                    reason = _reason[0].ToString();

                }
                if (result == "FAILURE" || !_ret) // ZD 100221 Email
                {

                    sendEmail.SendWebExFailureEmail(_confs[0], reason);

                    _ret = false;
                    _ret = _db.FetchWebExInfo(_confs[0].iDbID.ToString(), _confs[0].iInstanceID.ToString(), _confs[0].iRecurring.ToString(), ref meetingkey, ref HostURL, ref  AttendeeURL);
                    if (!_ret)
                    {
                        _log.Trace("Error in Fecthing Meeting Key");
                        return false;
                    }
                    _ret = false;

                    _ret = _db.UpdateConfWebEXMeetingKey(_confs[0].iDbID.ToString(), _confs[0].iInstanceID.ToString(), _confs[0].iRecurring.ToString(), meetingkey, HostURL, AttendeeURL);
                    if (!_ret)
                    {
                        _log.Trace("Error in Updating Meeting Key");
                        return false;
                    }
                }

                if (!_ret)
                {
                    _log.Trace("Error in Schedule conference in Webex");
                    return false;
                }

                _ret = false;
                _ret = ProcessConfCreateResponse(_receiveXML, _confs[0].iDbID, _confs[0].iInstanceID,_confs[0].sWebEXMeetingKey, _confs[0].iRecurring);

                if (!_ret)
                {
                    _log.Trace("Error in Spliting up the response");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                _log.Trace("webex ScheduleMeeting Block:" + ex.Message);
                return false;
            }
        }
        #endregion

        #region ScheduleMeetforRecExcep
        /// <summary>
        /// ScheduleMeetforRecExcep
        /// </summary>
        /// <param name="_conf"></param>
        /// <returns></returns>
        public bool ScheduleMeetforRecExcep(List<NS_MESSENGER.Conference> _confs,int Distinctmeetkeyvalue)
        {
          
            try
            {
                if (Distinctmeetkeyvalue != 0)
                {
                    _ret = false;
                    _ret = DeleteSyncList(_confs[0], 2);  // Second parameter to delete the exceptional cases 1=normal cases 2=Exceptional cases
                    if (!_ret)
                    {
                        _log.Trace("Failed to delete the sync list conference");
                        return false;
                    }
                }

                for (int _i = 0; _i < _confs.Count; _i++)
                {
                    _ret = false; 
                    _ret = ConferenceCreateXML(_confs[_i], ref _inXML);
                    if (!_ret)
                    {
                        _log.Trace("Error in creating the confernece confid:"+_confs[_i].iDbID+" Instance ID :"+_confs[_i].iInstanceID);
                        continue;

                    }
                    _ret = false; _receiveXML = "";
                    _ret = SendCommand(_confs[_i].sWebExURL, _inXML.ToString(), ref _receiveXML);

                    if (!_ret)
                    {
                        _log.Trace("Error in Schedule conference in Webex");
                        continue;
                    }

                    _ret = false;
                    _ret = ProcessConfCreateResponse(_receiveXML, _confs[_i].iDbID, _confs[_i].iInstanceID,_confs[_i].sWebEXMeetingKey, _confs[_i].iRecurring);

                    if (!_ret)
                    {
                        _log.Trace("Error in Spliting up the response");
                        continue;
                    }

                }

                return true;
            }
            catch (Exception ex)
            {
                _log.Trace("ScheduleMeetforRecExcep :"+ex.Message);
                return false;
            }
        }
        #endregion

        #region DeleteWebEXMeeting
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_tempConf"></param>
        /// <returns></returns>
        public bool DeleteWebEXMeeting(NS_MESSENGER.Conference _tempConf, int mode)
        {
            List<NS_MESSENGER.Conference> _ConfsList = null;
            int _i = 0;
            try
            {
                _Conf = new List<NS_MESSENGER.Conference>();
                _Conf.Add(_tempConf);


                _ConfsList = new List<NS_MESSENGER.Conference>(); _ret = false;
                _ret = _db.FetchConf(ref _Conf, ref _ConfsList, NS_MESSENGER.MCU.eType.Webex, mode);
                if (!_ret)
                {
                    _log.Trace("Error in FetchConf conference in Webex");
                    return false;
                }

                int Distinctmeetkeyvalue = (_ConfsList.Where(Meetid => (Meetid.sWebEXMeetingKey != null && Meetid.sWebEXMeetingKey != "")).Select(Meetid => Meetid.sWebEXMeetingKey).Distinct()).Count(); //If the existing webex followed the Recurrence pattern the Webex meetingkey will be same if not webex meeting key will be differ

                if (Distinctmeetkeyvalue == 1)
                    _i = _ConfsList.Count - 1;
                else
                    _i = 0;

                for (; _i < _ConfsList.Count(); _i++)
                {
                    _ret = false;
                    _ret = ConferenceDeleteXML(_ConfsList[_i], ref _inXML);
                    if (!_ret)
                    {
                        _log.Trace("Error in Delete conference in Webex");
                        continue;
                    }

                    _ret = false; _receiveXML = "";
                    _ret = SendCommand(_tempConf.sWebExURL, _inXML.ToString(), ref _receiveXML);
                    if (!_ret)
                    {
                        _log.Trace("Error in Delete conference in Webex");
                        continue;
                    }
                    if (mode == 3) //for Auto Approve after delete make the meeting key null
                        _ConfsList[0].sWebEXMeetingKey = "";
                    _ret = false;
                    _ret = ProcessConfCreateResponse(_receiveXML, _ConfsList[0].iDbID, _ConfsList[0].iInstanceID, _ConfsList[0].sWebEXMeetingKey, _ConfsList[0].iRecurring);

                    if (!_ret)
                    {
                        _log.Trace("Error in Spliting up the response");
                        continue;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                _log.Trace("webex DeleteMeeting Block:" + ex.Message);
                return false;
            }
        }
        #endregion

        #region DeleteSyncList
        /// <summary>
        /// DeleteSyncList
        /// </summary>
        /// <returns></returns>
        private bool DeleteSyncList(NS_MESSENGER.Conference _Conf,int mode)
        {
            List<NS_MESSENGER.Conference> _confListDelete = null;
            int _i = 0;
            try
            {
                _confListDelete = new List<NS_MESSENGER.Conference>();
                _confListDelete.Add(_Conf);
                
                _ret = false;
                _ret = _db.FetchSyncAdjustments(ref _confListDelete,NS_MESSENGER.MCU.eType.Webex);
                if (!_ret)
                {
                    _log.Trace("Error in Getting the FetchSyncAdjustments");
                    return false;
                }
                else
                    _confListDelete.All(Value => { Value.sWebEXUserName = _Conf.sWebEXUserName; Value.sWebEXEmail = _Conf.sWebEXEmail; Value.sWebExURL = _Conf.sWebExURL; Value.sWebEXSiteID = _Conf.sWebEXSiteID; Value.sWebEXPassword = _Conf.sWebEXPassword; Value.sWebEXPartnerID = _Conf.sWebEXPartnerID; return true; }); //To Assign Webex url, site id, partner id to the list

                if (mode == 1)
                    _i = _confListDelete.Count - 1;
                else
                    _i = 0;


                for (;_i < _confListDelete.Count; _i++)
                {
                    _ret = false;
                    _ret = ConferenceDeleteXML(_confListDelete[_i], ref _inXML);
                    if (!_ret)
                    {
                        _log.Trace("Error in Delete conference in Webex");
                        return false;
                    }

                    _ret = false; _receiveXML = "";
                    _ret = SendCommand(_Conf.sWebExURL, _inXML.ToString(), ref _receiveXML);
                    if (!_ret)
                    {
                        _log.Trace("Error in Delete conference in Webex");
                        return false;
                    }
                    if (mode == 3)
                        _confListDelete[_i].sWebEXMeetingKey = "";
                    _ret = false;
                    _ret = ProcessConfCreateResponse(_receiveXML, _Conf.iDbID, _Conf.iInstanceID, _confListDelete[_i].sWebEXMeetingKey, _Conf.iRecurring);

                    if (!_ret)
                    {
                        _log.Trace("Error in Spliting up the response");
                        return false;
                    }

                    else
                    {
                        _ret = false;
                        _ret = _db.UpdateSyncStatus(_confListDelete[_i]);
                        if (!_ret)
                            _log.Exception(100, "Delete failed in confSyncMCUAdj");
                    }

                }
               

                return true;
            }
            catch (Exception ex)
            {
                _log.Trace("DeleteSyncList block:" + ex.Message);
                return false;
            }
        }
        #endregion

        #region GenerateINXML

        #region ConferenceCreateXML
        /// <summary>
        /// ConferenceCreateXML
        /// </summary>
        /// <param name="_conf"></param>
        /// <param name="_inXML"></param>
        /// <returns></returns>
        private bool ConferenceCreateXML(NS_MESSENGER.Conference _conf, ref StringBuilder _inXML)
        {
            NS_MESSENGER.Party _party = null;
            _inXML = new StringBuilder();
           
            List<int> _value=null;
            try
            {
                _inXML.Append("<?xml version=\"1.0\"?><serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
                _inXML.Append("<header>");
                _inXML.Append("<securityContext>");
                _inXML.Append("<webExID>" + _conf.sWebEXUserName + "</webExID>");
                _inXML.Append("<password>" + _conf.sWebEXPassword + "</password>");
                _inXML.Append("<siteID>" + _conf.sWebEXSiteID + "</siteID>");
                _inXML.Append("<partnerID>" + _conf.sWebEXPartnerID + "</partnerID>");
                _inXML.Append("<email>" + _conf.sWebEXEmail + "</email>");
                _inXML.Append("</securityContext>");
                _inXML.Append("</header>");
                _inXML.Append("<body>");

                if (string.IsNullOrEmpty(_conf.sWebEXMeetingKey))
                    _inXML.Append("<bodyContent xsi:type=\"java:com.webex.service.binding.meeting.CreateMeeting\">");
                else
                {
                    _inXML.Append("<bodyContent xsi:type=\"java:com.webex.service.binding.meeting.SetMeeting\">");
                    _inXML.Append("<meetingkey>" + _conf.sWebEXMeetingKey.Trim() + "</meetingkey>");
                }

                

                _inXML.Append("<accessControl>");
                _inXML.Append("<meetingPassword>" + _conf.sWebEXMeetingpwd + "</meetingPassword>");
                _inXML.Append("</accessControl>");
                _inXML.Append("<metaData>");
                _inXML.Append("<confName>" + _conf.sExternalName + "</confName>");
                _inXML.Append("<agenda>" + _conf.sDescription + "</agenda>");  // myVRM Doesnt have any agenda so kept Description as agenda
                _inXML.Append("</metaData>");

                #region Participants
                _inXML.Append("<participants>");
                _inXML.Append("<maxUserNumber>" + _conf.qParties.Count + "</maxUserNumber>");
                _inXML.Append("<attendees>");
                while (_conf.qParties.Count > 0)
                {

                    _party = new NS_MESSENGER.Party();
                    _party = (NS_MESSENGER.Party)_conf.qParties.Dequeue();

                    _inXML.Append("<attendee>");
                    _inXML.Append("<person>");
                    _inXML.Append("<name>" + _party.sName + "</name>");
                    _inXML.Append("<email>" + _party.Emailaddress + "</email>");
                    _inXML.Append("</person>");
                    _inXML.Append("</attendee>");

                }
                _inXML.Append("</attendees>");
                _inXML.Append("</participants>");
                #endregion

                _inXML.Append("<enableOptions>");
                _inXML.Append("<chat>true</chat>");
                _inXML.Append("<poll>true</poll>");
                _inXML.Append("<audioVideo>true</audioVideo>");
                _inXML.Append("</enableOptions>");

                _inXML.Append("<schedule>");
                _inXML.Append("<startDate>" + _conf.dtStartDateTime.AddMinutes(-_conf.iMcuSetupTime).ToString("MM/dd/yyyy HH:mm:ss") + "</startDate>");
                _inXML.Append("<openTime>900</openTime>");
                _inXML.Append("<joinTeleconfBeforeHost>true</joinTeleconfBeforeHost>");
                _inXML.Append("<duration>" + _conf.iDuration + "</duration>");
                _inXML.Append("<timeZoneID>136</timeZoneID>"); //136 GMT timezoneid
                _inXML.Append("</schedule>");

                #region Recurrence
                if (_conf.iRecurring == 1 && _isRecurring)
                {
                    _rec = new NS_MESSENGER.Recurrence();
                    _rec = (NS_MESSENGER.Recurrence)_conf.Recurrence.Dequeue();
                    _inXML.Append("<repeat>");

                    switch (_rec.iRecurType)
                    {
                        case 1:
                            switch (_rec.iSubType)
                            {
                                case 1:
                                    _inXML.Append("<repeatType>DAILY</repeatType>");
                                    _inXML.Append("<interval>" + _rec.igap + "</interval>");
                                    if (_rec.iendType == 1 || _rec.iendType == 2)
                                        _inXML.Append("<afterMeetingNumber>" + _rec.ioccurrence + "</afterMeetingNumber>");
                                    else
                                        _inXML.Append("<expirationDate>" + _rec.dtendTime.ToString("MM/dd/yyyy HH:mm:ss") + "</expirationDate>");
                                    break;

                                case 2:
                                    _inXML.Append("<repeatType>DAILY</repeatType>");

                                    
                                    //var _daysToChoose = new DayOfWeek[] { DayOfWeek.Sunday, DayOfWeek.Saturday };

                                    //var _weekends = (Enumerable.Range(0, (int)(_conf.dtStartDateTime.AddDays(_rec.ioccurrence) - _conf.dtStartDateTime).TotalDays + 1)
                                    //                        .Select(d => _conf.dtStartDateTime.AddDays(d))
                                    //                        .Where(d => _daysToChoose.Contains(d.DayOfWeek))).Count();

                                    //_weekends = _rec.ioccurrence + _weekends;

                                    _inXML.Append("<dayInWeek>");
                                    //for (int _occur = 0; _occur < _weekends; _occur++)
                                    //{
                                    //    if (_conf.dtStartDateTime.AddDays(_occur).DayOfWeek.ToString().ToUpper() == "SUNDAY" || _conf.dtStartDateTime.AddDays(_occur).DayOfWeek.ToString().ToUpper() == "SATURDAY")
                                    //        continue;
                                    //    else
                                    //        _inXML.Append("<day>" + _conf.dtStartDateTime.AddDays(_occur).DayOfWeek.ToString().ToUpper() + "</day>");
                                    //}
                                    _inXML.Append("<day>MONDAY</day><day>TUESDAY</day><day>WEDNESDAY</day><day>THURSDAY</day><day>FRIDAY</day>");
                                   _inXML.Append("</dayInWeek>");
                                    if (_rec.iendType == 1 || _rec.iendType == 2)
                                        _inXML.Append("<afterMeetingNumber>" + _rec.ioccurrence + "</afterMeetingNumber>");
                                    else
                                        _inXML.Append("<expirationDate>" + _rec.dtendTime.ToString("MM/dd/yyyy HH:mm:ss") + "</expirationDate>");
                                    break;
                            }

                            break;
                        case 2:

                            _inXML.Append("<repeatType>WEEKLY</repeatType>");
                            _inXML.Append("<dayInWeek>");
                            _value = new List<int>();
                            if (_rec.sDays != null)
                                _value = _rec.sDays.Split(',').Select(value => Convert.ToInt32(value)).ToList();

                            if (_value != null & _value.Count > 0)
                            {
                                for (int _i = 0; _i < _value.Count; _i++)
                                    _inXML.Append("<day>" + Enum.GetName(typeof(NS_MESSENGER.RecurDayName), _value[_i]) + "</day>");
                            }
                            _inXML.Append("</dayInWeek>");
                            if (_rec.iendType == 1 || _rec.iendType == 2)
                                _inXML.Append("<afterMeetingNumber>" + _rec.ioccurrence + "</afterMeetingNumber>");
                            else
                                _inXML.Append("<expirationDate>" + _rec.dtendTime.ToString("MM/dd/yyyy HH:mm:ss") + "</expirationDate>");
                            break;
                        case 3:
                            switch (_rec.iSubType)
                            {
                                case 1:
                                    _inXML.Append("<repeatType>MONTHLY</repeatType>");
                                    _inXML.Append("<dayInMonth>" + _rec.idayno + "</dayInMonth>");
                                    _inXML.Append("<interval>" + _rec.igap + "</interval>");
                                    if (_rec.iendType == 1 || _rec.iendType == 2)
                                        _inXML.Append("<afterMeetingNumber>" + _rec.ioccurrence + "</afterMeetingNumber>");
                                    else
                                        _inXML.Append("<expirationDate>" + _rec.dtendTime.ToString("MM/dd/yyyy HH:mm:ss") + "</expirationDate>");
                                    break;
                                case 2:
                                    switch (_rec.sDays)
                                    {
                                        case "1":
                                        case "2":
                                        case "3":

                                            break;
                                        case "4":
                                        case "5":
                                        case "6":
                                        case "7":
                                        case "8":
                                        case "9":
                                        case "10":
                                            _inXML.Append("<repeatType>MONTHLY</repeatType>");
                                            _inXML.Append("<weekInMonth>" + _rec.idayno + "</weekInMonth>");
                                            _inXML.Append("<dayInWeek>");

                                            if (_rec.sDays != null)
                                                _value = _rec.sDays.Split(',').Select(value => Convert.ToInt32(value)).ToList();

                                            if (_value != null & _value.Count > 0)
                                            {
                                                for (int _i = 0; _i < _value.Count; _i++)
                                                    _inXML.Append("<day>" + Enum.GetName(typeof(NS_MESSENGER.RecurDayName), _value[_i] - 3) + "</day>"); // to use the same ENUM adujusted the days value
                                            }
                                            _inXML.Append("</dayInWeek>");
                                            _inXML.Append("<interval>" + _rec.igap + "</interval>");
                                            if (_rec.iendType == 1 || _rec.iendType == 2)
                                                _inXML.Append("<afterMeetingNumber>" + _rec.ioccurrence + "</afterMeetingNumber>");
                                            else
                                                _inXML.Append("<expirationDate>" + _rec.dtendTime.ToString("MM/dd/yyyy HH:mm:ss") + "</expirationDate>");
                                            break;
                                    }
                                    break;

                            }
                            break;
                        case 4:
                            switch (_rec.iSubType)
                            {
                                case 1:
                                    _inXML.Append("<repeatType>YEARLY</repeatType>");
                                    _inXML.Append("<dayInMonth>" + _rec.sDays + "</dayInMonth>");
                                    _inXML.Append("<monthInYear>" + _rec.iyearMonth + "</monthInYear>");
                                    if (_rec.iendType == 1 || _rec.iendType == 2)
                                        _inXML.Append("<afterMeetingNumber>" + _rec.ioccurrence + "</afterMeetingNumber>");
                                    else
                                        _inXML.Append("<expirationDate>" + _rec.dtendTime.ToString("MM/dd/yyyy HH:mm:ss") + "</expirationDate>");
                                    break;
                                case 2:
                                    switch (_rec.sDays)
                                    {
                                        case "1":
                                        case "2":
                                        case "3":
                                            break;
                                        case "4":
                                        case "5":
                                        case "6":
                                        case "7":
                                        case "8":
                                        case "9":
                                        case "10":
                                            _inXML.Append("<repeatType>YEARLY</repeatType>");
                                            _inXML.Append("<weekInMonth>" + _rec.idayno + "</weekInMonth>");
                                            _inXML.Append("<dayInWeek>");

                                            if (_rec.sDays != null)
                                                _value = _rec.sDays.Split(',').Select(value => Convert.ToInt32(value)).ToList();

                                            if (_value != null & _value.Count > 0)
                                            {
                                                for (int _i = 0; _i < _value.Count; _i++)
                                                    _inXML.Append("<day>" + Enum.GetName(typeof(NS_MESSENGER.RecurDayName), _value[_i] - 3) + "</day>"); // to use the same ENUM adujusted the days value
                                            }
                                            _inXML.Append("</dayInWeek>");
                                            _inXML.Append("<monthInYear>"+_rec.iyearMonth+"</monthInYear>");
                                            if (_rec.iendType == 1 || _rec.iendType == 2)
                                                _inXML.Append("<afterMeetingNumber>" + _rec.ioccurrence + "</afterMeetingNumber>");
                                            else
                                                _inXML.Append("<expirationDate>" + _rec.dtendTime.ToString("MM/dd/yyyy HH:mm:ss") + "</expirationDate>");
                                            break;
                                    }
                                    break;

                            }
                            break;

                    }
                    _inXML.Append("</repeat>");

                }
                #endregion

                _inXML.Append("<telephony>");
                _inXML.Append("<telephonySupport>CALLIN</telephonySupport>");
                _inXML.Append("<extTelephonyDescription>" + _conf.sDescription + "</extTelephonyDescription>");
                _inXML.Append("</telephony>");
                _inXML.Append("</bodyContent>");
                _inXML.Append("</body>");
                _inXML.Append("</serv:message>");


                return true;
            }
            catch (Exception ex)
            {

                _log.Trace("Error in ConferenceCreateXML Block:" + ex.Message);
                return false;
            }
        }
        #endregion

        #region ConferenceDeleteXML
        /// <summary>
        /// ConferenceDeleteXML
        /// </summary>
        /// <param name="_conf"></param>
        /// <param name="_inXML"></param>
        /// <returns></returns>
        private bool ConferenceDeleteXML(NS_MESSENGER.Conference _conf, ref StringBuilder _inXML)
        {
            _inXML = new StringBuilder();
            try
            {
                _inXML.Append("<?xml version=\"1.0\"?><serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
                _inXML.Append("<header>");
                _inXML.Append("<securityContext>");
                _inXML.Append("<webExID>" + _conf.sWebEXUserName + "</webExID>");
                _inXML.Append("<password>" + _conf.sWebEXPassword + "</password>");
                _inXML.Append("<siteID>" + _conf.sWebEXSiteID + "</siteID>");
                _inXML.Append("<partnerID>" + _conf.sWebEXPartnerID + "</partnerID>");
                _inXML.Append("<email>" + _conf.sWebEXEmail + "</email>");
                _inXML.Append("</securityContext>");
                _inXML.Append("</header>");
                _inXML.Append("<body>");
                _inXML.Append("<bodyContent xsi:type=\"java:com.webex.service.binding.meeting.DelMeeting\">");
                _inXML.Append("<meetingKey>" + _conf.sWebEXMeetingKey + "</meetingKey>");
                _inXML.Append("</bodyContent>");
                _inXML.Append("</body>");
                _inXML.Append("</serv:message>");

                return true;

            }
            catch (Exception ex)
            {
                _log.Trace("Error ConferenceDeleteXML:" + ex.Message);
                return false;
            }
        }
        #endregion

        #endregion

        #region WebService Call
        /// <summary>
        /// SendCommand
        /// </summary>
        /// <param name="_strUrl"></param>
        /// <param name="_inXML"></param>
        /// <param name="_receiveXML"></param>
        /// <returns></returns>
        private bool SendCommand(string _strUrl, string _inXML, ref string _receiveXML)
        {
            try
            {

                _log.Trace("***********************Entering into WebEX Call*****************************");
                Uri _mcuUri = new Uri(_strUrl);
                _log.Trace("URL:" + _strUrl);
                NetworkCredential mcuCredential = new NetworkCredential();
                CredentialCache mcuCredentialCache = new CredentialCache();
                mcuCredentialCache.Add(_mcuUri, "Basic", mcuCredential);
                HttpWebRequest _req = (HttpWebRequest)WebRequest.Create(_strUrl);

                _req.KeepAlive = false;
                _req.PreAuthenticate = true;
                _req.Credentials = CredentialCache.DefaultCredentials;
                _req.UseDefaultCredentials = true;
                _req.ProtocolVersion = HttpVersion.Version10;

                _req.ContentType = "text/xml";
                _req.Method = "POST";
                System.IO.Stream _stream = _req.GetRequestStream();

                _log.Trace("InXML:" + _inXML);
                byte[] _arrBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(_inXML);
                _stream.Write(_arrBytes, 0, _arrBytes.Length);
                _stream.Close();

                WebResponse _resp = _req.GetResponse();
                Stream _respStream = _resp.GetResponseStream();
                StreamReader _rdr = new StreamReader(_respStream, System.Text.Encoding.ASCII);
                _receiveXML = _rdr.ReadToEnd();

                if(!string.IsNullOrEmpty(_receiveXML))
                {
                    if (_receiveXML.ToLower().Contains("failue"))
                    {
                        _log.Trace("WebEX Response:" + _receiveXML);
                        return false;
                    }
                }

                _log.Trace("WebEX Response:" + _receiveXML);

                return true;
            }
            catch (WebException wex)
            {
                string _error = "We are into webexception";

                _log.Exception(100, _error);
                //FB 2689 Ends
                if (wex.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)wex.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            _error = reader.ReadToEnd();
                            _error = "Error exception from Webex = " + _error;

                            _log.Exception(100, _error);

                        }
                    }
                }
                if (_error == "")
                    _log.Exception(100, "EMTPY!!!!!");
                return false;
            }
            catch (Exception ex)
            {
                _log.Trace("Error in SendCommand" + ex.Message);
                return false;
            }
        }

        #endregion

        #region ProcessResponse
        private bool ProcessConfCreateResponse(string _receiveXML, int _confid, int _instanceid,string _MeetKEY,int _Recuring)
        {
            XDocument _xdoc = new XDocument();
            string _MeetKey = "";
            try
            {
                if (!string.IsNullOrEmpty(_receiveXML))
                {
                    if (_receiveXML.Contains('&'))
                        _receiveXML = _receiveXML.Replace('&', 'σ'); //Alt 229
                    if (_receiveXML.Contains("meet:"))
                        _receiveXML = _receiveXML.Replace("meet:", "");
                    if (_receiveXML.Contains("serv:"))
                        _receiveXML = _receiveXML.Replace("serv:", "");
                   

                    _xdoc = XDocument.Parse(_receiveXML);

                    var _MeetingKeyList = _xdoc.Descendants("meetingkey").Select(s => s.Value).ToList();
                    var _HostURL = _xdoc.Descendants("host").Select(s => s.Value).ToList();
                    var _AttendeeURL = _xdoc.Descendants("attendee").Select(s => s.Value).ToList();

                    _HostURL[0] = _HostURL[0].Replace('σ', '&');
                    _AttendeeURL[0] = _AttendeeURL[0].Replace('σ', '&');

                    if (_MeetingKeyList.Count > 0)
                        _MeetKey = _MeetingKeyList[0].Trim();
                    else
                        _MeetKey = _MeetKEY;

                    _ret = false;
                    _ret = _db.UpdateConfWebEXMeetingKey(_confid.ToString(), _instanceid.ToString(), _Recuring.ToString(), _MeetKey.Trim(), _HostURL[0], _AttendeeURL[0]); 
                    if (!_ret)
                    {
                        _log.Trace("Error in Updating Meeting Key");
                        return false;
                    }
                }
                else
                  _log.Trace("Response from webex server is null");
              


                return true;
            }
            catch (Exception ex)
            {
                _log.Trace("ProcessConfCreateResponse Block:" + ex.Message);
                return false;
            }
        }
        #endregion



    }
}
