/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.4.0 Starts(23 Nov 2013)                */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.4.0 Ends(23 Dec 2013)                */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3 Starts(24 Dec 2013)                */
/*                                                                                              */
/* ******************************************************************************************** */

/* ********************** ZD 100369 Starts ********************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.MCU_list_D ADD
	EnablePollFailure int NULL,
	PollCount int NULL,
	MultipleAddress nvarchar(MAX) NULL
GO
COMMIT

update MCU_list_D set EnablePollFailure=0 ,PollCount=-2 ,MultipleAddress = ''

/* ********************** ZD 100369 End ********************** */


/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.1 Ends(26 Dec 2013)                */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.2 Starts(26 Dec 2013)                */
/*                                                                                              */
/* ******************************************************************************************** */


/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.2 Ends(30 Dec 2013)                */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.3 Starts(31 Dec 2013)                */
/*                                                                                              */
/* ******************************************************************************************** */

/* ********************** ZD 100518 Starts 31 Dec 2013 ********************** */
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	MaxParticipants int NULL,
	MaxConcurrentCall int NULL
GO
COMMIT

Update Org_Settings_D set MaxParticipants=0,MaxConcurrentCall=0
/* ********************** ZD 100518 Ends 31 Dec 2013 ********************** */


/* ********************** ZD 100665 Starts 02 Jan 2014 ********************** */

Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (60,'HUAWEI Room Presence (RP100-46S/55S, RP200-46S/55S)',12)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (61,'HUAWEI TP3200 Series Panovision Telepresence',12)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (62,'HUAWEI TP3100 Series Immersive Telepresence',12)

/* ********************** ZD 100665 Ends 02 Jan 2014 ********************** */


/* ********************** ZD 100675 Start 03 Jan 2014 ********************** */

update MCU_Params_D set LoginURL = '' , PassPhrase = '' where BridgeTypeid = 11

update MCU_Params_D set LoginURL = 'login_change.html' , PassPhrase = 'user_name={0}||password={1}||ok=OK' where BridgeTypeid = 12

/* ********************** ZD 100675 End 03 Jan 2014 ********************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.3 Ends(3 Jan 2014)                */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.4 Starts(3 Jan 2014)                */
/*                                                                                              */
/* ******************************************************************************************** */
/* ********************** ZD 100629  Start 06 Jan 2014 ********************** */

update ept_list_d set EptOnlineStatus=0

/* ********************** ZD 100629 Ends 06 Jan 2014 ********************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.4 Ends(6 Dec 2013)                */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.5 Starts(7 Dec 2013)                */
/*                                                                                              */
/* ******************************************************************************************** */

/* ********************** ZD 100629 Ends 07 Jan 2014 ********************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Sys_WhygoSettings_D ADD
	WhyGoAdminEmail nvarchar(512) NULL
GO
ALTER TABLE dbo.Sys_WhygoSettings_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

update Sys_WhygoSettings_D set WhyGoAdminEmail=''

/* ********************** ZD 100629 Ends 07 Jan 2014 ********************** */
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.5 Ends(7 Dec 2013)                */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.6 Starts(8 Dec 2013)                */
/*                                                                                              */
/* ******************************************************************************************** */
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.6 Ends(7 Dec 2013)                */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.7 Starts(8 Dec 2013)                */
/*                                                                                              */
/* ******************************************************************************************** */
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.7 Ends(7 Dec 2013)                */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.8 Starts(15 jan 2014)                */
/*                                                                                              */
/* ******************************************************************************************** */

/* ********************** ZD 100707 Starts ********************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	ShowHideVMR int NULL,
	EnablePersonaVMR int NULL,
	EnableRoomVMR int NULL,
	EnableExternalVMR int NULL
GO
COMMIT

update Org_Settings_D set ShowHideVMR=0 ,EnablePersonaVMR=0 ,EnableRoomVMR=0 ,EnableExternalVMR=0


/* ********************** ZD 100707 End ********************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.8 Ends(16 Dec 2013)                */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.9 Starts(16 jan 2014)                */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.9 Ends(16 Dec 2013)                */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.10 Starts(17 jan 2014)                */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.10 Ends(20 Dec 2013)                */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.11 Starts(20 jan 2014)                */
/*                                                                                              */
/* ******************************************************************************************** */
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.11 Ends(20 Jan 2014)                */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.12 Starts(20 jan 2014)                */
/*                                                                                              */
/* ******************************************************************************************** */
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.12 Ends(23 Dec 2014)                */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.13 Starts(23 jan 2014)                */
/*                                                                                              */
/* ******************************************************************************************** */
/* ********************** ZD 100753 Starts 24 Jan 2014 ********************** */
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Loc_Room_D ADD
	[Password] nvarchar(50) NULL
GO
COMMIT

Update Loc_Room_D set [Password]=''
/* ********************** ZD 100753 Ends 24 Jan 2014 ********************** */

/* ********************** ZD 100719 Starts 28 Jan 2014 ********************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnableHotdeskingConference smallint NULL
GO
COMMIT

Update Org_Settings_D set EnableHotdeskingConference = 0

/* ********************** ZD 100719 Ends 28 Jan 2014 ********************** */



/* ********************** ZD 100520 Starts 21 Jan 2014 ********************** */

Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (63,'Polycom RealPresence G300',3)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (64,'Polycom RealPresence G500',3)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (65,'Polycom RealPresence G700',3)

/* ********************** ZD 100520 Ends 21 Jan 2014 ********************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.13 Ends(28 Jan 2014)                */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.14 Starts(28 jan 2014)                */
/*                                                                                              */
/* ******************************************************************************************** */
/* ********************** ZD 100755 Starts 28 Jan 2014 ********************** */
ALTER TABLE Usr_List_D ADD StaticID varchar(255);
ALTER TABLE Usr_List_D ADD IsStaticIDEnabled Int;

ALTER TABLE Usr_Inactive_D ADD StaticID varchar(255);
ALTER TABLE Usr_Inactive_D ADD IsStaticIDEnabled Int;
/* ********************** ZD 100755 Starts 28 Jan 2014 ********************** */

/* ********************** ZD 100221 Starts 31 Jan 2014 ********************** */
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	WebExURL nvarchar(MAX) NULL,
	WebExSiteID nvarchar(50) NULL,
	WebExPartnerID nvarchar(50) NULL,
	WebexUserLimit int NULL
GO
COMMIT

update Org_Settings_D set WebexUserLimit=0, WebExURL='',WebExSiteID='',WebExPartnerID=''

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	enableWebexUser smallint NULL,
	WebexUserName nvarchar(50) NULL,
	WebexPassword nvarchar(50) NULL
GO
COMMIT

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
	enableWebexUser smallint NULL,
	WebexUserName nvarchar(50) NULL,
	WebexPassword nvarchar(50) NULL
GO
COMMIT
 
Update Usr_List_D Set enableWebexUser=0, WebexUserName='', WebexPassword=''
Update Usr_Inactive_D Set enableWebexUser=0,WebexUserName='', WebexPassword=''
--Need to update License in Sys_Settings_d
--Added New Tag 'MaxWebexUsers' under User Tag.
--<MaxWebexUsers>5</MaxWebexUsers>
--<myVRMSiteLicense><IR>1-QSS09</IR><Site><ExpirationDate>10/30/2025</ExpirationDate><Cloud>0</Cloud><PublicRoomService>0</PublicRoomService><MaxOrganizations>2</MaxOrganizations><IsLDAP>1</IsLDAP><ServerActivation><ProcessorIDs><ID>BFEBFBFF000206A7</ID><ID>BFEBFBFF000206A7</ID></ProcessorIDs><MACAddresses><ID>EC:A8:6B:79:86:E5</ID><ID>EC:A8:6B:79:86:E5</ID></MACAddresses></ServerActivation></Site><Organizations><Rooms><MaxNonVideoRooms>50</MaxNonVideoRooms><MaxVideoRooms>50</MaxVideoRooms><MaxGuestRooms>5</MaxGuestRooms><MaxVMRRooms>50</MaxVMRRooms><MaxROHotdesking>10</MaxROHotdesking><MaxVCHotdesking>10</MaxVCHotdesking></Rooms><Hardware><MaxStandardMCUs>5</MaxStandardMCUs><MaxEnhancedMCUs>3</MaxEnhancedMCUs><MaxEndpoints>60</MaxEndpoints><MaxCDRs></MaxCDRs></Hardware><Users><MaxTotalUsers>70</MaxTotalUsers><MaxGuestsPerUser>10</MaxGuestsPerUser><MaxExchangeUsers>10</MaxExchangeUsers><MaxDominoUsers>10</MaxDominoUsers><MaxMobileUsers>10</MaxMobileUsers><MaxPCUsers>10</MaxPCUsers><MaxWebexUsers>5</MaxWebexUsers></Users><Modules><MaxFacilitiesModules>2</MaxFacilitiesModules><MaxCateringModules>2</MaxCateringModules><MaxHousekeepingModules>2</MaxHousekeepingModules><MaxAPIModules>2</MaxAPIModules><MaxBlueJeans>2</MaxBlueJeans><MaxJabber>2</MaxJabber><MaxLync>2</MaxLync><MaxVidtel>2</MaxVidtel><MaxAdvancedReport>2</MaxAdvancedReport></Modules><Languages><MaxSpanish></MaxSpanish><MaxMandarin></MaxMandarin><MaxHindi></MaxHindi><MaxFrench></MaxFrench></Languages></Organizations></myVRMSiteLicense>

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	WebExConf smallint NULL,
	WebExPW nvarchar(50) NULL,
	WebExMeetingKey nvarchar(100) NULL,
	WebExHostURL nvarchar(Max) NULL
GO
COMMIT

Update Conf_Conference_D set WebExConf= 0, WebExPW='',WebExMeetingKey='',WebExHostURL=''

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_User_D ADD
	WebEXAttendeeURL  nvarchar(Max) NULL
	
GO
COMMIT

Update Conf_User_D set WebEXAttendeeURL= ''

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_SyncMCUAdj_D ADD
	WebExMeetingKey  nvarchar(Max) NULL,
	WebExInstanceHandle int NULL,
	WebExHostURL nvarchar(Max) NULL,
	WebEXAttendeeURL nvarchar(Max) NULL
GO
COMMIT

Update Conf_SyncMCUAdj_D set WebExMeetingKey= '',WebExInstanceHandle=0,WebExHostURL='',WebEXAttendeeURL=''


/* ********************** ZD 100221 Ends 28 Jan 2014 ********************** */

--ZD 100393
Update Acc_Scheme_S set name='None' where id=0
--ZD 100393

/* ********************** ZD 100757 Starts 03 Feb 2014 ********************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Inv_WorkOrder_D ADD
	woCatAdmin int NULL
GO
COMMIT

Update Inv_WorkOrder_D set woCatAdmin = 0


/* ********************** ZD 100757 Starts 03 Feb 2014 ********************** */

/* ********************** ZD 100872 (100172) Starts 3 Feb 2014 ********************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	LandingPageTitle nvarchar(200) NULL,
	LandingPageLink nvarchar(200) NULL
GO
COMMIT

Update Usr_List_D set LandingPageTitle='',LandingPageLink=''


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
	LandingPageTitle nvarchar(200) NULL,
	LandingPageLink nvarchar(200) NULL
GO
COMMIT

Update Usr_Inactive_D set LandingPageTitle='',LandingPageLink=''

/* ********************** ZD 100872 (100172) Ends 3 Feb 2014 ********************** */
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.14 Ends(06 Feb 2014)                 */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.15 Starts(06 Feb  2014)              */
/*                                                                                              */
/* ******************************************************************************************** */
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.15 Ends(06 Feb 2014)                 */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.15 Starts(06 Feb 2014)               */
/*                                                                                              */
/* ******************************************************************************************** */
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.16 Start(06 Feb 2014)                 */
/*                                                                                              */
/* ******************************************************************************************** */

/* ************** ZD 100822 Starts ********************************/

	
insert into Mcu_Vendor_S (id,name,BridgeInterfaceId,videoparticipants,audioparticipants) values(15,'Cisco TMS',8,50,50)

INSERT INTO MCU_Params_D([BridgeName],[BridgeTypeid],[ConfLockUnLock],[confMessage],[confAudioTx],[confAudioRx],[confVideoTx],[confVideoRx]
           ,[confLayout],[confCamera],[confPacketloss],[confRecord],[partyBandwidth],[partySetfocus],[partyMessage],[partyAudioTx],[partyAudioRx],[partyVideoTx]
           ,[partyVideoRx],[partyLayout],[partyCamera],[partyPacketloss],[partyImagestream],[partyLockUnLock],[partyRecord],[partyLecturemode],[confMuteAllExcept]
           ,[confUnmuteAll],[partyLeader],[LoginURL],[PassPhrase],[activeSpeaker])
     VALUES
           ('Cisco TMS',15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'','','')
           
  
/* ************** ZD 100822 Ends ********************************/

/*   Features & Bugs for V2.9.3.17 Start(07 Feb 2014)                 */

/* ************** ZD 100814 START ********************************/


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Ept_List_D ADD
	UserName nvarchar(50) NULL
GO
ALTER TABLE dbo.Ept_List_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


/* ************** ZD 100814 END ********************************/


/* ************** ZD 100632 START ********************************/


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_User_D ADD
	TxAudioPacketsSent nvarchar(50) NULL,
	TxVideoPacketsSent nvarchar(50) NULL
GO
COMMIT

update Conf_User_D SET TxAudioPacketsSent='',TxVideoPacketsSent=''

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Room_D ADD
	TxAudioPacketsSent nvarchar(50) NULL,
	TxVideoPacketsSent nvarchar(50) NULL
GO
COMMIT

update Conf_Room_D SET TxAudioPacketsSent='',TxVideoPacketsSent=''

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Cascade_D ADD
	TxAudioPacketsSent nvarchar(50) NULL,
	TxVideoPacketsSent nvarchar(50) NULL
GO
COMMIT

update Conf_Cascade_D SET TxAudioPacketsSent='',TxVideoPacketsSent=''



/* ************** ZD 100632 END ********************************/

/* ************** ZD 100888 Starts 10 Feb 2014 ********************************/


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Public_CountryDataVocabulary ADD
	myVRMID int  NULL
GO

COMMIT

Update Public_CountryDataVocabulary set myVRMID=0


INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (239, N'Scotland', 0)

INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (240, N'Wales ', 0)

INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (241, N'Cote d''Ivoire', 0)

INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (242, N'Kosovo', 0)

INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (243, N'Isle of Man', 0)

INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (244, N'Channel Islands', 0)

INSERT INTO [dbo].[Gen_Country_S] ([CountryID], [CountryName], [Selected]) VALUES (245, N'Isle of Wight', 0)


truncate table [Public_CountryDataVocabulary]
/****** Object:  Table [dbo].[Public_CountryDataVocabulary]    Script Date: 02/06/2014 05:20:26 ******/
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (1, N'Australia', 13)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (2, N'New Zealand', 152)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (3, N'USA', 225)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (82, N'Pakistan', 161)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (160, N'Argentina', 10)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (161, N'Austria', 14)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (162, N'Barbados', 19)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (163, N'Belgium', 21)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (164, N'Bolivia', 26)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (165, N'Brazil', 30)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (167, N'Bulgaria', 33)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (168, N'Canada', 38)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (171, N'Chile', 43)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (172, N'China', 44)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (173, N'Colombia', 47)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (175, N'Croatia', 53)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (176, N'Cyprus', 55)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (177, N'Czech Republic', 56)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (178, N'Denmark', 57)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (179, N'Egypt', 62)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (180, N'Estonia', 66)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (181, N'Fiji', 70)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (182, N'Finland', 71)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (183, N'France', 72)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (184, N'French Guiana', 73)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (185, N'French Polynesia', 74)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (186, N'Germany', 79)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (187, N'Greece', 82)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (188, N'Guadeloupe', 85)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (189, N'Hungary', 96)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (190, N'India', 98)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (191, N'Indonesia', 99)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (192, N'Ireland, Republic of', 102)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (193, N'Israel', 103)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (194, N'Italy', 104)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (195, N'Jamaica', 105)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (196, N'Japan', 106)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (197, N'Jordan', 107)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (198, N'Luxembourg', 123)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (199, N'Macedonia', 125)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (200, N'Malaysia', 128)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (201, N'Malta', 131)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (202, N'Martinique', 133)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (203, N'Mauritius', 135)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (204, N'Morocco', 143)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (205, N'Namibia', 146)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (206, N'Netherlands', 149)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (207, N'Norway', 159)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (208, N'Oman', 160)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (209, N'Panama', 164)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (210, N'Peru', 167)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (211, N'Philippines', 168)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (212, N'Poland', 170)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (213, N'Portugal', 171)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (214, N'Puerto Rico', 172)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (215, N'Republic of San Marino', 184)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (216, N'La Reunion', 174)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (217, N'Romania', 175)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (218, N'Russian Federation', 176)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (219, N'Saudi Arabia', 186)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (220, N'Singapore', 191)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (221, N'South Africa', 196)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (222, N'Korea, Republic of', 112)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (223, N'Spain', 198)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (224, N'Sri Lanka',199)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (225, N'Sweden', 204)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (226, N'Switzerland', 205)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (227, N'Taiwan', 207)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (228, N'Thailand', 210)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (230, N'Turkey', 217)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (231, N'United Arab Emirates', 223)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (232, N'United Kingdom', 224)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (233, N'Uruguay', 227)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (234, N'Venezuela', 230)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (237, N'Vietnam', 231)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (238, N'Qatar', 173)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (240, N'Uganda', 221)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (241, N'Trinidad & Tobago', 215)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (242, N'Lebanon', 117)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (243, N'Kuwait', 113)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (244, N'Bangladesh',18)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (245, N'Tanzania', 209)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (246, N'Ireland, Northern',102)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (247, N'Scotland', 239)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (248, N'England', 224)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (249, N'Wales', 240)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (250, N'Bahrain', 17)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (251, N'Guatemala', 87)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (253, N'Slovakia', 192)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (254, N'Tunisia', 216)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (255, N'Ukraine', 222)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (256, N'Dominican Republic', 60)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (257, N'Latvia', 116)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (258, N'Mexico', 137)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (259, N'Ghana', 80)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (260, N'Botswana', 28)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (261, N'Papua New Guinea', 165)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (262, N'Cambodia', 36)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (263, N'Afghanistan', 1)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (264, N'Mozambique', 144)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (266, N'Iran', 100)
GO
print 'Processed 100 total records'
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (267, N'Iraq', 101)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (268, N'Yemen', 236)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (270, N'Syria', 206)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (271, N'Kazakhstan', 108)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (272, N'Mongolia', 141)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (273, N'Costa Rica', 52)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (274, N'Nigeria', 155)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (275, N'Lithuania', 122)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (276, N'Gibraltar', 81)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (277, N'Bermuda', 24)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (278, N'Bosnia-Herzegovina', 27)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (279, N'Albania', 2)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (280, N'Saint Kitts and Nevis', 179)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (281, N'Maldives', 129)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (282, N'Serbia', 188)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (283, N'Kenya', 109)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (284, N'Cote d''Ivoire', 241)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (285, N'Monaco', 140)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (286, N'US Virgin Islands', 233)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (287, N'French West Indies', 75)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (288, N'Slovenia', 193)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (289, N'Zimbabwe', 238)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (290, N'Sudan', 200)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (291, N'Ethiopia', 67)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (292, N'Burkino Faso', 34)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (293, N'Senegal', 187)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (294, N'Mauritania', 134)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (295, N'Ecuador', 61)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (297, N'Netherlands Antilles', 150)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (298, N'Saint Lucia', 180)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (299, N'Palestinian Authority', 163)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (300, N'Timor Leste', 211)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (301, N'Georgia', 78)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (302, N'Kosovo', 242)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (303, N'Armenia', 11)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (304, N'Azerbaijan', 15)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (305, N'Madagascar', 126)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (306, N'Nicaragua', 153)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (307, N'Benin', 23)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (308, N'Isle of Man', 243)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (309, N'Cayman Islands', 40)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (310, N'El Salvador', 63)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (312, N'Moldova', 139)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (313, N'Channel Islands', 244)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (314, N'Liechtenstein', 121)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (316, N'Brunei Darussalam', 32)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (317, N'Iceland', 97)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (318, N'Mali', 130)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (319, N'Cameroon', 37)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (321, N'Libya', 120)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (323, N'Bahamas', 16)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (324, N'Isle of Wight', 245)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (325, N'Nepal', 148)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (326, N'Belarus', 20)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (327, N'Sierra Leone', 190)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (328, N'Zambia', 237)
INSERT [dbo].[Public_CountryDataVocabulary] ([countryID], [countryName], [myVRMID]) VALUES (329, N'Vanuatu', 229)

/* ************** ZD 100888 Ends 10 Feb 2014 ********************************/



/* ************** ZD 100736 START 11 Feb 2014 ********************************/

Update Gen_VideoEquipment_S set VEname = 'Cisco C20 *' where familyid = 1 and VEid=30
Update Gen_VideoEquipment_S set VEname = 'Cisco C40 *' where familyid = 1 and VEid=31
Update Gen_VideoEquipment_S set VEname = 'Cisco C60 *' where familyid = 1 and VEid=32
Update Gen_VideoEquipment_S set VEname = 'Cisco C90 *' where familyid = 1 and VEid=33
Update Gen_VideoEquipment_S set VEname = 'Cisco MXP *' where familyid = 1 and VEid=35
Update Gen_VideoEquipment_S set VEname = 'Cisco EX90 *' where familyid = 1 and VEid=34
Update Gen_VideoEquipment_S set VEname = 'Cisco EX60 *' where familyid = 1 and VEid=55
Update Gen_VideoEquipment_S set VEname = 'Polycom HDX 400x *' where familyid = 2 and VEid=25
Update Gen_VideoEquipment_S set VEname = 'Polycom HDX 900x *' where familyid = 2 and VEid=26
Update Gen_VideoEquipment_S set VEname = 'Polycom HDX 600x *' where familyid = 2 and VEid=36
Update Gen_VideoEquipment_S set VEname = 'Polycom HDX 700x *' where familyid = 2 and VEid=37
Update Gen_VideoEquipment_S set VEname = 'Polycom HDX 800x *' where familyid = 2 and VEid=38
Update Gen_VideoEquipment_S set VEname = 'Polycom VSX 8000x *' where familyid = 3 and VEid=1
Update Gen_VideoEquipment_S set VEname = 'Polycom VSX 7000x *' where familyid = 3 and VEid=2
Update Gen_VideoEquipment_S set VEname = 'Polycom VSX 5000 *' where familyid = 3 and VEid=3
Update Gen_VideoEquipment_S set VEname = 'Polycom VSX 3000 *' where familyid = 3 and VEid=4
Update Gen_VideoEquipment_S set VEname = 'Polycom VSX 6000A *' where familyid = 3 and VEid=40
Update Gen_VideoEquipment_S set VEname = 'Polycom VSX 7000E *' where familyid = 3 and VEid=42
Update Gen_VideoEquipment_S set VEname = 'ViewStation FX and EX *' where familyid = 4 and VEid=44
Update Gen_VideoEquipment_S set VEname = 'ViewStation 4000 *' where familyid = 4 and VEid=45
Update Gen_VideoEquipment_S set VEname = 'VVX 1500' where familyid = 7 and VEid=49
Delete from Gen_VideoEquipment_S where VEid = 29 and VEname ='Polycom VPX' and Familyid=9


/* ************** ZD 100736 END 11 Feb 2014 ********************************/


/*** ZD_100736_Adding New ploycom Endpoint Models- START ***/
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (66,'Polycom ATX',3)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (67,'Polycom RPX',3)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (68,'Polycom CX7000',3)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (69,'Polycom HDX 4500',3)
/*** ZD_100736_Adding New ploycom Endpoint Models- END ***/


/*** ZD_100736_Adding Radivision Endpoint Models- START ***/
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (70,'Radvision XT1200',13)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (71,'Radvision XT4200',13)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (72,'Radvision XT5000',13)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (73,'Radvision XT240',13)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (74,'Radvision Telepresence System',13)
Update Gen_VideoEquipment_S set familyid = 14 where VEid=24
/*** ZD_100736_Adding Radivision Endpoint Models- END ***/




/* ZD 100704 Starts */
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnableExpressConfType int NULL
GO
COMMIT

Update Org_Settings_D set EnableExpressConfType = 0

/* ZD 100704 End */

/* ************** ZD 100935 Starts 14 Feb 2014 ********************************/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnableWebExIntg smallint NULL
GO
COMMIT


Update Org_Settings_D set EnableWebExIntg=0;
/* ************** ZD 100935 Ends 14 Feb 2014 ********************************/
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.17 Ends(18 Feb 2014)                 */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.18 Starts(18 Feb  2014)              */
/*                                                                                              */
/* ******************************************************************************************** */

--ZD 100899 Starts - (18 Feb  2014)
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	ScheduleLimit int NULL
GO
COMMIT

Update Org_Settings_D set ScheduleLimit=0
--ZD 100899 End - (18 Feb  2014)

/* ************** ZD 100958 Starts 20 Feb 2014 ********************************/

/****** Object:  StoredProcedure [dbo].[SelectCalendarDetails]    Script Date: 02/18/2014 11:53:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[SelectCalendarDetails] 
(
	-- Add the parameters for the stored procedure here
	@timezone			int=null,
	@startTime			datetime=null,
	@endTime			datetime=null,
	@specificEntityCode int=null,
	@codeType			int=null,
	@tablename			varchar(200)=null,
	@dtFormat			varchar(15) = null,
	@tformat			varchar(10)=null
)

As
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if (@timezone is null)
	begin
		return
	end
	
	if (@codeType is null)
	begin
		return
	end

	/* Temp table for Calendar details */

	CREATE TABLE #TempCalendarDT(
	[Date] datetime,
	Start varchar(50),
	[End] varchar(50), 
	Duration int, 
	Room varchar(500),
	[Meeting Title] varchar(1000),
	[Last Name] varchar(100),
	First varchar(100),
	email varchar(200),
	Phone varchar(50),
	State varchar(100),
	City varchar(100),
	[Conf #] varchar(50), 
	[Meeting Type] varchar(100),
	Category varchar(50),
	Status varchar(50),
	Remarks varchar(2500),
	conftype int,
	connectionType int,
	defVideoProtocol int,
	vidProtocol varchar(20),
	TypeFlag char(1),
	CustomAttributeId int, 
	SelectedOptionId int,
	addresstype int,
	langauge int,
	[Entity Code] varchar(50),
	[Entity Name] varchar(200)
	
)

	declare @ecode as varchar(50)
	declare @optionid as int
	declare @calqry as varchar(4000)
	declare @where as varchar(2000)
	declare @cond as varchar(1000)
	declare @filter as varchar(1000)

	declare @seloptionid as int
	--FB 2045 start	
	declare @syscustattrid as varchar(50)
	SELECT @syscustattrid = CustomAttributeId FROM Dept_CustomAttr_D 
    where CreateType = 1 and DisplayTitle = 'Entity Code'
    --FB 2045 end

	if(@codeType = 1) -- None (Need to identify code)
		begin
			set @seloptionid = ''
			set @ecode = 'NA'			
		end
	else if(@codeType = 2) -- Specific
		begin
			set @seloptionid = @specificEntityCode	
			SELECT top 1 @ecode = Caption FROM Dept_CustomAttr_Option_D where OptionId=@specificEntityCode and Caption is not null
		end
	
	set @filter = ''

	if (@startTime is not null And @endTime is not null)
		begin
			if(@tformat = 'HHmmZ')
				set @filter = @filter + ' AND c.confdate BETWEEN '''+ cast(@startTime as varchar(40)) +''' AND '''+ cast(@endTime as varchar(40)) +''''
			else
				set @filter = @filter + ' AND c.confdate BETWEEN dbo.changeTOGMTtime('+ cast(@timezone as varchar(2)) +','''+ cast(@startTime as varchar(40)) +''') AND dbo.changeTOGMTtime('+ cast(@timezone as varchar(2)) +','''+ cast(@endTime as varchar(40)) +''')'
		end

	if (@tablename <> '' AND @tablename is not null)
		begin
			if exists (select * from dbo.sysobjects where [name]=@tablename and xtype='U')
			 begin
				set @filter = @filter +' AND cu.roomid IN (Select roomid from '+ @tablename +')'
			 end
		end

/* Details Part */
	set @calqry = 'Select a.*, isnull(d.[Entity Code],''NA'') as [Entity Code], isnull(d.[Entity Name],''NA'') as [Entity Name] From '
	if(@tformat = 'HHmmZ')
	Begin
		set @calqry = @calqry + '(SELECT c.confdate AS Date,'
	End
	else
	Begin
		set @calqry = @calqry + '(SELECT dbo.changeTime('+ cast(@timezone as varchar(2)) +',c.confdate) AS Date,'
	End
	--FB 2588
	if(@tformat = 'HHmmZ')
	Begin
		set @calqry = @calqry + 'replace(CONVERT(varchar(5),c.confdate,114),'':'','''') + ''Z'' as Start,'
		set @calqry = @calqry + 'replace(CONVERT(varchar(5),dateadd(minute,c.duration, c.confdate),114),'':'','''') + ''Z'' as [End],'
	End
	else if(@tformat = 'HH:mm')
	Begin
		set @calqry = @calqry + 'CONVERT(varchar(5),dbo.changeTime('+ cast(@timezone as varchar(2)) +', c.confdate),108) as Start,'
		set @calqry = @calqry + 'CONVERT(varchar(5),dbo.changeTime('+ cast(@timezone as varchar(2)) +', dateadd(minute,c.duration, c.confdate)),108) as [End],'	
	End
	else
	Begin
		set @calqry = @calqry + 'substring(convert(char(30),dbo.changeTime('+ cast(@timezone as varchar(2)) +', c.confdate) - dateadd(dd,0, datediff(dd,0, dbo.changeTime('+ cast(@timezone as varchar(2)) +', c.confdate)))),13,8) as Start,'
		set @calqry = @calqry + 'substring(convert(char(30),dbo.changeTime('+ cast(@timezone as varchar(2)) +',dateadd(minute,c.duration, c.confdate)) - dateadd(dd,0, datediff(dd,0, dbo.changeTime('+ cast(@timezone as varchar(2)) +',dateadd(minute,c.duration, c.confdate))))),13,8) as [End],'
	End
	set @calqry = @calqry + ' c.Duration, l.name AS Room, c.externalname AS [Meeting Title], u.lastname as [Last Name], '
	set @calqry = @calqry + ' u.firstname as First, u.email,'
	set @calqry = @calqry + ' Case when ((len(isnull(l.AssistantPhone,''''))>0) And (len(isnull(u.Telephone,''''))>0)) Then (l.AssistantPhone + '', '' + u.Telephone) ' 
	set @calqry = @calqry + ' when ((len(isnull(l.AssistantPhone,''''))>0) And (len(isnull(u.Telephone,''''))=0)) Then (l.AssistantPhone) '
	set @calqry = @calqry + ' when ((len(isnull(l.AssistantPhone,''''))=0) And (len(isnull(u.Telephone,''''))>0)) Then (u.Telephone) ' 
	set @calqry = @calqry + ' when ((len(isnull(l.AssistantPhone,''''))=0) And (len(isnull(u.Telephone,''''))=0)) Then ('''')'
	set @calqry = @calqry + ' End   AS Phone ' 
	set @calqry = @calqry + ' ,l3.name AS State, l2.name AS City, '
	set @calqry = @calqry + ' c.confnumname AS [Conf #],'
	set @calqry = @calqry + ' case c.conftype '
	set @calqry = @calqry + ' when 1 then ''Video'' '
	set @calqry = @calqry + ' when 2 then ''AudioVideo'' '
	set @calqry = @calqry + ' when 3 then ''Immediate'' '
	set @calqry = @calqry + ' when 4 then ''Point to Point'' '
	set @calqry = @calqry + ' when 5 then ''Template'' '
	set @calqry = @calqry + ' when 6 then ''AudioOnly'' '
	set @calqry = @calqry + ' when 7 then ''RoomOnly'' '
	set @calqry = @calqry + ' when 11 then ''Phantom'' '
	set @calqry = @calqry + ' end AS [Meeting Type],'

/*	set @calqry = @calqry + ' case cu.connectionType'
	set @calqry = @calqry + ' when 0 then ''Outbound'' '
	set @calqry = @calqry + ' else ''Inbound'' '
	set @calqry = @calqry + ' end AS Category,'
*/
	set @calqry = @calqry + ' case cu.connectionType'
	set @calqry = @calqry + ' when 0 then ''Dial-out from MCU'' '
	set @calqry = @calqry + ' when 1 then ''Dial-in to MCU'' '
	set @calqry = @calqry + ' when 3 then ''Direct to MCU'' '
	set @calqry = @calqry + ' end AS Category,'

	set @calqry = @calqry + ' case c.Status '
	set @calqry = @calqry + ' when 0 then ''Confirmed'' '
	set @calqry = @calqry + ' else ''Pending'''
	set @calqry = @calqry + ' end AS ''Status'', c.description  as Remarks'
	set @calqry = @calqry + ',c.conftype,cu.connectionType,cu.defVideoProtocol,'
	
	--set @calqry = @calqry + ' case cu.defVideoProtocol '
	
	set @calqry = @calqry + ' case cu.addresstype '
	set @calqry = @calqry + ' when 4 then ''ISDN'''
	set @calqry = @calqry + ' else ''IP'''
	set @calqry = @calqry + ' end AS vidProtocol, ''D'' as TypeFlag'
	set @calqry = @calqry + ' ,CustomAttributeId, SelectedOptionId, cu.addresstype, u.language'
/* Where Part */

	set @where = ' FROM '
	set @where = @where + ' conf_conference_d c,'
	set @where = @where + ' usr_list_d u, '
	set @where = @where + ' conf_room_d cu, '
	set @where = @where + ' loc_room_d l, '
	set @where = @where + ' loc_tier2_d l2,'
	set @where = @where + ' loc_tier3_d l3,'
	set @where = @where + ' conf_customattr_d ca'
	set @where = @where + ' WHERE (c.confid = cu.confid AND c.instanceid = cu.instanceid)'
	set @where = @where + ' AND (c.deleted = 0)'
	set @where = @where + ' AND c.owner = u.userid'
	set @where = @where + ' AND l.roomid = cu.roomid'
	set @where = @where + ' AND l2.id = l.l2locationid'
	set @where = @where + ' AND l3.id = l.l3locationid'
	set @where = @where + ' AND (c.confid = ca.confid AND c.instanceid = ca.instanceid)'

	if(len(@filter) > 1)
		set @where = @where + @filter

	set @where = @where + ' )a'
	set @where = @where + ' left outer join '
	set @where = @where + ' (Select Caption AS [Entity Code], helpText as [Entity Name], '
	set @where = @where + ' OptionID, CustomAttributeId, languageid from Dept_CustomAttr_Option_D '
	--set @where = @where + ' Where CustomAttributeId=1'
    --set @where = @where + ' Where CustomAttributeId='''+ @syscustattrid +'''' --FB 2045
	--Set @where = @where + ' And OptionType=6'
	--Set @where = @where + ' And DeptId=0'
	--Set @where = @where + ' And OptionValue=0)D '
	set @where = @where + ' )D on a.CustomAttributeId = D.CustomAttributeId'
	set @where = @where + ' AND a.SelectedOptionId=D.OptionID'
	set @where = @where + ' AND a.language = D.Languageid'

	declare @cmdsql as varchar(8000)

	if(@codeType=0) -- All
		Begin
			set @seloptionid = ''
			Declare cursor1 Cursor for
				
				SELECT Distinct Caption AS EntityCode, OptionID as OptionID
				FROM Dept_CustomAttr_Option_D where Caption is not null

			Open cursor1

			Fetch Next from cursor1 into @ecode,@optionid

			While @@Fetch_Status = 0
			Begin
								
				set @cond = ''
				Set @cond = ' Where a.SelectedOptionId ='+ Cast(@optionid as varchar(10))
												
				Set @cmdsql = 'INSERT INTO #TempCalendarDT ' + @calqry + @where + @cond + ' ORDER BY dbo.changeTOGMTtime('+ cast(@timezone as varchar(2)) +',a.[Date]),[Meeting Title], Room'
				--print @cmdsql
				exec (@cmdsql)
				set @cond = ''
				set @cmdsql = ''

				Fetch Next from cursor1 into @ecode,@optionid
			End

			Close cursor1
			Deallocate cursor1

			/* Need to identify the entity codes */
			
			set @cond = ''
			Set @cond = ' Where a.SelectedOptionId ='''' OR a.SelectedOptionId = -1'
						
			Set @cmdsql = 'INSERT INTO #TempCalendarDT ' + @calqry + @where + @cond + ' ORDER BY dbo.changeTOGMTtime('+ cast(@timezone as varchar(2)) +',a.[Date]),[Meeting Title], Room'
			--print @cmdsql
			exec (@cmdsql)
			set @cond = ''
			set @cmdsql = ''

		End
	else 
		Begin
			
			set @cond = ''
			if @codetype = 1
				set @cond = ' Where [Entity code] is null or [Entity code] = ''-1'''
			else
				set @cond = ' Where a.SelectedOptionId ='+ cast(@seloptionid as varchar(10))
			
			Set @cmdsql = 'INSERT INTO #TempCalendarDT ' + @calqry + @where + @cond + ' ORDER BY dbo.changeTOGMTtime('+ cast(@timezone as varchar(2)) +',a.[Date]),[Meeting Title], Room'
			print @cmdsql
			exec (@cmdsql)
			set @cmdsql = ''			
			set @cond = ''
		End

/* ** Updates the duration as per requirement **

conftype = 11 phantom
conftype = 4  point-point
conftype = 7  room only

7. If a conference type is room only then the value posted in the duration field will always be equal to 0
*/
set @cmdsql = @cmdsql + 'Update #TempCalendarDT set Duration=0 where conftype in(7,11) and TypeFlag=''D'''
exec (@cmdsql)
set @cmdsql = ''

/*
8. Due to the method of connectivity by NGC, in a point-to-point defined conference there will be three rooms with the same meeting title, and the same date/time displayed, however,  one of the rooms is a phantom room and should be assigned a duration of 0.
   All point-to-point calls are to be billed as ISDN calls.

 PROGRAMMING NOTE: Any ISDN Room which doesn't belong to State = "ISDN Network" and City = "PacBell PRI's" should have duration as zero (0).
*/
set @cmdsql = @cmdsql + 'Update #TempCalendarDT set Duration=0 where (rtrim(State) <> ''ISDN Network'' AND rtrim(City) <> ''PacBell PRIs'' AND addresstype=4) and TypeFlag=''D'''
exec (@cmdsql)
set @cmdsql = ''

set @calqry = ''

if (@tablename <> '' AND @tablename is not null)
begin
	if exists (select * from dbo.sysobjects where [name]=@tablename and xtype='U')
	 begin
		set @cmdsql = ''
		set @cmdsql = 'drop table '+ @tablename
		exec(@cmdsql)
	 end
end
set @cmdsql = ''

/* Detail */
set @cmdsql = null
--FB 2588
-- ZD 100958
declare @dformat as varchar(10);
if(@dtFormat = 'dd/MM/yyyy')
Begin
	set @dformat = '103';
End
else if(@dtFormat = 'dd MMM yyyy')
Begin
	set @dformat = '106';
End
else
Begin
	set @dformat = '101';
End

set @cmdsql = ' Select ltrim(rtrim(convert(char,[Date],'+ @dformat +'))) as [Date],'

set @cmdsql = @cmdsql +'ltrim(rtrim(Start)) as Start,ltrim(rtrim([End])) as [End],Duration,Room,[Meeting Title],'
set @cmdsql = @cmdsql + ' [Last Name],First,email,Phone,State,City,[Conf #],'
set @cmdsql = @cmdsql + ' [Entity Code],[Entity Name],[Meeting Type] as [Conference Type]'
set @cmdsql = @cmdsql + ' ,vidProtocol as Protocol,Category as [Connection Type],'
set @cmdsql = @cmdsql + ' Status,Remarks from #TempCalendarDT where TypeFlag=''D'';'

/* Consolidated Summary */

set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as Entity_Code, sum([Minutes]) as [Minutes],sum([Hours]) as [Hours],'
set @cmdsql = @cmdsql + ' sum([Connects]) as [Connects],'

set @cmdsql = @cmdsql + ' sum([VOIP Totals]) as [IP(Hours)], '
set @cmdsql = @cmdsql + ' case when( sum([Hours]) > 0 ) Then ((sum([VOIP Totals])/sum([Hours]))*100) '
set @cmdsql = @cmdsql + ' else 0 '
set @cmdsql = @cmdsql + ' End as [IP %],'

set @cmdsql = @cmdsql + ' sum([Inbound ISDN Totals]) as [ISDN Dial-in (Hours)],'

set @cmdsql = @cmdsql + ' sum([ISDN Outbound Totals]) as [ISDN Dial-out (Hours)],'
set @cmdsql = @cmdsql + ' case when( sum([Hours]) > 0 ) Then ((sum([ISDN Outbound Totals])/sum([Hours]))*100)'
set @cmdsql = @cmdsql + ' else 0 '
set @cmdsql = @cmdsql + ' End as [ISDN Dial-out %]'
--ISDN Outbound
set @cmdsql = @cmdsql + ' from '
set @cmdsql = @cmdsql + ' (Select isnull([Entity Code],''NA'') as [Entity Code],'
set @cmdsql = @cmdsql + ' isnull((sum(isnull(Duration,0))/60),0) as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Connects, 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype=4 AND connectionType=0) and  TypeFlag=''D'''
set @cmdsql = @cmdsql + ' group by [Entity Code] '

set @cmdsql = @cmdsql + ' Union '
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], '
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' isnull((sum(isnull(Duration,0))/60),0) as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Connects, 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype=4 AND connectionType=1) and TypeFlag=''D'' group by [Entity Code]'

set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], '
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], isnull((sum(isnull(Duration,0))/60),0) as [VOIP Totals],0 as Connects, 0 as Hours, 0 as Minutes '
set @cmdsql = @cmdsql + ' from #TempCalendarDT where addresstype <> 4 and  TypeFlag=''D'' group by [Entity Code]'

set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], '
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], count(*) as Connects, 0 as Hours, 0 as Minutes '
set @cmdsql = @cmdsql + ' from #TempCalendarDT  Where TypeFlag=''D'' group by [Entity Code]'

set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Connects, isnull((sum(isnull(Duration,0))/60),0) as Hours, 0 as Minutes '
set @cmdsql = @cmdsql + ' from #TempCalendarDT Where TypeFlag=''D'' group by [Entity Code]'

set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Connects, 0 as Hours, sum(isnull(Duration,0)) as [Minutes] '
set @cmdsql = @cmdsql + ' from #TempCalendarDT where TypeFlag=''D'' group by [Entity Code]) d group by [Entity Code];'

/* Entity codewise Summary */

set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], sum([Minutes]) as [Minutes],sum([Hours]) as [Hours],'
set @cmdsql = @cmdsql + ' sum([Pt-Pt Meetings]) as [Point-to-Point Conferences],'
set @cmdsql = @cmdsql + ' sum([ISDN Outbound Totals]) as [ISDN Dial-out connections],'
set @cmdsql = @cmdsql + ' sum([Inbound ISDN Totals]) as [ISDN Dial-in connections],'
set @cmdsql = @cmdsql + ' sum([VOIP Totals]) as [IP Dial-out connections],'
set @cmdsql = @cmdsql + ' sum([Audio-Video Conferences]) as [Audio-Video Conferences],'
set @cmdsql = @cmdsql + ' sum([IP Dial-in connections]) as [IP Dial-in connections],'
set @cmdsql = @cmdsql + ' (sum([Pt-Pt Meetings])+ sum([Audio-Video Conferences])) as [Total Conferences]'
set @cmdsql = @cmdsql + ' from ('
--Pt-Pt Mins
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code],'
set @cmdsql = @cmdsql + ' sum(isnull(Duration,0)) as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where ConfType=4 and TypeFlag=''D'' group by [Entity Code]'

--ISDN Outbound Mins
set @cmdsql = @cmdsql + ' Union '
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' sum(isnull(Duration,0)) as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype=4 AND connectionType=0) and  TypeFlag=''D'' '
set @cmdsql = @cmdsql + ' group by [Entity Code] '

--ISDN Inbound Mins
set @cmdsql = @cmdsql + ' Union '
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' sum(isnull(Duration,0)) as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype=4 AND connectionType=1) and  TypeFlag=''D'' group by [Entity Code]'

--IP Dial-Out Mins
set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], sum(isnull(Duration,0)) as [VOIP Totals],0 as Hours, 0 as Minutes '
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype<>4 AND connectionType=0) and  TypeFlag=''D'' group by [Entity Code]'

--Hours
set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals],isnull((sum(isnull(Duration,0))/60),0) as Hours, 0 as Minutes '
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT Where TypeFlag=''D'' group by [Entity Code]'

--Minutes
set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, sum(isnull(Duration,0)) as [Minutes] '
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where TypeFlag=''D'' group by [Entity Code]'
--Audio-Video Mins
set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' ,sum(isnull(Duration,0)) as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where ConfType=2 and TypeFlag=''D'' group by [Entity Code]'

--IP Dial-in Mins
set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], sum(isnull(Duration,0)) as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype <> 4 AND connectionType=1) and TypeFlag=''D'' group by [Entity Code]'

set @cmdsql = @cmdsql + ') d group by [Entity Code];'

--ISDN Network - seperate tab
--All point-to-point calls are to be billed as ISDN calls.
-- Conftype = 4 (Point-Point)
--FB 2588
-- ZD 100958
set @cmdsql = @cmdsql + ' Select ltrim(rtrim(convert(char,[Date],'+ @dformat +'))) as [Date],'

set @cmdsql = @cmdsql + ' ltrim(rtrim(Start)) as Start,ltrim(rtrim([End])) as [End],Duration,Room,[Meeting Title],'
set @cmdsql = @cmdsql + ' [Last Name],First,email,Phone,State,City,[Conf #],'
set @cmdsql = @cmdsql + ' [Entity Code],[Entity Name],[Meeting Type] as [Conference Type]'
set @cmdsql = @cmdsql + ' ,vidProtocol as Protocol,Category as [Connection Type],'
set @cmdsql = @cmdsql + ' Status,Remarks from #TempCalendarDT where ((addresstype=4 and TypeFlag=''D'''
set @cmdsql = @cmdsql + ' and State=''ISDN Network'' and City=''PacBell PRIs'') OR (Conftype=4));'


/* ISDN Summary */
declare @condisdn as varchar (1000)
declare @condisdn1 as varchar (1000)
declare @cmdsql2 as varchar(5000)

set @condisdn1 =  'and ((addresstype=4 and State=''ISDN Network'' and City=''PacBell PRIs''))'

set @cmdsql2 = ' Select ''ISDN'' as [Entity Code], sum([Minutes]) as [Minutes],sum([Hours]) as [Hours],'
set @cmdsql2 = @cmdsql2 + ' sum([Pt-Pt Meetings]) as [Point-to-Point Conferences],'
set @cmdsql2 = @cmdsql2 + ' sum([ISDN Outbound Totals]) as [ISDN Dial-out connections],'
set @cmdsql2 = @cmdsql2 + ' sum([Inbound ISDN Totals]) as [ISDN Dial-in connections],'
set @cmdsql2 = @cmdsql2 + ' sum([VOIP Totals]) as [IP Dial-out connections],'
set @cmdsql2 = @cmdsql2 + ' sum([Audio-Video Conferences]) as [Audio-Video Conferences],'
set @cmdsql2 = @cmdsql2 + ' sum([IP Dial-in connections]) as [IP Dial-in connections],'
set @cmdsql2 = @cmdsql2 + ' (sum([Pt-Pt Meetings])+ sum([Audio-Video Conferences])) as [Total Conferences]'

set @cmdsql2 = @cmdsql2 + ' from ('
--Pt-Pt Mins
set @cmdsql2 = @cmdsql2 + ' Select sum(isnull(Duration,0)) as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' 0 as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql2 = @cmdsql2 + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT where ConfType=4 and TypeFlag=''D'''
set @cmdsql2 = @cmdsql2 + @condisdn1

--ISDN Outbound Mins
set @cmdsql2 = @cmdsql2 + ' Union '
set @cmdsql2 = @cmdsql2 + ' Select 0 as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' sum(isnull(Duration,0)) as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql2 = @cmdsql2 + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT where connectionType=0 and  TypeFlag=''D'''
set @cmdsql2 = @cmdsql2 + @condisdn1

--ISDN Inbound Mins
set @cmdsql2 = @cmdsql2 + ' Union '
set @cmdsql2 = @cmdsql2 + ' Select 0 as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' 0 as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' sum(isnull(Duration,0)) as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Hours, 0 as Minutes'
set @cmdsql2 = @cmdsql2 + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT where connectionType=1 and TypeFlag=''D'''
set @cmdsql2 = @cmdsql2 + @condisdn1

--Hours
set @cmdsql2 = @cmdsql2 + ' Union'
set @cmdsql2 = @cmdsql2 + ' Select 0 as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' 0 as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals],isnull((sum(isnull(Duration,0))/60),0) as Hours, 0 as Minutes '
set @cmdsql2 = @cmdsql2 + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT Where TypeFlag=''D'' '
set @cmdsql2 = @cmdsql2 + @condisdn1

--Minutes
set @cmdsql2 = @cmdsql2 + ' Union'
set @cmdsql2 = @cmdsql2 + ' Select 0 as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' 0 as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, sum(isnull(Duration,0)) as [Minutes] '
set @cmdsql2 = @cmdsql2 + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT where TypeFlag=''D'' '
set @cmdsql2 = @cmdsql2 + @condisdn1
--Audio-Video Mins
set @cmdsql2 = @cmdsql2 + ' Union'
set @cmdsql2 = @cmdsql2 + ' Select 0 as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' 0 as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql2 = @cmdsql2 + ' ,sum(isnull(Duration,0)) as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT where ConfType=2 and TypeFlag=''D'' '
set @cmdsql2 = @cmdsql2 + @condisdn1

--IP Dial-in Mins
set @cmdsql2 = @cmdsql2 + ')d;'

exec(@cmdsql + @cmdsql2)


END

GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

/* ************** ZD 100958 Ends 20 Feb 2014 ********************************/

/* ************** ZD 100923 Ends 20 Feb 2014 ********************************/

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Roles_D ADD
	orgID int NULL
GO
COMMIT

Update Usr_Roles_D set orgID = 0 where createType =1

--Update Usr_Roles_D set orgID = 11 where createType = 0

/* ************** ZD 100923 Ends 20 Feb 2014 ********************************/


/* ************** ZD 100969 Start 21 Feb 2014 ********************************/

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	MeetingId int NULL,
	EnableStaticID int NULL
GO
COMMIT


update Usr_Roles_D set roleMenuMask = '9*7-2*0+4*0+8*0+5*0+2*0+8*0+2*0+2*0+2*0+1*0-6*14' where roleName = 'Express Profile'

update usr_list_d set usr_list_d.menumask = b.rolemenumask from usr_roles_d as b where usr_list_d.roleid  = b.roleid
update Usr_Inactive_D set Usr_Inactive_D.menumask = b.rolemenumask 
from usr_roles_d as b where Usr_Inactive_D.roleid  = b.roleid


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	ExternalUserId nvarchar(200) NULL,
	VirtualRoomId nvarchar(200) NULL

GO
COMMIT

Update Usr_List_D set ExternalUserId = '', VirtualRoomId = ''



BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
	ExternalUserId nvarchar(200) NULL,
	VirtualRoomId nvarchar(200) NULL

GO
COMMIT

Update Usr_Inactive_D set ExternalUserId = '', VirtualRoomId = ''



/* ************** ZD 100969 End 21 Feb 2014 ********************************/
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.18 Ends(21 Feb 2014)                 */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.19 Starts(21 Feb  2014)              */
/*                                                                                              */
/* ******************************************************************************************** */

/* **********************************ZD 100965 START************************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Gen_EquipmentManufacturer_s
       (
       MID int NULL,
       ManufacturerName nvarchar(250) NULL
       )  ON [PRIMARY]
GO
COMMIT

INSERT INTO [dbo].[Gen_EquipmentManufacturer_s] ([MID], [ManufacturerName]) VALUES (1, N'Tandberg')
INSERT INTO [dbo].[Gen_EquipmentManufacturer_s] ([MID], [ManufacturerName]) VALUES (2, N'Cisco')
INSERT INTO [dbo].[Gen_EquipmentManufacturer_s] ([MID], [ManufacturerName]) VALUES (3, N'Polycom')
INSERT INTO [dbo].[Gen_EquipmentManufacturer_s] ([MID], [ManufacturerName]) VALUES (4, N'ViewStation')
INSERT INTO [dbo].[Gen_EquipmentManufacturer_s] ([MID], [ManufacturerName]) VALUES (5, N'Lifesize Room')
INSERT INTO [dbo].[Gen_EquipmentManufacturer_s] ([MID], [ManufacturerName]) VALUES (6, N'Codian')
INSERT INTO [dbo].[Gen_EquipmentManufacturer_s] ([MID], [ManufacturerName]) VALUES (7, N'Vidyo')
INSERT INTO [dbo].[Gen_EquipmentManufacturer_s] ([MID], [ManufacturerName]) VALUES (8, N'HUAWEI')
INSERT INTO [dbo].[Gen_EquipmentManufacturer_s] ([MID], [ManufacturerName]) VALUES (9, N'Radvision')

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Ept_List_D ADD
	ManufacturerID int NULL
GO
COMMIT




/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Gen_VideoEquipment_S ADD
	DisplayName nvarchar(256) NULL
GO
COMMIT





/**  Bind Tandberg Family Display Model Name ZD 100736 **/
Update Gen_VideoEquipment_S set DisplayName = '8000 MXP' where familyid = 1 and VEid=7
Update Gen_VideoEquipment_S set DisplayName = '7000 MXP' where familyid = 1 and VEid=8
Update Gen_VideoEquipment_S set DisplayName = '6000 MXP' where familyid = 1 and VEid=9
Update Gen_VideoEquipment_S set DisplayName = '3000 MXP' where familyid = 1 and VEid=10
Update Gen_VideoEquipment_S set DisplayName = '2000 MXP' where familyid = 1 and VEid=11
Update Gen_VideoEquipment_S set DisplayName = '1500 MXP' where familyid = 1 and VEid=12
Update Gen_VideoEquipment_S set DisplayName = '1000 MXP' where familyid = 1 and VEid=13
Update Gen_VideoEquipment_S set DisplayName = '990 MXP' where familyid = 1 and VEid=14
Update Gen_VideoEquipment_S set DisplayName = '880 MXP' where familyid = 1 and VEid=15
Update Gen_VideoEquipment_S set DisplayName = '770 MXP' where familyid = 1 and VEid=16
Update Gen_VideoEquipment_S set DisplayName = '550 MXP' where familyid = 1 and VEid=17
Update Gen_VideoEquipment_S set DisplayName = '150 MXP' where familyid = 1 and VEid=18
Update Gen_VideoEquipment_S set DisplayName = 'Director MXP' where familyid = 1 and VEid=19
Update Gen_VideoEquipment_S set DisplayName = 'Maestro MXP' where familyid = 1 and VEid=20
Update Gen_VideoEquipment_S set DisplayName = 'Edge 95 MXP' where familyid = 1 and VEid=58
Update Gen_VideoEquipment_S set DisplayName = '1700 MXP' where familyid = 1 and VEid=59

/**  Bind Cisco Family Display Model Name ZD 100736 **/
Update Gen_VideoEquipment_S set DisplayName = 'Telepresence System', familyid = 2 where VEid=27
Update Gen_VideoEquipment_S set DisplayName = 'C20 *' , familyid = 2 where VEid=30
Update Gen_VideoEquipment_S set DisplayName = 'C40 *' , familyid = 2 where VEid=31
Update Gen_VideoEquipment_S set DisplayName = 'C60 *' , familyid = 2 where VEid=32
Update Gen_VideoEquipment_S set DisplayName = 'C90 *' , familyid = 2 where VEid=33
Update Gen_VideoEquipment_S set DisplayName = 'EX90 *' , familyid = 2 where VEid=34
Update Gen_VideoEquipment_S set DisplayName = 'MXP *' , familyid = 2 where VEid=35
Update Gen_VideoEquipment_S set DisplayName = 'SX-20' , familyid = 2 where VEid=52
Update Gen_VideoEquipment_S set DisplayName = 'MX200' , familyid = 2 where VEid=53
Update Gen_VideoEquipment_S set DisplayName = 'MX300' , familyid = 2 where VEid=54
Update Gen_VideoEquipment_S set DisplayName = 'EX60 *' , familyid = 2 where VEid=55
Update Gen_VideoEquipment_S set DisplayName = 'CTS 3010' , familyid = 2 where VEid=56
Update Gen_VideoEquipment_S set DisplayName = 'TX 1310' , familyid = 2 where VEid=57

/**  Bind Polycom Family Display Model Name ZD 100736 **/

Update Gen_VideoEquipment_S set DisplayName = 'HDX 600x *' , familyid = 3 where VEid=36
Update Gen_VideoEquipment_S set DisplayName = 'HDX 700x *' , familyid = 3 where VEid=37
Update Gen_VideoEquipment_S set DisplayName = 'HDX 800x *' , familyid = 3 where VEid=38
Update Gen_VideoEquipment_S set DisplayName = 'HDX 400x *' , familyid = 3 where VEid=25
Update Gen_VideoEquipment_S set DisplayName = 'HDX 900x *' , familyid = 3 where VEid=26
Update Gen_VideoEquipment_S set DisplayName = 'VSX 8000x *' , familyid = 3 where VEid=1
Update Gen_VideoEquipment_S set DisplayName = 'VSX 7000x *' , familyid = 3 where VEid=2
Update Gen_VideoEquipment_S set DisplayName = 'VSX 5000 *' , familyid = 3 where VEid=3 
Update Gen_VideoEquipment_S set DisplayName = 'VSX 3000 *' , familyid = 3 where VEid=4 
Update Gen_VideoEquipment_S set DisplayName = 'V 500' , familyid = 3 where VEid=5
Update Gen_VideoEquipment_S set DisplayName = 'PVX' , familyid = 3 where VEid=6 
Update Gen_VideoEquipment_S set DisplayName = 'VSX 3000A' , familyid = 3 where VEid=39 
Update Gen_VideoEquipment_S set DisplayName = 'VSX 6000A *' , familyid = 3 where VEid=40 
Update Gen_VideoEquipment_S set DisplayName = 'VSX 7000A' , familyid = 3 where VEid=41
Update Gen_VideoEquipment_S set DisplayName = 'VSX 7000E *' , familyid = 3 where VEid=42
Update Gen_VideoEquipment_S set DisplayName = 'RealPresence G300' , familyid = 3 where VEid=63
Update Gen_VideoEquipment_S set DisplayName = 'RealPresence G500' , familyid = 3 where VEid=64
Update Gen_VideoEquipment_S set DisplayName = 'RealPresence G700' , familyid = 3 where VEid=65
Update Gen_VideoEquipment_S set DisplayName = 'ATX' , familyid = 3 where VEid=66
Update Gen_VideoEquipment_S set DisplayName = 'RPX' , familyid = 3 where VEid=67 
Update Gen_VideoEquipment_S set DisplayName = 'CX7000' , familyid = 3 where VEid=68 
Update Gen_VideoEquipment_S set DisplayName = 'HDX 4500' , familyid = 3 where VEid=69 
Update Gen_VideoEquipment_S set DisplayName = 'QDX 6000' , familyid = 3 where VEid=48
Update Gen_VideoEquipment_S set DisplayName = 'VVX 1500' , familyid = 3 where VEid=49
Update Gen_VideoEquipment_S set DisplayName = 'OTX' , familyid = 3 where VEid=28

/**  Bind ViewStation Family Display Model Name ZD 100736 **/

Update Gen_VideoEquipment_S set DisplayName = 'FX and EX *' , familyid = 4 where VEid=44
Update Gen_VideoEquipment_S set DisplayName = '4000 *' , familyid = 4 where VEid=45 
Update Gen_VideoEquipment_S set DisplayName = 'ViewStation' , familyid = 4 where VEid=46
Update Gen_VideoEquipment_S set DisplayName = '7.0' , familyid = 4 where VEid=47

/**  Bind Lifesize Family Display Model Name ZD 100736 **/ 

Update Gen_VideoEquipment_S set DisplayName = 'Lifesize Room' , familyid = 5 where VEid=43

/**  Bind Codian Family Display Model Name ZD 100736 **/

Update Gen_VideoEquipment_S set DisplayName = 'IP VCR 2210' , familyid = 6 where VEid=21
Update Gen_VideoEquipment_S set DisplayName = 'IP VCR 2220' , familyid = 6 where VEid=22
Update Gen_VideoEquipment_S set DisplayName = 'IP VCR 2240' , familyid = 6 where VEid=23

/**  Bind Vidyo Family Display Model Name ZD 100736 **/
Update Gen_VideoEquipment_S set DisplayName = 'HD 110' , familyid = 7 where VEid=50
Update Gen_VideoEquipment_S set DisplayName = 'HD 220' , familyid = 7 where VEid=51

/**  Bind HUAWEI Family Display Model Name ZD 100736 **/

Update Gen_VideoEquipment_S set DisplayName = 'Room Presence (RP100-46S/55S, RP200-46S/55S)' , familyid = 8 where VEid=60
Update Gen_VideoEquipment_S set DisplayName = 'TP3200 Series Panovision Telepresence' , familyid = 8 where VEid=61
Update Gen_VideoEquipment_S set DisplayName = 'TP3100 Series Immersive Telepresence' , familyid = 8 where VEid=62

/**  Bind Radvision Family Display Model Name ZD 100736 **/

Update Gen_VideoEquipment_S set DisplayName = 'XT1200' , familyid = 9 where VEid=70
Update Gen_VideoEquipment_S set DisplayName = 'XT4200' , familyid = 9 where VEid=71
Update Gen_VideoEquipment_S set DisplayName = 'XT5000' , familyid = 9 where VEid=72
Update Gen_VideoEquipment_S set DisplayName = 'XT240' , familyid = 9 where VEid=73
Update Gen_VideoEquipment_S set DisplayName = 'Telepresence System' , familyid = 9 where VEid=74


UPDATE  Ept_List_D SET
ManufacturerID = a.Familyid 
FROM Gen_VideoEquipment_S a where videoequipmentid = a.VEid

update Ept_List_D set ManufacturerID=3,videoequipmentid=66 where videoequipmentid=24

/* **********************************ZD 100965 END************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.19 Ends(26 Feb 2014)                 */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.20 Starts(26 Feb  2014)              */
/*                                                                                              */
/* ******************************************************************************************** */

/* **********************************ZD 100781 Starts 26 Feb 2014************************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	PasswordTime datetime NULL,
	IsPasswordReset smallint NULL
GO
COMMIT



BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
	PasswordTime datetime NULL,
	IsPasswordReset smallint NULL
GO
COMMIT


Update Usr_List_D set PasswordTime= GETUTCDATE() ,IsPasswordReset = 0 

Update Usr_Inactive_D set PasswordTime=  GETUTCDATE(),IsPasswordReset = 0 


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnablePasswordExp smallint NULL,
	PasswordMonths int NULL
GO
COMMIT

Update Org_Settings_D set EnablePasswordExp=0, PasswordMonths= 3

/* **********************************ZD 100781 Ends 26 Feb 2014************************** */



/* **********************************VR2.9.3.21 START************************** */

/* **********************************ZD 100963 START 28 Feb 2014************************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnableRoomCalendarView int NULL
GO
COMMIT

update Org_Settings_D set EnableRoomCalendarView=3

/* **********************************ZD 100963 END 28 Feb 2014************************** */

/* **********************************ZD 100902 START 3 March 2014************************** */


CREATE TABLE [dbo].[Audit_Conf_AdvAVParams_D](
	[ConfID] [int] NOT NULL,
	[InstanceID] [int] NOT NULL,
	[uId] [int] NOT NULL,
	[confuId] [int] NOT NULL,
	[lineRateID] [smallint] NULL,
	[audioAlgorithmID] [smallint] NULL,
	[videoProtocolID] [smallint] NULL,
	[mediaID] [smallint] NULL,
	[videoLayoutID] [int] NULL,
	[dualStreamModeID] [smallint] NULL,
	[conferenceOnPort] [smallint] NULL,
	[encryption] [smallint] NULL,
	[maxAudioParticipants] [int] NULL,
	[maxVideoParticipants] [int] NULL,
	[videoSession] [int] NULL,
	[LectureMode] [smallint] NULL,
	[videoMode] [int] NULL,
	[singleDialin] [int] NULL,
	[internalBridge] [nvarchar](50) NULL,
	[externalBridge] [nvarchar](50) NULL,
	[feccMode] [smallint] NULL,
	[Layout] [int] NULL,
	[MuteRxaudio] [int] NULL,
	[MuteTxaudio] [int] NULL,
	[MuteRxvideo] [int] NULL,
	[MuteTxvideo] [int] NULL,
	[ConfLockUnlock] [int] NULL,
	[Recording] [int] NULL,
	[Camera] [int] NULL,
	[PacketLoss] [nvarchar](max) NULL,
	[PolycomSendEmail] [int] NULL,
	[PolycomTemplate] [nvarchar](max) NULL
)
GO


CREATE TABLE [dbo].[Audit_Conf_Approval_D](
	[uId] [int] NOT NULL,
	[confid] [int] NOT NULL,
	[instanceid] [int] NOT NULL,
	[entitytype] [smallint] NOT NULL,
	[entityid] [int] NOT NULL,
	[approverid] [int] NULL,
	[decision] [smallint] NOT NULL,
	[responsetimestamp] [datetime] NULL,
	[responsemessage] [nvarchar](2000) NULL
) 

GO

CREATE TABLE [dbo].[Audit_Conf_Bridge_D](
	[ConfID] [bigint] NOT NULL,
	[InstanceID] [int] NOT NULL,
	[BridgeID] [int] NOT NULL,
	[uId] [int] NOT NULL,
	[confuId] [int] NULL,
	[BridgeName] [nvarchar](250) NULL,
	[BridgeTypeid] [int] NULL,
	[BridgeIPISDNAddress] [nvarchar](250) NULL,
	[TotalPortsUsed] [int] NULL,
	[BridgeExtNo] [nvarchar](50) NOT NULL,
	[E164Dialnumber] [nvarchar](250) NULL,
	[Synchronous] [int] NULL,
	[RPRMUserid] [int] NULL,
	[CDRFetchStatus] [smallint] NULL,
	[ConfUIDonMCU] [nvarchar](max) NULL,
	[ConfRMXServiceID] [int] NULL
)

GO

CREATE TABLE [dbo].[Audit_Conf_Cascade_D](
	[uId] [int] NOT NULL,
	[confid] [int] NOT NULL,
	[instanceid] [int] NOT NULL,
	[cascadelinkid] [int] NOT NULL,
	[cascadelinkname] [nvarchar](500) NULL,
	[masterorslave] [smallint] NULL,
	[defvideoprotocol] [smallint] NULL,
	[connectiontype] [smallint] NULL,
	[ipisdnaddress] [nvarchar](256) NULL,
	[bridgeid] [int] NULL,
	[bridgeipisdnaddress] [nvarchar](256) NULL,
	[connectstatus] [smallint] NULL,
	[outsidenetwork] [smallint] NULL,
	[mcuservicename] [nvarchar](256) NULL,
	[audioorvideo] [smallint] NULL,
	[deflinerate] [int] NULL,
	[mute] [smallint] NULL,
	[addresstype] [smallint] NULL,
	[bridgeaddresstype] [smallint] NULL,
	[layout] [int] NULL,
	[prefix] [nvarchar](50) NULL,
	[isLecturer] [int] NOT NULL,
	[OnlineStatus] [int] NULL,
	[LastRunDateTime] [datetime] NULL,
	[remoteEndPointIP] [nvarchar](50) NULL,
	[GUID] [nvarchar](max) NULL,
	[RxAudioPacketsReceived] [nvarchar](50) NULL,
	[RxAudioPacketErrors] [nvarchar](50) NULL,
	[RxAudioPacketsMissing] [nvarchar](50) NULL,
	[RxVideoPacketsReceived] [nvarchar](50) NULL,
	[RxVideoPacketErrors] [nvarchar](50) NULL,
	[RxVideoPacketsMissing] [nvarchar](50) NULL,
	[TerminalType] [smallint] NULL,
	[ChairPerson] [smallint] NULL,
	[TxAudioPacketsSent] [nvarchar](50) NULL,
	[TxVideoPacketsSent] [nvarchar](50) NULL
)

CREATE TABLE [dbo].[Audit_Conf_Conference_D](
	[userid] [int] NULL,
	[confid] [int] NOT NULL,
	[externalname] [nvarchar](256) NULL,
	[internalname] [nvarchar](300) NULL,
	[password] [nvarchar](256) NULL,
	[owner] [int] NULL,
	[confdate] [datetime] NULL,
	[conftime] [datetime] NULL,
	[timezone] [int] NULL,
	[immediate] [int] NULL,
	[audio] [int] NULL,
	[videoprotocol] [int] NULL,
	[videosession] [int] NULL,
	[linerate] [int] NULL,
	[recuring] [int] NULL,
	[duration] [int] NULL,
	[description] [nvarchar](2000) NULL,
	[public] [int] NULL,
	[deleted] [smallint] NULL,
	[continous] [int] NULL,
	[transcoding] [int] NULL,
	[deletereason] [nvarchar](2000) NULL,
	[instanceid] [int] NOT NULL,
	[advanced] [int] NULL,
	[totalpoints] [int] NULL,
	[connect2] [int] NULL,
	[settingtime] [datetime] NULL,
	[videolayout] [int] NULL,
	[manualvideolayout] [int] NULL,
	[conftype] [int] NULL,
	[confnumname] [int] NOT NULL,
	[status] [int] NULL,
	[lecturemode] [int] NULL,
	[lecturer] [nvarchar](256) NULL,
	[dynamicinvite] [int] NULL,
	[CreateType] [int] NULL,
	[ConfDeptID] [int] NULL,
	[ConfOrigin] [int] NOT NULL,
	[SetupTime] [datetime] NULL,
	[TearDownTime] [datetime] NULL,
	[orgId] [int] NULL,
	[LastRunDateTime] [datetime] NULL,
	[IcalID] [varchar](max) NULL,
	[confMode] [smallint] NULL,
	[isDedicatedEngineer] [smallint] NULL,
	[isLiveAssistant] [smallint] NULL,
	[isVIP] [smallint] NULL,
	[isReminder] [int] NULL,
	[ServiceType] [smallint] NULL,
	[ConceirgeSupport] [nvarchar](50) NULL,
	[sentSurvey] [int] NOT NULL,
	[isVMR] [smallint] NULL,
	[ESId] [nvarchar](50) NULL,
	[ESType] [nvarchar](50) NULL,
	[PushedToExternal] [int] NULL,
	[isTextMsg] [int] NULL,
	[startmode] [int] NULL,
	[loginUser] [int] NULL,
	[GUID] [nvarchar](max) NULL,
	[OnSiteAVSupport] [smallint] NULL,
	[MeetandGreet] [smallint] NULL,
	[ConciergeMonitoring] [smallint] NULL,
	[DedicatedVNOCOperator] [smallint] NULL,
	[E164Dialing] [int] NULL,
	[H323Dialing] [int] NULL,
	[Secured] [int] NULL,
	[NetworkSwitch] [int] NULL,
	[DialString] [nvarchar](50) NULL,
	[Etag] [nvarchar](50) NULL,
	[HostName] [nvarchar](250) NULL,
	[isPCconference] [smallint] NULL,
	[pcVendorId] [smallint] NULL,
	[CloudConferencing] [smallint] NULL,
	[ProcessStatus] [smallint] NULL,
	[Uniqueid] [int] NULL,
	[Seats] [int] NULL,
	[EnableNumericID] [smallint] NULL,
	[McuSetupTime] [int] NULL,
	[MCUTeardonwnTime] [int] NULL,
	[SetupTimeinMin] [int] NULL,
	[TearDownTimeinMin] [int] NULL,
	[WebExConf] [smallint] NULL,
	[WebExPW] [nvarchar](50) NULL,
	[WebExMeetingKey] [nvarchar](100) NULL,
	[WebExHostURL] [nvarchar](max) NULL
	)

CREATE TABLE [dbo].[Audit_Conf_CustomAttr_D](
	[ConfId] [int] NOT NULL,
	[InstanceId] [int] NOT NULL,
	[CustomAttributeId] [int] NOT NULL,
	[SelectedOptionId] [int] NOT NULL,
	[SelectedValue] [nvarchar](4000) NULL,
	[ConfAttrID] [int] NOT NULL
)
GO

CREATE TABLE [dbo].[Audit_Conf_Message_D](
	[uID] [int] NOT NULL,
	[orgid] [int] NOT NULL,
	[confid] [int] NOT NULL,
	[instanceid] [int] NOT NULL,
	[Languageid] [int] NULL,
	[confMessage] [nvarchar](max) NOT NULL,
	[duration] [int] NOT NULL,
	[controlID] [int] NOT NULL,
	[durationID] [nvarchar](10) NULL
) 
GO

CREATE TABLE [dbo].[Audit_Conf_RecurInfo_D](
	[confid] [int] NOT NULL,
	[uId] [int] NOT NULL,
	[confuId] [int] NOT NULL,
	[timezoneid] [int] NULL,
	[duration] [int] NULL,
	[recurType] [int] NULL,
	[subType] [int] NULL,
	[yearMonth] [int] NULL,
	[days] [nvarchar](256) NULL,
	[dayNo] [int] NULL,
	[gap] [int] NULL,
	[startTime] [datetime] NULL,
	[endTime] [datetime] NULL,
	[endType] [int] NULL,
	[occurrence] [int] NULL,
	[dirty] [smallint] NULL,
	[RecurringPattern] [nvarchar](4000) NULL
) 
GO

CREATE TABLE [dbo].[Audit_Conf_RecurInfoDefunt_D](
	[confid] [int] NOT NULL,
	[uId] [int] NOT NULL,
	[confuId] [int] NOT NULL,
	[timezoneid] [int] NULL,
	[duration] [int] NULL,
	[recurType] [int] NULL,
	[subType] [int] NULL,
	[yearMonth] [int] NULL,
	[days] [nvarchar](256) NULL,
	[dayNo] [int] NULL,
	[gap] [int] NULL,
	[startTime] [datetime] NULL,
	[endTime] [datetime] NULL,
	[endType] [int] NULL,
	[occurrence] [int] NULL,
	[dirty] [smallint] NULL,
	[RecurringPattern] [nvarchar](4000) NULL,
	[SetupDuration] [int] NULL,
	[TeardownDuration] [int] NULL
) 
GO

CREATE TABLE [dbo].[Audit_Conf_Room_D](
	[ConfID] [int] NOT NULL,
	[RoomID] [int] NOT NULL,
	[DefLineRate] [int] NULL,
	[DefVideoProtocol] [int] NULL,
	[StartDate] [datetime] NULL,
	[Duration] [int] NULL,
	[instanceID] [int] NOT NULL,
	[connect2] [smallint] NULL,
	[bridgeIPISDNAddress] [nvarchar](256) NULL,
	[bridgeid] [int] NULL,
	[connectiontype] [smallint] NULL,
	[ipisdnaddress] [nvarchar](256) NULL,
	[connectstatus] [smallint] NULL,
	[outsidenetwork] [smallint] NULL,
	[mcuservicename] [nvarchar](4000) NULL,
	[audioorvideo] [smallint] NULL,
	[mute] [smallint] NULL,
	[addresstype] [smallint] NULL,
	[bridgeaddresstype] [smallint] NULL,
	[layout] [int] NULL,
	[endpointId] [int] NULL,
	[uId] [int] NOT NULL,
	[confuId] [int] NOT NULL,
	[profileId] [int] NULL,
	[prefix] [nvarchar](50) NULL,
	[isLecturer] [int] NOT NULL,
	[OnlineStatus] [int] NULL,
	[LastRunDateTime] [datetime] NULL,
	[ApiPortNo] [int] NULL,
	[endptURL] [nvarchar](50) NULL,
	[remoteEndPointIP] [nvarchar](50) NULL,
	[disabled] [smallint] NULL,
	[MultiCodecAddress] [nvarchar](2000) NULL,
	[Extroom] [int] NULL,
	[isTextMsg] [int] NULL,
	[GUID] [nvarchar](max) NULL,
	[Setfocus] [int] NULL,
	[Message] [int] NULL,
	[MuteRxaudio] [int] NULL,
	[MuteRxvideo] [int] NULL,
	[MuteTxvideo] [int] NULL,
	[Camera] [int] NULL,
	[Packetloss] [int] NULL,
	[LockUnLock] [int] NULL,
	[Record] [int] NULL,
	[Stream] [nvarchar](max) NULL,
	[RxAudioPacketsReceived] [nvarchar](50) NULL,
	[RxAudioPacketErrors] [nvarchar](50) NULL,
	[RxAudioPacketsMissing] [nvarchar](50) NULL,
	[RxVideoPacketsReceived] [nvarchar](50) NULL,
	[RxVideoPacketErrors] [nvarchar](50) NULL,
	[RxVideoPacketsMissing] [nvarchar](50) NULL,
	[TerminalType] [smallint] NULL,
	[BridgeExtNo] [nvarchar](50) NOT NULL,
	[Secured] [int] NULL,
	[PartyName] [nvarchar](250) NULL,
	[isDoubleBooking] [smallint] NULL,
	[ChairPerson] [smallint] NULL,
	[GateKeeeperAddress] [nvarchar](50) NULL,
	[activeSpeaker] [int] NULL,
	[TxAudioPacketsSent] [nvarchar](50) NULL,
	[TxVideoPacketsSent] [nvarchar](50) NULL
)
 GO

CREATE TABLE [dbo].[Audit_Conf_User_D](
	[ConfID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[CC] [int] NULL,
	[Status] [int] NULL,
	[Reason] [nvarchar](2000) NULL,
	[ResponseDate] [datetime] NULL,
	[DefLineRate] [int] NULL,
	[Invitee] [int] NULL,
	[RoomID] [int] NULL,
	[DefVideoProtocol] [int] NULL,
	[InstanceId] [int] NOT NULL,
	[IpAddress] [nvarchar](256) NULL,
	[connectionType] [int] NULL,
	[connect2] [smallint] NULL,
	[AudioOrVideo] [smallint] NULL,
	[IPISDN] [smallint] NULL,
	[IPISDNAddress] [nchar](256) NULL,
	[bridgeIPISDNAddress] [nchar](256) NULL,
	[bridgeid] [int] NULL,
	[connectstatus] [smallint] NULL,
	[outsidenetwork] [smallint] NULL,
	[mcuservicename] [nvarchar](256) NULL,
	[emailreminder] [smallint] NULL,
	[mute] [smallint] NULL,
	[addresstype] [smallint] NULL,
	[bridgeaddresstype] [smallint] NULL,
	[layout] [int] NULL,
	[PartyNotify] [smallint] NULL,
	[interfacetype] [smallint] NULL,
	[uId] [int] NOT NULL,
	[confuId] [int] NOT NULL,
	[videoEquipment] [int] NULL,
	[prefix] [nvarchar](50) NULL,
	[isLecturer] [int] NOT NULL,
	[ExchangeID] [nvarchar](200) NULL,
	[OnlineStatus] [int] NULL,
	[LastRunDateTime] [datetime] NULL,
	[ApiPortNo] [int] NULL,
	[endptURL] [nvarchar](50) NULL,
	[remoteEndPointIP] [nvarchar](50) NULL,
	[NotifyOnEdit] [smallint] NULL,
	[Survey] [int] NULL,
	[isTextMsg] [int] NOT NULL,
	[GUID] [nvarchar](max) NULL,
	[Setfocus] [int] NULL,
	[Message] [int] NULL,
	[MuteRxaudio] [int] NULL,
	[MuteRxvideo] [int] NULL,
	[MuteTxvideo] [int] NULL,
	[Camera] [int] NULL,
	[Packetloss] [int] NULL,
	[LockUnLock] [int] NULL,
	[Record] [int] NULL,
	[Stream] [nvarchar](max) NULL,
	[RxAudioPacketsReceived] [nvarchar](50) NULL,
	[RxAudioPacketErrors] [nvarchar](50) NULL,
	[RxAudioPacketsMissing] [nvarchar](50) NULL,
	[RxVideoPacketsReceived] [nvarchar](50) NULL,
	[RxVideoPacketErrors] [nvarchar](50) NULL,
	[RxVideoPacketsMissing] [nvarchar](50) NULL,
	[TerminalType] [smallint] NULL,
	[PublicVMRParty] [smallint] NOT NULL,
	[Secured] [int] NULL,
	[PartyName] [nvarchar](250) NULL,
	[ChairPerson] [smallint] NULL,
	[RFIDValue] [nvarchar](250) NULL,
	[Attended] [int] NULL,
	[activeSpeaker] [int] NULL,
	[TxAudioPacketsSent] [nvarchar](50) NULL,
	[TxVideoPacketsSent] [nvarchar](50) NULL,
	[WebEXAttendeeURL] [nvarchar](max) NULL
)
GO

CREATE TABLE [dbo].[Audit_Conf_VNOCOperator_D](
	[uId] [int] NOT NULL,
	[confid] [int] NOT NULL,
	[instanceid] [int] NOT NULL,
	[vnocId] [int] NOT NULL,
	[vnocAssignAdminId] [int] NOT NULL,
	[decision] [smallint] NOT NULL,
	[responsetimestamp] [datetime] NULL,
	[responsemessage] [nvarchar](2000) NULL,
	[confuId] [int] NOT NULL,
	[OperatorName] [nvarchar](250) NULL
)
GO

CREATE TABLE [dbo].[Audit_Inv_WorkOrder_D](
	LastRunDateTime [datetime] NOT NULL,
	[ID] [int] NOT NULL,
	[Type] [int] NOT NULL,
	[CatID] [int] NOT NULL,
	[AdminId] [int] NOT NULL,
	[ConfID] [int] NOT NULL,
	[InstanceID] [int] NOT NULL,
	[Name] [nvarchar](300) NULL,
	[RoomID] [int] NOT NULL,
	[CompletedBy] [datetime] NULL,
	[startBy] [datetime] NULL,
	[woTimeZone] [int] NULL,
	[comment] [nvarchar](2048) NULL,
	[Status] [int] NOT NULL,
	[Reminder] [int] NOT NULL,
	[Notify] [int] NOT NULL,
	[WkOType] [int] NOT NULL,
	[description] [nvarchar](2000) NULL,
	[deliveryType] [int] NULL,
	[deliveryCost] [float] NULL,
	[serviceCharge] [float] NULL,
	[woTax] [float] NULL,
	[woTtlCost] [float] NULL,
	[deleted] [int] NOT NULL,
	[strData] [nvarchar](2000) NULL,
	[orgId] [int] NULL,
	[woTtlDlvryCost] [float] NULL,
	[woTtlSrvceCost] [float] NULL,
	[woCatAdmin] [int] NULL
) 
GO

CREATE TABLE [dbo].[Audit_Inv_WorkItem_D](
	[ID] [int] NOT NULL,
	[ItemID] [int] NOT NULL,
	[WorkOrderID] [int] NOT NULL,
	[quantity] [int] NOT NULL,
	[deliveryType] [int] NULL,
	[deliveryCost] [float] NULL,
	[description] [nvarchar](256) NULL,
	[serviceCharge] [float] NULL,
	[deleted] [int] NOT NULL,
	[serviceId] [int] NULL
) 
GO

CREATE TABLE [dbo].[Conf_AuditSummary_D](
	[uId] [int] IDENTITY(1,1) NOT NULL,
	[AuditSummary] [varchar](2000) NULL,
	[CreatedDate] [datetime] NULL,
	[ConfID] [int] NULL,
	[confnumname] [varchar](50) NULL,
	[UserID] [int] NULL,
	[OrgID] [int] NULL
) ON [PRIMARY]

GO


ALTER TABLE dbo.Audit_Conf_Conference_D ADD
	MeetingId int NULL,
	EnableStaticID int NULL
GO

/* **********************************ZD 100902 Ends 3 March 2014************************** */


/* *************  ZD 100036 Starts ************* */
 
--Insert into Icons_Ref_S (IconID, IconUri, Label, IconTarget) values (48,'../en/img/OnMCUIcon.png','On MCU Conferences','ConferenceList.aspx?t=8')
insert into icons_ref_s values('48','../en/img/OnMCUIcon.png','On MCU Conferences','ConferenceList.aspx?t=8')

/* *************  ZD 100036 End ************* */


/* **********************************ZD 100973 Starts 5 Mar 2014************************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	GoogleGUID nvarchar(MAX) NULL,
	GoogleSequence int NULL,
	GoogleConfLastUpdated datetime NULL
GO
COMMIT

update Conf_Conference_D set GoogleGUID='',GoogleSequence=0,GoogleConfLastUpdated=''

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Audit_Conf_Conference_D ADD
	GoogleGUID nvarchar(MAX) NULL,
	GoogleSequence int NULL,
	GoogleConfLastUpdated datetime NULL
GO
COMMIT
update Audit_Conf_Conference_D set GoogleGUID='',GoogleSequence=0,GoogleConfLastUpdated=''

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Sys_Settings_D ADD
	GoogleClientID nvarchar(MAX) NULL,
	GoogleSecretID nvarchar(MAX) NULL,
	GoogleAPIKey nvarchar(MAX) NULL,
	PushtoGoogle int NULL
GO
COMMIT

update Sys_Settings_D set GoogleClientID='',GoogleSecretID='',GoogleAPIKey='',PushtoGoogle=0





BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO

CREATE TABLE [dbo].[Usr_Tokens_D](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userid] [int] NULL,
	[orgid] [int] NULL,
	[AccessToken] [nvarchar](max) NULL,
	[refreshtoken] [nvarchar](max) NULL,
	[ChannelID] [nvarchar](max) NULL,
	[ChannelEtag] [nvarchar](max) NULL,
	[ChannelResourceID] [nvarchar](max) NULL,
	[ChannelResourseURL] [nvarchar](max) NULL,
	[ChannelExpiration] [datetime] NULL,
	[userEmail] [nvarchar](512) NULL
) ON [PRIMARY]

GO
COMMIT

ALTER TABLE email_queue_d
ALTER COLUMN Message nvarchar(MAX) NULL

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.temp_GooglePoll_d
	(
	userid int NOT NULL,
	PollStatus int NULL
	)  ON [PRIMARY]
GO
COMMIT
/* **********************************ZD 100973 Ends 5 Mar 2014************************** */

/* **********************************ZD 100526 Starts 6 Mar 2014************************** */
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Sys_Settings_D ADD
	ConfIDStartValue int NULL
GO
ALTER TABLE dbo.Sys_Settings_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
/* **********************************ZD 100526 Ends 6 Mar 2014************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.3.21 Ends( Mar 2014)                   */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.91.3.22 Starts( Mar 2014 )                */
/*                                                                                              */
/* ******************************************************************************************** */

/* *****************************ZD 101019 START********************************** */
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	VideoSourceURL1 nvarchar(MAX) NULL,
	ScreenPosition1 nvarchar(20) NULL,
	VideoSourceURL2 nvarchar(MAX) NULL,
	ScreenPosition2 nvarchar(20) NULL,
	VideoSourceURL3 nvarchar(MAX) NULL,
	ScreenPosition3 nvarchar(20) NULL,
	VideoSourceURL4 nvarchar(MAX) NULL,
	ScreenPosition4 nvarchar(20) NULL
GO
COMMIT

/* **********************************ZD 101011 Starts 10 Mar 2014************************** */
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Sys_Settings_D ADD
	SessionTimeout int NULL
GO
COMMIT

update Sys_Settings_D set SessionTimeout=60

/* **********************************ZD 101011 Ends 10 Mar 2014************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.91.3.22 Ends( 11 Mar 2014)               */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.91.3.23 Starts(12 Mar 2014 )             */
/*                                                                                              */
/* ******************************************************************************************** */


/* **********************************ZD 101069 START 12 Mar 2014************************** */

Update Gen_VideoEquipment_S set DisplayName = 'MX200 *' , VEname = 'Cisco MX200 *' where VEid=53
Update Gen_VideoEquipment_S set DisplayName = 'MX300 *' , VEname = 'Cisco MX300 *' where VEid=54
Update Gen_VideoEquipment_S set DisplayName = 'SX-20 *' , VEname = 'Cisco SX-20 *' where VEid=52

/* **********************************ZD 101069 End 12 Mar 2014************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.91.3.23 Ends( 12 Mar 2014)               */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.91.3.24 Starts(12 Mar 2014 )             */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************** ZD 101026-Audit User 13 March  2014   - Starts ***************************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_list_d ADD
	[LastModifiedUser] int NULL,
	[LastModifiedDate][datetime] NULL	
GO
COMMIT

update Usr_list_d set LastModifiedUser = 11, LastModifiedDate = GETUTCDATE()

CREATE TABLE [dbo].[Audit_Usr_List_D](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[FirstName] [nvarchar](256) NULL,
	[LastName] [nvarchar](256) NULL,
	[Email] [nvarchar](512) NULL,
	[Financial] [int] NULL,
	[Admin] [smallint] NULL,
	[Login] [nvarchar](256) NULL,
	[Password] [nvarchar](1000) NULL,
	[Company] [nvarchar](512) NULL,
	[TimeZone] [smallint] NULL,
	[Language] [smallint] NULL,
	[PreferedRoom] [varchar](150) NULL,
	[AlternativeEmail] [nvarchar](512) NULL,
	[DoubleEmail] [smallint] NULL,
	[PreferedGroup] [smallint] NULL,
	[CCGroup] [smallint] NULL,
	[Telephone] [nvarchar](50) NULL,
	[RoomID] [int] NULL,
	[PreferedOperator] [int] NULL,
	[lockCntTrns] [int] NULL,
	[DefLineRate] [int] NULL,
	[DefVideoProtocol] [int] NULL,
	[Deleted] [int] NOT NULL,
	[DefAudio] [int] NULL,
	[DefVideoSession] [int] NULL,
	[MenuMask] [nvarchar](200) NULL,
	[initialTime] [int] NULL,
	[EmailClient] [smallint] NULL,
	[newUser] [smallint] NULL,
	[PrefTZSID] [int] NULL,
	[IPISDNAddress] [nvarchar](256) NULL,
	[DefaultEquipmentId] [smallint] NOT NULL,
	[connectionType] [smallint] NULL,
	[companyId] [int] NULL,
	[securityKey] [nvarchar](256) NULL,
	[LockStatus] [smallint] NOT NULL,
	[LastLogin] [datetime] NULL,
	[outsidenetwork] [smallint] NULL,
	[emailmask] [bigint] NULL,
	[roleID] [int] NULL,
	[addresstype] [smallint] NULL,
	[accountexpiry] [datetime] NULL,
	[approverCount] [int] NULL,
	[BridgeID] [int] NOT NULL,
	[Title] [nvarchar](50) NULL,
	[LevelID] [int] NULL,
	[preferredISDN] [nvarchar](50) NULL,
	[portletId] [int] NULL,
	[searchId] [int] NULL,
	[endpointId] [int] NOT NULL,
	[DateFormat] [nvarchar](50) NULL,
	[enableAV] [int] NULL,
	[timezonedisplay] [int] NULL,
	[timeformat] [int] NULL,
	[enableParticipants] [int] NULL,
	[TickerStatus] [int] NULL,
	[TickerSpeed] [int] NULL,
	[TickerPosition] [int] NULL,
	[TickerBackground] [nchar](10) NULL,
	[TickerDisplay] [int] NULL,
	[RSSFeedLink] [nvarchar](50) NULL,
	[TickerStatus1] [int] NULL,
	[TickerSpeed1] [int] NULL,
	[TickerPosition1] [int] NULL,
	[TickerBackground1] [nchar](10) NULL,
	[TickerDisplay1] [int] NULL,
	[RSSFeedLink1] [nvarchar](50) NULL,
	[enableExchange] [int] NULL,
	[enableDomino] [int] NULL,
	[Audioaddon] [nvarchar](50) NULL,
	[WorkPhone] [nvarchar](50) NULL,
	[CellPhone] [nvarchar](50) NULL,
	[ConferenceCode] [nvarchar](50) NULL,
	[LeaderPin] [nvarchar](50) NULL,
	[DefaultTemplate] [smallint] NULL,
	[EmailLangId] [int] NULL,
	[MailBlocked] [smallint] NULL,
	[MailBlockedDate] [datetime] NULL,
	[enableMobile] [int] NULL,
	[PluginConfirmations] [smallint] NULL,
	[InternalVideoNumber] [nvarchar](50) NOT NULL,
	[ExternalVideoNumber] [nvarchar](50) NOT NULL,
	[HelpReqEmailID] [nvarchar](512) NULL,
	[HelpReqPhone] [nvarchar](50) NULL,
	[SendSurveyEmail] [int] NULL,
	[PrivateVMR] [nvarchar](max) NULL,
	[EnableVNOCselection] [int] NULL,
	[EntityID] [int] NULL,
	[Extension] [nvarchar](50) NULL,
	[Pin] [nvarchar](50) NULL,
	[MemberID] [int] NULL,
	[isLocked] [int] NULL,
	[VidyoURL] [nvarchar](max) NULL,
	[Type] [nvarchar](50) NULL,
	[allowPersonalMeeting] [int] NULL,
	[allowCallDirect] [int] NULL,
	[ESUserID] [int] NULL,
	[ParentReseller] [int] NULL,
	[CorpUser] [int] NULL,
	[Region] [int] NULL,
	[Secured] [smallint] NULL,
	[PreConfCode] [nvarchar](50) NULL,
	[PreLeaderPin] [nvarchar](50) NULL,
	[PostLeaderPin] [nvarchar](50) NULL,
	[AudioDialInPrefix] [nvarchar](50) NULL,
	[enablePCUser] [smallint] NULL,
	[RFIDValue] [nvarchar](250) NULL,
	[BrokerRoomNum] [nvarchar](50) NULL,
	[PerCalStartTime] [datetime] NULL,
	[PerCalEndTime] [datetime] NULL,
	[RoomCalStartTime] [datetime] NULL,
	[RoomCalEndime] [datetime] NULL,
	[PerCalShowHours] [smallint] NULL,
	[RoomCalShowHours] [smallint] NULL,
	[RequestID] [nvarchar](max) NULL,
	[StaticID] [varchar](255) NULL,
	[IsStaticIDEnabled] [int] NULL,
	[enableWebexUser] [smallint] NULL,
	[WebexUserName] [nvarchar](50) NULL,
	[WebexPassword] [nvarchar](50) NULL,
	[LandingPageTitle] [nvarchar](200) NULL,
	[LandingPageLink] [nvarchar](200) NULL,
	[Lastmodifieddate] [datetime] NULL,
	[ExternalUserId] [nvarchar](200) NULL,
	[VirtualRoomId] [nvarchar](200) NULL,
	[PasswordTime] [datetime] NULL,
	[IsPasswordReset] [smallint] NULL,
	[LastModifiedUser] [int] NULL,
	[ValueChange] [nvarchar](150) NULL
)


CREATE TABLE [dbo].[Audit_Usr_PCDetails_D](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userid] [int] NOT NULL,
	[PCId] [int] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[SkypeURL] [nvarchar](max) NULL,
	[VCDialinIP] [nvarchar](50) NULL,
	[VCMeetingID] [nvarchar](50) NULL,
	[VCDialinSIP] [nvarchar](50) NULL,
	[VCDialinH323] [nvarchar](50) NULL,
	[VCPin] [nvarchar](50) NULL,
	[PCDialinNum] [nvarchar](50) NULL,
	[PCDialinFreeNum] [nvarchar](50) NULL,
	[PCMeetingID] [nvarchar](50) NULL,
	[PCPin] [nvarchar](50) NULL,
	[Intructions] [nvarchar](max) NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]
GO


CREATE TABLE [dbo].[Audit_Usr_Dept_D](
	[departmentId] [int] NOT NULL,
	[userid] [int] NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]

GO


CREATE TABLE [dbo].[Audit_Acc_Balance_D](
	[UserID] [int] NOT NULL,
	[TotalTime] [int] NOT NULL,
	[TimeRemaining] [int] NOT NULL,
	[InitTime] [datetime] NULL,
	[ExpirationTime] [datetime] NULL,
	[orgId] [int] NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]

GO


CREATE TABLE [dbo].[Audit_Summary_UsrList_D](
	[UID] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedUser] [int] NULL,
	[Description] [nvarchar](100) NULL,
	[ModifiedDetails] [nvarchar](max) NULL,
) ON [PRIMARY]

GO


 Insert into Audit_Usr_List_D (  [UserID],[FirstName],[LastName],[Email],[Financial],[Admin],[Login],
 [Password],[Company], [TimeZone],[Language],[PreferedRoom],[AlternativeEmail],[DoubleEmail],
 [PreferedGroup],[CCGroup], [Telephone],[RoomID],[PreferedOperator],[lockCntTrns],[DefLineRate],
 [DefVideoProtocol],[Deleted], [DefAudio],[DefVideoSession],[MenuMask],[initialTime],[EmailClient],
 [newUser],[PrefTZSID], [IPISDNAddress],[DefaultEquipmentId],[connectionType],[companyId],[securityKey],
 [LockStatus], [LastLogin],[outsidenetwork],[emailmask],[roleID],[addresstype],[accountexpiry],[approverCount], 
 [BridgeID],[Title],[LevelID],[preferredISDN],[portletId],[searchId],[endpointId],[DateFormat], [enableAV],
 [timezonedisplay],[timeformat],[enableParticipants],[TickerStatus],[TickerSpeed], [TickerPosition],
 [TickerBackground],[TickerDisplay],[RSSFeedLink],[TickerStatus1],[TickerSpeed1], [TickerPosition1],
 [TickerBackground1],[TickerDisplay1],[RSSFeedLink1],[enableExchange],[enableDomino], [Audioaddon],
 [WorkPhone],[CellPhone],[ConferenceCode],[LeaderPin],[DefaultTemplate],[EmailLangId], [MailBlocked],
 [MailBlockedDate],[enableMobile],[PluginConfirmations],[InternalVideoNumber],  [ExternalVideoNumber],
 [HelpReqEmailID],[HelpReqPhone],[SendSurveyEmail],[PrivateVMR], [EnableVNOCselection],[EntityID],
 [Extension],[Pin],[MemberID],[isLocked],[VidyoURL],[Type],  [allowPersonalMeeting],[allowCallDirect],
 [ESUserID],[ParentReseller],[CorpUser],[Region],  [Secured],[PreConfCode],[PreLeaderPin],[PostLeaderPin],
 [AudioDialInPrefix],[enablePCUser],  [RFIDValue],[BrokerRoomNum],[PerCalStartTime],[PerCalEndTime],
 [RoomCalStartTime],[RoomCalEndime],  [PerCalShowHours],[RoomCalShowHours],[RequestID],[StaticID],
 [IsStaticIDEnabled],[enableWebexUser],[WebexUserName],[WebexPassword],[LandingPageTitle],
 [LandingPageLink], [LastModifiedUser],[PasswordTime],[IsPasswordReset],[lastmodifieddate], [ValueChange])  
 (Select [UserID],[FirstName],[LastName],[Email],[Financial],[Admin],[Login],[Password],[Company], 
 [TimeZone],[Language],[PreferedRoom],[AlternativeEmail],[DoubleEmail],[PreferedGroup],[CCGroup], 
 [Telephone],[RoomID],[PreferedOperator],[lockCntTrns],[DefLineRate],[DefVideoProtocol],[Deleted], 
 [DefAudio],[DefVideoSession],[MenuMask],[initialTime],[EmailClient],[newUser],[PrefTZSID], 
 [IPISDNAddress],[DefaultEquipmentId],[connectionType],[companyId],[securityKey],[LockStatus], 
 [LastLogin],[outsidenetwork],[emailmask],[roleID],[addresstype],[accountexpiry],[approverCount], 
 [BridgeID],[Title],[LevelID],[preferredISDN],[portletId],[searchId],[endpointId],[DateFormat], 
 [enableAV],[timezonedisplay],[timeformat],[enableParticipants],[TickerStatus],[TickerSpeed], 
 [TickerPosition],[TickerBackground],[TickerDisplay],[RSSFeedLink],[TickerStatus1],[TickerSpeed1], 
 [TickerPosition1],[TickerBackground1],[TickerDisplay1],[RSSFeedLink1],[enableExchange],[enableDomino], 
 [Audioaddon],[WorkPhone],[CellPhone],[ConferenceCode],[LeaderPin],[DefaultTemplate],[EmailLangId], 
 [MailBlocked],[MailBlockedDate],[enableMobile],[PluginConfirmations],[InternalVideoNumber],  
 [ExternalVideoNumber],[HelpReqEmailID],[HelpReqPhone],[SendSurveyEmail],[PrivateVMR], [EnableVNOCselection],
 [EntityID],[Extension],[Pin],[MemberID],[isLocked],[VidyoURL],[Type],  [allowPersonalMeeting],
 [allowCallDirect],[ESUserID],[ParentReseller],[CorpUser],[Region],  [Secured],[PreConfCode],[PreLeaderPin],
 [PostLeaderPin],[AudioDialInPrefix],[enablePCUser], [RFIDValue],[BrokerRoomNum],[PerCalStartTime],
 [PerCalEndTime],[RoomCalStartTime],[RoomCalEndime], [PerCalShowHours],[RoomCalShowHours],[RequestID],
 [StaticID],[IsStaticIDEnabled],[enableWebexUser], [WebexUserName],[WebexPassword],
 [LandingPageTitle],[LandingPageLink], [LastModifiedUser],[PasswordTime],[IsPasswordReset],
 [lastmodifieddate], 'No Changes Made'  from Usr_List_D where Deleted = 0)
 

Insert into [Audit_Usr_Dept_D] ([DepartmentId], [userid], [LastModifiedDate])
( select [DepartmentId], [userid] , GETUTCDATE() from Usr_Dept_D)


Insert into [Audit_Usr_PCDetails_D] ([Description], [Intructions], [PCDialinFreeNum], 
[PCDialinNum], [PCId], [PCMeetingID], [PCPin], [SkypeURL], [userid], [VCDialinH323], [VCDialinIP], 
[VCDialinSIP], [VCMeetingID], [VCPin], [LastModifiedDate])
( select [Description], [Intructions], [PCDialinFreeNum], 
[PCDialinNum], [PCId], [PCMeetingID], [PCPin], [SkypeURL], [userid], [VCDialinH323], [VCDialinIP], 
[VCDialinSIP], [VCMeetingID], [VCPin], GETUTCDATE() from Usr_PCDetails_D)

Insert into [Audit_Acc_Balance_D] ([ExpirationTime], [InitTime], [orgId], [TimeRemaining], 
[TotalTime], [UserID], [LastModifiedDate])
( select [ExpirationTime], [InitTime], [orgId], [TimeRemaining], 
[TotalTime], [UserID], GETUTCDATE() from Acc_Balance_D)



/* ******************** ZD 101026-Audit User 13 March 2014  - End   ***************************** */


/* ******************** ZD 101026-Audit Room 13 March 2014  - Starts   ***************************** */

CREATE TABLE [dbo].[Audit_Loc_Room_D](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoomID] [int] NULL,
	[Name] [nvarchar](256) NULL,
	[RoomBuilding] [nvarchar](256) NULL,
	[RoomFloor] [nvarchar](256) NULL,
	[RoomNumber] [nvarchar](50) NULL,
	[RoomPhone] [nvarchar](50) NULL,
	[Capacity] [nvarchar](50) NULL,
	[Assistant] [nvarchar](256) NULL,
	[AssistantPhone] [nvarchar](20) NULL,
	[ProjectorAvailable] [int] NULL,
	[MaxPhoneCall] [int] NULL,
	[AdminID] [int] NULL,
	[videoAvailable] [smallint] NOT NULL,
	[DefaultEquipmentid] [smallint] NOT NULL,
	[DynamicRoomLayout] [smallint] NULL,
	[Caterer] [smallint] NULL,
	[L2LocationId] [int] NULL,
	[L3LocationId] [int] NULL,
	[Disabled] [smallint] NOT NULL,
	[responsetime] [int] NULL,
	[responsemessage] [nvarchar](2000) NULL,
	[roomimage] [nvarchar](500) NULL,
	[setuptime] [int] NULL,
	[teardowntime] [int] NULL,
	[auxattachments] [nvarchar](4000) NULL,
	[endpointid] [int] NULL,
	[costcenterid] [int] NULL,
	[notifyemails] [nvarchar](4000) NULL,
	[Address1] [nvarchar](500) NULL,
	[Address2] [nvarchar](500) NULL,
	[City] [nvarchar](200) NULL,
	[State] [int] NULL,
	[Zipcode] [nvarchar](20) NULL,
	[Country] [int] NULL,
	[Maplink] [nvarchar](2000) NULL,
	[ParkingDirections] [nvarchar](2000) NULL,
	[AdditionalComments] [nvarchar](1000) NULL,
	[TimezoneID] [int] NULL,
	[Longitude] [nvarchar](50) NULL,
	[Latitude] [nvarchar](50) NULL,
	[auxattachments2] [nvarchar](4000) NULL,
	[MapImage1] [nvarchar](200) NULL,
	[MapImage2] [nvarchar](200) NULL,
	[SecurityImage1] [nvarchar](200) NULL,
	[SecurityImage2] [nvarchar](200) NULL,
	[MiscImage1] [nvarchar](200) NULL,
	[MiscImage2] [nvarchar](200) NULL,
	[Custom1] [nvarchar](50) NULL,
	[Custom2] [nvarchar](50) NULL,
	[Custom3] [nvarchar](50) NULL,
	[Custom4] [nvarchar](50) NULL,
	[Custom5] [nvarchar](50) NULL,
	[Custom6] [nvarchar](50) NULL,
	[Custom7] [nvarchar](50) NULL,
	[Custom8] [nvarchar](50) NULL,
	[Custom9] [nvarchar](50) NULL,
	[Custom10] [nvarchar](50) NULL,
	[orgId] [int] NULL,
	[HandiCappedAccess] [smallint] NULL,
	[Lastmodifieddate] [datetime] NULL,
	[MapImage1Id] [int] NULL,
	[MapImage2Id] [int] NULL,
	[SecurityImage1Id] [int] NULL,
	[SecurityImage2Id] [int] NULL,
	[MiscImage1Id] [int] NULL,
	[MiscImage2Id] [int] NULL,
	[RoomImageId] [nvarchar](200) NULL,
	[isVIP] [smallint] NULL,
	[isTelepresence] [smallint] NULL,
	[ServiceType] [smallint] NULL,
	[DedicatedVideo] [nvarchar](50) NULL,
	[RoomQueue] [nvarchar](256) NULL,
	[DedicatedCodec] [nvarchar](50) NULL,
	[AVOnsiteSupportEmail] [nvarchar](50) NULL,
	[Extroom] [int] NULL,
	[IsVMR] [smallint] NOT NULL,
	[InternalNumber] [nvarchar](50) NULL,
	[ExternalNumber] [nvarchar](50) NULL,
	[EntityID] [int] NULL,
	[Type] [nvarchar](50) NULL,
	[OwnerID] [int] NULL,
	[Extension] [nvarchar](50) NULL,
	[VidyoURL] [nvarchar](max) NULL,
	[Pin] [nvarchar](50) NULL,
	[isLocked] [int] NULL,
	[allowCallDirect] [int] NULL,
	[MemberID] [int] NULL,
	[allowPersonalMeeting] [int] NULL,
	[isPublic] [int] NOT NULL,
	[RoomCategory] [int] NULL,
	[RoomToken] [nvarchar](250) NULL,
	[LastModifiedUser] [int] NULL,
	[VMRLink] [nvarchar](max) NULL,
	[RoomIconTypeId] [int] NULL,
	[RoomUID] [nvarchar](250) NULL,
	[Password] [nvarchar](50) NULL,
	[ValueChange] [nvarchar](150) NULL
) 

 

CREATE TABLE [dbo].[Audit_Loc_Department_D](
	[RoomId] [bigint] NOT NULL,
	[DepartmentId] [bigint] NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]


CREATE TABLE [dbo].[Audit_Loc_Approver_D](
	[roomid] [int] NOT NULL,
	[approverid] [int] NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]


CREATE TABLE [dbo].[Audit_Summary_LocRoom_D](
	[UID] [int] IDENTITY(1,1) NOT NULL,
	[RoomId] [int] NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedUser] [int] NULL,
	[Description] [nvarchar](100) NULL,
	[ModifiedDetails] [nvarchar](max) NULL
) ON [PRIMARY]

GO


Insert into Audit_Loc_Room_D (  [RoomID],[Name],[RoomBuilding],[RoomFloor],[RoomNumber],[RoomPhone],
 [Capacity],[Assistant], [AssistantPhone],[ProjectorAvailable],[MaxPhoneCall],[AdminID],[videoAvailable],
 [DefaultEquipmentid], [DynamicRoomLayout],[Caterer],[L2LocationId],[L3LocationId],[Disabled],[responsetime], 
 [responsemessage],[roomimage],[setuptime],[teardowntime],[auxattachments],[endpointid], 
 [costcenterid],[notifyemails],[Address1],[Address2],[City],[State],[Zipcode],[Country], 
 [Maplink],[ParkingDirections],[AdditionalComments],[TimezoneID],[Longitude],[Latitude], 
 [auxattachments2],[MapImage1],[MapImage2],[SecurityImage1],[SecurityImage2],[MiscImage1], 
 [MiscImage2],[Custom1],[Custom2],[Custom3],[Custom4],[Custom5],[Custom6],[Custom7], 
 [Custom8],[Custom9],[Custom10],[orgId],[HandiCappedAccess],[Lastmodifieddate],[MapImage1Id], 
 [MapImage2Id],[SecurityImage1Id],[SecurityImage2Id],[MiscImage1Id],[MiscImage2Id], 
 [RoomImageId],[isVIP],[isTelepresence],[ServiceType],[DedicatedVideo],[RoomQueue], 
 [DedicatedCodec],[AVOnsiteSupportEmail],[Extroom],[IsVMR],[InternalNumber],[ExternalNumber], 
 [EntityID],[Type],[OwnerID],[Extension],[VidyoURL],[Pin],[isLocked],[allowCallDirect], 
 [MemberID],[allowPersonalMeeting],[isPublic],[RoomCategory],[RoomToken],[LastModifiedUser], 
 [VMRLink],[RoomIconTypeId],[RoomUID],[Password], [ValueChange]) 
 (Select [RoomID],[Name],[RoomBuilding],[RoomFloor],[RoomNumber],[RoomPhone],[Capacity],[Assistant], 
 [AssistantPhone],[ProjectorAvailable],[MaxPhoneCall],[AdminID],[videoAvailable],[DefaultEquipmentid], 
 [DynamicRoomLayout],[Caterer],[L2LocationId],[L3LocationId],[Disabled],[responsetime], 
 [responsemessage],[roomimage],[setuptime],[teardowntime],[auxattachments],[endpointid], 
 [costcenterid],[notifyemails],[Address1],[Address2],[City],[State],[Zipcode],[Country], 
 [Maplink],[ParkingDirections],[AdditionalComments],[TimezoneID],[Longitude],[Latitude], 
 [auxattachments2],[MapImage1],[MapImage2],[SecurityImage1],[SecurityImage2],[MiscImage1], 
 [MiscImage2],[Custom1],[Custom2],[Custom3],[Custom4],[Custom5],[Custom6],[Custom7], 
 [Custom8],[Custom9],[Custom10],[orgId],[HandiCappedAccess],[Lastmodifieddate],[MapImage1Id], 
 [MapImage2Id],[SecurityImage1Id],[SecurityImage2Id],[MiscImage1Id],[MiscImage2Id], 
 [RoomImageId],[isVIP],[isTelepresence],[ServiceType],[DedicatedVideo],[RoomQueue], 
 [DedicatedCodec],[AVOnsiteSupportEmail],[Extroom],[IsVMR],[InternalNumber],[ExternalNumber], 
 [EntityID],[Type],[OwnerID],[Extension],[VidyoURL],[Pin],[isLocked],[allowCallDirect], 
 [MemberID],[allowPersonalMeeting],[isPublic],[RoomCategory],[RoomToken],[LastModifiedUser], 
 [VMRLink],[RoomIconTypeId],[RoomUID],[Password], 'No Changes Made' from Loc_Room_D)
 


Insert into [Audit_Loc_Department_D] ([DepartmentId], [RoomId], [LastModifiedDate])
( select [DepartmentId], [RoomId] , GETUTCDATE() from Loc_Department_D)

Insert into [Audit_Loc_Approver_D] ([RoomId], [approverid], [LastModifiedDate])
( select [RoomId], [approverid], GETUTCDATE() from Loc_Approver_D)


/* ******************** ZD 101026-Audit Room 13 March 2014  - End ***************************** */


/* ******************** ZD 101026-Audit MCU 13 March 2014 - Starts ***************************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.MCU_list_D ADD
	[LastModifiedUser] int NULL
GO
COMMIT

update MCU_list_D set LastModifiedUser = 11


CREATE TABLE [dbo].[Audit_Mcu_List_D](
    [ID] [int] IDENTITY(1,1) NOT NULL,
	[BridgeID] [int] NULL,
	[BridgeLogin] [nvarchar](256) NULL,
	[BridgePassword] [nvarchar](1024) NULL,
	[BridgeAddress] [nvarchar](256) NULL,
	[Status] [int] NULL,
	[Admin] [int] NULL,
	[LastModified] [datetime] NULL,
	[BridgeName] [nvarchar](256) NULL,
	[ChainPosition] [int] NULL,
	[updateInBridge] [smallint] NULL,
	[NewBridge] [smallint] NULL,
	[ISDNPhonePrefix] [nvarchar](50) NULL,
	[BridgeTypeId] [int] NULL,
	[SoftwareVer] [nvarchar](100) NULL,
	[UpdateFromBridge] [smallint] NULL,
	[VirtualBridge] [smallint] NULL,
	[isdnportrate] [decimal](18, 0) NULL,
	[isdnlinerate] [decimal](18, 0) NULL,
	[isdnmaxcost] [decimal](18, 0) NULL,
	[isdnthreshold] [decimal](18, 0) NULL,
	[timezone] [int] NULL,
	[syncflag] [smallint] NULL,
	[isdnthresholdalert] [smallint] NULL,
	[malfunctionalert] [smallint] NULL,
	[reservedportperc] [int] NULL,
	[responsetime] [int] NULL,
	[responsemessage] [nvarchar](4000) NULL,
	[deleted] [smallint] NOT NULL,
	[maxConcurrentAudioCalls] [int] NULL,
	[maxConcurrentVideoCalls] [int] NULL,
	[orgId] [int] NULL,
	[ApiPortNo] [int] NULL,
	[EnableIVR] [int] NULL,
	[IVRServiceName] [nvarchar](250) NULL,
	[EnableRecording] [smallint] NULL,
	[isPublic] [smallint] NOT NULL,
	[ISDNGateway] [varchar](50) NULL,
	[ISDNAudioPrefix] [varchar](5) NULL,
	[ISDNVideoPrefix] [varchar](5) NULL,
	[ConfServiceID] [int] NULL,
	[LPR] [smallint] NULL,
	[CurrentDateTime] [datetime] NULL,
	[EnableMessage] [int] NULL,
	[setFavourite] [smallint] NOT NULL,
	[portsVideoTotal] [int] NULL,
	[portsVideoFree] [int] NULL,
	[portsAudioTotal] [int] NULL,
	[portsAudioFree] [int] NULL,
	[BridgeExtNo] [nvarchar](50) NOT NULL,
	[E164Dialing] [int] NULL,
	[H323Dialing] [int] NULL,
	[EnhancedMCU] [int] NULL,
	[DMAMonitorMCU] [int] NULL,
	[DMASendMail] [int] NULL,
	[DMADomain] [nvarchar](max) NULL,
	[DMAName] [nvarchar](max) NULL,
	[DMALogin] [nvarchar](256) NULL,
	[DMAPassword] [nvarchar](1024) NULL,
	[DMAURL] [nvarchar](max) NULL,
	[DMAPort] [int] NULL,
	[Synchronous] [int] NULL,
	[EnableCDR] [smallint] NOT NULL,
	[DeleteCDRDays] [int] NOT NULL,
	[DialinPrefix] [nvarchar](50) NULL,
	[LoginName] [nvarchar](50) NULL,
	[loginCount] [int] NULL,
	[RPRMEmailaddress] [nvarchar](150) NULL,
	[RPRMDomain] [nvarchar](250) NULL,
	[IsMultitenant] [int] NULL,
	[LastDateTimeCDRPoll] [datetime] NULL,
	[URLAccess] [int] NULL,
	[EnablePollFailure] [int] NULL,
	[PollCount] [int] NULL,
	[MultipleAddress] [nvarchar](max) NULL,
	[LastModifiedUser] [int] NULL,
	[LastModifiedDate] [datetime] NULL,
	[ValueChange] [nvarchar](150) NULL
	)


CREATE TABLE [dbo].[Audit_Summary_MCUList_D](
	[UID] [int] IDENTITY(1,1) NOT NULL,
	[BridgeId] [int] NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedUser] [int] NULL,
	[Description] [nvarchar](100) NULL,
	[ModifiedDetails] [nvarchar](max) NULL
) ON [PRIMARY]

GO



CREATE TABLE [dbo].[Audit_Mcu_Approver_D](
	[mcuid] [int] NOT NULL,
	[approverid] [int] NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[Audit_Mcu_CardList_D](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[BridgeID] [int] NOT NULL,
	[cardTypeId] [int] NOT NULL,
	[maxCalls] [int] NOT NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]

GO


CREATE TABLE [dbo].[Audit_MCU_E164Services_D](
	[uId] [int] IDENTITY(1,1) NOT NULL,
	[BridgeID] [int] NULL,
	[StartRange] [nvarchar](50) NULL,
	[EndRange] [nvarchar](50) NULL,
	[SortID] [int] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Audit_Mcu_IPServices_D](
	[BridgeID] [int] NOT NULL,
	[SortID] [int] NOT NULL,
	[ServiceName] [nvarchar](256) NOT NULL,
	[AddressType] [smallint] NOT NULL,
	[IPAddress] [nvarchar](256) NOT NULL,
	[NetworkAccess] [smallint] NOT NULL,
	[Usage] [smallint] NOT NULL,
	[uId] [int] IDENTITY(1,1) NOT NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[Audit_Mcu_ISDNServices_D](
	[BridgeID] [int] NOT NULL,
	[SortID] [int] NOT NULL,
	[ServiceName] [nvarchar](256) NULL,
	[Prefix] [nvarchar](50) NULL,
	[StartNumber] [bigint] NOT NULL,
	[EndNumber] [bigint] NOT NULL,
	[NetworkAccess] [smallint] NOT NULL,
	[Usage] [smallint] NOT NULL,
	[RangeSortOrder] [smallint] NOT NULL,
	[uId] [int] IDENTITY(1,1) NOT NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]

GO


CREATE TABLE [dbo].[Audit_Mcu_MPIServices_D](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BridgeID] [int] NOT NULL,
	[SortID] [int] NOT NULL,
	[ServiceName] [nvarchar](256) NOT NULL,
	[AddressType] [int] NULL,
	[IPAddress] [nvarchar](256) NULL,
	[NetworkAccess] [int] NULL,
	[Usage] [int] NULL,
	[RangeSortOrder] [int] NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]

GO



CREATE TABLE [dbo].[Audit_MCU_Orgspecific_D](
	[bridgeID] [smallint] NOT NULL,
	[orgID] [smallint] NOT NULL,
	[uID] [int] IDENTITY(1,1) NOT NULL,
	[propertyName] [nvarchar](150) NULL,
	[propertyValue] [nvarchar](500) NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]

GO


CREATE TABLE [dbo].[Audit_MCU_Profiles_D](
	[UID] [int] IDENTITY(1,1) NOT NULL,
	[MCUId] [int] NULL,
	[ProfileId] [int] NULL,
	[ProfileName] [nvarchar](50) NULL,
	[LastModifiedDate] [datetime] NULL
) ON [PRIMARY]

GO


 Insert into Audit_Mcu_List_D   ([BridgeID],[BridgeLogin],[BridgePassword],[BridgeAddress],[Status],
 [Admin],[LastModified],[BridgeName], [ChainPosition],[updateInBridge],[NewBridge],[ISDNPhonePrefix],
 [BridgeTypeId],[SoftwareVer],[UpdateFromBridge], [VirtualBridge],[isdnportrate],[isdnlinerate],
 [isdnmaxcost],[isdnthreshold],[timezone],[syncflag], [isdnthresholdalert],[malfunctionalert],
 [reservedportperc],[responsetime],[responsemessage], [deleted],[maxConcurrentAudioCalls],
 [maxConcurrentVideoCalls],[orgId],[ApiPortNo],[EnableIVR], [IVRServiceName],[EnableRecording],
 [isPublic],[ISDNGateway],[ISDNAudioPrefix],[ISDNVideoPrefix], [ConfServiceID],[LPR],[CurrentDateTime],
 [EnableMessage],[setFavourite],[portsVideoTotal],[portsVideoFree], [portsAudioTotal],[portsAudioFree],
 [BridgeExtNo],[E164Dialing],[H323Dialing],[EnhancedMCU], [DMAMonitorMCU],[DMASendMail],[DMADomain],
 [DMAName],[DMALogin],[DMAPassword],[DMAURL],[DMAPort], [Synchronous],[EnableCDR],[DeleteCDRDays],
 [DialinPrefix],[LoginName],[loginCount],[RPRMEmailaddress], [RPRMDomain],[IsMultitenant],[LastDateTimeCDRPoll],
 [URLAccess],[EnablePollFailure],[PollCount], [MultipleAddress],[LastModifiedUser], ValueChange)  
 (Select [BridgeID],[BridgeLogin],[BridgePassword],[BridgeAddress],[Status],[Admin],
 [LastModified],[BridgeName], [ChainPosition],[updateInBridge],[NewBridge],[ISDNPhonePrefix],
 [BridgeTypeId],[SoftwareVer],[UpdateFromBridge], [VirtualBridge],[isdnportrate],[isdnlinerate],
 [isdnmaxcost],[isdnthreshold],[timezone],[syncflag], [isdnthresholdalert],[malfunctionalert],
 [reservedportperc],[responsetime],[responsemessage], [deleted],[maxConcurrentAudioCalls],
 [maxConcurrentVideoCalls],[orgId],[ApiPortNo],[EnableIVR], [IVRServiceName],[EnableRecording],
 [isPublic],[ISDNGateway],[ISDNAudioPrefix],[ISDNVideoPrefix], [ConfServiceID],[LPR],[CurrentDateTime],
 [EnableMessage],[setFavourite],[portsVideoTotal],[portsVideoFree], [portsAudioTotal],
 [portsAudioFree],[BridgeExtNo],[E164Dialing],[H323Dialing],[EnhancedMCU], [DMAMonitorMCU],
 [DMASendMail],[DMADomain],[DMAName],[DMALogin],[DMAPassword],[DMAURL],[DMAPort], [Synchronous],
 [EnableCDR],[DeleteCDRDays],[DialinPrefix],[LoginName],[loginCount],[RPRMEmailaddress], 
 [RPRMDomain],[IsMultitenant],[LastDateTimeCDRPoll],[URLAccess],[EnablePollFailure],[PollCount],
 [MultipleAddress],[LastModifiedUser], 'No Changes Made'  from Mcu_List_D where deleted = 0)
	

Insert into [Audit_Mcu_Approver_D] ([mcuid], [approverid], [LastModifiedDate])
( select [mcuid], [approverid] , GETUTCDATE() from Mcu_Approver_D)


Insert into [Audit_Mcu_CardList_D] ([BridgeID], [cardTypeId], [maxCalls], [LastModifiedDate])
( select [BridgeID], [cardTypeId], [maxCalls], GETUTCDATE() from Mcu_CardList_D)


Insert into [Audit_MCU_E164Services_D] ([BridgeID], [StartRange], [EndRange], [SortID], [LastModifiedDate])
( select [BridgeID], [StartRange], [EndRange], [SortID], GETUTCDATE() from MCU_E164Services_D)

Insert into [Audit_Mcu_IPServices_D] ([BridgeID], [SortID], [ServiceName], [AddressType], 
[IPAddress], [NetworkAccess], [Usage], [LastModifiedDate])
( select [BridgeID], [SortID], [ServiceName], [AddressType], 
 [IPAddress], [NetworkAccess], [Usage], GETUTCDATE() from Mcu_IPServices_D)
 
Insert into [Audit_Mcu_ISDNServices_D] ([BridgeID], [EndNumber], [NetworkAccess], [Prefix], [RangeSortOrder], 
[ServiceName], [SortID], [StartNumber], [Usage], [LastModifiedDate])
(select [BridgeID], [EndNumber], [NetworkAccess], [Prefix], [RangeSortOrder], 
[ServiceName], [SortID], [StartNumber], [Usage], GETUTCDATE() from Mcu_ISDNServices_D)

Insert into [Audit_Mcu_MPIServices_D] ([AddressType], [BridgeID], [IPAddress], [NetworkAccess], 
[RangeSortOrder], [ServiceName], [SortID], [Usage], [LastModifiedDate])
(select [AddressType], [BridgeID], [IPAddress], [NetworkAccess], 
[RangeSortOrder], [ServiceName], [SortID], [Usage], GETUTCDATE() from Mcu_MPIServices_D)


Insert into [Audit_MCU_Orgspecific_D] ([bridgeID], [orgID], [propertyName], [propertyValue], [LastModifiedDate])
(select [bridgeID], [orgID], [propertyName], [propertyValue], GETUTCDATE() from MCU_Orgspecific_D)


Insert into [Audit_MCU_Profiles_D] ([MCUId], [ProfileId], [ProfileName], [LastModifiedDate])
(select [MCUId], [ProfileId], [ProfileName], GETUTCDATE() from MCU_Profiles_D)


/* ******************** ZD 101026-Audit MCU 13 March 2014 -  End ***************************** */



/* ******************** ZD 101026-Audit Endpoint 13 March 2014  -  Starts ***************************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Ept_List_D ADD
	[LastModifiedUser] [int] NULL,
	[Lastmodifieddate][datetime] NULL
GO
COMMIT

update Ept_List_D set LastModifiedUser = 11, Lastmodifieddate = GETUTCDATE()



CREATE TABLE [dbo].[Audit_Ept_List_D](
	[uId] [int] IDENTITY(1,1) NOT NULL,
	[endpointId] [int] NOT NULL,
	[name] [nvarchar](256) NULL,
	[password] [nvarchar](256) NULL,
	[protocol] [smallint] NULL,
	[connectiontype] [smallint] NULL,
	[addresstype] [smallint] NULL,
	[address] [nvarchar](512) NULL,
	[deleted] [smallint] NOT NULL,
	[outsidenetwork] [smallint] NULL,
	[videoequipmentid] [int] NULL,
	[linerateid] [int] NULL,
	[bridgeid] [int] NULL,
	[endptURL] [nvarchar](512) NULL,
	[profileId] [int] NOT NULL,
	[profileName] [nvarchar](256) NULL,
	[isDefault] [int] NOT NULL,
	[encrypted] [int] NOT NULL,
	[MCUAddress] [nvarchar](512) NULL,
	[MCUAddressType] [int] NULL,
	[TelnetAPI] [int] NULL,
	[orgId] [int] NULL,
	[ExchangeID] [nvarchar](200) NULL,
	[CalendarInvite] [int] NULL,
	[ApiPortNo] [int] NULL,
	[ConferenceCode] [nvarchar](50) NULL,
	[LeaderPin] [nvarchar](50) NULL,
	[isTelePresence] [smallint] NULL,
	[MultiCodecAddress] [nvarchar](2000) NULL,
	[RearSecCameraAddress] [nvarchar](512) NULL,
	[Extendpoint] [int] NULL,
	[EptOnlineStatus] [int] NULL,
	[PublicEndPoint] [smallint] NOT NULL,
	[NetworkURL] [nvarchar](max) NULL,
	[Secureport] [int] NULL,
	[Secured] [int] NULL,
	[EptCurrentStatus] [int] NULL,
	[GateKeeeperAddress] [nvarchar](50) NULL,
	[UserName] [nvarchar](50) NULL,
	[ManufacturerID] [int] NULL,
	[Lastmodifieddate] [datetime] NULL,
	[LastModifiedUser] [int] NULL,
	[ValueChange] [nvarchar](150) NULL
	)

CREATE TABLE [dbo].[Audit_Summary_EptList_D](
	[UID] [int] IDENTITY(1,1) NOT NULL,
	[EndpointId] [int] NULL,
	[ProfileId] [int] NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedUser] [int] NULL,
	[Description] [nvarchar](100) NULL,
	[ModifiedDetails] [nvarchar](max) NULL
) ON [PRIMARY]

GO


Insert into Audit_Ept_List_D 
([endpointId],[name],[password],[protocol],[connectiontype],[addresstype],[address], 
[deleted],[outsidenetwork],[videoequipmentid],[linerateid],[bridgeid],[endptURL],[profileId], 
[profileName],[isDefault],[encrypted],[MCUAddress],[MCUAddressType],[TelnetAPI],[orgId], 
[ExchangeID],[CalendarInvite],[ApiPortNo],[ConferenceCode],[LeaderPin],[isTelePresence], 
[MultiCodecAddress],[RearSecCameraAddress],[Extendpoint],[EptOnlineStatus],[PublicEndPoint], 
[NetworkURL],[Secureport],[Secured],[EptCurrentStatus],[GateKeeeperAddress],[UserName],[ManufacturerID], 
[LastModifiedUser],[Lastmodifieddate], [ValueChange])  
(select [endpointId],[name],[password],[protocol],[connectiontype],[addresstype],[address], 
[deleted],[outsidenetwork],[videoequipmentid],[linerateid],[bridgeid],[endptURL],[profileId], 
[profileName],[isDefault],[encrypted],[MCUAddress],[MCUAddressType],[TelnetAPI],[orgId], 
[ExchangeID],[CalendarInvite],[ApiPortNo],[ConferenceCode],[LeaderPin],[isTelePresence], 
[MultiCodecAddress],[RearSecCameraAddress],[Extendpoint],[EptOnlineStatus],[PublicEndPoint], 
[NetworkURL],[Secureport],[Secured],[EptCurrentStatus],[GateKeeeperAddress],[UserName],[ManufacturerID], 
[LastModifiedUser],[Lastmodifieddate], 'No Changes Made' from Ept_List_D)



/* ******************** ZD 101026-Audit Endpoint 13 March 2014 - End ***************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.91.3.24 Ends( 14 Mar 2014)               */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.91.3.25 Starts(14 Mar 2014 )             */
/*                                                                                              */
/* ******************************************************************************************** */

/* **********************************ZD 101006 Starts 14 Mar 2014************************** */
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.User_Lobby_D ADD
	SelectedColor nvarchar(50) NULL,
	SelectedImage nvarchar(MAX) NULL
GO
COMMIT

Update User_Lobby_D set SelectedColor='',SelectedImage=''

update Icons_Ref_S set IconUri=REPLACE(IconUri,'.jpg','.png')

/* **********************************ZD 101006 Ends 14 Mar 2014************************** */

/* **********************************ZD 101042 Starts 14 Mar 2014************************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.MCU_Params_D ADD
	snapshotSupport smallint NULL
GO
COMMIT


Update MCU_Params_D set snapshotSupport = 0-- where BridgeTypeid in ()
Update MCU_Params_D set snapshotSupport = 1 where BridgeTypeid in (4,5,6,12)

/* **********************************ZD 101042 Ends 14 Mar 2014************************** */

/*********************************** ZD 101056 Starts 14 Mar 2014 **************************/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_User_D ADD
	PartyNameonMCU nvarchar(MAX) NULL
GO
ALTER TABLE dbo.Conf_User_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

update Conf_User_D set PartyNameonMCU=''

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Room_D ADD
	PartyNameonMCU nvarchar(MAX) NULL
GO
ALTER TABLE dbo.Conf_Room_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

update Conf_Room_D set PartyNameonMCU=''

/*********************************** ZD 101056 Ends14 Mar 2014 **************************/