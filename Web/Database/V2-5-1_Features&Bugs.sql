/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.5.1 to V2.5.x                            */
/*                                                                                              */
/* ******************************************************************************************** */


/* *************************** FB 2136 - Start *************************** */
--FB 2136 Move Old Security Image to New table


Declare @secImgID as int, @roomid as int
DECLARE getStrCursor CURSOR for

select securityimage1id, roomid from loc_room_D where securityimage1id in
 (select imageid from img_list_d where attributetype = 4)

OPEN getStrCursor

FETCH NEXT FROM getStrCursor INTO  @secImgID, @roomid
WHILE @@FETCH_STATUS = 0
BEGIN

Declare @i as int, @imgid as int
set @i = 0

declare @attImg as varbinary(MAX), @imgName as varchar(100)

select @i = max(DisplayOrder) from Security_Badge_D
set @i = @i + 1

select @attImg = AttributeImage from img_list_d where imageid = @secImgID

insert into Security_Badge_D(ImgName,BadgeImage,TextAxis, PhotoAxis, BarCodeAxis, orgid,DisplayOrder) values 
('img', @attImg,'103|60','1|1','1|152', 11, @i)

set @imgid = @@identity 

set @imgName = 'Security_' + cast(@imgid as varchar(10))

update Security_Badge_D set ImgName = @imgName where badgeid = @imgid

update loc_room_D set securityimage1id = @@identity, securityimage1 = @imgName where Roomid = @roomid

--delete img_list_d where Imageid = @secImgID

FETCH NEXT FROM getStrCursor INTO @secImgID, @roomid
END
CLOSE getStrCursor
DEALLOCATE getStrCursor



/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Security_Badge_D
	(
	BadgeId int NOT NULL IDENTITY (1, 1),
	ImgName nvarchar(50) NULL,
	BadgeImage varbinary(MAX) NULL,
	TextAxis nvarchar(50) NULL,
	PhotoAxis nvarchar(50) NULL,
	BarCodeAxis nvarchar(50) NULL,
        OrgId int NULL,
	DisplayOrder int NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
COMMIT



/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnableSecurityBadge smallint NOT NULL CONSTRAINT DF_Org_Settings_D_EnableSecBadge DEFAULT 1,
	SecurityBadgeType smallint NOT NULL CONSTRAINT DF_Org_Settings_D_SecurityBadgeType DEFAULT 1,
	SecurityDeskEmailId nvarchar(100) NULL
GO
COMMIT



/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Gen_SecurityBadgeType_S
	(
	Id int NULL,
	SecBadgeType nvarchar(50) NULL
	)  ON [PRIMARY]
GO
COMMIT

Insert into Gen_SecurityBadgeType_S (Id, SecBadgeType) values (1,'User Only')
Insert into Gen_SecurityBadgeType_S (Id, SecBadgeType) values (2,'SecurityDesk Only')
Insert into Gen_SecurityBadgeType_S (Id, SecBadgeType) values (3,'Both')

/* *************************** FB 2136 - end *************************** */

/* *************************** FB 2036 - start *************************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
 EnableImmediateConference smallint 
GO
COMMIT 

update Org_Settings_D set EnableImmediateConference= 0

/* *************************** FB 2036 - end *************************** */

/* *************************** FB 2303 - start *************************** */

SET IDENTITY_INSERT Icons_Ref_S ON
delete Icons_Ref_S where iconid = 19
delete Icons_Ref_S where iconid = 45
insert into Icons_Ref_S (IconID,IconUri,Label,IconTarget) values(19,'../en/img/expressicon.png','Express Conference','ExpressConference.aspx?t=n&op=1')
SET IDENTITY_INSERT Icons_Ref_S off


/* *************************** FB 2303 - end *************************** */



/* *************************** FB 2339 - start *************************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
 EnablePasswordRule smallint 
GO
COMMIT 

update Org_Settings_D set EnablePasswordRule= 0

/* *************************** FB 2339 - end *************************** */




/* *************************** FB 2334 - start *************************** */


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Loc_Room_D ADD
	DedicatedVideo nvarchar(50) NULL
GO
COMMIT


Update Loc_Room_D  set Dedicatedvideo = '0'


/* *************************** FB 2334 - end *************************** */

/* *************************** FB 2335 - Start *************************** */

INSERT INTO [dbo].[Gen_VideoEquipment_S]
           ([VEid]
           ,[VEname])
     VALUES
           (28,
           'Polycom OTX')
GO


INSERT INTO [dbo].[Gen_VideoEquipment_S]
           ([VEid]
           ,[VEname])
     VALUES
           (29,
           'Polycom VPX')
GO

/*
   Tuesday, December 20, 20111:25:15 AM
   
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	DefPolycomRMXLO int NULL,
	DefPolycomMGCLO int NULL,
	DefCiscoTPLO int NULL,
	DefCTMSLO int NULL
GO

COMMIT

update dbo.Org_Settings_D set DefPolycomRMXLO = 01,DefPolycomMGCLO = 01,DefCiscoTPLO = 01,DefCTMSLO = 01

/* *************************** FB 2335 - End *************************** */


/* *************************** FB 2334 - Start *************************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_d ADD
	DedicatedVideo nvarchar(50) NULL
GO
COMMIT


Update Org_Settings_d  set Dedicatedvideo = '0'


/* *************************** FB 2334 - end *************************** */

/* *************************** FB 2023 - Start *************************** */

update Usr_List_D set FirstName = FirstName+' ' + LastName,LastName = '' where Audioaddon =1 and Deleted = 0

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnableAudioBridges smallint NOT NULL DEFAULT 1
GO
COMMIT


/*  User Roles Menumask Updation */

--Make sure the ROLEIDs are correct for the corresponding ROLE.

update usr_roles_d set roleMenuMask='8*240-4*15+8*0+4*0+2*0+8*0+2*0+2*0+2*0+1*0-6*14' where roleID=1

update usr_roles_d set roleMenuMask='8*248-4*15+8*255+4*15+2*3+8*255+2*3+2*3+2*3+1*0-6*63' where roleID=2

update usr_roles_d set roleMenuMask='8*252-4*15+8*255+4*15+2*3+8*255+2*3+2*3+2*3+1*1-6*63' where roleID=3

update usr_roles_d set roleMenuMask='8*136-4*0+8*2+4*0+2*0+8*0+2*0+2*3+2*0+1*0-6*63' where roleID=4

update usr_roles_d set roleMenuMask='8*136-4*0+8*4+4*0+2*0+8*0+2*3+2*0+2*0+1*0-6*63' where roleID=5

update usr_roles_d set roleMenuMask='8*136-4*0+8*1+4*0+2*0+8*0+2*0+2*0+2*3+1*0-6*63' where roleID=6

update usr_list_d set usr_list_d.menumask = b.rolemenumask from usr_roles_d as b where usr_list_d.roleid  = b.roleid


update Usr_Inactive_D set Usr_Inactive_D.menumask = b.rolemenumask from usr_roles_d as b where Usr_Inactive_D.roleid  = b.roleid


update Usr_Inactive_D set audioaddon = '' where audioaddon = null


/* *************************** FB 2023 - End *************************** */

/* *************************** FB 2337 - Start *************************** */
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Org_License_d
	(
	UID int NOT NULL  IDENTITY (1, 1),
	OrgID int NULL,
	EndUserLicAgrmnt varchar(MAX) NOT NULL DEFAULT ('')
	) 
GO
COMMIT
/* *************************** FB 2337 - End *************************** */

/* *************************** FB 2341 - Start *************************** */
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	[ConceirgeSupport] [nvarchar](50)
GO
COMMIT
/* *************************** FB 2341 - End *************************** */



/* *************************** FB 2359 - Start *************************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnableConfPassword smallint NOT NULL DEFAULT 1,
	EnablePublicConf smallint NOT NULL DEFAULT 1,
	EnableRoomParam smallint NOT NULL DEFAULT 1
GO
COMMIT

/* *************************** FB 2359 - End *************************** */



/* *************************** FB 2359 Update- start*************************** */

update Org_Settings_D set EnableConfPassword =1 where EnableConfPassword <>0 
update Org_Settings_D set EnablePublicConf =1 where EnablePublicConf <>0 
update Org_Settings_D set EnableRoomParam =1 where EnableRoomParam <>0

/* *************************** FB 2359 Update- end*************************** */

/* *************************** FB 2023 Update- start*************************** */

update Usr_Inactive_D set audioaddon = 0 where audioaddon is null

/* *************************** FB 2023 Update- End *************************** */



/* ******** FB 2343 - Starts ******** */


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.[Rpt_Month_D]
	(
	UID int NOT NULL IDENTITY (1, 1),
	ID int NULL,
	OrgId int NULL,
	MonthName varchar(20) NULL,
	MonthWorkingDays int NULL,
	MonthStartDate datetime NULL,
	MonthEndDate datetime NULL,
	CurrentYear int NULL
	)  ON [PRIMARY]
GO
COMMIT

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.[Rpt_Week_D]
	(
	UID int NOT NULL IDENTITY (1, 1),
	ID int NULL,
	OrgId int NULL,
	WeekNumber varchar(20) NULL,
	WeekWorkingDays int NULL,
	WeekStartDate datetime NULL,
	WeekEndDate datetime NULL,
	CurrentYear int NULL
	)  ON [PRIMARY]
GO
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	WorkingHours int NULL
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_WorkingHours DEFAULT 8 FOR WorkingHours
GO
COMMIT

Update Org_Settings_D set WorkingHours = 8

/* ******** FB 2343 - End ******** */


/* *************************** FB 2342 - Start *************************** */

-- alteration for loc_room_d --
alter table loc_room_d add RoomQueue nvarchar(256)


/* *************************** FB 2342 - end *************************** */



/* ********************* FB 2377.sql - Start (07 Feb 2012)****************** */
/* Express Interface - Custom Attributes Specific */

DECLARE @orgID INT

DECLARE cusAttrCursor CURSOR for
SELECT orgID FROM Org_List_D 

OPEN cusAttrCursor

FETCH NEXT FROM cusAttrCursor INTO @orgID
WHILE @@FETCH_STATUS = 0
BEGIN

Declare @id int

set @id = (select isnull(MAX(CustomAttributeId),0)+ 1 from Dept_CustomAttr_D)

INSERT INTO [Dept_CustomAttr_D]
([DeptID],[CustomAttributeId],[DisplayTitle],[Description],[Type],[Mandatory],[Deleted]
,[status],[IncludeInEmail],[orgId],[RoomApp],[McuApp],[SystemApp],[Scheduler],[Host]
,[Party] ,[McuAdmin],[RoomAdmin],[CreateType])
VALUES
(0,@id,'Meet and Greet','Concierge Support',2,0,0,0,0,@orgID,0,0,0,0,0,0,0,0,1)

set @id = (select MAX(CustomAttributeId)+ 1 from Dept_CustomAttr_D)

INSERT INTO [Dept_CustomAttr_D]
([DeptID],[CustomAttributeId],[DisplayTitle],[Description],[Type],[Mandatory],[Deleted]
,[status],[IncludeInEmail],[orgId],[RoomApp],[McuApp],[SystemApp],[Scheduler],[Host]
,[Party] ,[McuAdmin],[RoomAdmin],[CreateType])
VALUES
(0,@id,'Dedicated VNOC Operator','Concierge Support',2,0,0,0,0,@orgID,0,0,0,0,0,0,0,0,1)

set @id = (select MAX(CustomAttributeId)+ 1 from Dept_CustomAttr_D)

INSERT INTO [Dept_CustomAttr_D]
([DeptID],[CustomAttributeId],[DisplayTitle],[Description],[Type],[Mandatory],[Deleted]
,[status],[IncludeInEmail],[orgId],[RoomApp],[McuApp],[SystemApp],[Scheduler],[Host]
,[Party] ,[McuAdmin],[RoomAdmin],[CreateType])
VALUES
(0,@id,'On-Site A/V Support','Concierge Support',2,0,0,0,1,@orgID,0,0,0,0,0,0,1,0,1)


FETCH NEXT
FROM cusAttrCursor INTO @orgID
END
CLOSE cusAttrCursor
DEALLOCATE cusAttrCursor

/* ********************* FB 2377.sql - end   ****************** */

/****** FB 2347, FB 2348 and FB 2376 - V2.6(08Feb,2012) * *********/

/********* * FB 2347 * *********/
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnablePCModule int NULL
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_EnablePCModule DEFAULT 0 FOR EnablePCModule
GO
COMMIT

/* 
<MaxPCModules>5</MaxPCModules>

<myVRMSiteLicense><IR>1-myVRMQA</IR><Site><ExpirationDate>1/31/2013</ExpirationDate><MaxOrganizations>10</MaxOrganizations><IsLDAP>1</IsLDAP><ServerActivation><ProcessorIDs><ID></ID><ID></ID></ProcessorIDs><MACAddresses><ID></ID><ID></ID></MACAddresses></ServerActivation></Site><Organizations><Rooms><MaxNonVideoRooms>150</MaxNonVideoRooms><MaxVideoRooms>400</MaxVideoRooms></Rooms><Hardware><MaxMCUs>25</MaxMCUs><MaxEndpoints>400</MaxEndpoints><MaxCDRs></MaxCDRs></Hardware><Users><MaxTotalUsers>400</MaxTotalUsers><MaxGuestsPerUser>10</MaxGuestsPerUser><MaxExchangeUsers>100</MaxExchangeUsers><MaxDominoUsers>100</MaxDominoUsers><MaxMobileUsers>5</MaxMobileUsers></Users><Modules><MaxPCModules>5</MaxPCModules><MaxFacilitiesModules>5</MaxFacilitiesModules><MaxCateringModules>5</MaxCateringModules><MaxHousekeepingModules>5</MaxHousekeepingModules><MaxAPIModules>10</MaxAPIModules><Languages><MaxSpanish></MaxSpanish><MaxMandarin></MaxMandarin><MaxHindi></MaxHindi><MaxFrench></MaxFrench></Languages></Modules></Organizations></myVRMSiteLicense> 
*/

/******* * FB 2347 -End */

/********* * FB 2348 - Starts * *********/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnableSurvey int NULL,
	SurveyOption int NULL,
	SurveyURL nvarchar(250) NULL,
	TimeDuration int NULL

ALTER TABLE dbo.Usr_List_D ADD
	SendSurveyEmail int NULL

ALTER TABLE dbo.Usr_Inactive_D ADD
	SendSurveyEmail int NULL

ALTER TABLE dbo.Usr_GuestList_D ADD
	SendSurveyEmail int NULL

ALTER TABLE dbo.Conf_User_D ADD
	Survey int NULL

ALTER TABLE dbo.Tmp_Participant_D ADD
	Survey int NULL

ALTER TABLE Conf_Conference_D ADD
	sentSurvey int NOT NULL CONSTRAINT DF_Conf_Conference_D_sentSurvey DEFAULT 0

GO
COMMIT

update Org_Settings_D set EnableSurvey = 0,SurveyURL='http://localhost'
update Org_Settings_D set SurveyURL = 'http://localhost'
update Usr_List_D set SendSurveyEmail = 0
update Usr_Inactive_D set SendSurveyEmail = 0
update Usr_GuestList_D set SendSurveyEmail = 0
update Conf_Conference_D set sentSurvey=0
/********* * FB 2348 - End * *********/

/********** * FB 2376 - Starts * ********/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnableVMR smallint NOT NULL DEFAULT 1
GO
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	isVMR smallint NULL
GO
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_AdvAVParams_D ADD
	internalBridge varchar(50) NULL,
	externalBridge varchar(50) NULL
GO
COMMIT


/********** * FB 2376 - End * ********/


/****** * FB 2377-Starts V2.6(09Feb2012) * ***************/

/****** * Need to RUN ONLY OCNCE * *********/

DECLARE @ConceirgeSupport AS VARCHAR(10), @confid AS int ,@instanceid AS INT,@orgid AS int

DECLARE cusAttrCursor CURSOR FOR

SELECT ConceirgeSupport, confid,instanceid,orgId FROM dbo.Conf_Conference_D WHERE 
(ConceirgeSupport IS NOT  NULL and ConceirgeSupport != '')

OPEN cusAttrCursor

FETCH NEXT FROM cusAttrCursor INTO @ConceirgeSupport,@confid,@instanceid,@orgid
WHILE @@FETCH_STATUS = 0
BEGIN
	
	DECLARE @confAttID1 INT,@confAttID2 INT,@confAttID3 INT
	DECLARE @isVal AS INT, @isAvail AS INT
	
	SELECT @confAttID1 = CustomAttributeId FROM dbo.Dept_CustomAttr_D WHERE orgId = @orgid AND Description ='Concierge Support'
	AND DisplayTitle = 'Meet and Greet' 
	
	SELECT @confAttID2 = CustomAttributeId FROM dbo.Dept_CustomAttr_D WHERE orgId = @orgid AND Description ='Concierge Support'
	AND DisplayTitle = 'Dedicated VNOC Operator' 
	
	SELECT @confAttID3 = CustomAttributeId FROM dbo.Dept_CustomAttr_D WHERE orgId = @orgid AND Description ='Concierge Support'
	AND DisplayTitle = 'On-Site A/V Support' 
	
	SET @isVal = charindex('1', @ConceirgeSupport)
	SET @isAvail = 0;
	
	IF(@isVal > 0)
		SET @isAvail = 1
	
	INSERT INTO Conf_CustomAttr_D(ConfId, InstanceId, CustomAttributeId, SelectedOptionId,SelectedValue) VALUES
			(@confid,@instanceid,@confAttID1,-1,@isAvail)
	
	SET @isVal = charindex('2', @ConceirgeSupport)
	SET @isAvail = 0;
	
	IF(@isVal > 0)
		SET @isAvail = 1
	
	INSERT INTO Conf_CustomAttr_D(ConfId, InstanceId, CustomAttributeId, SelectedOptionId,SelectedValue) VALUES
			(@confid,@instanceid,@confAttID2,-1,@isAvail)
	
	SET @isVal = charindex('3', @ConceirgeSupport)
	SET @isAvail = 0;
	
	IF(@isVal > 0)
		SET @isAvail = 1
	
	INSERT INTO Conf_CustomAttr_D(ConfId, InstanceId, CustomAttributeId, SelectedOptionId,SelectedValue) VALUES
			(@confid,@instanceid,@confAttID3,-1,@isAvail)

FETCH NEXT
FROM cusAttrCursor INTO @ConceirgeSupport,@confid,@instanceid,@orgid
END
CLOSE cusAttrCursor
DEALLOCATE cusAttrCursor


Update Conf_Conference_D set ConceirgeSupport = '' where ConceirgeSupport IS NOT  NULL and ConceirgeSupport != ''


/****** * FB 2377-End V2.6(09Feb2012) * ***************/



/****** * FB 2272(Italy)-Starts V2.6(21stFeb2012) * ***************/

Insert into Gen_Language_S (LanguageID, Name, Country, languagefolder) values (7,'Italian','Italy','it')

/****** * FB 2272(Italy) -End V2.6(21stFeb2012) * ***************/












/*  Room Queue Update Script*/

/*

Please verify before updating the Client Email Domain.
*/

update Loc_Room_D set RoomQueue = [Name]+'@myvrm.com'


/* AUdio Bridge Specific Script */

/* 
 Please Check the Audio Bridge Screen 
 if User available update Audioaddon as 1 in Usr_List_D, Update other users Audioaddon as 0 
*/

UPDATE Usr_List_D SET Audioaddon = 1 WHERE FirstName LIKE '%domestic%'

UPDATE Usr_List_D SET Audioaddon = 1 WHERE FirstName LIKE '%International%'



/****** * FB 2363-Start (21Feb2012) ****************/


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_ESSettings_D](
	[UId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CustomerID] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PartnerName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PartnerEmail] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PartnerURL] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UserName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Password] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF


/****** Object:  Table [dbo].[ES_Event_D]    Script Date: 02/03/2012 05:05:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ES_Event_D](
	[UId] [int] IDENTITY(1,1) NOT NULL,
	[RequestID] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ConfNumName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TaskType] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TypeID] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CustomerID] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EventTrigger] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EventTime] [datetime] NULL CONSTRAINT [DF_ES_Event_D_EventTime]  DEFAULT (getdate()),
	[Status] [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StatusDate] [datetime] NULL,
	[StatusMessage] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	RequestCall [varchar](MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ResponseCall [varchar](MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
	
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	ESId varchar(50) NULL,
	ESType varchar(50) NULL,
	PushedToExternal int NULL
GO
ALTER TABLE dbo.Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_PushedToExternal DEFAULT 0 FOR PushedToExternal
GO
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.ES_MailUsrRptSettings_d
 (
	[UId] [int] IDENTITY(1,1) NOT NULL,
	CustomerName nvarchar(256) NULL,
	DeliveryType smallint NOT NULL DEFAULT 1,
	RptDestination nvarchar(512) NULL,
	StartTime smalldatetime NULL,
	FrequencyType smallint NOT NULL DEFAULT 1,
	FrequencyCount smallint NOT NULL DEFAULT 1,
	[Sent] smallint NOT NULL DEFAULT 0,
	SentTime smalldatetime NULL
	)
GO
COMMIT


/****** * FB 2363-end (21Feb2012) ****************/



/************** * FB 2272(Italy) - 24rdFeb (v2.6) * **********************/

DECLARE @CustomAttributeId INT, @DeptID INT, @OptionID INT, @OptionType INT, @OptionValue nvarchar(4000),
@Caption nvarchar(50),@HelpText nvarchar(4000),@LanguageId INT, @countlan INT 

DECLARE cusAttrCursor CURSOR for
Select b.[CustomAttributeId], b.[DeptID],b.[OptionID],b.[OptionType],b.[OptionValue] ,b.[Caption],  b.[HelpText] 
from Dept_CustomAttr_D a, Dept_CustomAttr_Option_D b
where a.[CustomAttributeId] = b.[CustomAttributeId] and b.[LanguageID] =1  

OPEN cusAttrCursor

FETCH NEXT FROM cusAttrCursor INTO @CustomAttributeId, @DeptID , @OptionID , @OptionType , @OptionValue ,
@Caption ,@HelpText 
WHILE @@FETCH_STATUS = 0
BEGIN
	set @LanguageId = 7

	set @countlan = (select count(*) from Dept_CustomAttr_D a, Dept_CustomAttr_Option_D b
	where a.[CustomAttributeId] = b.[CustomAttributeId] and b.[LanguageID] =@LanguageId and 
	b.[CustomAttributeId] = @CustomAttributeId and b.[OptionValue] = @OptionValue )

	if(@countlan <= 0)
	BEGIN

		INSERT INTO [Dept_CustomAttr_Option_D]
		([CustomAttributeId],[DeptID],[OptionID],[OptionType],[OptionValue],[Caption],[HelpText],[LanguageID])
		values (
		@CustomAttributeId , @DeptID , @OptionID , @OptionType , @OptionValue ,
		@Caption ,@HelpText ,@LanguageId 
		)

	END

	FETCH NEXT
	FROM cusAttrCursor INTO @CustomAttributeId,@DeptID , @OptionID , @OptionType , @OptionValue ,
	@Caption ,@HelpText
END
CLOSE cusAttrCursor
DEALLOCATE cusAttrCursor



/******* Deleting Duplicate Rows * *****************/
DECLARE @LanguageID2 INT


DECLARE cusAttrCursor CURSOR for
SELECT LanguageID FROM Gen_Language_S 

OPEN cusAttrCursor

FETCH NEXT FROM cusAttrCursor INTO @LanguageID2
WHILE @@FETCH_STATUS = 0
BEGIN

WITH CTE ([Text], DuplicateCount)
AS
(
SELECT Text,
ROW_NUMBER() OVER(PARTITION BY [Text] ORDER BY [Text]) AS DuplicateCount
FROM Launguage_Translation_Text where LanguageID = @LanguageID2
)
DELETE
FROM CTE
WHERE DuplicateCount > 1


FETCH NEXT
FROM cusAttrCursor INTO @LanguageID2
END
CLOSE cusAttrCursor
DEALLOCATE cusAttrCursor

/************** * FB 2272(Italy) - End * **********************/



/* *************************** FB 2390 - start *************************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Gen_VideoEquipment_S ADD
	Familyid int NULL
GO
COMMIT

update Gen_VideoEquipment_S set Familyid = 0


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Loc_Room_D ADD
	DedicatedCodec nvarchar(50) NULL
GO
COMMIT


Update Loc_Room_D  set DedicatedCodec = '0'


INSERT INTO [Gen_AddressType_S]([id],[name])VALUES(6,'SIP')

Update Gen_VideoEquipment_S set Familyid = 1 where VEid in(7,8,9,10,11,12,13,14,15,16,17,18,19,20,27)
Update Gen_VideoEquipment_S set Familyid = 2 where VEid in (25,26)
Update Gen_VideoEquipment_S set Familyid = 3 where VEid in(1,2,3,4,5,6)
Update Gen_VideoEquipment_S set Familyid = 11 where VEid = 24
Update Gen_VideoEquipment_S set Familyid = 9 where VEid in(28,29)
Update Gen_VideoEquipment_S set Familyid = 10 where VEid in(21,22,23)
Update Gen_VideoEquipment_S set VEname = 'Polycom HDX 400X' where familyid = 2 and VEname = 'Polycom HDX 4000'
Update Gen_VideoEquipment_S set VEname = 'Polycom HDX 900X' where familyid = 2 and  VEname = 'Polycom HDX 9000'
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (30,'Cisco C20',1)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (31,'Cisco C40',1)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (32,'Cisco C60',1)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (33,'Cisco C90',1)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (34,'Cisco EX90',1)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (35,'Cisco MXP',1)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (36,'Polycom HDX 600x',2)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (37,'Polycom HDX 700x',2)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (38,'Polycom HDX 800x',2)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (39,'Polycom VSX 3000A',3)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (40,'Polycom VSX 6000A',3)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (41,'Polycom VSX 7000A',3)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (42,'Polycom VSX 7000E',3)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (43,'Lifesize Room',8)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (44,'ViewStation FX and EX',4)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (45,'ViewStation 4000',4)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (46,'ViewStation',5)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (47,'ViewStation 7.0',5)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (48,'QDX 6000',6)
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid) values (49,'VVXVVX 1500',7)


/* *************************** FB 2390 - end *************************** */

/* *********************** FB 2401 - Starts ************************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnableEPDetails int NULL

ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_EnableEPDetails DEFAULT 0 FOR EnableEPDetails

GO
COMMIT

Update Org_Settings_D set EnableEPDetails =0

/* *********************** FB 2401 - Ends ************************** */


/**************  FB 2360 Starts **********************/

IF NOT EXISTS (select * from Gen_ServiceType_S where id =1 and servicetype = 'Silver')

BEGIN

truncate table Gen_ServiceType_S

Insert into Gen_ServiceType_S (ID, ServiceType) values (1,'Silver')
Insert into Gen_ServiceType_S (ID, ServiceType) values (2,'Gold')
Insert into Gen_ServiceType_S (ID, ServiceType) values (3,'Platinum')

update  Loc_Room_D set ServiceType = 10 where ServiceType =1
update  Loc_Room_D set ServiceType = 20 where ServiceType =2
update  Loc_Room_D set ServiceType = 30 where ServiceType =3


update  Loc_Room_D set ServiceType = 2 where ServiceType =10
update  Loc_Room_D set ServiceType = 3 where ServiceType =20
update  Loc_Room_D set ServiceType = 1 where ServiceType =30

update  Conf_Conference_D set ServiceType = 10 where ServiceType =1
update  Conf_Conference_D set ServiceType = 20 where ServiceType =2
update  Conf_Conference_D set ServiceType = 30 where ServiceType =3


update  Conf_Conference_D set ServiceType = 2 where ServiceType =10
update  Conf_Conference_D set ServiceType = 3 where ServiceType =20
update  Conf_Conference_D set ServiceType = 1 where ServiceType =30

END

/**************  FB 2360 Ends **********************/


/************************** * FB 2398(V2.6) - Starts * *****************************/

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	SetupTime int NULL,
	TearDownTime int NULL
GO
COMMIT

update Org_Settings_D set  SetupTime = 0 , TearDownTime = 0

/************************** * FB 2398(V2.6) - End * *****************************/


/************** FB 2416 Starts ******************/

IF EXISTS  (select * from Icons_Ref_S where iconid=19 and [label]='Express Conference')

BEGIN

IF EXISTS (select * from User_Lobby_D where iconid = 19 and [Label]='View Scheduled Calls')

BEGIN

update User_Lobby_D set [label]='Express Conference' where IconID=19
Delete User_Lobby_D where IconID = 45

END

END

/************** FB 2416 Ends ******************/

/* *************************** FB 2415 - start *************************** */


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Loc_Room_D ADD
	AVOnsiteSupportEmail nvarchar(50) NULL
GO
COMMIT


Update Loc_Room_D  set AVOnsiteSupportEmail= ''

/* *************************** FB 2415 - End *************************** */

/****************************************************** FB 2363 Start ***************************************************************/

/*
Monday, March 19, 201202:35:58 AM
User: myvrm
Server: VENI\SQLEXPRESS
Database: DisneyNew
Application: 
/

/ To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
CustomerID varchar(50) NULL,
CustomerName varchar(50) NULL
GO
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ES_Event_D ADD
	RetryCount int NULL
GO
COMMIT


Update ES_Event_D  set RetryCount= 0


/********* Alter Event Table *********/

ALTER TABLE ES_Event_D
ALTER COLUMN StatusMessage nvarchar(max) NULL

/************************************************ FB 2363 End ****************************************************/

/****************************** FB 2419 Starts - 09April 2012 **********************************/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnableAcceptDecline int NULL
GO
COMMIT

/****************************** FB 2419 Starts - 09April 2012 **********************************/


/************** * FB 2272(Japanese) - 09April2012 (v2.6) Start * **********************/

Insert into Gen_Language_S 
(LanguageID, Name, Country, languagefolder) 
values 
(6,'Japanese','Japan','jp')



DECLARE @CustomAttributeId INT, @DeptID INT, @OptionID INT, @OptionType INT, @OptionValue nvarchar(4000),
@Caption nvarchar(50),@HelpText nvarchar(4000),@LanguageId INT, @countlan INT 

DECLARE cusAttrCursor CURSOR for
Select b.[CustomAttributeId], b.[DeptID],b.[OptionID],b.[OptionType],b.[OptionValue] ,b.[Caption],  b.[HelpText] 
from Dept_CustomAttr_D a, Dept_CustomAttr_Option_D b
where a.[CustomAttributeId] = b.[CustomAttributeId] and b.[LanguageID] =1  

OPEN cusAttrCursor

FETCH NEXT FROM cusAttrCursor INTO @CustomAttributeId, @DeptID , @OptionID , @OptionType , @OptionValue ,
@Caption ,@HelpText 
WHILE @@FETCH_STATUS = 0
BEGIN
	set @LanguageId = 6

	set @countlan = (select count(*) from Dept_CustomAttr_D a, Dept_CustomAttr_Option_D b
	where a.[CustomAttributeId] = b.[CustomAttributeId] and b.[LanguageID] =@LanguageId and 
	b.[CustomAttributeId] = @CustomAttributeId and b.[OptionValue] = @OptionValue )

	if(@countlan <= 0)
	BEGIN

		INSERT INTO [Dept_CustomAttr_Option_D]
		([CustomAttributeId],[DeptID],[OptionID],[OptionType],[OptionValue],[Caption],[HelpText],[LanguageID])
		values (
		@CustomAttributeId , @DeptID , @OptionID , @OptionType , @OptionValue ,
		@Caption ,@HelpText ,@LanguageId 
		)

	END

	FETCH NEXT
	FROM cusAttrCursor INTO @CustomAttributeId,@DeptID , @OptionID , @OptionType , @OptionValue ,
	@Caption ,@HelpText
END
CLOSE cusAttrCursor
DEALLOCATE cusAttrCursor

/************** * FB 2272(Japanese) - 09April2012 (v2.6)  End * **********************/






/*******************************    * NVAR CHANGES START _ 09April Start *  ***********************************/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO

ALTER TABLE Conf_AdvAVParams_D ALTER COLUMN internalBridge NVARCHAR(50)
GO
ALTER TABLE Conf_AdvAVParams_D ALTER COLUMN externalBridge NVARCHAR(50)
GO

ALTER TABLE Conf_Location_D ALTER COLUMN name NVARCHAR(256)
GO
ALTER TABLE Conf_Location_D ALTER COLUMN Address NVARCHAR(512)
GO

ALTER TABLE Conf_Conference_D ALTER COLUMN ESId NVARCHAR(50)
GO
ALTER TABLE Conf_Conference_D ALTER COLUMN ESType NVARCHAR(50)
GO

ALTER TABLE ES_Event_D ALTER COLUMN RequestID NVARCHAR(50)
GO
ALTER TABLE ES_Event_D ALTER COLUMN ConfNumName NVARCHAR(50)
GO
ALTER TABLE ES_Event_D ALTER COLUMN TaskType NVARCHAR(50)
GO
ALTER TABLE ES_Event_D ALTER COLUMN TypeID NVARCHAR(50)
GO
ALTER TABLE ES_Event_D ALTER COLUMN CustomerID NVARCHAR(50)
GO
ALTER TABLE ES_Event_D ALTER COLUMN EventTrigger NVARCHAR(50)
GO
ALTER TABLE ES_Event_D ALTER COLUMN Status NVARCHAR(50)
GO
ALTER TABLE ES_Event_D ALTER COLUMN StatusMessage NVARCHAR(500)
GO
ALTER TABLE ES_Event_D ALTER COLUMN RequestCall NVARCHAR(MAX)
GO
ALTER TABLE ES_Event_D ALTER COLUMN ResponseCall NVARCHAR(50)
GO

ALTER TABLE Mcu_List_D ALTER COLUMN PreConfCode NVARCHAR(50)
GO
ALTER TABLE Mcu_List_D ALTER COLUMN PreLeaderPin NVARCHAR(50)
GO
ALTER TABLE Mcu_List_D ALTER COLUMN PostLeaderPin NVARCHAR(50)
GO

ALTER TABLE Rpt_Month_D ALTER COLUMN MonthName NVARCHAR(20)
GO

ALTER TABLE Rpt_Week_D ALTER COLUMN WeekNumber NVARCHAR(20)
GO

ALTER TABLE Sys_ESSettings_D ALTER COLUMN CustomerName NVARCHAR(50)
GO
ALTER TABLE Sys_ESSettings_D ALTER COLUMN CustomerID NVARCHAR(50)
GO
ALTER TABLE Sys_ESSettings_D ALTER COLUMN PartnerName NVARCHAR(50)
GO
ALTER TABLE Sys_ESSettings_D ALTER COLUMN PartnerEmail NVARCHAR(50)
GO
ALTER TABLE Sys_ESSettings_D ALTER COLUMN PartnerURL NVARCHAR(500)
GO
ALTER TABLE Sys_ESSettings_D ALTER COLUMN UserName NVARCHAR(50)
GO
ALTER TABLE Sys_ESSettings_D ALTER COLUMN Password NVARCHAR(50)
GO

ALTER TABLE Sys_Settings_D ALTER COLUMN Companymessage NVARCHAR(50)
GO

ALTER TABLE Usr_Inactive_D ALTER COLUMN PreferedRoom NVARCHAR(150)
GO

GO
COMMIT


/*******************************    * NVAR CHANGES START _ 09April End*  ***********************************/


 

/**************  * FB 2429 (V2.6) - 10April2012 * ***************/

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	DefLinerate int NULL
GO
COMMIT

Update Org_Settings_D Set DefLinerate = 384

/**************  * FB 2429 (V2.6) - 10April2012 * ***************/




/* ******** FB 2437 Starts (V2.8) - 08May2012 ************* */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Sys_Settings_D ADD
	EnableLaunchBufferP2P int NULL
GO
COMMIT

Update Sys_Settings_D set EnableLaunchBufferP2P = 1

/* ******** FB 2437 End - (V2.8) - 08May2012 ************* */




/******** Polycom CMA start **********/
--After complete full fleged CMA will uncomment this
--INSERT INTO [dbo].[Mcu_Vendor_S] ([id], [name], [BridgeInterfaceId], [videoparticipants], [audioparticipants]) VALUES (10, N'Polycom CMA', 1, 25, 25)
/******** Polycom CMA start **********/

--FB 2440 & FB 2261 - Start

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	McuSetupTime int NULL,
	MCUTeardonwnTime int NULL
GO
COMMIT

update dbo.Org_Settings_D set McuSetupTime  = 0 , MCUTeardonwnTime=  0


INSERT INTO [dbo].[Mcu_Vendor_S] ([id], [name], [BridgeInterfaceId], [videoparticipants], [audioparticipants]) VALUES (11, N'Life Size',7, 25, 25)


--FB 2440 & FB 2261 - End



/* *************************** FB 2400 - Start *************************** */
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE Ept_List_D ADD
	isTelePresence smallint NULL,
	MultiCodecAddress nvarchar(2000) NULL,
	RearSecCameraAddress nvarchar(512) NULL
GO
COMMIT

update Ept_List_D set MultiCodecAddress='',RearSecCameraAddress='',isTelePresence = 0


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE conf_room_d ADD
	MultiCodecAddress nvarchar(2000) NULL
GO
COMMIT

update conf_room_d set MultiCodecAddress=''
/* *************************** FB 2400 - End *************************** */


/* ************** FB 2440 Starts - V2.8(5thJune2012) ************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	MCUBufferPriority smallint NULL
GO
COMMIT

Update Org_Settings_D set MCUBufferPriority =0


/* ************** FB 2440 End - V2.8(5thJune2012) ************** */




/********* * FB 2426 Start * *********/

/* 

<MaxGuestRooms>10</MaxGuestRooms>

<myVRMSiteLicense><IR>1-myVRMQA</IR><Site><ExpirationDate>1/31/2013</ExpirationDate><MaxOrganizations>10</MaxOrganizations><IsLDAP>1</IsLDAP><ServerActivation><ProcessorIDs><ID></ID><ID></ID></ProcessorIDs><MACAddresses><ID></ID><ID></ID></MACAddresses></ServerActivation></Site><Organizations><Rooms><MaxNonVideoRooms>150</MaxNonVideoRooms><MaxVideoRooms>220</MaxVideoRooms><MaxGuestRooms>10</MaxGuestRooms></Rooms><Hardware><MaxMCUs>25</MaxMCUs><MaxEndpoints>220</MaxEndpoints><MaxCDRs></MaxCDRs></Hardware><Users><MaxTotalUsers>400</MaxTotalUsers><MaxGuestsPerUser>10</MaxGuestsPerUser><MaxExchangeUsers>100</MaxExchangeUsers><MaxDominoUsers>100</MaxDominoUsers><MaxMobileUsers>5</MaxMobileUsers></Users><Modules><MaxPCModules>10</MaxPCModules><MaxFacilitiesModules>5</MaxFacilitiesModules><MaxCateringModules>5</MaxCateringModules><MaxHousekeepingModules>5</MaxHousekeepingModules><MaxAPIModules>10</MaxAPIModules><Languages><MaxSpanish></MaxSpanish><MaxMandarin></MaxMandarin><MaxHindi></MaxHindi><MaxFrench></MaxFrench></Languages></Modules></Organizations></myVRMSiteLicense>

*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	GuestRoomLimit int NULL
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_GuestRoomLimit  DEFAULT 0 FOR GuestRoomLimit
GO
COMMIT



/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	GuestRoomPerUser int NULL
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_GuestRoomPerUser  DEFAULT 0 FOR GuestRoomPerUser
GO
COMMIT



/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Loc_Room_D ADD
	Extroom int NULL
GO
COMMIT

Update Loc_Room_D  set Extroom = 0


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	TopTier varchar(50) NULL,
	MiddleTier varchar(50) NULL,
	OnflyTopTierID int NULL,
	OnflyMiddleTierID int NULL
GO
COMMIT


Update Org_Settings_D set OnflyTopTierID = 0
Update Org_Settings_D set OnflyMiddleTierID = 0




/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Ept_list_D ADD
	Extendpoint int NULL
GO
COMMIT

Update Ept_list_D  set Extendpoint = 0



/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Room_D ADD
	Extroom int NULL
GO
COMMIT

Update Conf_Room_D  set Extroom = 0



/* ********************* FB 2426 - V2.8 (5thJune2012) ************** */
/* ***************** Only once need to run this script ************* */

DECLARE @orgID INT

DECLARE onflyTierCursor CURSOR for
SELECT orgID FROM Org_List_D 

OPEN onflyTierCursor

FETCH NEXT FROM onflyTierCursor INTO @orgID
WHILE @@FETCH_STATUS = 0
BEGIN

Declare @t3id as int, @t2id as int

IF EXISTS (SELECT top 1 Id FROM [Loc_Tier3_D] WHERE Id > 0 and orgId=@orgID)
	Begin
		Select top 1 @t3id = Id FROM [Loc_Tier3_D] WHERE Id > 0 and orgId=@orgID
	End
Else
	Begin
		set @t3id = (select isnull(MAX(Id),0)+ 1 from [Loc_Tier3_D] where orgId=@orgID)
		
		SET IDENTITY_INSERT [Loc_Tier3_D] ON
		
		INSERT INTO [Loc_Tier3_D] ([Id], [Name], [Address], [disabled], [orgId]) 
		VALUES (@t3id, N'Top Tier', N'', 0, @orgID)
		
		SET IDENTITY_INSERT [Loc_Tier3_D] OFF

	End
	
IF EXISTS (SELECT top 1 Id FROM [Loc_Tier2_D] WHERE Id > 0 and orgId=@orgID)
	Begin
		Select top 1 @t2id = Id FROM [Loc_Tier2_D] WHERE Id > 0 and orgId=@orgID
	End
Else
	Begin
		set @t2id = (select isnull(MAX(Id),0)+ 1 from [Loc_Tier2_D] where orgId=@orgID)
		
		SET IDENTITY_INSERT [Loc_Tier2_D] ON

		INSERT INTO [Loc_Tier2_D] ([Id], [Name], [Address], [L3LocationId], [Comment], [disabled], [orgId])
		VALUES (@t2id, N'Middle Tier', NULL, @t3id, N'Tier2-Fly', 0, @orgID)
		
		SET IDENTITY_INSERT [Loc_Tier2_D] OFF
	End

	update Org_Settings_D set OnflyTopTierID = @t3id, OnflyMiddleTierID = @t2id , 
	TopTier= (select Name from Loc_Tier3_D where Id = @t3id and orgId= @orgID), 
	MiddleTier = (select Name from Loc_Tier2_D where Id = @t2id and orgId= @orgID)
	where OrgId = @orgID

FETCH NEXT
FROM onflyTierCursor INTO @orgID
END
CLOSE onflyTierCursor
DEALLOCATE onflyTierCursor

/********* * FB 2426 End V2.8 (5thJune2012) * *********/


/********* * FB 2363 Start V2.8 (6thJune2012) * *********/


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ES_MailUsrRptSettings_d ADD
	orgId int NULL,
	Type nvarchar(50) NULL
GO
COMMIT



BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Sys_ESSettings_D ADD
	TimeoutValue int NULL
GO
COMMIT


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ES_Event_D ADD
	orgId int NULL	
GO
COMMIT



/********* * FB 2363 End V2.8 (6thJune2012)* *********/

/********* * FB 2469/2470 Starts V2.8 (15thJune2012)* *********/


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnableConfTZinLoc int NULL,
	SendConfirmationEmail int NULL
GO
COMMIT

Update Org_Settings_D Set EnableConfTZinLoc = 0, SendConfirmationEmail = 0 

/********* * FB 2469/2470 Ends V2.8 (15thJune2012)* *********/

/* *************************** FB 2448 - start *************************** */
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Mcu_list_D ADD
	CurrentDateTime datetime NULL
GO
COMMIT

update Mcu_list_D set CurrentDateTime  = ''

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE Loc_Room_D ADD
	IsVMR smallint not null default 0,
	InternalNumber nvarchar(50) NULL,
	ExternalNumber nvarchar(50) NULL
GO
COMMIT


Update Loc_Room_D set IsVMR = 0,InternalNumber='',ExternalNumber=''


/* *************************** FB 2448 - end *************************** */

/********** FB 2481(V2.8) 12thJuly2012- Starts ***********************/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	PrivateVMR nvarchar(1000) NULL
GO
COMMIT

Update Usr_List_D set PrivateVMR = ''



/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
	PrivateVMR nvarchar(1000) NULL
GO
COMMIT

Update Usr_Inactive_D set PrivateVMR = ''

/********** FB 2481(V2.8) - End ***********************/



/********** FB 2472(V2.8) - Start ***********************/


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	MCUAlert smallint  NULL
GO
COMMIT


Update Org_Settings_D set MCUAlert = 3



/********** FB 2472(V2.8) - End ***********************/


/* ************************ FB 2486 Starts (09 Aug 2012)************************  */

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Message_D](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	msgId int NULL,
	Languageid int NULL,
	[Message] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OrgId] [int] NULL,
	[Type] [int] NULL CONSTRAINT [DF_Message_D_Type]  DEFAULT ((0))
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF



--New Node Added
--<MaxStandardMCUs>25</MaxStandardMCUs><MaxEnhancedMCUs>25</MaxEnhancedMCUs>
--<myVRMSiteLicense><IR>1-myVRMQA</IR><Site><ExpirationDate>1/31/2014</ExpirationDate><MaxOrganizations>1</MaxOrganizations><IsLDAP>1</IsLDAP><ServerActivation><ProcessorIDs><ID></ID><ID></ID></ProcessorIDs><MACAddresses><ID></ID><ID></ID></MACAddresses></ServerActivation></Site><Organizations><Cloud>0</Cloud><Rooms><MaxNonVideoRooms>150</MaxNonVideoRooms><MaxVideoRooms>400</MaxVideoRooms><MaxGuestRooms>10</MaxGuestRooms></Rooms><Hardware><MaxStandardMCUs>25</MaxStandardMCUs><MaxEnhancedMCUs>25</MaxEnhancedMCUs><MaxEndpoints>400</MaxEndpoints><MaxCDRs></MaxCDRs></Hardware><Users><MaxTotalUsers>400</MaxTotalUsers><MaxGuestsPerUser>10</MaxGuestsPerUser><MaxExchangeUsers>100</MaxExchangeUsers><MaxDominoUsers>100</MaxDominoUsers><MaxMobileUsers>5</MaxMobileUsers></Users><Modules><MaxPCModules>10</MaxPCModules><MaxFacilitiesModules>5</MaxFacilitiesModules><MaxCateringModules>5</MaxCateringModules><MaxHousekeepingModules>5</MaxHousekeepingModules><MaxAPIModules>1</MaxAPIModules><Languages><MaxSpanish></MaxSpanish><MaxMandarin></MaxMandarin><MaxHindi></MaxHindi><MaxFrench></MaxFrench></Languages></Modules></Organizations></myVRMSiteLicense>
--KmzsDo+0yAjTApoYqR4/Eg7EKo0CAB7gYbQFeyfWKO696d+8NpwrdevCY9iQxycR9MD6E5NnbYlIx+XFPh3dt4l+OP7T+xoaLaCkUelq3k/fUi3k2Cvau1GrbeZCvvB1SmDplgvH5hnvKdjwEcmDQNW/kKDh+IR4oeHNWhhES0yrz4XC9mbbug/W82pCKfs7v3Xp+2mmbTnb2dyJ9w0RmFXnTuC3YLjLFs9mQLxOXxRryDptT3cSMUCgEBmuk6M1WC3meFeT/85Y1A6X0AN0lLfEc1k6+EbsOlkqvs8vQ1GoOH/PLyVCCqyMn6uv7/NEOlaByl++d/6q5l9gVGn8SKHSt4VqWHnSLBWkgj/e/E4jsjWGcQSqjeRpSjo5dYrN9nl5kw0NCJ0ZFialw2KiemJW19WUr+kFnJmBMoeFE2HVOTMc3dx3AT1VqaXYr1cQ3Uhbw+B/bVrVAKlgKNuWf+DIdqG3gt2REJH0IwPo8hE4S/P9Wp8bE4rmYfe7I9TeahSWm3Hd/JPvhh06tNHld6sYeCHCBekJ8q+Tyk8i0D1oRUVLGXOIV2BXQZ9WCozA+DEGdgC3yc/dDGqB0taVu7v0I420+22X8kEzURW5/iSrHcYGFoYIerIPROA8wbhOgr+cLExpyzziY5J8SssGWXg0VzY29qu8X2hb4+01jSGy0ZQX1YpUlCHgKO1FZYVJI15ow+ktNfKUNYGLiTOoTncRM4O7OdANy0HJnuUlRqx8t9+m3SPkBQSpfk3gc523CDtZdTf2oLiPKUj5V3SrXdQ1xJ6yX7sjQUu++Q6gPrS+e3GPqSEg+veT6AHutodfofhC2XmJ67r0adstoytcHnqGkBJJOWXekVMcPgcwPTy86xT3hcH7YyGSXsu6aNfR+U7FEH5zLwXXdJmQ4PxL74EapLDbvvhGkNP7VTgGzcIE4ZYPYVEnDbv+F9YWIp3ncQnc6hrVy0YPLbDoP+QZy7ct1PaFfBWqi5MBEFieLlYY2Nr8ZXRYplJd0VgEQukXVxwC9H+CzD/lZPALtjmhjkZj4qVUnmsqlKol4szVX+p3vvuK0SArsTYPqseVUNpIL+X5siGpvXJ7woLZcW96mqIr1HSYc2QqKPchrFZ1WFZxBNL9QzV2DT0x/zZiQJPDFQqApodciFyMCmNsY/XVLZgt+ALC8bSZqgh93N/e4xiKsLD4hTa9l4us33EQhC78K4T4vHxHTTimX5DluowlOnF94nUE3QnHHd2rFX93iDMZQXbReWGlyGIQJuaDDXuI+WbgjgZVG6IxVpa0wle/PNZ7OzPhq3azkOdprp17vE6B3pd1arZ+5gYmaoK58DAD347aZ7RWNd/Jpz3I0qksIWCQ+1R94x3ukMsBEPqOq9i9xzmg8FDev76Px2d0rdYaeM6akTsOorpr2Evmqn4dayxAzqFGn91CZFp6EFcnc6/8TtR62oAAGygLBPs0BuqX/RHKyp3lfNwzBZ5eqsHCuAF2TayF8RTnRFbr2w3ZxHNi4BPeO6ZOxyRK5+d7XFHOyfPZIPnIYAhPf9V9gaFAAfjAgHP/xCfh397DlElg6+XBx05av32rpJ86k0dOksHzaqDbdDZTDzSdDvzyhAODmA==


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	MCUEnchancedLimit int NULL,
	DefaultTxtMsg nvarchar(MAX)  NULL
	
COMMIT

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Mcu_List_D ADD
       EnableMessage int NULL        
GO

COMMIT

update Mcu_List_D set EnableMessage = 0
Update Org_Settings_D set DefaultTxtMsg = '1#1#5M;2#2#2M;3#3#30s;'


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Conf_Message_D
	(
	[uID] int NOT NULL IDENTITY (1, 1),
	orgid int NOT NULL,
	confid int NOT NULL,
	instanceid int NOT NULL,
	Languageid int NULL,
	confMessage nvarchar(MAX) NOT NULL,
	duration int NOT NULL,
	controlID int NOT NULL,
	durationID nvarchar(10)  NULL
	)  ON [PRIMARY]
GO
COMMIT



DECLARE @orgID INT

DECLARE msgCursor CURSOR for
SELECT orgID FROM Org_List_D 

OPEN msgCursor

FETCH NEXT FROM msgCursor INTO @orgID
WHILE @@FETCH_STATUS = 0
BEGIN

insert into Message_D (Message,orgid,Type,Languageid,msgId) values ('The conference will end in 5 minutes',@orgID,1,1,1)
insert into Message_D (Message,orgid,Type,Languageid,msgId) values ('The conference will end in 2 minutes',@orgID,1,1,2)
insert into Message_D (Message,orgid,Type,Languageid,msgId) values ('The conference will end in 30 seconds',@orgID,1,1,3)

FETCH NEXT
FROM msgCursor INTO @orgID
END
CLOSE msgCursor
DEALLOCATE msgCursor



/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_User_D ADD
	isTextMsg int NOT  NULL default 0
GO
COMMIT

Update Conf_User_D  set isTextMsg = 0


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Room_D ADD
	isTextMsg int NULL default 0
GO
COMMIT

Update Conf_Room_D set isTextMsg = 0



/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	isTextMsg int NULL default 0
GO
COMMIT

Update Conf_Conference_D set isTextMsg = 0


/* ************************ FB 2486 Ends (09 Aug 2012) ************************  */


/* ************************FB 2498  ************************  */

ALTER TABLE Sys_TechSupport_D ALTER COLUMN phone NVARCHAR(250)

/*  **************** FB 2506 Starts (2012-08-29) ****************** */
DECLARE @orgID INT

DECLARE msgCursor CURSOR for
SELECT orgID FROM Org_List_D 
 
OPEN msgCursor

FETCH NEXT FROM msgCursor INTO @orgID
WHILE @@FETCH_STATUS = 0
BEGIN

IF NOT EXISTS (Select * from Message_D where orgID = @orgID)
Begin
	Insert into Message_D (Message,orgid,Type,Languageid,msgId) values ('The conference will end in 5 minutes',@orgID,1,1,1)
	Insert into Message_D (Message,orgid,Type,Languageid,msgId) values ('The conference will end in 2 minutes',@orgID,1,1,2)
	Insert into Message_D (Message,orgid,Type,Languageid,msgId) values ('The conference will end in 30 seconds',@orgID,1,1,3)
End

IF EXISTS (Select * from Org_Settings_D where orgID = @orgID and DefaultTxtMsg is null)
Begin
	Update Org_Settings_D set DefaultTxtMsg = '1#1#5M;2#2#2M;3#3#30s;' where orgID = @orgID
End

FETCH NEXT
FROM msgCursor INTO @orgID
END
CLOSE msgCursor
DEALLOCATE msgCursor

/*  **************** FB 2506 Ends (2012-08-29) ****************** */

/* *************************** FB 2430 - start *************************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
 EnableSmartP2P smallint 
GO
COMMIT 

update Org_Settings_D set EnableSmartP2P = 0

/* *************************** FB 2430 - end *************************** */

-- ********************************* FB 2501 StartMode - Starts - 13th Sep 2012 ******************************************************

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.sys_settings_d ADD
 startmode int 
GO
COMMIT


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.conf_conference_d ADD startmode int
GO
COMMIT

-- To update existing Conference Startmode to Automatic
update conf_conference_d set startmode=0 
-- ********************************* FB 2501 StartMode - Ends - 13th Sep 2012******************************************************//



/* ************************* FB 2451  VNOC Starts - 26Sep2012************************* */

update [Dept_CustomAttr_D] set Deleted = 1,IncludeInCalendar=0,IncludeInEmail=0,RoomApp=0,McuApp=0,SystemApp=0,Host=0,Party=0,McuAdmin=0,RoomAdmin=0,Mandatory=0 where DisplayTitle='Dedicated VNOC Operator'

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_AdvAVParams_D ADD
	confVNOC smallint NULL
GO
COMMIT


Update Conf_AdvAVParams_D set confVNOC = 0


/* ************************* FB 2451 VNOC Ends - 26Sep2012 ************************* */

/* ************************* FB 2501 Starts - 27Sep2012************************* */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
 DefaultConfDuration smallint 
GO
COMMIT 

update Org_Settings_D set DefaultConfDuration = 60

/* ************************* FB 2501 Ends - 27Sep2012 ************************* */
--***************************** FB 2501_ Overwrite of Meeting Requestor name(27th Sep 2012)**************************--
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
Alter table dbo.conf_conference_D add loginUser int
GO
COMMIT

--***************************** FB 2501_ Overwrite of Meeting Requestor name(27th Sep 2012)**************************--


--***************************** FB 2501 FECC (27th Sep 2012)**************************--

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_AdvAVParams_D ADD
	feccMode smallint NULL
GO
ALTER TABLE dbo.Conf_AdvAVParams_D ADD CONSTRAINT
	DF_Conf_AdvAVParams_D_feccMode DEFAULT 0 FOR feccMode
GO
COMMIT

--***************************** FB 2501 FECC (27th Sep 2012)**************************--
/* ************************* FB 2501 -VNOC Changes Starts - 08Oct2012 ************************* */


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	confVNOC smallint NULL
GO
COMMIT


Update Conf_Conference_D set confVNOC = 0

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Dept_CustomAttr_D ADD
	VNOCOperator smallint NULL
GO
COMMIT


Update Dept_CustomAttr_D set VNOCOperator = 0

update [Dept_CustomAttr_D] set Deleted = 0,IncludeInCalendar=1,IncludeInEmail=0,RoomApp=0,McuApp=0,SystemApp=0,Host=0,Party=0,McuAdmin=0,RoomAdmin=0,Mandatory=0 where DisplayTitle='Dedicated VNOC Operator'


/* ************************* FB 2501  - VNOC Changes Ends - 08Oct2012 ************************* */

/* ************************* FB 2536  - Starts - 09Oct2012 ************************* */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ES_Event_D
	DROP CONSTRAINT DF_ES_Event_D_EventTime
GO
CREATE TABLE dbo.Tmp_ES_Event_D
	(
	UId int NOT NULL IDENTITY (1, 1),
	RequestID nvarchar(50) NULL,
	ConfNumName nvarchar(50) NULL,
	TaskType nvarchar(50) NULL,
	TypeID nvarchar(50) NULL,
	CustomerID nvarchar(50) NULL,
	EventTrigger nvarchar(50) NULL,
	EventTime datetime NULL,
	Status nvarchar(50) NULL,
	StatusDate datetime NULL,
	StatusMessage nvarchar(MAX) NULL,
	RequestCall nvarchar(MAX) NULL,
	ResponseCall nvarchar(MAX) NULL,
	RetryCount int NULL,
	orgId int NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_ES_Event_D SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_ES_Event_D ADD CONSTRAINT
	DF_ES_Event_D_EventTime DEFAULT (getdate()) FOR EventTime
GO
SET IDENTITY_INSERT dbo.Tmp_ES_Event_D ON
GO
IF EXISTS(SELECT * FROM dbo.ES_Event_D)
	 EXEC('INSERT INTO dbo.Tmp_ES_Event_D (UId, RequestID, ConfNumName, TaskType, TypeID, CustomerID, EventTrigger, EventTime, Status, StatusDate, StatusMessage, RequestCall, ResponseCall, RetryCount, orgId)
		SELECT UId, RequestID, ConfNumName, TaskType, TypeID, CustomerID, EventTrigger, EventTime, Status, StatusDate, StatusMessage, RequestCall, CONVERT(nvarchar(50), ResponseCall), RetryCount, orgId FROM dbo.ES_Event_D WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_ES_Event_D OFF
GO
DROP TABLE dbo.ES_Event_D
GO
EXECUTE sp_rename N'dbo.Tmp_ES_Event_D', N'ES_Event_D', 'OBJECT' 
GO
COMMIT


/* ************************* FB 2536  - Ends - 09Oct2012   ************************* */

/* ************************* FB 2501  - Call Monitoring Starts - 21Nov2012   ************************* */


INSERT INTO [dbo].[Mcu_Vendor_S] ([id], [name], [BridgeInterfaceId], [videoparticipants], [audioparticipants])
 VALUES (12, N'Cisco MSE 8710', 8, 50, 50)


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	GUID nvarchar(MAX) NULL
GO

COMMIT

update Conf_Conference_D set GUID = ''


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Cascade_D ADD
	GUID nvarchar(MAX) NULL
GO
COMMIT

update Conf_Cascade_D set GUID = ''


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.MCU_Params_D
(
UID int NOT NULL IDENTITY (1, 1),
BridgeName nvarchar(256) NULL,
BridgeTypeid int NULL,
ConfLockUnLock int NOT NULL DEFAULT 0,
confMessage int NOT NULL DEFAULT 0,
confAudioTx int NOT NULL DEFAULT 0,
confAudioRx int NOT NULL DEFAULT 0,
confVideoTx int NOT NULL DEFAULT 0,
confVideoRx int NOT NULL DEFAULT 0,
confLayout int NOT NULL DEFAULT 0,
confCamera int NOT NULL DEFAULT 0,
confPacketloss int NOT NULL DEFAULT 0,
confRecord int NOT NULL DEFAULT 0,
partyBandwidth int NOT NULL DEFAULT 0,
partySetfocus int NOT NULL DEFAULT 0,
partyMessage int NOT NULL DEFAULT 0,
partyAudioTx int NOT NULL DEFAULT 0,
partyAudioRx int NOT NULL DEFAULT 0,
partyVideoTx int NOT NULL DEFAULT 0,
partyVideoRx int NOT NULL DEFAULT 0,
partyLayout int NOT NULL DEFAULT 0,
partyCamera int NOT NULL DEFAULT 0,
partyPacketloss int NOT NULL DEFAULT 0,
partyImagestream int NOT NULL DEFAULT 0,
partyLockUnLock int NOT NULL DEFAULT 0,
partyRecord int NOT NULL DEFAULT 0,
partyLecturemode int NOT NULL DEFAULT 0
) ON [PRIMARY]
GO
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_AdvAVParams_D ADD
	Layout int NULL,
	MuteRxaudio int NULL,
	MuteTxaudio int NULL,
	MuteRxvideo int NULL,
	MuteTxvideo int NULL,
	ConfLockUnlock int NULL,
	Recording int NULL,
	Camera int NULL,
	PacketLoss nvarchar(MAX) NULL
GO

COMMIT

update Conf_AdvAVParams_D set ConfLockUnlock =0,MuteRxaudio=0,MuteRxvideo=0,MuteTxvideo=0,Recording =0,Layout =0,PacketLoss ='',Camera=0


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_User_D ADD
	GUID nvarchar(MAX) NULL,
	Setfocus int NULL,
	Message int NULL,
	MuteRxaudio int NULL,
	MuteRxvideo int NULL,
	MuteTxvideo int NULL,
	Camera int NULL,
	Packetloss int NULL,
	LockUnLock int NULL,
	Record int NULL,
	Stream nvarchar(max) NULL
GO

COMMIT

update Conf_User_D set GUID = '',Setfocus=0,Message=0,MuteRxaudio=0,MuteRxvideo=0,MuteTxvideo=0,Camera=0,Packetloss=0,LockUnLock=0,Record=0


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Room_D ADD
	GUID nvarchar(MAX) NULL,
	Setfocus int NULL,
	Message int NULL,
	MuteRxaudio int NULL,
	MuteRxvideo int NULL,
	MuteTxvideo int NULL,
	Camera int NULL,
	Packetloss int NULL,
	LockUnLock int NULL,
	Record int NULL,
	Stream nvarchar(max) NULL
GO

COMMIT

update Conf_Room_D set GUID = '',Setfocus=0,Message=0,MuteRxaudio=0,MuteRxvideo=0,MuteTxvideo=0,Camera=0,Packetloss=0,LockUnLock=0,Record=0

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Mcu_List_D ADD
	setFavourite smallint NOT NULL CONSTRAINT DF_Mcu_List_D_setFavourite DEFAULT 0
GO
COMMIT



/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
--Conf_Room_D
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Room_D ADD
	RxAudioPacketsReceived nvarchar(50) NULL,
	RxAudioPacketErrors nvarchar(50) NULL,
	RxAudioPacketsMissing nvarchar(50) NULL,
	RxVideoPacketsReceived nvarchar(50) NULL,
	RxVideoPacketErrors nvarchar(50) NULL,
	RxVideoPacketsMissing nvarchar(50) NULL
GO
COMMIT

Update Conf_Room_D set RxAudioPacketsReceived = 0, RxAudioPacketErrors = 0, RxAudioPacketsMissing = 0, 
RxVideoPacketsReceived = 0, RxVideoPacketErrors = 0, RxVideoPacketsMissing = 0


--Conf_User_D
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_User_D ADD
	RxAudioPacketsReceived nvarchar(50) NULL,
	RxAudioPacketErrors nvarchar(50) NULL,
	RxAudioPacketsMissing nvarchar(50) NULL,
	RxVideoPacketsReceived nvarchar(50) NULL,
	RxVideoPacketErrors nvarchar(50) NULL,
	RxVideoPacketsMissing nvarchar(50) NULL
GO
COMMIT

update Conf_User_D set RxAudioPacketsReceived = 0, RxAudioPacketErrors = 0,
RxAudioPacketsMissing = 0, RxVideoPacketsReceived = 0, RxVideoPacketErrors = 0, RxVideoPacketsMissing = 0


--Conf_Cascade_D
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Cascade_D ADD
	RxAudioPacketsReceived nvarchar(50) NULL,
	RxAudioPacketErrors nvarchar(50) NULL,
	RxAudioPacketsMissing nvarchar(50) NULL,
	RxVideoPacketsReceived nvarchar(50) NULL,
	RxVideoPacketErrors nvarchar(50) NULL,
	RxVideoPacketsMissing nvarchar(50) NULL
GO
COMMIT

update Conf_Cascade_D set RxAudioPacketsReceived = 0, RxAudioPacketErrors = 0, RxAudioPacketsMissing = 0, 
RxVideoPacketsReceived = 0, RxVideoPacketErrors = 0, RxVideoPacketsMissing = 0


DBCC CHECKIDENT ('icons_ref_s', reseed, 44)
insert into icons_ref_s values('../en/img/CallMonitor.png','Call Monitor','MonitorMCU.aspx')


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_User_D ADD
       TerminalType smallint NULL
GO
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Room_D ADD
       TerminalType smallint NULL
GO
COMMIT



/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Cascade_D ADD
       TerminalType smallint NULL
GO
COMMIT


update conf_user_d
set terminaltype=1
where userid in (select userid from usr_list_D)

update conf_user_d
set terminaltype=3
where userid in (select userid from usr_guestlist_d)



Insert into mcu_params_d (BridgeName,BridgeTypeid,ConfLockUnLock,confMessage,confAudioTx,confAudioRx,confVideoTx,confVideoRx,
confLayout,confCamera,confPacketloss,confRecord,partyBandwidth,partySetfocus,partyMessage,partyAudioTx,partyAudioRx,partyVideoTx,
partyVideoRx ,partyLayout ,partyCamera,partyPacketloss,partyImagestream,partyLockUnLock,partyRecord,partyLecturemode) values 
('Polycom MGC 25',1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)

Insert into mcu_params_d (BridgeName,BridgeTypeid,ConfLockUnLock,confMessage,confAudioTx,confAudioRx,confVideoTx,confVideoRx,
confLayout,confCamera,confPacketloss,confRecord,partyBandwidth,partySetfocus,partyMessage,partyAudioTx,partyAudioRx,partyVideoTx,
partyVideoRx ,partyLayout ,partyCamera,partyPacketloss,partyImagestream,partyLockUnLock,partyRecord,partyLecturemode) values 
('Polycom MGC 50',2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)

Insert into mcu_params_d (BridgeName,BridgeTypeid,ConfLockUnLock,confMessage,confAudioTx,confAudioRx,confVideoTx,confVideoRx,
confLayout,confCamera,confPacketloss,confRecord,partyBandwidth,partySetfocus,partyMessage,partyAudioTx,partyAudioRx,partyVideoTx,
partyVideoRx ,partyLayout ,partyCamera,partyPacketloss,partyImagestream,partyLockUnLock,partyRecord,partyLecturemode) values 
('Polycom MGC 100',3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)

Insert into mcu_params_d (BridgeName,BridgeTypeid,ConfLockUnLock,confMessage,confAudioTx,confAudioRx,confVideoTx,confVideoRx,
confLayout,confCamera,confPacketloss,confRecord,partyBandwidth,partySetfocus,partyMessage,partyAudioTx,partyAudioRx,partyVideoTx,
partyVideoRx ,partyLayout ,partyCamera,partyPacketloss,partyImagestream,partyLockUnLock,partyRecord,partyLecturemode) values 
('Codian MCU 4200',4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)


Insert into mcu_params_d (BridgeName,BridgeTypeid,ConfLockUnLock,confMessage,confAudioTx,confAudioRx,confVideoTx,confVideoRx,
confLayout,confCamera,confPacketloss,confRecord,partyBandwidth,partySetfocus,partyMessage,partyAudioTx,partyAudioRx,partyVideoTx,
partyVideoRx ,partyLayout ,partyCamera,partyPacketloss,partyImagestream,partyLockUnLock,partyRecord,partyLecturemode) values 
('Codian MCU 4500',5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)


Insert into mcu_params_d (BridgeName,BridgeTypeid,ConfLockUnLock,confMessage,confAudioTx,confAudioRx,confVideoTx,confVideoRx,
confLayout,confCamera,confPacketloss,confRecord,partyBandwidth,partySetfocus,partyMessage,partyAudioTx,partyAudioRx,partyVideoTx,
partyVideoRx ,partyLayout ,partyCamera,partyPacketloss,partyImagestream,partyLockUnLock,partyRecord,partyLecturemode) values 
('Codian  MSE 8000 Series',6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)


Insert into mcu_params_d (BridgeName,BridgeTypeid,ConfLockUnLock,confMessage,confAudioTx,confAudioRx,confVideoTx,confVideoRx,
confLayout,confCamera,confPacketloss,confRecord,partyBandwidth,partySetfocus,partyMessage,partyAudioTx,partyAudioRx,partyVideoTx,
partyVideoRx ,partyLayout ,partyCamera,partyPacketloss,partyImagestream,partyLockUnLock,partyRecord,partyLecturemode) values 
('Tandberg MPS 800 Series',7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)


Insert into mcu_params_d (BridgeName,BridgeTypeid,ConfLockUnLock,confMessage,confAudioTx,confAudioRx,confVideoTx,confVideoRx,
confLayout,confCamera,confPacketloss,confRecord,partyBandwidth,partySetfocus,partyMessage,partyAudioTx,partyAudioRx,partyVideoTx,
partyVideoRx ,partyLayout ,partyCamera,partyPacketloss,partyImagestream,partyLockUnLock,partyRecord,partyLecturemode) values 
('Polycom RMX 2000',8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)


Insert into mcu_params_d (BridgeName,BridgeTypeid,ConfLockUnLock,confMessage,confAudioTx,confAudioRx,confVideoTx,confVideoRx,
confLayout,confCamera,confPacketloss,confRecord,partyBandwidth,partySetfocus,partyMessage,partyAudioTx,partyAudioRx,partyVideoTx,
partyVideoRx ,partyLayout ,partyCamera,partyPacketloss,partyImagestream,partyLockUnLock,partyRecord,partyLecturemode) values 
('Radvision Scopia',9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)

Insert into mcu_params_d (BridgeName,BridgeTypeid,ConfLockUnLock,confMessage,confAudioTx,confAudioRx,confVideoTx,confVideoRx,
confLayout,confCamera,confPacketloss,confRecord,partyBandwidth,partySetfocus,partyMessage,partyAudioTx,partyAudioRx,partyVideoTx,
partyVideoRx ,partyLayout ,partyCamera,partyPacketloss,partyImagestream,partyLockUnLock,partyRecord,partyLecturemode) values 
('Life Size',11,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)


Insert into mcu_params_d (BridgeName,BridgeTypeid,ConfLockUnLock,confMessage,confAudioTx,confAudioRx,confVideoTx,confVideoRx,
confLayout,confCamera,confPacketloss,confRecord,partyBandwidth,partySetfocus,partyMessage,partyAudioTx,partyAudioRx,partyVideoTx,
partyVideoRx ,partyLayout ,partyCamera,partyPacketloss,partyImagestream,partyLockUnLock,partyRecord,partyLecturemode) values 
('Cisco MSE 8710',12,1,1,1,1,1,1,0,0,0,0,0,0,1,1,1,1,1,0,0,1,1,1,0,0)


/* ************************* FB 2501  - Call Monitoring Ends - 21Nov2012   ************************* */


/* ************************* FB 2501  - P2P Call Monitoring Starts - 21Nov2012   ************************* */


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Conf_P2PAlert_D
	(
	UID int NOT NULL IDENTITY (1, 1),
	ConfID int NULL,
	InstanceID int NULL,
	AlertTypeID int NULL,
	TimeStamp datetime NULL,
	Message nvarchar(MAX) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Conf_P2PAlert_D ADD CONSTRAINT
	PK_Table_1 PRIMARY KEY CLUSTERED 
	(
	UID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

COMMIT

insert into icons_ref_s values('../en/img/p2p.jpg','Point To Point','point2point.aspx')


/* ************************* FB 2501  - P2P Call Monitoring Ends - 21Nov2012   ************************* */


/* ************************* FB 2501  - CDR Starts - 21Nov2012   ************************* */


/* FB 2501 - Cisco CDR  */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.CDR_Cisco_D
	(
	UID int NOT NULL IDENTITY (1, 1),
	ConferenceGUID nvarchar(MAX) NULL,
	ParticipantGUID nvarchar(MAX) NULL,
	EventType nvarchar(50) NULL,
	BridgeId int NULL,
	BridgeType int NULL,
	CDRIndex int NULL,
	[time] nvarchar(50) NULL, 
	ConfName nvarchar(50) NULL,
	NumericId int NULL,
	CallID nvarchar(50) NULL,
	Uri nvarchar(50) NULL,
	PinProtected nvarchar(50) NULL,
	CallDirection nvarchar(50) NULL,
	CallProtocol nvarchar(50) NULL,
	EndpointIPAddress nvarchar(50) NULL,
	EndpointDisplayName nvarchar(50) NULL,
	EndpointURI nvarchar(50) NULL,
	EndpointConfiguredName nvarchar(50) NULL,
	TimeInConference nvarchar(50) NULL,
	DisconnectReason nvarchar(50) NULL,
	MaxSimultaneousAudioVideoParticipants nvarchar(50) NULL,
	MaxSimultaneousAudioOnlyParticipants nvarchar(50) NULL,
	TotalAudioVideoParticipants int NULL,
	TotalAudioOnlyParticipants int NULL,
	SessionDuration int NULL,
	Name nvarchar(50) NULL,
	ActiveTime nvarchar(50) NULL,
	EncryptedTime nvarchar(50) NULL,
	Width int NULL,
	Height int NULL,
	MaxBandwidth int NULL,
	Bandwidth int NULL,
	PacketsReceived int NULL,
	PacketsLost int NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.CDR_Cisco_D ADD CONSTRAINT
	PK_CDR_Cisco_D PRIMARY KEY CLUSTERED 
	(
	UID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT



/* FB 2501 - Report CDR  */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Conf_CDR_logs_D
	(
	UID int NOT NULL IDENTITY (1, 1),
	ConfId int NULL,
	InstanceId int NULL,
	OrgId int NULL,
	Title nvarchar(256) NULL,
	ConfNumname int NULL,
	EventId int NULL,
	EventType nvarchar(50) NULL,
	EventDate datetime NULL,
	EventDescription nvarchar(2000) NULL,
	McuId int NULL,
	McuName nvarchar(256) NULL,
	McuTypeid int NULL,
	McuType nchar(256) NULL,
	McuAddress nvarchar(256) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Conf_CDR_logs_D ADD CONSTRAINT
	PK_Conf_CDR_logs_D PRIMARY KEY CLUSTERED 
	(
	UID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT


/* ************************* FB 2501  - CDR Ends - 21Nov2012   ************************* */

/* ************************* FB 2501  -EM7 Starts - 21Nov2012   ************************* */
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.sys_settings_d ADD
	EM7URI nvarchar(Max) NULL,
	EM7Username nvarchar(Max) NULL,
	EM7Password nvarchar(Max) NULL,
	EM7Port int NULL
GO
COMMIT

update Sys_Settings_D set EM7Port=80
--****************************************************************************--


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[org_EM7Silo_D](
	[UID] [int] IDENTITY(1,1) NOT NULL,
	[OrgId] [int] NULL,
	[EM7Orgname] [nvarchar](max) NULL,
	[EM7OrgEmail] [nvarchar](max) NULL,
	[EM7UserName] [nvarchar](max) NULL,
	[EM7Password] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

--****************************************************************************--
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.org_settings_d ADD
	EM7OrgID  int NULL
GO
COMMIT

update Org_Settings_D set em7orgid=-1
--****************************************************************************--
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ept_list_d ADD
	EptOnlineStatus  [int] NULL
	
GO
COMMIT

Update Ept_List_D Set EptOnlineStatus = 1

/* ************************* FB 2501  -EM7 Ends - 21Nov2012   ************************* */

/* ************************* FB 2552  -Retry Count Starts - 21Nov2012   ************************* */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Sys_MailServer_D ADD
	RetryCount int NULL
GO
ALTER TABLE dbo.Sys_MailServer_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

Update Sys_MailServer_D set RetryCount =30

/* ************************* FB 2552  -Retry Count Ends - 21Nov2012   ************************* 
*/

/* ************************* FB 2501  - Call Monitoring 10DEc2012 ************************* */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.mcu_list_d ADD
	portsVideoTotal int NULL,
	portsVideoFree int NULL,
	portsAudioTotal int NULL,
	portsAudioFree int NULL
GO
ALTER TABLE dbo.mcu_list_d SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

Update mcu_list_d set portsVideoTotal = 0
Update mcu_list_d set portsVideoFree = 0
Update mcu_list_d set portsAudioTotal = 0
Update mcu_list_d set portsAudioFree = 0

/* ************************* FB 2501  - Call Monitoring 10Dec2012 ************************* */


/* ********* FB 2535 *********** */

Update Launguage_Translation_Text Set TranslatedText = Replace(TranslatedText,'''','')  
Where TranslatedText like '%''%'


/* ********* FB 2550 - Starts *********** */


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_User_D ADD
	PublicVMRParty smallint NOT NULL CONSTRAINT DF_Conf_User_D_PublicVMRParty DEFAULT 0
GO
ALTER TABLE dbo.Conf_User_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

Update  Conf_User_D  set  PublicVMRParty = 0



BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	MaxPublicVMRParty smallint NULL
GO
ALTER TABLE dbo.Org_Settings_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


Update Org_Settings_D  Set  MaxPublicVMRParty = 0


/* ********* FB 2550 - End *********** */



/* ************************* FB 2566  - Call Monitoring 10DEc2012 ************************* */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Bridge_D ADD
	uId int NOT NULL IDENTITY (1, 1),
	confuId int NULL,
	BridgeName nvarchar(250) NULL,
	BridgeTypeid int NULL,
	BridgeIPISDNAddress nvarchar(250) NULL,
	TotalPortsUsed int NULL
GO
ALTER TABLE dbo.Conf_Bridge_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

Update Conf_Bridge_D set confuId = 0
Update Conf_Bridge_D set BridgeName = ''
Update Conf_Bridge_D set BridgeTypeid = 0
Update Conf_Bridge_D set BridgeIPISDNAddress = ''
Update Conf_Bridge_D set TotalPortsUsed = 0

/* ************************* FB 2566  - Call Monitoring 10Dec2012 ************************* */




/* ************************* FB 2555  - Email Date Format 17 Dec2012 Starts ************************* */


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EmailDateFormat smallint NULL
GO
ALTER TABLE dbo.Org_Settings_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


Update Org_Settings_D set EmailDateFormat = 0

/* ************************* FB 2555  - Email Date Format 17 Dec2012  Ends ************************* */





/************** * FB 2272(Spanish) Starts 18 Dec 2012* **********************/
delete from [Gen_Language_S] where [LanguageID] = 3

INSERT INTO [dbo].[Gen_Language_S] ([LanguageID], [Name], [Country],[languagefolder]) VALUES (3, N'Spanish', N'US',N'sp')



DECLARE @CustomAttributeId INT, @DeptID INT, @OptionID INT, @OptionType INT, @OptionValue nvarchar(4000),
@Caption nvarchar(50),@HelpText nvarchar(4000),@LanguageId INT, @countlan INT 

DECLARE cusAttrCursor CURSOR for
Select b.[CustomAttributeId], b.[DeptID],b.[OptionID],b.[OptionType],b.[OptionValue] ,b.[Caption],  b.[HelpText] 
from Dept_CustomAttr_D a, Dept_CustomAttr_Option_D b
where a.[CustomAttributeId] = b.[CustomAttributeId] and b.[LanguageID] =1  

OPEN cusAttrCursor

FETCH NEXT FROM cusAttrCursor INTO @CustomAttributeId, @DeptID , @OptionID , @OptionType , @OptionValue ,
@Caption ,@HelpText 
WHILE @@FETCH_STATUS = 0
BEGIN
	set @LanguageId = 3

	set @countlan = (select count(*) from Dept_CustomAttr_D a, Dept_CustomAttr_Option_D b
	where a.[CustomAttributeId] = b.[CustomAttributeId] and b.[LanguageID] =@LanguageId and 
	b.[CustomAttributeId] = @CustomAttributeId and b.[OptionValue] = @OptionValue )

	if(@countlan <= 0)
	BEGIN

		INSERT INTO [Dept_CustomAttr_Option_D]
		([CustomAttributeId],[DeptID],[OptionID],[OptionType],[OptionValue],[Caption],[HelpText],[LanguageID])
		values (
		@CustomAttributeId , @DeptID , @OptionID , @OptionType , @OptionValue ,
		@Caption ,@HelpText ,@LanguageId 
		)

	END

	FETCH NEXT
	FROM cusAttrCursor INTO @CustomAttributeId,@DeptID , @OptionID , @OptionType , @OptionValue ,
	@Caption ,@HelpText
END
CLOSE cusAttrCursor
DEALLOCATE cusAttrCursor



/******* Deleting Duplicate Rows * *****************/
DECLARE @LanguageID2 INT


DECLARE cusAttrCursor CURSOR for
SELECT LanguageID FROM Gen_Language_S 

OPEN cusAttrCursor

FETCH NEXT FROM cusAttrCursor INTO @LanguageID2
WHILE @@FETCH_STATUS = 0
BEGIN

WITH CTE ([Text], DuplicateCount)
AS
(
SELECT Text,
ROW_NUMBER() OVER(PARTITION BY [Text] ORDER BY [Text]) AS DuplicateCount
FROM Launguage_Translation_Text where LanguageID = @LanguageID2
)
DELETE
FROM CTE
WHERE DuplicateCount > 1


FETCH NEXT
FROM cusAttrCursor INTO @LanguageID2
END
CLOSE cusAttrCursor
DEALLOCATE cusAttrCursor

/************** * FB 2272(Spanish) Ends 18 Dec 2012* **********************/


/* *************************** FB 2571 - start *************************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
 EnableFECC smallint ,
 DefaultFECC smallint
GO
COMMIT 

update Org_Settings_D set EnableFECC= 0
update Org_Settings_D set DefaultFECC= 0

/* *************************** FB 2571 - end *************************** */

/* *************************** FB 2331 -  24 Dec 2012 start *************************** */

-- Delete the Existing Reports

Delete User_Lobby_D where UserID in ( select UserID from Usr_List_D where roleID = 1 or roleID = 10) and IconID in (20,21)

-- General User Role to hide the Reports 

update Usr_Roles_D set roleMenuMask='8*240-4*11+8*0+4*0+2*0+8*0+2*0+2*0+2*0+1*0-6*14' where roleID=1

Update usr_list_d set MenuMask = '8*240-4*11+8*0+4*0+2*0+8*0+2*0+2*0+2*0+1*0-6*14' where roleID = 1 

Update usr_inactive_d set MenuMask = '8*240-4*11+8*0+4*0+2*0+8*0+2*0+2*0+2*0+1*0-6*14' where roleID = 1 

-- View Only role

update Usr_Roles_D set roleMenuMask='8*208-4*11+8*0+4*0+2*0+8*0+2*0+2*0+2*0+1*0-6*14' where roleID=10

Update usr_list_d set MenuMask = '8*208-4*11+8*0+4*0+2*0+8*0+2*0+2*0+2*0+1*0-6*14' where roleID = 10

Update usr_inactive_d set MenuMask = '8*208-4*11+8*0+4*0+2*0+8*0+2*0+2*0+2*0+1*0-6*14' where roleID = 10

/* *************************** FB 2331 - 24 Dec 2012 end *************************** */



---*******************************FB 2575 Dec 24 2012 starts *************************--
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_em7orgid DEFAULT -1 FOR em7orgid
GO
COMMIT



update org_settings_d set EM7OrgID = -1 where (EM7OrgID < -1 OR EM7OrgID is null)

---*******************************FB 2575 Dec 24 2012 Ends *************************--

/* *************************** FB 2553 - Codian Start *************************** */

update MCU_Params_D set conflockunlock=1,confMessage=1,confAudioTx=1,partyAudioTx=1,confVideoTx=0,confAudioRx=1,
partyAudioRx=1,confVideoRx=1,partyVideoRx=1,confLayout=1,partyLayout=1,partypacketloss=1,partyMessage=1,
partyCamera=1 where bridgetypeid in (4,5,6)

/* *************************** FB 2553 - Codian end *************************** */

/* *************************** FB 2598 - Start *************************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
 EnableCallMonitor smallint 
 ALTER TABLE dbo.Org_Settings_D ADD
 EnableEM7 smallint 
 ALTER TABLE dbo.Org_Settings_D ADD
 EnableCDR smallint 
GO
COMMIT 

update Org_Settings_D set EnableCallMonitor= 0
update Org_Settings_D set EnableEM7= 0
update Org_Settings_D set EnableCDR= 0



/* *************************** FB 2598 - end *************************** */

/* *************************** FB 2610 Starts 18Jan2013 *************************** */
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	IsBridgeExtNo smallint NULL
GO
ALTER TABLE dbo.Org_Settings_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


Update Org_Settings_D set IsBridgeExtNo  = 0

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Room_D ADD
	BridgeExtNo nvarchar(50) NOT NULL DEFAULT ''
GO
ALTER TABLE dbo.Conf_Room_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


Update Conf_Room_D set BridgeExtNo ='' 

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Bridge_D ADD
	BridgeExtNo nvarchar(50) NOT NULL DEFAULT ''
GO
ALTER TABLE dbo.Conf_Bridge_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

Update Conf_Bridge_D set BridgeExtNo ='' 


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.mcu_list_d ADD 
  BridgeExtNo nvarchar(50) NOT NULL DEFAULT ''
GO
COMMIT 

-- Update mcu_list_d
Update Mcu_List_D set BridgeExtNo =''

/* *************************** FB 2610 Ends 18Jan2013 *************************** */

/* ********************* FB 2608 Starts 29thJan2013 *********************  */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	EnableVNOCselection int NULL
GO
ALTER TABLE dbo.Usr_List_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

update Usr_List_D set EnableVNOCselection = 1 where [admin] = 2
update Usr_List_D set EnableVNOCselection = 0 where [admin] != 2


/* ********************* FB 2608  End 29thJan2013 *********************  */


/* *************************** FB 2609 - start 29thJan2013 *************************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
 MeetandGreetBuffer int 
GO
COMMIT 

Update Org_Settings_D set MeetandGreetBuffer= 0

/* *************************** FB 2609 - end 29thJan2013 *************************** */

/* ************************************ FB 2577 Starts - 1stFeb2013 ************************************* */
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_AdvAVParams_D
	DROP COLUMN confVNOC
GO
ALTER TABLE dbo.Conf_AdvAVParams_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
/* ************************************ FB 2577 End - 1stFeb2013 ************************************* */


/* **************** FB 2617 Starts 1stFeb2013 ************ */

-- Change searchid from 1 to -1 for all AudioBridges User

Update Usr_List_D set searchId = -1 where Audioaddon = 1

Update Usr_Inactive_D set searchId = -1

/* **************** FB 2617 End 1stFeb2013 ************ */

/* **************** FB 2611 End 4thFeb2013 ************ */

delete from [Gen_Language_S] where [LanguageID] = 4
--INSERT INTO [dbo].[Gen_Language_S] ([LanguageID], [Name], [Country]) VALUES (4, N'Mandarin', N'China')

Delete from Dept_CustomAttr_Option_D where LanguageId = 4 --Mandarin

/* **************** FB 2611 End 4thFeb2013 ************ */


/* ****************** FB 2570 Starts 6th JAN 2013 **************** */


DROP TABLE Icons_Ref_S
GO
CREATE TABLE Icons_Ref_S
(
IconID int NOT NULL UNIQUE,
IconUri nvarchar(max) NOT NULL,
Label nvarchar(300) NOT NULL,
IconTarget nvarchar(max) NOT NULL
)
GO
insert into icons_ref_s values('1','../en/img/BULKTOOLicon.png','Bulk User Management','Allocation.aspx')
insert into icons_ref_s values('2','../en/img/OngoingIcon.png','Ongoing Conferences','ConferenceList.aspx?t=2')
insert into icons_ref_s values('3','../en/img/MyReservationsIcon.jpg','Reservations','ConferenceList.aspx?t=3')
insert into icons_ref_s values('4','../en/img/PublicConferenceIcon.png','Public Conferences','ConferenceList.aspx?t=4')
insert into icons_ref_s values('5','../en/img/PendingIcon[2].png','Pending Conferences','ConferenceList.aspx?t=5')
insert into icons_ref_s values('6','../en/img/ApprovalPendingIcon.png','Approval Pending','ConferenceList.aspx?t=6')
insert into icons_ref_s values('7','../en/img/InventoryWorkorderIcon.png','Audiovisual Work Orders','ConferenceOrders.aspx?t=1')
insert into icons_ref_s values('8','../en/img/CateringWorkorderIcon.png','Catering Work Orders','ConferenceOrders.aspx?t=2')
insert into icons_ref_s values('9','../en/img/HousekeepingWorkordersIcon.png','Facility Services Work Orders','ConferenceOrders.aspx?t=3')
insert into icons_ref_s values('10','../en/img/ManageInventorySetsIcon.png','Audiovisual Inventories','InventoryManagement.aspx?t=1')
insert into icons_ref_s values('11','../en/img/ManageCateringIcon[1].png','Catering Menus','InventoryManagement.aspx?t=2')
insert into icons_ref_s values('12','../en/img/ManagineHousekeepingItemsIcon.png','Facility Services','InventoryManagement.aspx?t=3')
insert into icons_ref_s values('13','../en/img/NewConferenceIcon.png','New Conference','ConferenceSetup.aspx?t=n&op=1')
insert into icons_ref_s values('14','../en/img/DashboardIcon.png','Conference Dashboard','Dashboard.aspx')
insert into icons_ref_s values('15','../en/img/CustomOrganizationEmailIcon.png','Customize Organization Emails','EmailCustomization.aspx?tp=o')
insert into icons_ref_s values('16','../en/img/CustomizeEmail.png','Customize My Email','EmailCustomization.aspx?tp=u')
insert into icons_ref_s values('17','../en/img/ManageEndpointIcons[1].png','Manage Endpoints','EndpointList.aspx?t=')
insert into icons_ref_s values('18','../en/img/SystemsDiagnosticsIcon.png','Diagnostics','EventLog.aspx')
insert into icons_ref_s values('19','../en/img/expressicon.png','Express New Conference','ExpressConference.aspx?t=n&op=1')
insert into icons_ref_s values('20','../en/img/REPORTicon[2].png','Reports','GraphicalReport.aspx')
insert into icons_ref_s values('21','../en/img/AdvanceReportIcon.png','Advanced Reports','MasterChildReport.aspx')
insert into icons_ref_s values('22','../en/img/DirectoryImportIcon.png','LDAP Directory Import','LDAPImport.aspx')
insert into icons_ref_s values('23','../en/img/LobbyIcon.png','Manage My Lobby','LobbyManagement.aspx')
insert into icons_ref_s values('24','../en/img/OrganizationOptionsIcon.png','Organization Options','mainadministrator.aspx')
insert into icons_ref_s values('25','../en/img/ManageMCUIcon.png','Manage MCUs','ManageBridge.aspx')
insert into icons_ref_s values('26','../en/img/ManageDepartmentIcon[1].png','Manage Departments','ManageDepartment.aspx')
insert into icons_ref_s values('27','../en/img/ManageGroupsIcon[1].png','Manage Groups','ManageGroup.aspx')
insert into icons_ref_s values('28','../en/img/OrganizationSetupIcon.png','Manage Organizations','ManageOrganization.aspx')
insert into icons_ref_s values('29','../en/img/SiteSettingsIcon[2].png','Site Settings','SuperAdministrator.aspx')
insert into icons_ref_s values('30','../en/img/ManageRooms.png','Manage Rooms','manageroom.aspx?hf=&m=&pub=&d=&comp=&f=&frm=')
insert into icons_ref_s values('31','../en/img/ManageTiersIcon.png','Manage Tiers','ManageTiers.aspx')
insert into icons_ref_s values('32','../en/img/ManageTemplatesIcon.png','Manage Templates','ManageTemplate.aspx')
insert into icons_ref_s values('33','../en/img/ManageActiveUserIcon[2].png','Manage Active Users','ManageUser.aspx?t=1')
insert into icons_ref_s values('34','../en/img/ManageGuestsIcon[1].png','Manage Guests','ManageUser.aspx?t=2')
insert into icons_ref_s values('35','../en/img/ManageInactiveUsersIcon.png','Manage Inactive Users','ManageUser.aspx?t=3')
insert into icons_ref_s values('36','../en/img/MyPreferencesIcons[1].png','My Preferences','ManageUserProfile.aspx')
insert into icons_ref_s values('37','../en/img/ManageRolesIcon.png','Manage User Roles','manageuserroles.aspx')
insert into icons_ref_s values('38','../en/img/SETTINGSicon[2].png','Organization Settings','OrganisationSettings.aspx')
insert into icons_ref_s values('39','../en/img/ManageOrganizationsUITextIcon.png','User Interface Text Settings','UITextChange.aspx')
insert into icons_ref_s values('40','../en/img/MyOrganizationUIicon[1].png','User Interface Banner & Theme','UISettings.aspx')
insert into icons_ref_s values('41','../en/img/ManageEmailQueue[1].png','Manage Blocked Email','ViewBlockedMails.aspx?tp=o')
insert into icons_ref_s values('42','../en/img/ManageOrganizationCustomOptionsIcon.jpg','Organization Custom Options','ViewCustomAttributes.aspx')
insert into icons_ref_s values('43','../en/img/CalendarIcon.png','Personal Calendar','personalCalendar.aspx?v=1&r=1&hf=&m=&pub=&d=&comp=&f=v')
insert into icons_ref_s values('44','../en/img/RoomCalendarIcon[1].png','Room Calendar','roomcalendar.aspx?v=1&r=1&hf=&d=&pub=&m=&comp=')
insert into icons_ref_s values('45','../en/img/CallMonitor.png','Call Monitor','MonitorMCU.aspx')
insert into icons_ref_s values('46','../en/img/p2p.jpg','Point To Point','point2point.aspx')
GO

Update Usr_Roles_D set roleName='Facility Administrator' where roleName='Housekeeping Administrator'
GO
Update Usr_Roles_D set roleName='Audiovisual Administrator' where roleName='Inventory Administrator'
GO
/* ****************** FB 2570 End 6th JAN 2013 **************** */



/* ************************* FB 2632 - Starts ********************************* */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	OnSiteAVSupport smallint NULL,
	MeetandGreet smallint NULL,
	ConciergeMonitoring smallint NULL,
	DedicatedVNOCOperator smallint NULL
GO
ALTER TABLE dbo.Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_OnSiteAVSupport DEFAULT 0 FOR OnSiteAVSupport
GO
ALTER TABLE dbo.Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_MeetandGreet DEFAULT 0 FOR MeetandGreet
GO
ALTER TABLE dbo.Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_ConciergeMonitoring DEFAULT 0 FOR ConciergeMonitoring
GO
ALTER TABLE dbo.Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_DedicatedVNOCOperator DEFAULT 0 FOR DedicatedVNOCOperator
GO
COMMIT

Update Conf_Conference_D set MeetandGreet = 0, OnSiteAVSupport = 0, ConciergeMonitoring = 0, DedicatedVNOCOperator = 0
------------------------------------------
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnableCncigSupport smallint NULL,
	AVOnsiteinEmail smallint NULL,
	MeetandGreetinEmail smallint NULL,
	CncigMoniteringinEmail smallint NULL,
	VNOCinEmail smallint NULL
GO
ALTER TABLE dbo.Org_Settings_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

Update Org_Settings_D set EnableCncigSupport =0,AVOnsiteinEmail=0, MeetandGreetinEmail=0, CncigMoniteringinEmail=0,VNOCinEmail=0
------------------------------------------
DECLARE @confid AS int ,@instanceid AS INT, @confAttID as int, @cusType as VARCHAR(5), @selectedvalue as INT

DECLARE cusAttrCursor CURSOR FOR

select case 
when DisplayTitle = 'Meet and Greet' then 'M' 
when DisplayTitle = 'Dedicated VNOC Operator' then 'D'
when DisplayTitle = 'On-Site A/V Support' then 'O' 
end as custype
, confid,instanceid,ConfAttrID,selectedvalue from Dept_CustomAttr_D a, Conf_CustomAttr_D b
where a.CustomAttributeid = b.CustomAttributeid and Description ='Concierge Support'

OPEN cusAttrCursor

FETCH NEXT FROM cusAttrCursor INTO @cusType,@confid,@instanceid,@confAttID, @selectedvalue
WHILE @@FETCH_STATUS = 0
BEGIN
	
	IF(@cusType = 'M')
		Update Conf_Conference_D set MeetandGreet = @selectedvalue where confid = @confid and instanceid = @instanceid
	IF(@cusType = 'O')
		Update Conf_Conference_D set OnSiteAVSupport = @selectedvalue where confid = @confid and instanceid = @instanceid
	IF(@cusType = 'D')
		Update Conf_Conference_D set DedicatedVNOCOperator = @selectedvalue where confid = @confid and instanceid = @instanceid

	delete Conf_CustomAttr_D where ConfAttrID = @confAttID
	
FETCH NEXT
FROM cusAttrCursor INTO @cusType,@confid,@instanceid,@confAttID, @selectedvalue
END
CLOSE cusAttrCursor
DEALLOCATE cusAttrCursor

------------------------------------------
Delete from Dept_CustomAttr_D where Description ='Concierge Support'

------------------------------------------

/* ************************* FB 2632 - End 7th Feb 2013 ********************************* */


/* ****************** FB 2571 Starts 7th Feb 2013 **************** */

Update Org_Settings_D set EnableFECC = 1
Update Org_Settings_D set DefaultFECC = 1 

/* ****************** FB 2571 End 7th Feb 2013 **************** */




/* ****************** FB 2633  7th Feb 2013 Starts **************** */
update Usr_Roles_D set roleMenuMask='8*240-4*11+8*0+5*0+2*0+8*0+2*0+2*0+2*0+1*0-6*14' where roleID=1
update Usr_Roles_D set roleMenuMask='8*248-4*15+8*255+5*255+2*3+8*255+2*3+2*3+2*3+1*0-6*63' where roleID=2
update Usr_Roles_D set roleMenuMask='8*252-4*15+8*255+5*255+2*3+8*255+2*3+2*3+2*3+1*1-6*63' where roleID=3
update Usr_Roles_D set roleMenuMask='8*136-4*0+8*2+5*0+2*0+8*0+2*0+2*3+2*0+1*0-6*63' where roleID=4
update Usr_Roles_D set roleMenuMask='8*136-4*0+8*4+5*0+2*0+8*0+2*3+2*0+2*0+1*0-6*63' where roleID=5
update Usr_Roles_D set roleMenuMask='8*136-4*0+8*1+5*0+2*0+8*0+2*0+2*0+2*3+1*0-6*63' where roleID=6

update usr_list_d set usr_list_d.menumask = b.rolemenumask from usr_roles_d as b where usr_list_d.roleid  = b.roleid


update Usr_Inactive_D set Usr_Inactive_D.menumask = b.rolemenumask from usr_roles_d as b where Usr_Inactive_D.roleid  = b.roleid



 insert into icons_ref_s values('47','../en/img/EM7.jpg','EM7','EM7Dashboard.aspx')
 
 
 /****************************************** FB 2633 7th Feb 2013 Ends ******************************************************************************/



/* ****************** FB 2591 End 12th Feb 2013 **************** */


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MCU_Profiles_D](
	[UID] [int] IDENTITY(1,1) NOT NULL,
	[MCUId] [int] NULL,
	[ProfileId] [int] NULL,
	[ProfileName] [nvarchar](50) NULL,
 CONSTRAINT [PK_MCU_Profiles_D] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



/* ****************** FB 2591 End 12th Feb 2013 **************** */
