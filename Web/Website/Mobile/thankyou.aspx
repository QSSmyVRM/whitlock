<%--ZD 100147 start--%>
<%--/* Copyright (C) 2014 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_Thankyou" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml" >
  
<head runat="server">
    <title>myVRM Video Conference Reservation Scheduler</title>
  
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
           
            <tr>
              
                <td align="center">
	                 <p>Thank you! You have logged out.</p>
	                    <p>Click <a href="../Droid/login.aspx">here</a> to login again.</p> 
                        <%                                                        
		                Response.Cookies["VRMuser"]["mact"] = "";
		                Response.Cookies["VRMuser"]["mpwd"] = "";
                                
                        Response.Redirect("~/Mobile/login.aspx?m=1");                           
	                           
                        %>
                </td>
            </tr>            
        </table>
    </div>
    </form>
</body>
</html>
