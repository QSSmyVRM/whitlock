﻿/* Copyright (C) 2014 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/ 
//ZD 100147 ZD 100886
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Xml;
using System.Data;
using System.Text;

namespace en_BridgeList
{

    public partial class BridgeList : System.Web.UI.Page
    {
        #region Protected Data Members

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;

        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.DropDownList lstBridgeType;
        protected System.Web.UI.WebControls.DropDownList lstBridgeStatus;
        protected System.Web.UI.WebControls.DataGrid dgMCUs;
        protected System.Web.UI.WebControls.Table tblNoMCUs;
        protected System.Web.UI.HtmlControls.HtmlInputHidden locstr; //ZD 100718
        #endregion


        public BridgeList()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }

        #region Page_Load
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("bridgelist.aspx", Request.Url.AbsoluteUri.ToLower());

                if (!IsPostBack)
                    BindData();

            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("Page_Load:" + ex.Message);
            }

        }
        #endregion

        #region BindData
        /// <summary>
        /// BindData
        /// </summary>
        /// <returns></returns>
        private bool BindData()
        {
            string inXML = "", outXML = "";

            try
            {
                inXML = "<login>" + obj.OrgXMLElement() + " <userID>" + Session["userID"].ToString() + "</userID></login>";
                outXML = obj.CallMyVRMServer("GetBridgeList", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//bridgeInfo/bridges/bridge");
                    if (nodes.Count > 0)
                    {
                        XmlNodeList nodesTemp = xmldoc.SelectNodes("//bridgeInfo/bridgeTypes/type");
                        LoadList(lstBridgeType, nodesTemp);
                        nodesTemp = xmldoc.SelectNodes("//bridgeInfo/bridgeStatuses/status");
                        LoadList(lstBridgeStatus, nodesTemp);
                        LoadMCUGrid(nodes);

                        dgMCUs.Visible = true;
                        tblNoMCUs.Visible = false;
                    }
                    else
                    {
                        dgMCUs.Visible = false;
                        tblNoMCUs.Visible = true;
                    }
                }
                else
                {
                    errLabel.Text = obj.ShowSystemMessage();
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("Page_Load:" + ex.Message);
            }
            return true;
        }
        #endregion

        #region LoadList
        /// <summary>
        /// LoadList
        /// </summary>
        /// <param name="lstTemp"></param>
        /// <param name="nodes"></param>
        protected void LoadList(DropDownList lstTemp, XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataTable dt = new DataTable();

                if (ds.Tables.Count > 0)
                    dt = ds.Tables[0];
                lstTemp.DataSource = dt;
                lstTemp.DataBind();
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("LoadList:" + ex.Message);
                errLabel.Visible = true;
            }
        }
        #endregion

        #region LoadMCUGrid
        /// <summary>
        /// LoadMCUGrid
        /// </summary>
        /// <param name="nodes"></param>
        protected void LoadMCUGrid(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataTable dt = new DataTable();
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }

                foreach (DataRow dr in dt.Rows)
                {

                    if (dr["exist"].ToString().Equals("1"))
                        dr["exist"] = obj.GetTranslatedText("Existing");
                    else
                        dr["exist"] = obj.GetTranslatedText("Virtual");
                    dr["status"] = obj.GetTranslatedText(lstBridgeStatus.Items.FindByValue(dr["status"].ToString()).Text);
                    dr["interfaceType"] = lstBridgeType.Items.FindByValue(dr["interfaceType"].ToString()).Text;

                    if (dr["PollStatus"].ToString() == "-2")
                    {
                        dr["PollStatus"] = "<font color=#FF0000> "+ obj.GetTranslatedText("FAIL")+" </font>";
                    }
                    else if (dr["PollStatus"].ToString() == "-1")
                    {
                        dr["PollStatus"] = "<font color=#F5B800> " + obj.GetTranslatedText("PASS") + " </font>";
                    }
                    else if (dr["PollStatus"].ToString() == "0")
                    {
                        dr["PollStatus"] = "<font color=#008000> " + obj.GetTranslatedText("PASS") + " </font>";
                    }

                    if (dr["userstate"].ToString() == "I")
                    {
                        dr["administrator"] = "<font color=red>" + dr["administrator"].ToString() + " (In-Active)</font>";
                    }
                    else if (dr["userstate"].ToString() == "D")
                    {
                        dr["administrator"] = "<font color=red>" + dr["administrator"].ToString() + "</font>";
                    }


                }

                dgMCUs.DataSource = dt;
                dgMCUs.DataBind();
				//ZD 100718 Starts
                string MCUId = "";
                bool isChecked = false, isUnChecked = false;
                RadioButton tmpBtn = null;
                if (Request.QueryString["hd"] != null && Request.QueryString["hd"].ToString() != "")
                {
                    MCUId = Request.QueryString["hd"].ToString();
                    for (int i = 0; i < dgMCUs.Items.Count; i++)
                    {
                        tmpBtn = ((RadioButton)dgMCUs.Items[i].FindControl("rdSelectMCU"));
                        if (i == 0)
                        {
                            tmpBtn.Checked = true;
                            isChecked = true;
                        }
                        if (dgMCUs.Items[i].Cells[0].Text == MCUId)
                        {
                            tmpBtn.Checked = false;
                            tmpBtn.Enabled = false;
                            if (i == 0)
                                isUnChecked = true;
                        }
                        if (dgMCUs.Items.Count > 1 && isChecked && isUnChecked)
                        {
                            ((RadioButton)dgMCUs.Items[i + 1].FindControl("rdSelectMCU")).Checked = true;
                            break;
                        }
                    }
                }
				//ZD 100718 End
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("LoadMCUGrid:" + ex.Message);
                errLabel.Visible = true;
            }
        }
        #endregion

        #region SubmitMCUSelection
        /// <summary>
        /// SubmitMCUSelection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SubmitMCUSelection(Object sender, EventArgs e)
        {
            try
            {
                string MCUId = "", MCUName = "";
                bool isMCUSelected = false;

                foreach (DataGridItem dgMCUList in dgMCUs.Items)
                {
                    if (((RadioButton)dgMCUList.FindControl("rdSelectMCU")).Checked == true)
                    {
                        isMCUSelected = true;
                        Session.Remove("SelectedMCUId");
                        Session.Remove("SelectedMCUName");

                        MCUId = dgMCUList.Cells[0].Text;
                        MCUName = dgMCUList.Cells[1].Text;

                        Session.Add("SelectedMCUId", MCUId);
                        Session.Add("SelectedMCUName", MCUName);
                    }
                }

                if (isMCUSelected == false)
                {
                    Session.Remove("SelectedMCUId");
                    Session.Remove("SelectedMCUName");
                }

                //ZD 100718 Start
                if (Request.QueryString["hdnLnkBtnId"] != null)
                {
                    locstr.Value = MCUId + "|" + MCUName + "++";
                    string lnkbtnid = Request.QueryString["hdnLnkBtnId"].ToString();
                    ClientScript.RegisterStartupScript(this.GetType(), "ChangeMCU", "<script>ChangeMCU('" + lnkbtnid + "');</script>");//ClosePopup();
                }
                //ZD 100718 End
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("SubmitMCUSelection:" + ex.Message);
            }
        }
        #endregion
    }
}