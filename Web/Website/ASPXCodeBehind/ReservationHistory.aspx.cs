/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Data;
using System.Xml;
using System.Text;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Xml.Schema;
using System.Collections.Generic;

using DevExpress.Web;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxGridView.Export.Helper;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxHtmlEditor;
using DevExpress.Web.ASPxPopupControl;
using DevExpress.Web.ASPxMenu;
using DevExpress.XtraPrinting;
using DevExpress.XtraCharts.Native;
using DevExpress.XtraCharts;
using DevExpress.Utils;
using DevExpress.Web.ASPxGridView.Export;
using System.Drawing;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
                

/// <summary>
/// Summary description for SuperAdministrator.
/// </summary>

public partial class ReservationHistory : System.Web.UI.Page
{
    #region protected Members

    protected System.Web.UI.WebControls.Label lblHeading;
    protected System.Web.UI.WebControls.Label errLabel;
    protected System.Web.UI.WebControls.TextBox ConferenceID;    
    protected DevExpress.Web.ASPxGridView.ASPxGridView MainGrid;
    protected DevExpress.Web.ASPxGridView.Export.ASPxGridViewExporter gridExport;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnValue;

    
    #endregion

    #region protected data members

    private myVRMNet.NETFunctions obj;
    private MyVRMNet.LoginManagement obj1;
    private ns_Logger.Logger log;

    protected Int32 orgId = 11;
    protected XmlDocument xmlDoc = null;
    protected DataSet ds = null;
    protected DataTable rptTable = new DataTable();
    protected DataTable participantTable = new DataTable();
    protected String tmzone = "";
    protected String organizationID = "";
    protected Int32 usrID = 11;

    #endregion

    #region Constructor
    public ReservationHistory()
    {
        obj = new myVRMNet.NETFunctions();
        log = new ns_Logger.Logger();
        obj1 = new MyVRMNet.LoginManagement();
    }

    #endregion

    #region Page_init
    /// <summary>
    /// Page_init
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_init(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
                Session["ReportXML"] = null;

            if (Session["organizationID"] != null)
                Int32.TryParse(Session["organizationID"].ToString(), out orgId);

            if (Session["timezoneID"] != null)
                tmzone = Session["timezoneID"].ToString();
            else
                tmzone = "26";

            if (Session["ReportXML"] != null)
                btnOk_Click(null, null);

        }
        catch (Exception ex)
        {
            log.Trace("Page_init" + ex.Message);
            errLabel.Text = obj.ShowSystemMessage();
            errLabel.Visible = true;
        }
    }

    #endregion

    #region Methods Executed on Page Load

    private void Page_Load(object sender, System.EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("reservationhistory.aspx", Request.Url.AbsoluteUri.ToLower());

            if (Session["userID"] != null)
            {
                if (Session["userID"].ToString() != "")
                    Int32.TryParse(Session["userID"].ToString(), out usrID);
            }

            if (hdnValue.Value == "1")
            {
                Session["ReportXML"] = null;
                hdnValue.Value = "";
            }
           
            if (!IsPostBack)
                BindData();
        }
        catch (Exception ex)
        {
            log.Trace("Page_Load" + ex.Message);
            errLabel.Text = obj.ShowSystemMessage();
            errLabel.Visible = true;
        }
    }

    #endregion

    #region BindData

    private void BindData()
    {
        try
        {
            btnOk_Click(null, null);
        }
        catch (Exception ex)
        {
            log.Trace("BindData: " + ex.StackTrace);
            errLabel.Text = obj.ShowSystemMessage();
            errLabel.Visible = true;
        }
    }

    #endregion

    #region MainGrid_DataBound

    protected void MainGrid_DataBound(object sender, EventArgs e)
    {
        try
        {
            ASPxGridView gridView = sender as ASPxGridView;

            for (Int32 c = 0; c < gridView.Columns.Count; c++)
            {
                gridView.Columns[c].CellStyle.Wrap = DevExpress.Utils.DefaultBoolean.False;
            }
        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            errLabel.Text = obj.ShowSystemMessage();
            log.Trace("MainGrid_DataBound" + ex.Message);
        }
    }
    #endregion

    protected void GridExporter_OnRenderBrick(object sender, ASPxGridViewExportRenderingEventArgs e)
    {
        e.Text = e.Text.ToString().Replace("<br>", " ");
        if (e.Text.Length > 255 && e.TextValue != null)
            e.TextValue = null;
    }
    
    #region btnOk_Click
    protected void btnOk_Click(object sender, EventArgs e)
    {
        StringBuilder inXml = new StringBuilder();
        String outXML = "";
        try
        {
            inXml.Append("<GetUsageReports>");
            inXml.Append(obj.OrgXMLElement());
            inXml.Append("<DateFrom></DateFrom>");
            inXml.Append("<DateTo></DateTo>");
            inXml.Append("<DateFormat></DateFormat>");
            inXml.Append("<UserID>" + usrID + "</UserID>");
            inXml.Append("<ReportType>RH</ReportType>");
            inXml.Append("<InputType></InputType>");
            if (ConferenceID.Text != "")
                inXml.Append("<UniqueID>" + ConferenceID.Text.Trim() + "</UniqueID>");
            inXml.Append("</GetUsageReports>");

            if ((Session["ReportXML"] == null) && inXml.ToString() != "")
            {
                outXML = obj.CallMyVRMServer("GetUsageReports", inXml.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                Session["ReportXML"] = outXML;
            }
            else if (Session["ReportXML"] != null && Session["ReportXML"].ToString() != "")
                outXML = Session["ReportXML"].ToString();

            xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(outXML);
            ds = new DataSet();
            ds.ReadXml(new XmlNodeReader(xmlDoc));
            MainGrid.Columns.Clear();

            if (ds.Tables.Count > 0)
            {
                CreateTable();
                CreateGridColumns();

                try
                {
                    MainGrid.DataSource = rptTable;
                    MainGrid.DataBind();
                }
                catch (Exception ex)
                {
                    log.Trace(ex.StackTrace + " : " + ex.Message);
                }
            }
            else
            {
                errLabel.Visible = true;
                errLabel.Text = obj.GetTranslatedText("No Records");
            }

            ConferenceID.Text = "";
        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            errLabel.Text = obj.ShowSystemMessage();
            log.Trace(ex.Message);
        }
    }

    #endregion


    #region CreateTable

    private void CreateTable()
    {
        try
        {
            String colName = "";
            rptTable = new DataTable();
            
            for (Int32 t1 = 0; t1 < ds.Tables.Count; t1++)
            {
                DataTable colTable = ds.Tables[t1];

                for (Int32 i = 0; i < colTable.Columns.Count; i++)
                {
                    colName = "";
                    colName = colTable.Columns[i].ToString().ToLower();

                    rptTable.Columns.Add(obj.GetTranslatedText(colTable.Columns[i].ToString()), typeof(String));
                }
            }

            for (Int32 t = 0; t < ds.Tables.Count; t++)
            {
                DataTable detailsTable = ds.Tables[t];

                for (Int32 i = 0; i < detailsTable.Rows.Count; i++)
                {
                    DataRow dataRow = null;
                    dataRow = rptTable.NewRow();

                    if (detailsTable.Rows[0][1].ToString() == "")
                        break;

                    Int32 rCnt = 0;
                    rCnt = detailsTable.Columns.Count;

                    Int32 c = 0;
                    String pwd = "";
                    for (int j = 0; j < rCnt; j++)
                    {
                        colName = detailsTable.Columns[j].ColumnName.ToLower();
                        dataRow[c] = detailsTable.Rows[i][colName].ToString();  

                        c = c + 1;
                    }
                    rptTable.Rows.Add(dataRow);
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace("CreateTable " + ex.Message);
            errLabel.Text = obj.ShowSystemMessage();
        }
    }

    #endregion

    #region CreateGridColumns

    private void CreateGridColumns()
    {
        try
        {
            GridViewDataColumn dataColumn = new GridViewDataColumn();
            MainGrid.Columns.Clear();
            //for (Int32 i = 0; i < rptTable.Columns.Count; i++)
            //{
                String colName = "";
                colName = obj.GetTranslatedText("Meeting ID");
                dataColumn = new GridViewDataColumn();
                dataColumn.Caption = colName;
                dataColumn.FieldName = colName;
                dataColumn.Width = Unit.Pixel(25);
                MainGrid.Columns.Add(dataColumn);

                colName = obj.GetTranslatedText("Description");
                GridViewDataTextColumn datatxtColumn = new GridViewDataTextColumn();
                datatxtColumn.PropertiesEdit.EncodeHtml = false;
                datatxtColumn.Caption = colName;
                datatxtColumn.FieldName = colName;
                MainGrid.Columns.Add(datatxtColumn);
            //}
        }
        catch (Exception ex)
        {
            log.Trace("CreateGridColumns" + ex.Message);
            errLabel.Text = obj.ShowSystemMessage();
        }
    }

    #endregion

    #region ExportExcel

    protected void ExportExcel(object sender, EventArgs e)
    {
        try
        {
            gridExport.FileName = "Reservation History - " + DateTime.Now.ToString("MM-dd-yyyy");
            gridExport.WriteXlsToResponse();
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);
        }
    }


    #endregion

}