/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.IO;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web.Services;

namespace en_InstantConference
{
    public partial class InstantConference : System.Web.UI.Page
    {
        #region Private Data Members

        protected System.Web.UI.WebControls.DropDownList lstStaticDynamic;
        protected System.Web.UI.WebControls.Button btnConfSubmit;
        protected System.Web.UI.WebControls.Label errLabel;
        MyVRMNet.Util utilObj = null; 
        protected ns_Logger.Logger log = null;
        private myVRMNet.NETFunctions obj = null;
        
        String[] pipeDelim = { "||" };
        String[] ExclamDelim = { "!!" };
        public string isEditMode = "";
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnemailalert;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnstatic; //ZD 100890
        List<int> xPartys;
        protected System.Web.UI.WebControls.TextBox txtInstantConferenceParticipant;
        string TBDStatiscID = "", StaticID = "";
        string sDektopURL = "", sE164DialNumber = "", URL = "";
        #endregion

        #region Public Constructor

        public InstantConference()
        {
            utilObj = new MyVRMNet.Util(); 
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();
           
        }
        
        #endregion 
     
        #region Page Load
        /// <summary>
        /// Page_Load method changed for ZD 100890
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            errLabel.Text = "";
            try
            {
                if (!IsPostBack)
                {
                    String inXML = "<login><userID>" + Session["userID"].ToString() + "</userID><user><userID>" + Session["userID"].ToString() + "</userID></user></login>";
                    String outXML = obj.CallMyVRMServer("GetOldUser", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    if (outXML.IndexOf("<error>") < 0)
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(outXML);
                        StaticID = xmldoc.SelectSingleNode("//oldUser/StaticID").InnerText;
                        hdnstatic.Value = StaticID;

                        lstStaticDynamic.ClearSelection();
                        if (lstStaticDynamic.Items.Count <= 0)
                        {
                            if (StaticID != "")
                                lstStaticDynamic.Items.Insert(0, "Static (" + StaticID + ")");
                            else
                                lstStaticDynamic.Items.Insert(0, "Static");
                            lstStaticDynamic.Items.Insert(1, "Dynamic");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("Simple Conference :Page_load" + ex.Message);

            }
        }
         
        #endregion

        #region SetConference

         protected void SetConference(object sender, EventArgs e)
         {
             try
             {
                 SetConference();
             }
             catch (Exception ex)
             {
                 log.Trace("SetConference : " + ex.Message);
                 errLabel.Visible = true;
                 errLabel.Text = ex.Message;
             }
         }

         protected bool SetConference()
         {
             DataTable dtRooms = new DataTable();
             try
             {
                 //CreateDataTable(dtRooms);

                 int maxDuration = 24;
                 if (Application["MaxConferenceDurationInHours"] != null)
                     if (!Application["MaxConferenceDurationInHours"].ToString().Trim().Equals(""))
                         maxDuration = Int32.Parse(Application["MaxConferenceDurationInHours"].ToString().Trim());

                 //GetXConfParams(); //FB 1830 Email Edit


                 StringBuilder inxml = new StringBuilder();
                 inxml.Append("<conference>");
                 inxml.Append(obj.OrgXMLElement());
                 inxml.Append("<requestorID>" + Session["userid"].ToString() + "</requestorID>");
                 inxml.Append("<userID>" + Session["userid"].ToString() + "</userID>");
                 inxml.Append("<confInfo>");
                 inxml.Append("<confID>new</confID>");
                 inxml.Append("<confName>" + Session["DefaultSubject"].ToString() + "</confName>");
                 inxml.Append("<confHost>" + Session["userid"].ToString() + "</confHost>");
                 inxml.Append("<confOrigin>0</confOrigin>");
                 inxml.Append("<timeCheck></timeCheck>");
                 inxml.Append("<confPassword></confPassword>");
                 inxml.Append("<isVMR>0</isVMR>");
                 inxml.Append("<MeetingId>" + lstStaticDynamic.SelectedIndex + "</MeetingId>");

                 if (lstStaticDynamic.SelectedIndex == 0)
                     inxml.Append("<StaticID>" + hdnstatic.Value + "</StaticID>");
                 else
                     inxml.Append("<StaticID></StaticID>");

                 inxml.Append("<CloudConferencing>0</CloudConferencing>");
                 inxml.Append("<isPCconference>0</isPCconference>");
                 inxml.Append("<pcVendorId>0</pcVendorId>");
                 inxml.Append("<StartMode>0</StartMode>");
                 inxml.Append("<MeetandGreetBuffer>0</MeetandGreetBuffer>");
                 inxml.Append("<Secured>0</Secured>");
                 inxml.Append("<EnableNumericID>0</EnableNumericID>");
                 inxml.Append("<CTNumericID></CTNumericID>");
                 inxml.Append("<EnableNumericID>0</EnableNumericID>");
                 inxml.Append("<CTNumericID></CTNumericID>");
                 inxml.Append("<McuSetupTime>0</McuSetupTime>");
                 inxml.Append("<MCUTeardonwnTime>0</MCUTeardonwnTime>");
                 inxml.Append("<immediate>1</immediate>");
                 inxml.Append("<recurring>0</recurring>");
                 inxml.Append("<recurringText></recurringText>");
                 inxml.Append("<startDate></startDate>");
                 inxml.Append("<startHour></startHour>");
                 inxml.Append("<startMin></startMin>");
                 inxml.Append("<startSet></startSet>");
                 inxml.Append("<timeZone>26</timeZone>");
                 inxml.Append("<setupDuration>0</setupDuration>");
                 inxml.Append("<teardownDuration>0</teardownDuration>");
                 inxml.Append("<setupDateTime></setupDateTime>");
                 inxml.Append("<teardownDateTime></teardownDateTime>");
                 inxml.Append("<createBy>2</createBy>");
                 inxml.Append("<durationMin>" + Session["DefaultConfDuration"].ToString() + "</durationMin>");
                 inxml.Append("<description></description>");
                 inxml.Append("<locationList>");
                 inxml.Append("<selected>");
                 inxml.Append("</selected>");
                 inxml.Append("</locationList>");
                 inxml.Append("<ConfGuestRooms></ConfGuestRooms>");
                 inxml.Append("<publicConf>0</publicConf>");
                 inxml.Append("<dynamicInvite>0</dynamicInvite>");
                 inxml.Append("<advAVParam> ");
                 inxml.Append("<maxAudioPart></maxAudioPart>");
                 inxml.Append("<maxVideoPart></maxVideoPart>");
                 inxml.Append("<restrictProtocol>3</restrictProtocol>");
                 inxml.Append("<restrictAV>2</restrictAV>");
                 inxml.Append("<videoLayout>01</videoLayout>");
                 inxml.Append("<maxLineRateID>384</maxLineRateID>");
                 inxml.Append("<audioCodec>0</audioCodec>");
                 inxml.Append("<videoCodec>0</videoCodec>");
                 inxml.Append("<dualStream>1</dualStream>");
                 inxml.Append("<confOnPort>0</confOnPort>");
                 inxml.Append("<encryption>0</encryption>");
                 inxml.Append("<lectureMode>0</lectureMode>");
                 inxml.Append("<VideoMode>3</VideoMode>");
                 inxml.Append("<SingleDialin>0</SingleDialin>");
                 inxml.Append("<internalBridge></internalBridge>");
                 inxml.Append("<externalBridge></externalBridge>");
                 inxml.Append("<FECCMode>1</FECCMode>");
                 inxml.Append("</advAVParam>");
                 string[] partysary = txtInstantConferenceParticipant.Text.Split(';');
                 int partynum = partysary.Length;
                 
                 //For Participant Biding
                 inxml.Append("<partys>");
                 for (int i = 0; i < partynum - 1; i++)
                 {
                     string[] partyary = partysary[i].Split(ExclamDelim, StringSplitOptions.None);
                     inxml.Append("<party>");
                     String Email = partyary[0].ToString();

                     inxml.Append("<partyID>new</partyID>");
                     inxml.Append("<partyFirstName>Test</partyFirstName>");
                     inxml.Append("<partyLastName>Test</partyLastName>");
                     inxml.Append("<partyEmail>" + Email + "</partyEmail>");
                     inxml.Append("<partyInvite>2</partyInvite>");
                     inxml.Append("<partyNotify>1</partyNotify>");
                     inxml.Append("<partyAudVid>2</partyAudVid>");
                     inxml.Append("<notifyOnEdit>1</notifyOnEdit>");
                     inxml.Append("<partyPublicVMR>0</partyPublicVMR>");
                     inxml.Append("</party>");
                 }
                 inxml.Append("</partys>");

                 inxml.Append("<ModifyType>0</ModifyType>");
                 inxml.Append("<fileUpload>");
                 inxml.Append("<file></file>");
                 inxml.Append("<file></file>");
                 inxml.Append("<file></file>");
                 inxml.Append("</fileUpload>");
                 inxml.Append("<ConciergeSupport>");
                 inxml.Append("</ConciergeSupport>");
                 inxml.Append("<CustomAttributesList>");
                 inxml.Append("</CustomAttributesList>");
                 inxml.Append("<ICALAttachment></ICALAttachment>");
                 inxml.Append("<MCUs>");
                 inxml.Append("</MCUs>");
                 inxml.Append("</confInfo>");
                 inxml.Append("</conference>");

                 string outxml = obj.CallMyVRMServer("SetConference", inxml.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                     
                 //SET ADVANCED A/V SETTING CALL
                 log.Trace("INSTANT CONFERENCE OutXML- " + outxml);
                 if (outxml.IndexOf("<error>") < 0)
                 {
                     Session.Add("outxml", outxml);
                     XmlDocument xmlout = new XmlDocument();
                     xmlout.LoadXml(outxml);

                     String confID = xmlout.SelectSingleNode("//setConference/conferences/conference/confID").InnerText;
                     Session["confID"] = confID;

                     TBDStatiscID = xmlout.SelectSingleNode("//setConference/conferences/conference/StaticID").InnerText; //ZD 100890

                     SetEndpoints();

                     if (URL.Trim() != "")
                     {
                         URL = "http://" + URL.Trim();//ZD 100969
                         ScriptManager.RegisterStartupScript(this, this.GetType(), "invokeURL", "<script>window.open('" + URL + "','_blank');</script>", false);
                     }
                     return true;
                 }
                 else
                 {
                     errLabel.Visible = true;
                     errLabel.Text = obj.ShowErrorMessage(outxml);
                     return false;
                 }
                 //SET ADVANCED A/V SETTING CALL
                //ZD 100167 SetInstantConference- end
             }

             catch (Exception ex)
             {
                 log.Trace("Error occurred in SetConference. Please try later.\n" + ex.Message);
                 errLabel.Visible = true;
                 errLabel.Text = ex.Message;
                 return false;

             }
          return true;
         }

         #endregion

        #region SetEndpoints
         /// <summary>
         /// SetEndpoints
         /// </summary>
         /// <param name="dtrms"></param>
         /// <returns></returns>
         protected bool SetEndpoints()
         {
             StringBuilder inXML = new StringBuilder();
             XmlDocument xd = null;

             try
             {
                 inXML.Append("<SetAdvancedAVSettings><editFromWeb>1</editFromWeb><language>en</language><isVIP>0</isVIP><isReminder>0</isReminder>");
                 inXML.Append(obj.OrgXMLElement());
                 inXML.Append("<UserID>" + Session["userID"].ToString() + "</UserID>");
                 inXML.Append("<ConfID>" + Session["confID"].ToString() + "</ConfID>");
                 inXML.Append("<AVParams><isDedicatedEngineer>0</isDedicatedEngineer><isLiveAssitant>0</isLiveAssitant><SingleDialin>0</SingleDialin>");

                 if (lstStaticDynamic.SelectedIndex == 0)
                     inXML.Append("<EnableStaticID>1</EnableStaticID>");
                 else
                     inXML.Append("<EnableStaticID>0</EnableStaticID>");

                 inXML.Append("<StaticID>" + TBDStatiscID + "</StaticID></AVParams></SetAdvancedAVSettings>");
                 String outXML = obj.CallMyVRMServer("SetInstantAdvancedAVSettings", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                 if (outXML.IndexOf("<error>") >= 0)
                 {
                     errLabel.Visible = true;
                     errLabel.Text = obj.ShowErrorMessage(outXML);
                     return false;
                 }
                 else
                 {
                     xd = new XmlDocument();
                     xd.LoadXml(outXML);

                     if (xd.SelectSingleNode("//success/externalId") != null)
                         sE164DialNumber = xd.SelectSingleNode("//success/externalId").InnerText;

                     if (xd.SelectSingleNode("//success/conferenceUrl") != null)
                         sDektopURL = xd.SelectSingleNode("//success/conferenceUrl").InnerText;

                     URL = sDektopURL + "?ID=" + sE164DialNumber + "&autojoin";


                 }

                 return true;
             }
             catch (Exception ex)
             {
                 log.Trace(ex.Message);
                 errLabel.Visible = true;
                 return false;
             }
         }

         #endregion

    }
}
