﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Xml;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Google.Apis.Util;
using Google.Apis.Authentication.OAuth2;
using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OAuth2;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using System.Threading;
using Google.Apis.Services;
using Google.Apis.Authentication.OAuth2.DotNetOpenAuth;
using System.Text;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;

namespace ns_MyVRM
{
    public partial class GoogleAuth : System.Web.UI.Page
    {
        #region Declaration

        #region myVRM Declartion

        ns_Logger.Logger _log;
        private myVRMNet.NETFunctions _obj;
        string _outXML = "";
        StringBuilder _inXML = null;

        #endregion


        #region Google Calendar
        private CalendarService _service;
        private OAuth2Authenticator<WebServerClient> _authenticator;
        private IAuthorizationState _state;
        private Google.Apis.Calendar.v3.Data.Channel _channel = null;
        private Google.Apis.Calendar.v3.Data.Channel _chresp = null;

        private IAuthorizationState AuthState
        {
            get
            {
                return _state ?? HttpContext.Current.Session["AUTH_STATE"] as IAuthorizationState;
            }
        }
        #endregion

        #endregion

        #region Constructor
        public GoogleAuth()
        {
            _log = new ns_Logger.Logger();
            _obj = new myVRMNet.NETFunctions();
        }
        # endregion

        #region Page_Load
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (HttpContext.Current.Session["SERVICE_OBJECT"] != null)
                {
                    _service = (CalendarService)HttpContext.Current.Session["SERVICE_OBJECT"];
                    HttpContext.Current.Response.Redirect("~/" + HttpContext.Current.Session["language"].ToString() + "/SettingSelect2.aspx?c=GH");
                }
                else
                {

                    _service = new CalendarService(new BaseClientService.Initializer() { Authenticator = _authenticator = CreateAuthenticator() });

                    _authenticator.LoadAccessToken();
                }
            }
            catch (Exception ex)
            {
                _log.Trace("" + ex.StackTrace);
            }
        }
        #endregion

        #region CreateAuthenticator
        /// <summary>
        /// CreateAuthenticator
        /// </summary>
        /// <returns></returns>
        private OAuth2Authenticator<WebServerClient> CreateAuthenticator()
        {

            try
            {

                // Register the authenticator.
                var provider = new WebServerClient(GoogleAuthenticationServer.Description, _obj.GetDecryptText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), HttpContext.Current.Session["GoogleClientID"].ToString()), _obj.GetDecryptText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), HttpContext.Current.Session["GoogleSecretID"].ToString()));

                var authenticator =
                    new OAuth2Authenticator<WebServerClient>(provider, GetAuthorization) { NoCaching = true };
                return authenticator;
            }
            catch (Exception e)
            {
                _log.Trace("" + e.StackTrace);
                return null;
            }
        }
        #endregion

        #region GetAuthorization
        /// <summary>
        /// GetAuthorization
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        private IAuthorizationState GetAuthorization(WebServerClient client)
        {
            string currentGoogleEmail = "";
            try
            {
                if (_obj == null)
                    _obj = new myVRMNet.NETFunctions();
                // If this user is already authenticated, then just return the auth state.

                IAuthorizationState state = AuthState;

                if (state != null)
                    return state;

                // Check if an authorization request already is in progress.
                state = client.ProcessUserAuthorization(new HttpRequestInfo(HttpContext.Current.Request));
                if (state == null)
                {
                    string scope = CalendarService.Scopes.Calendar.GetStringValue();
                    List<string> scopesl = new List<string>();
                    scopesl.Add(scope);
                    IEnumerable<string> scopes = scopesl.AsEnumerable();
                    OutgoingWebResponse response = client.PrepareRequestUserAuthorization(new[] { scope }, null);
                    if (HttpContext.Current.Session["GoogleRefreshToken"].ToString().IsNullOrEmpty())
                        response.Headers["Location"] += "&access_type=offline&approval_prompt=force";
                    response.Send();

                }

                if (state != null && (!string.IsNullOrEmpty(state.AccessToken) || !string.IsNullOrEmpty(state.RefreshToken)))
                {
                    //Checking the authenticated email account
                    var accessToken = state.AccessToken;

                    var urlBuilder = new System.Text.StringBuilder();

                    urlBuilder.Append("https://");
                    urlBuilder.Append("www.googleapis.com");
                    urlBuilder.Append("/calendar/v3/users/me/calendarList");
                    urlBuilder.Append("?minAccessRole=writer");

                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(urlBuilder.ToString());
                    // Get the response.
                    httpWebRequest.CookieContainer = new CookieContainer();
                    httpWebRequest.Headers["Authorization"] =
                        string.Format("Bearer {0}", accessToken);

                    var response1 = (HttpWebResponse)httpWebRequest.GetResponse();
                    // Get the stream containing content returned by the server.
                    var dataStream = response1.GetResponseStream();
                    // Open the stream using a StreamReader for easy access.
                    var reader = new StreamReader(dataStream);
                    // Read the content. 
                    var jsonString = reader.ReadToEnd();
                    // Cleanup the streams and the response.
                    reader.Close();
                    dataStream.Close();
                    response1.Close();

                    XmlDocument doc = (XmlDocument)Newtonsoft.Json.JsonConvert.DeserializeXmlNode(jsonString, "Root");

                    if (doc.SelectSingleNode("Root/items/id") != null)
                        currentGoogleEmail = doc.SelectSingleNode("Root/items/id").InnerText;

                    if (currentGoogleEmail != HttpContext.Current.Session["userEmail"].ToString())
                    {
                        HttpContext.Current.Response.Redirect("~/" + HttpContext.Current.Session["language"].ToString() + "/GenLogin.aspx?G=f");
                    }


                    HttpContext.Current.Session["AUTH_STATE"] = _state = state;
                    if (DateTime.Parse(HttpContext.Current.Session["GoogleChanelExpiration"].ToString()) < DateTime.UtcNow)
                    {
                        _channel = new Google.Apis.Calendar.v3.Data.Channel();
                        _channel.Id = Guid.NewGuid().ToString();

                        _channel.Type = "web_hook";
                        _channel.Address = HttpContext.Current.Session["GoogleChannelAddress"].ToString() + @"/en/GoogleReceiver.aspx";
                        _channel.Token = HttpContext.Current.Session["userID"].ToString();

                        _chresp = new Google.Apis.Calendar.v3.Data.Channel();
                        _chresp = _service.Events.Watch(_channel, HttpContext.Current.Session["userEmail"].ToString()).Execute();

                    }


                    // Store and return the credentials.


                    _inXML = new StringBuilder();
                    _inXML.Append("<UserToken>");
                    _inXML.Append("<UserId>" + HttpContext.Current.Session["userID"].ToString() + "</UserId>");
                    _inXML.Append("<UserEmail>" + HttpContext.Current.Session["userEmail"].ToString() + "</UserEmail>");
                    _inXML.Append(_obj.OrgXMLElement());

                    _inXML.Append("<AccessToken>" + state.AccessToken.ToString() + "</AccessToken>");
                    HttpContext.Current.Session["GoogleAccessToken"] = _obj.GetEncrpytedText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), state.AccessToken.ToString().Trim());

                    if (state.RefreshToken.IsNullOrEmpty())
                        _inXML.Append("<RefreshToken>" + _obj.GetDecryptText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), HttpContext.Current.Session["GoogleRefreshToken"].ToString()) + "</RefreshToken>");
                    else
                    {
                        _inXML.Append("<RefreshToken>" + state.RefreshToken.ToString().Trim() + "</RefreshToken>");
                        HttpContext.Current.Session["GoogleRefreshToken"] = _obj.GetEncrpytedText(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString(), state.RefreshToken.ToString().Trim());
                    }

                    if (_chresp != null)
                    {
                        _inXML.Append("<ChannelID>" + _chresp.Id + "</ChannelID>");
                        _inXML.Append("<ChannelEtag>" + _chresp.ETag + "</ChannelEtag>");

                        double _Expiration = 0;
                        double.TryParse(_chresp.Expiration, out _Expiration);

                        DateTime _dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                        _dtDateTime = _dtDateTime.AddMilliseconds(_Expiration).ToUniversalTime();

                        _inXML.Append("<ChannelExpiration>" + _dtDateTime.ToString() + "</ChannelExpiration>");
                        _inXML.Append("<ChannelResourceId>" + _chresp.ResourceId + "</ChannelResourceId>");
                        _inXML.Append("<ChannelResourceUri>" + _chresp.ResourceUri + "</ChannelResourceUri>");
                        HttpContext.Current.Session["GoogleChannelID"] = "";
                        HttpContext.Current.Session["GoogleChannelAddress"] = "";
                        HttpContext.Current.Session["GoogleChanelExpiration"] = "";
                    }

                    _inXML.Append("</UserToken>");

                    _outXML = _obj.CallMyVRMServer("SetUserToken", _inXML.ToString(), Application["RTC_ConfigPath"].ToString());

                    XmlDocument errDoc = null;
                    if (_outXML.IndexOf("<error>") >= 0)
                    {
                        errDoc = new XmlDocument();
                        errDoc.LoadXml(_outXML);
                        if (errDoc.SelectSingleNode("error/message") != null)
                        {
                            HttpContext.Current.Response.Redirect("~/en/genlogin.aspx");

                        }
                    }

                    Session.Remove("SERVICE_OBJECT");
                    Session["SERVICE_OBJECT"] = _service;
                    HttpContext.Current.Session["AUTH_STATE"] = "";

                    if (Session["isExpressUser"] != null)


                        if (Session["isExpressUser"].ToString() == "1")
                        {
                            if (Session["LandingPageLink"] != null && Session["LandingPageLink"].ToString() != "" && _obj.checkURLSessionStatus(Session["LandingPageLink"].ToString())) // ZD 100172
                            {
                                Response.Redirect("~/" + HttpContext.Current.Session["language"].ToString() + "/" + Session["LandingPageLink"].ToString());
                            }
                            else
                                Response.Redirect("~/" + HttpContext.Current.Session["language"].ToString() + "/ExpressConference.aspx?t=n"); //FB 1830
                        }


                    if (Session["LandingPageLink"] != null && Session["LandingPageLink"].ToString() != "" && _obj.checkURLSessionStatus(Session["LandingPageLink"].ToString())) // ZD 100172
                        Response.Redirect("~/" + HttpContext.Current.Session["language"].ToString() + "/" + Session["LandingPageLink"].ToString());
                    else
                        Response.Redirect("~/" + HttpContext.Current.Session["language"].ToString() + "/SettingSelect2.aspx?c=GH");

                }
                else
                    HttpContext.Current.Response.Redirect("~/en/genlogin.aspx?G=A");


                return null;
            }
            catch (System.Threading.ThreadAbortException ex)
            {
                _log.Trace("" + ex.StackTrace);
                return null;
            }
            catch (Exception e)
            {
                _log.Trace("" + e.StackTrace);
                return null;
            }


        }
        #endregion

    }
}
