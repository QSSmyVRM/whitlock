<%--ZD 100147 start--%>
<%--/* Copyright (C) 2014 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" Debug="true" Inherits="ns_MYVRM.MainAdministrator" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls"
    TagPrefix="mbcbb" %>
    
<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2779 Ends--%>


<script type="text/javascript" src="script/myprompt.js"></script>

<!-- JavaScript begin -->

<script language="JavaScript1.2" src="inc/functions.js"></script>

<script language="JavaScript">
<!--
    //ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    //ZD 100604 End
    //<%--FB 1490 Start--%>
    function fncheckTime() {
        var stdate = '';
        if (document.getElementById("systemEndTime_Text") && document.getElementById("systemStartTime_Text")) {
            stdate = GetDefaultDate('01/01/1901', '<%=((Session["timeFormat"] == null) ? "1" : Session["timeFormat"])%>');
            if (Date.parse(stdate + " " + document.getElementById("systemEndTime_Text").value) < Date.parse(stdate + " " + document.getElementById("systemStartTime_Text").value)) {
                alert("End Time Should be greater than Start Time."); //FB 2148
                document.getElementById("systemStartTime_Text").focus();
                return false;
            }
            else if (Date.parse(stdate + " " + document.getElementById("systemEndTime_Text").value) == Date.parse(stdate + " " + document.getElementById("systemStartTime_Text").value)) {
                alert("End Time Should be greater than Start Time.");
                document.getElementById("systemEndTime_Text").focus();
                return false;
            }
        }
        return true;
    }
    //<%--FB 1490 End--%>
    //FB 2486
    function toggle() {
        var ele = document.getElementById("toggleText");
        var text = document.getElementById("displayText");
        if (ele.style.display == "block" || ele.style.display == "") {
            ele.style.width = "100%";
            ele.style.display = "none";
            text.innerHTML = "More";
        }
        else {
            ele.style.display = "";
            ele.style.width = "100%";
            text.innerHTML = "Less";
        }
    }
    function open24() {
        t = (document.frmMainadminiatrator.Open24.checked) ? "none" : ''; //Edited For FF...
        for (var i = 1; i < 5; i++) {
            document.getElementById("Open24DIV" + i).style.display = t;
        }
        document.getElementById("systemStartTime_Text").style.width = "100px";
        document.getElementById("systemEndTime_Text").style.width = "100px";
    }


    function ValidateInput() {
        if ((document.getElementById("lstDefaultConferenceType").value == "2") && (document.getElementById("lstEnableAudioVideoConference").value == "0")) {
            alert("Please enable Audio/Video Conference first in order to make it default conference type.");
            document.getElementById("lstEnableAudioVideoConference").focus();
            return false;
        }
        if ((document.getElementById("lstDefaultConferenceType").value == "7") && (document.getElementById("lstEnableRoomConference").value == "0")) {
            alert("Please enable Room Conference first in order to make it default conference type.");
            document.getElementById("lstEnableRoomConference").focus();
            return false;
        }
        if ((document.getElementById("lstDefaultConferenceType").value == "6") && (document.getElementById("lstEnableAudioOnlyConference").value == "0")) {
            alert("Please enable Audio-Only Conference first in order to make it default conference type.");
            document.getElementById("lstEnableAudioOnlyConference").focus();
            return false;
        }
        if ((document.getElementById("lstDefaultConferenceType").value == "4") && (document.getElementById("p2pConfEnabled").value == "0")) {
            alert("Please enable P2P Conference first in order to make it default conference type.");
            document.getElementById("p2pConfEnabled").focus();
            return false;
        }
        //ZD 100719 - Start
        if ((document.getElementById("lstDefaultConferenceType").value == "8") && (document.getElementById("lstEnableHotdeskingConference").value == "0")) {
            alert("Please enable Hotdesking Conference first in order to make it default conference type.");
            document.getElementById("lstEnableHotdeskingConference").focus();
            return false;
        }
        //ZD 100719 - End
        //<%--FB 1490 Start--%>
        if (!fncheckTime())
            return false;
        //<%--FB 1490 End--%>

        return true;
    }

    //FB 2136
    function modedisplay() {
        var mode = document.getElementById("drpenablesecuritybadge");
        var type = document.getElementById("drpsecuritybadgetype");

        if (document.getElementById("drpenablesecuritybadge").value == "1") {
            document.getElementById("tdSecurityType").style.display = "block";
            type.style.display = "block";
            emaildisplay();
        }
        else {
            document.getElementById("tdSecurityType").style.display = "none";
            type.style.display = "none";
            document.getElementById("tdsecdeskemailid").style.visibility = "hidden";
        }
    }

    function emaildisplay() {
        var type = document.getElementById("drpsecuritybadgetype");

        if (document.getElementById("drpsecuritybadgetype").value == "2" || document.getElementById("drpsecuritybadgetype").value == "3") {
            document.getElementById("tdsecdeskemailid").style.visibility = "visible";
        }
        else {
            document.getElementById("tdsecdeskemailid").style.visibility = "hidden";
        }
    }
    //FB 2348 Start
    function modedisplay1() {
        if (document.getElementById("drpenablesurvey").value == "1") {
            document.getElementById("tdSurveyengine").style.visibility = "visible";
            document.getElementById("tdsurveyoption").style.visibility = "visible";
            modesurvey()

        }
        else {
            document.getElementById("divSurveytimedur").style.display = "none";
            document.getElementById("divSurveyURL").style.display = "none";
            document.getElementById("tdSurveyengine").style.visibility = "hidden";
            document.getElementById("tdsurveyoption").style.visibility = "hidden";
            ValidatorEnable(document.getElementById("ReqSurWebsiteURL"), false);
            ValidatorEnable(document.getElementById("RegSurWebsiteURL"), false);
            ValidatorEnable(document.getElementById("RegTimeDur"), false);

        }
    }
    function modesurvey() {
        if (document.getElementById("drpsurveyoption").value == "2") {
            document.getElementById("divSurveyURL").style.display = "block";
            document.getElementById("divSurveytimedur").style.display = "block";
            ValidatorEnable(document.getElementById("ReqSurWebsiteURL"), true);
            ValidatorEnable(document.getElementById("RegSurWebsiteURL"), true);
            ValidatorEnable(document.getElementById("RegTimeDur"), true);
        }
        else {
            document.getElementById("divSurveyURL").style.display = "none";
            document.getElementById("divSurveytimedur").style.display = "none";
            ValidatorEnable(document.getElementById("ReqSurWebsiteURL"), false);
            ValidatorEnable(document.getElementById("RegSurWebsiteURL"), false);
            ValidatorEnable(document.getElementById("RegTimeDur"), false);

        }
    }
    //FB 2347T
    function ChangeValidator() {
        if (document.getElementById("drpenablesurvey").value == "2" || document.getElementById("drpsurveyoption").value == "1") {
            ValidatorEnable(document.getElementById("ReqSurWebsiteURL"), false);
            ValidatorEnable(document.getElementById("RegSurWebsiteURL"), false);
            ValidatorEnable(document.getElementById("RegTimeDur"), false);
        }
    }
    // FB 2841 Start
    function ExpandAll() {
        var bool = "";
        if (document.getElementById("chkExpandCollapse").checked == true)
            bool = "0";
        else
            bool = "1"
        ExpandCollapse(document.getElementById("img_USROPT"), "trUSROPT", bool);
        ExpandCollapse(document.getElementById("img_CONFOPT"), "trCONFOPT", bool);
        ExpandCollapse(document.getElementById("img_CONFDEF"), "trCONFDEF", bool);
        ExpandCollapse(document.getElementById("img_CONFTYPE"), "trCONFTYPE", bool);
        ExpandCollapse(document.getElementById("img_FEAT"), "trFEAT", bool);
        ExpandCollapse(document.getElementById("img_AUD"), "trAUD", bool);
        ExpandCollapse(document.getElementById("img_CONFMAIL"), "trCONFMAIL", bool);
        if ('<%=Session["EnableNetworkFeatures"]%>' == "1") //FB 2993
            ExpandCollapse(document.getElementById("img_NETSWT"), "trNETSWT", bool);
        ExpandCollapse(document.getElementById("img_EPT"), "trEPT", bool);
        ExpandCollapse(document.getElementById("img_FLY"), "trFLY", bool);
        ExpandCollapse(document.getElementById("img_AUTO"), "trAUTO", bool);
        ExpandCollapse(document.getElementById("img_SYS"), "trSYS", bool);
        ExpandCollapse(document.getElementById("img_CONFSECDESK"), "trCONFSECDESK", bool);
        ExpandCollapse(document.getElementById("img_PIM"), "trPIM", bool);
        ExpandCollapse(document.getElementById("img_SUR"), "trSUR", bool);
        ExpandCollapse(document.getElementById("img_ADMOPT"), "trADMOPT", bool);
        ExpandCollapse(document.getElementById("img_ICP"), "trICP", bool); //FB 2724
        if ('<%=Session["WebexUserLimit"]%>' != "0") //ZD 100935
        ExpandCollapse(document.getElementById("img_WEBCONF"), "trWebCre", bool); //ZD 100221
        
    }
    // FB 2841 End
    function ExpandCollapse(img, str, frmCheck) {
        obj = document.getElementById(str);

        if (str == "trFLY" && frmCheck == true) // FB 2426
        {
            var drptopObj = document.getElementById("lstTopTier");
            var drpmiddleObj = document.getElementById("lstMiddleTier");
            if(drptopObj.options[drptopObj.selectedIndex] != "undefined" && drptopObj.options[drptopObj.selectedIndex] != null)//ZD 100719
                var selectop = drptopObj.options[drptopObj.selectedIndex].text;
            if(drpmiddleObj.options[drpmiddleObj.selectedIndex] != "undefined" && drpmiddleObj.options[drpmiddleObj.selectedIndex] != null)
                var selecmiddle = drpmiddleObj.options[drpmiddleObj.selectedIndex].text;
            if (selectop == "Please select..." && obj.style.display == "")
                return false;
            if (selecmiddle == "Please select..." && obj.style.display == "")
                return false;
        }
       //ZD 100068 start
        if (str == "trPIM" && frmCheck == true) 
        {
            var drptopObj1 = document.getElementById("lstVMRTopTier");
            var drpmiddleObj1 = document.getElementById("lstVMRMiddleTier");
            if (drptopObj1.options[drptopObj1.selectedIndex] != "undefined" && drptopObj1.options[drptopObj1.selectedIndex] != null)//ZD 100719
                var selectop1 = drptopObj1.options[drptopObj1.selectedIndex].text;
            if (drpmiddleObj1.options[drpmiddleObj1.selectedIndex] != "undefined" && drpmiddleObj1.options[drpmiddleObj1.selectedIndex] != null)
                var selecmiddle1 = drpmiddleObj1.options[drpmiddleObj1.selectedIndex].text;
            if (selectop1 == "Please select..." && obj.style.display == "")
                return false;
            if (selecmiddle1 == "Please select..." && obj.style.display == "")
                return false;
                
        }
       //ZD 100068 End
        if (obj != null)
        {
                if (frmCheck == "1")
                {
                    img.src = img.src.replace("minus", "plus");
                    obj.style.display = "none";
                    img.className = "0";// ZD 100419
                }
                else
                {
                    img.src = img.src.replace("plus", "minus");
                    obj.style.display = "";
                    img.className = "1"; // ZD 100419
                }
          }
         // FB 2841 start
        /*
        if (obj != null) {
            if (frmCheck == true) {
                if (document.getElementById("chkExpandCollapse").checked) {
                    img.src = img.src.replace("minus", "plus");
                    obj.style.display = "none";
                }
                else {
                    img.src = img.src.replace("plus", "minus");
                    obj.style.display = "";
                }
            }
            if (frmCheck == false) {
                if (img.src.indexOf("minus") >= 0) {
                    img.src = img.src.replace("minus", "plus");
                    obj.style.display = "none";
                }
                else {
                    img.src = img.src.replace("plus", "minus");
                    obj.style.display = "";
                    // FB 2565 Starts
                    if (str == "trCONFOPT") {
                        document.getElementById("trCONFDEF").style.display = "";
                        var imgobj = document.getElementById("img_CONFDEF");
                        imgobj.src = imgobj.src.replace("plus", "minus")

                        var idArray = new Array("CONFTYPE", "FEAT", "AUD", "CONFMAIL");
                        for (var k = 0; k < idArray.length; k++) {
                            document.getElementById("tr" + idArray[k]).style.display = "none";
                            imgobj = document.getElementById("img_" + idArray[k]);
                            imgobj.src = imgobj.src.replace("minus", "plus")
                        }
                    }
                    // FB 2565 Ends
                }
            }
        }
        */// FB 2841 End
        
        document.getElementById("systemStartTime_Text").style.width = "100px";
        document.getElementById("systemEndTime_Text").style.width = "100px";
    }
    //FB 2348 End

    function fnBufferOptions()//FB 2398
    {
        if (document.getElementById("EnableBufferZone").value == "1") {
            document.getElementById("trBufferOptions").style.display = "";//TCK  #100154
            //document.getElementById("trMCUBufferOptions").style.display = ""; //FB 2440  //TCK  #100154 //ZD 100085
            //document.getElementById("trForceMCUBuffer").style.display = ""; //FB 2440 //TCK  #100154
        }
        else {
            document.getElementById("trBufferOptions").style.display = "None"; //TCK  #100154
            //document.getElementById("trMCUBufferOptions").style.display = "None"; //FB 2440 //TCK  #100154 //ZD 100085
            //document.getElementById("trForceMCUBuffer").style.display = "None"; //FB 2440 //TCK  #100154
        }
    }

    //FB 2426 Start
    function Submit() {
        if (document.getElementById('drpenablesurvey').value == "0" || document.getElementById("drpsurveyoption").value == "1") {
            ValidatorEnable(document.getElementById("ReqSurWebsiteURL"), false);
            ValidatorEnable(document.getElementById("RegSurWebsiteURL"), false);
        }

    fnUpdatePosVertStatus(); // ZD 101019
        if (!Page_ClientValidate())//ZD 100381
            return Page_IsValid;
    	//ZD 100068 starts
        var topVal = document.getElementById("lstVMRTopTier").value;
        if (topVal == '-1') {            
            document.getElementById("reqTopTier1").style.display = 'block';            
            return false;
        }

        var MidVal = document.getElementById("lstVMRMiddleTier").value;        
        if (MidVal == '-1') {
            document.getElementById("reqMiddleTier1").style.display = 'block';
            return false;
        }
   		//ZD 100068 ends //ZD 100381 - Start
        var lsttoptier;
        var lstmiddletier;
        if(document.getElementById('lstTopTier')!= null)
             lsttoptier = document.getElementById('lstTopTier').value;
        if (document.getElementById('lstMiddleTier') != null)
            lstmiddletier = document.getElementById('lstMiddleTier').value;
        //FB 2501
        //ZD 100420

        if (lsttoptier == "-1") {
            document.getElementById('reqTopTier').style.display = 'block';
            document.getElementById('lstTopTier').focus();                      
            return false;
        }
        if (lstmiddletier == "-1") {
            document.getElementById('reqMiddleTier').style.display = 'block';
            document.getElementById('lstMiddleTier').focus();
            return false;
        }
        //ZD 100381 - End
        var SercureLaunch = document.getElementById('txtSecureLaunch').value;
        if (SercureLaunch < 0)
            return false;
        DataLoading(1); //ZD 100176
        
        //ZD 100433 Validation Part Starts
        var setupDur = 0; var mcupreStart = 0;
        if (document.getElementById("txtSetupTime") != null)
            setupDur = parseInt(document.getElementById("txtSetupTime").value, 10);
        if (document.getElementById("txtMCUSetupTime") != null)
            mcupreStart = parseInt(document.getElementById("txtMCUSetupTime").value, 10);

        if (setupDur != 0) {
            if (setupDur < mcupreStart && setupDur != mcupreStart) {
                document.getElementById("customMCUPreStart").style.display = 'block';
            }
            else {
                document.getElementById("customMCUPreStart").style.display = 'none';
            }
        }
        else {
            document.getElementById("customMCUPreStart").style.display = 'none';
        }

        var TearDown = 0; var mcupreEnd = 0;
        if (document.getElementById("txtTearDownTime") != null)
            TearDown = parseInt(document.getElementById("txtTearDownTime").value, 10);
        if (document.getElementById("txtMCUTearDownTime") != null)
            mcupreEnd = parseInt(document.getElementById("txtMCUTearDownTime").value, 10);
        if (TearDown != 0) {
            if (TearDown < mcupreEnd && TearDown != mcupreEnd) {
                document.getElementById("customMCUPreEnd").style.display = 'block';
            }
            else {
                document.getElementById("customMCUPreEnd").style.display = 'none';
            }
        }
        else {
            document.getElementById("customMCUPreEnd").style.display = 'none';
        }
        //ZD 100433 Validation Part End        
        //FB 3020
        if (document.getElementById("RangeDefaultConfDuration").style.display == 'inline' || document.getElementById("RegDefaultConfDuration").style.display == 'inline' || document.getElementById("RegScheduleLimit").style.display == 'inline') { //ZD 100899
            window.location.hash = "#";
            window.location.hash = "#topSpace";
            return false;
        }
        //ZD 100433 Validation Part Starts
        if (document.getElementById("customMCUPreStart").style.display == 'block' || document.getElementById("RegMCUSetupTime").style.display == 'inline' || document.getElementById("rangesetuptime").style.display == 'inline' || document.getElementById("RegtxtSetupTime").style.display == 'inline')//ZD 100433 
        {
            window.location.hash = "#";
            window.location.hash = "#DefaultPublic"; //Used to focus the cursor on error after submit the page.
            return false;
        }
        if (document.getElementById("customMCUPreEnd").style.display == 'block' || document.getElementById("RegMCUTearDownTime").style.display == 'inline' || document.getElementById("Rangeteardowntime").style.display == 'inline' || document.getElementById("RegtxtTearDownTime").style.display == 'inline')//ZD 100433
        {
            window.location.hash = "#";
            window.location.hash = "#DefaultPublic";
            return false;
        }
        //ZD 100433 Validation Part End
        //ZD 100781 Starts
        if (document.getElementById("RangePwdExpDays").style.display == 'inline' || document.getElementById("RegPwdExpDays").style.display == 'inline' || document.getElementById("reqExpDays").style.display == 'inline') {
            window.location.hash = "#";
            window.location.hash = "#topSpace";
            return false;
        }
        //ZD 100781 Ends
        /*
        var duration = document.getElementById('txtDefaultConfDuration').value;
        if (duration < 15 || duration > 1440)
            return false;
        
        */

        
         //FB 2988 Validation Part Starts//ZD 100085
//        if (document.getElementById("cmpNumbers").style.display == 'inline')
//            return false;
//        if (document.getElementById("cmpTeardown").style.display == 'inline')
//            return false;
		//FB 2988 Validation Part Ends
    }
    //FB 2426 End
    //FB 2595 Start
    function Securedisplay() {
        if (document.getElementById("drpSecureSwitch").value == "1") {
            document.getElementById("tdsecureadminaddress").style.visibility = "visible";
            document.getElementById("txtHardwareAdminEmail").style.visibility = "visible";
            document.getElementById("trNwtSwtiching").style.visibility = "visible";
            document.getElementById("trNwtCallBuffer").style.visibility = "visible";
        }
        else {
            document.getElementById("tdsecureadminaddress").style.visibility = "hidden";
            document.getElementById("txtHardwareAdminEmail").style.visibility = "hidden";
            document.getElementById("trNwtSwtiching").style.visibility = "hidden";
            document.getElementById("trNwtCallBuffer").style.visibility = "hidden";
        }
    }
    //FB 2595 End
//-->
</script>

<%--FB 2486--%>

<script type="text/javascript">

    function fnCheck(arg) {
        var srcID = document.getElementById(arg);
        var ckboxName = "chkmsg";
        var ctrlIDNo = arg.substring(arg.length, arg.length - 1)
        var drpName = srcID.id.replace(ctrlIDNo, "");
        var pVal = srcID.getAttribute("PreValue");
        var ckboxSel = document.getElementById(ckboxName + "" + ctrlIDNo);

        if (ckboxSel && ckboxSel.checked == false)
            return true;

        if (pVal == null)
            pVal = srcID.options[0].text;

        for (var i = 1; i <= 9; i++) {
            var ckbox = document.getElementById(ckboxName + "" + i);
            if (ckbox) {
                if (i == ctrlIDNo)
                    continue;

                if (ckbox.checked) {
                    var destDrpName = document.getElementById(drpName + i);
                    if (destDrpName) {
                        if (destDrpName.value == srcID.value) {
                            srcID.value = pVal;
                            alert("The selected time is already defined for other message");
                            if (ckboxSel)
                                ckboxSel.checked = false;
                            return false;
                        }
                    }
                }
            }
        }

        srcID.setAttribute("PreValue", srcID.value);
        return true;
    }

    //FB 2632
    function fnUpdateCngSupport() {
        var e = document.getElementById("drpCngSupport");
        var drpVal = e.options[e.selectedIndex].value;
        if (drpVal == 0) {
            document.getElementById("chkMeetandGreet").disabled = true;
            document.getElementById("chkOnSiteAVSupport").disabled = true;
            document.getElementById("chkConciergeMonitoring").disabled = true;
            document.getElementById("chkDedicatedVNOCOperator").disabled = true;
        }
        else {
            document.getElementById("chkMeetandGreet").disabled = false;
            document.getElementById("chkOnSiteAVSupport").disabled = false;
            document.getElementById("chkConciergeMonitoring").disabled = false;
            document.getElementById("chkDedicatedVNOCOperator").disabled = false;

        }
    }
    //FB 2588
    function fnChangeZulu() {
        document.getElementById("hdnZuluChange").value = "1";
    }

    //ZD 100176 start
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif' alt='Loading..'>";//ZD 100419
        else
            document.getElementById("dataLoadingDIV").innerHTML = "";
    }
    //ZD 100176 End


    //ZD 100263 Starts
    function AddRemoveWhiteList(opr) {
        var lstFileList = document.getElementById("lstWhiteList");
        var txtFileList = document.getElementById("txtWhiteList");
        var hdnFileList = document.getElementById("hdnFileWhiteList");

        if (opr == "Rem") {
            var i;
            for (i = lstFileList.options.length - 1; i >= 0; i--) {
                if (lstFileList.options[i].selected) {
                    hdnFileList.value = hdnFileList.value.replace(lstFileList.options[i].text, "").replace(/;/i, "");
                    lstFileList.remove(i);
                }
            }
        }
        else if (opr == "add") {
            if (txtFileList.value.replace(/\s/g, "") == "") //trim the textbox
            {
                txtFileList.value = "";
                return false;
            }

            if (!txtFileList.value.match(/^[a-zA-Z]+$/)) {
                alert("Please Enter only alphabets");
                return false;
            }

            var fileList = hdnFileList.value.split(';');

            for (i = 0; i < fileList.length; i++) {
                if (fileList[i].toLowerCase() == txtFileList.value.toLowerCase()) {
                    alert("Already Added in List");
                    return false;
                }
            }

            if (lstFileList.options.length > 0)
                hdnFileList.value = hdnFileList.value + ";";

            var option = document.createElement("Option");
            option.text = txtFileList.value;
            option.title = txtFileList.value;
            lstFileList.add(option);
            hdnFileList.value = hdnFileList.value + txtFileList.value;

            txtFileList.value = "";
            txtFileList.focus();
        }

        return false;
    }

    //ZD 100263 Start
    function Filewhitelistshowhide() {

        var enableWhiteList = document.getElementById("chkWhiteList");

        if ((enableWhiteList != null) && enableWhiteList.checked) {
            document.getElementById("txtWhiteList").style.display = "";
            document.getElementById("btnWhiteList").style.display = "";
            document.getElementById("trWhiteList").style.display = "";
        }
        else {
            document.getElementById("txtWhiteList").style.display = "None";
            document.getElementById("btnWhiteList").style.display = "None";
            document.getElementById("trWhiteList").style.display = "None";
        }
    }
    //ZD 100263 End

    //ZD 100433 start
    function fnCompareSetup(sender, args) {
        var setupDur = 0; var mcupreStart = 0;
        if(document.getElementById("txtSetupTime") != null)
            setupDur = parseInt(document.getElementById("txtSetupTime").value, 10);
        if(document.getElementById("txtMCUSetupTime") != null)    
            mcupreStart = parseInt(document.getElementById("txtMCUSetupTime").value, 10);

        if (setupDur != 0) {
            if (setupDur < mcupreStart && setupDur != mcupreStart) {
                document.getElementById("customMCUPreStart").style.display = 'block';
                return args.IsValid = false;
            }
            else {
                document.getElementById("customMCUPreStart").style.display = 'none';
                return args.IsValid = true;
            }
        }
        else {
            document.getElementById("customMCUPreStart").style.display = 'none';
            return args.IsValid = true;
        }
    }
    function fnCompareTear(sender, args) {
        var TearDown = 0; var mcupreEnd = 0;
        if(document.getElementById("txtTearDownTime") != null)
            TearDown = parseInt(document.getElementById("txtTearDownTime").value, 10);
        if(document.getElementById("txtMCUTearDownTime") != null)    
            mcupreEnd = parseInt(document.getElementById("txtMCUTearDownTime").value, 10);
        if (TearDown != 0) {
            if (TearDown < mcupreEnd && TearDown != mcupreEnd) {
                document.getElementById("customMCUPreEnd").style.display = 'block';
                return args.IsValid = false;
            }
            else {
                document.getElementById("customMCUPreEnd").style.display = 'none';
                return args.IsValid = true;
            }
        }
        else {
            document.getElementById("customMCUPreEnd").style.display = 'none';
            return args.IsValid = true;
        }

    }
    //ZD 100433 End
    //ZD 100707 Start
    function ShowHideVMROptions() {

        var EnableVMR = document.getElementById("drpEnableVMR");

        if ((EnableVMR != null) && EnableVMR.value == "1") {
            document.getElementById("tdPersonalVMR").style.display = "";
            document.getElementById("tddrpEnablePersonaVMR").style.display = "";
            document.getElementById("trEnableRoomExternalVRM").style.display = "";
        }
        else {
            document.getElementById("tdPersonalVMR").style.display = "None";
            document.getElementById("tddrpEnablePersonaVMR").style.display = "None";
            document.getElementById("trEnableRoomExternalVRM").style.display = "None";
        }
    }
    //ZD 100707 End
    //ZD 100781 Starts
    function fnUpdatePasswordExpEnable() {
        var e = document.getElementById("DrpEnablePwdExp");
        var drpVal = 0;
        if( e != null)
        drpVal =  e.options[e.selectedIndex].value;
        if (drpVal == 1) {
            document.getElementById("lblPwdExp").style.display = "";
            document.getElementById("tdDrpPwdExp").style.display = "";
        }
        else {
            document.getElementById("lblPwdExp").style.display = "None";
            document.getElementById("tdDrpPwdExp").style.display = "None";
        }
    }
     
    //ZD 100781 Ends
</script>

<!-- JavaScript finish -->
<html>
<body>
<div style="text-align: left">
    <form name="frmMainadminiatrator" id="frmMainadminiatrator" method="Post" action="mainadministrator.aspx"
    onsubmit="return ValidateInput()" language="JavaScript" runat="server">
    <asp:scriptmanager id="OrgOptionScriptManager" runat="server" asyncpostbacktimeout="600">
        </asp:scriptmanager>
    <input type="hidden" name="cmd" value="SetSystemDetails" />
    <input type="hidden" name="ClosedDay" value="" />
    <input type="hidden" id="helpPage" value="91" />
    <input type="hidden" id="Poly2MGC" runat="server" value="" />
    <input type="hidden" id="Poly2RMX" runat="server" value="" />
    <input type="hidden" id="CTMS2Cisco" runat="server" value="" />
    <input type="hidden" id="CTMS2Poly" runat="server" value="" />
    <input type="hidden" id="hdnSurURL" runat="server" value="" />
    <input type="hidden" id="hdnTimeDur" runat="server" value="" />
    <input type="hidden" id="hdnTierIDs" runat="server" value="" />
    <%--FB 2637--%>
    <input type="hidden" id="hdnZuluChange" runat="server" value="" />
    <%--FB 2588--%>
    <input type="hidden" name="hdnFileWhiteList" id="hdnFileWhiteList" runat="server" /> <%--ZD 100085--%>
    
    <%--ZD 101019 START--%>
    <input id="chkPosAVert" type="hidden" runat="server" value="0" />
    <input id="chkPosBVert" type="hidden" runat="server" value="0" />
    <input id="chkPosCVert" type="hidden" runat="server" value="0" />
    <input id="chkPosDVert" type="hidden" runat="server" value="0" />
    
    <input id="chkPosAHor" type="hidden" runat="server" value="" />
    <input id="chkPosBHor" type="hidden" runat="server" value="" />
    <input id="chkPosCHor" type="hidden" runat="server" value="" />
    <input id="chkPosDHor" type="hidden" runat="server" value="" />

    <%--ZD 101019 END--%>
    
    <div align="center">
        <h3>
            Organization Options</h3>
        <br />
        <asp:label id="errLabel" cssclass="lblError" runat="server" text="Label" visible="False"></asp:label>
    </div>
    <div id="dataLoadingDIV" align="center"></div> <%--ZD 100176--%>
    <div align="center">
        <table cellpadding="2" cellspacing="0" width="100%" border="0">
         <%--FB 2841 start--%>
        <tr>
         <td align="left" valign="top" style="margin-left:20px">
         <span class="subtitleblueblodtext">Expand All</span>
          <input id="chkExpandCollapse" type="checkbox" onclick="javascript:ExpandAll();" onkeydown="if(event.keyCode == 13){return false;}" /></td></tr><%--ZD 100420--%>
         <%--FB 2841 End--%>
            <tr>
            </tr>
            <tr>
                <td colspan="7" height="10">
                </td>
            </tr>
            <tr>
                <td colspan="7" align="left">
                    <table width="100%">
                        <tr>
                            <td width="100%" height="20" valign="bottom" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:imagebutton alternatetext="Expand/Collapse"  cssclass ="0" id="img_USROPT" runat="server" imageurl="image/loc/nolines_plus.gif" 
                                                height="25" width="25" vspace="0" hspace="0"  /> <%--FB 2841--%> <%--ZD 100419--%>
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext">User Options</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trUSROPT" runat="server" style="display: none;"><%-- FB 2841--%>
                <td colspan="7">
                    <table width="100%" border="0" cellpadding="5" style="margin-left: -10px;"> <%-- FB 2842--%>
                        <tr>
                            <td style="width: 1%;">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 25%;">
                                Show these Time Zones Only                               <%--FB 2926--%>
                                <%--<asp:ImageButton ID="Imgshowotime" valign="center" src="image/info.png" runat="server" ToolTip="Timezone control in overall application."/>--%>
                            </td>
                            <td align="left" style="width: 25%">
                                <asp:dropdownlist id="TimezoneSystems" runat="server" cssclass="altLong0SelectFormat"
                                    width="170px">
                                    </asp:dropdownlist>
                            </td>
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 27%">
                                Enable Departments
                                <%--<asp:ImageButton ID="Imgdeptuser" valign="center" src="image/info.png" runat="server" ToolTip="Control active user display in addressbook."/>--%>
                            </td>
                            <td align="left" style="width: 20%">
                                <asp:dropdownlist id="DrpDwnListDeptUser" runat="server" cssclass="alt2SelectFormat">
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="0">No</asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 2%;">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 25%;">
                                Enable Password Rule                                            <%--FB 2926--%>
                                <%--<asp:ImageButton ID="Imgpasswordrule" valign="center"  src="image/info.png" runat="server" ToolTip="Strong password rule for users"/>--%>
                            </td>
                            <td valign="top" align="left" style="width: 25%">
                                <asp:dropdownlist id="DrpDwnPasswordRule" runat="server" cssclass="alt2SelectFormat">
                                        <asp:ListItem Value="0">No</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            
                              <%--ZD 100164 START--%>
                              <td style="width: 1%">
                            </td>
                            <td id="tdEnableDisableadvanceduser" runat="server" align="left" class="blackblodtext" style="width: 27%">
                                Enable Advanced User Options
                             </td>
                            <td id="tdDrpDwnAdvUserOption" runat="server" align="left" style="width: 20%">
                                <asp:dropdownlist id="DrpDwnAdvUserOption" runat="server" cssclass="alt2SelectFormat">
                                        <asp:ListItem Value="1">No</asp:ListItem>
                                        <asp:ListItem Value="0">Yes</asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <td style="width: 1%">
                            </td>
                            <%--ZD 100164 END--%>
                        
                        </tr>
                         <%--ZD 100781 Starts --%>
                        <tr>
                            <td style="width: 2%;">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 25%;">
                                Enable Password Expiration            
                            </td>
                            <td valign="top" align="left" style="width: 25%">
                                <asp:dropdownlist id="DrpEnablePwdExp" runat="server" cssclass="alt2SelectFormat" onclick="JavaScript:fnUpdatePasswordExpEnable();">
                                        <asp:ListItem Value="0">No</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                               <td style="width: 1%">
                            </td>
                            <td id="lblPwdExp" runat="server" align="left" class="blackblodtext" style="width: 27%">
                                Password Expiration
                             </td>
                            <td id="tdDrpPwdExp" runat="server" align="left" style="width: 20%">
                                <asp:textbox id="txtPwdExpDays" runat="server" Maxlength="4" cssclass="altText" width="50px"> </asp:textbox>
                                (Months)
                                <asp:RangeValidator ID="RangePwdExpDays" Type="Integer"
                                        MinimumValue="1" MaximumValue="9999" Display="Dynamic" ControlToValidate="txtPwdExpDays"
                                        ValidationGroup="Submit" runat="server" ErrorMessage="Password expiration day cannot be zero."></asp:RangeValidator>
                                <asp:regularexpressionvalidator id="RegPwdExpDays" validationgroup="Submit"
                                    controltovalidate="txtPwdExpDays" display="dynamic" runat="server"
                                    errormessage="Numeric values only." validationexpression="\d+"></asp:regularexpressionvalidator>
                                <asp:requiredfieldvalidator id="reqExpDays" runat="server" controltovalidate="txtPwdExpDays"
                                    display="dynamic" errormessage="Please enter Password Expiration"></asp:requiredfieldvalidator>
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr>
                        <%--ZD 100781 Ends --%>
                    </table>
                </td>
            </tr>
            <tr style="display: none;">
                <td colspan="7" align="left">
                    <table width="100%">
                        <tr>
                            <td width="100%" height="20" valign="bottom" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:imagebutton alternatetext="Expand/Collapse" cssclass ="0"  id="img_ROOM" runat="server" imageurl="image/loc/nolines_minus.gif"
                                                height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext">Room Options</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trROOM" runat="server" style="display: none;">
                <td colspan="8">
                    <table border="0" width="100%">
                        <tr>
                            <td style="width: 6%;">
                            </td>
                            <td align="left" valign="top" style="width: 185px;" class="blackblodtext">
                                Room Tree Expand Level
                            </td>
                            <td style="width: 0px">
                            </td>
                            <td valign="top" align="left">
                                <%--  <asp:listitem value="0">
                                </asp:listitem>--%>
                                <asp:dropdownlist id="lstRoomTreeLevel" runat="server" cssclass="altLong0SelectFormat"
                                    width="205px">
                                        <asp:ListItem Value="1">Expanded - Top Tier Only</asp:ListItem>
                                        <asp:ListItem Value="2">Expanded - Middle Tier Only</asp:ListItem>
                                        <asp:ListItem Value="3" Selected="True">Expanded - All Levels</asp:ListItem>
                                        <asp:ListItem Value="list">List View</asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <td colspan="4">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:imagebutton id="img_CONFOPT" alternatetext="Expand/Collapse" cssclass ="0"  runat="server" imageurl="image/loc/nolines_plus.gif"
                                                height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext">
                                                <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                                    {%>Hearing Options<%}
                                                else
                                                { %>Conference Options<%}%></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trCONFOPT" runat="server" style="display: none">
                <td colspan="7">
                    <table border="0" width="100%" cellpadding="5">
                        <tr>
                            <td colspan="7" align="left" style="height: 49px">
                                <table width="100%">
                                    <tr>
                                        <td width="100%" height="20" align="left">
                                            <table>
                                                <tr>
                                                    <td> 
                                                        <asp:imagebutton id="img_CONFDEF" alternatetext="Expand/Collapse"  cssclass ="0" runat="server" imageurl="image/loc/nolines_plus.gif"
                                                            height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                                    </td>
                                                    <td valign="top">
                                                        <span class="subtitleblueblodtext">Default Settings </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trCONFDEF" runat="server" style="display: none">
                            <td colspan="7">
                                <table border="0" width="100%" cellpadding="5" style="margin-left:-4px"> <%-- FB 2842--%>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" class="blackblodtext" rowspan="1" style="width: 25%;" valign="top">
                                            <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                                      {%>Default hearing type<%}
                                                      else
                                                      { %>Default Conference Type<%}%><%--added for FB 1428 Start--%>  <%--FB 2926--%>
                                            <%--<asp:ImageButton ID="Imgdeconftype"  valign="center" src="image/info.png" runat="server" ToolTip="Default all conference to selected type."/>--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 25%;">
                                            <asp:dropdownlist id="lstDefaultConferenceType" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Selected="True" Value="6">Audio-Only</asp:ListItem>
                                                    <asp:ListItem Value="2">Audio/Video</asp:ListItem>
                                                    <asp:ListItem Value="4">Point-to-Point</asp:ListItem>
                                                    <asp:ListItem Value="7">Room-Only</asp:ListItem>
                                                    <asp:ListItem Value="8">Hotdesking</asp:ListItem><%--ZD 100719--%>
                                                </asp:dropdownlist>
                                        </td>
                                        <td align="right" style="width: 1%;" valign="top">
                                        </td>
                                        <td style="width: 27%;" align="left" class="blackblodtext">
                                            <asp:label id="Label7" runat="server" text="Default Line Rate"></asp:label>
                                            <%--<asp:ImageButton ID="Imgdelinerate"  valign="center" src="image/info.png" runat="server" ToolTip="Default linerate to all calls."/>--%>
                                        </td>
                                        <td style="width: 20%;" align="left">
                                            <asp:dropdownlist cssclass="altSelectFormat" width="120px" id="lstLineRate" runat="server"
                                                datatextfield="LineRateName" datavaluefield="LineRateID">
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                            <span style="align: left">Default Conference Duration</span>            <%--FB 2926--%>
                                            <%--<asp:ImageButton ID="Imgdeconfdur"  valign="center" src="image/info.png" runat="server" ToolTip="Default all calls to given duration."/>--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 25%">
                                            <asp:textbox id="txtDefaultConfDuration" runat="server" cssclass="altText" width="50px"> </asp:textbox>
                                            (mins)
                                            <asp:RangeValidator ID="RangeDefaultConfDuration" Type="Integer"
                                                    MinimumValue="15" MaximumValue="1440" Display="Dynamic" ControlToValidate="txtDefaultConfDuration"
                                                    ValidationGroup="Submit" runat="server" ErrorMessage="Conference duration range 15 to 1440 minutes."></asp:RangeValidator>
                                            <asp:regularexpressionvalidator id="RegDefaultConfDuration" validationgroup="Submit"
                                                controltovalidate="txtDefaultConfDuration" display="dynamic" runat="server"
                                                errormessage="Numeric values only." validationexpression="\d+"></asp:regularexpressionvalidator>
                                            <asp:requiredfieldvalidator id="reqdefaultduration" runat="server" controltovalidate="txtDefaultConfDuration"
                                                display="dynamic" errormessage="Conference duration range 15 to 1440 minutes"></asp:requiredfieldvalidator>
                                            <%--FB 2635--%>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 27%;" class="blackblodtext" valign="top"><%--FB 2934--%>
                                            Active Message Delivery
                                            <%--<asp:ImageButton ID="Imgmsgolay"  valign="center"  src="image/info.png" runat="server" ToolTip="Default conference end message for Audio/Video calls."/>--%>
                                        </td>
                                        <td valign="top" style="width: 20%;" align="left">
											<%--ZD 100420--%>
                                            <%--<asp:button width="70%" runat="server" id="btmTxtmsgPopup" cssclass="altMedium0BlueButtonFormat"
                                                text="Manage" />--%>
                                                <button width="70%" runat="server" id="btmTxtmsgPopup" class="altMedium0BlueButtonFormat">Manage</button>
											<%--ZD 100420--%>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <%--ZD 100899 start--%>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                            <span style="align: left">Schedule Limit</span>          
                                        </td>
                                        <td valign="top" align="left" style="width: 25%">
                                            <asp:textbox id="txtScheduleLimit" runat="server" cssclass="altText" width="50px"> </asp:textbox>
                                            (Months)
                                            <asp:regularexpressionvalidator id="RegScheduleLimit" validationgroup="Submit"
                                                controltovalidate="txtScheduleLimit" display="dynamic" runat="server"
                                                errormessage="Numeric values only." validationexpression="\d+"></asp:regularexpressionvalidator>
                                        </td>
                                    </tr>
                                    <%--ZD 100899 start--%>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7" align="left" style="height: 49px">
                                <table width="100%">
                                    <tr>
                                        <td width="100%" height="20" align="left">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:imagebutton id="img_CONFTYPE" alternatetext="Expand/Collapse" cssclass ="0" runat="server" imageurl="image/loc/nolines_plus.gif"
                                                            height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>                                                
                                                    </td>
                                                    <td valign="top">
                                                        <span class="subtitleblueblodtext">Types </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trCONFTYPE" runat="server" style="display: none">
                            <td colspan="7">
                                <table border="0" width="100%" cellpadding="5" style="margin-left:-4px"> <%-- FB 2842--%>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" valign="top">
                                            <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                                      {%>Enable audio/video hearing<%}
                                                      else
                                                      { %>Enable Audio/Video Conferences<%}%><%--added for FB 1428 Start--%>
                                            <%--<asp:ImageButton ID="Imgeavconf" valign="center" src="image/info.png" runat="server" ToolTip="Default all calls to audio/video type."/>--%>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left">
                                            <asp:dropdownlist id="lstEnableAudioVideoConference" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 27%;" class="blackblodtext" rowspan="1" valign="top">
                                            <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                                      {%>Enable audio only hearing<%}
                                                      else
                                                      { %>Enable Audio-Only Conferences<%}%><%--added for FB 1428 Start--%>
                                            <%--<asp:ImageButton ID="Imgeaconf" valign="center"  src="image/info.png" runat="server" ToolTip="Default all calls to audio type."/>--%>
                                        </td>
                                        <td style="width: 20%;" valign="top" align="left">
                                            <asp:dropdownlist id="lstEnableAudioOnlyConference" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" valign="top">
                                            <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                                      {%>Enable room hearing<%}
                                                      else
                                                      { %>Enable Room-Only Conferences<%}%>
                                            <%--<asp:ImageButton ID="Imgeroomconf" valign="center"  src="image/info.png" runat="server" ToolTip="Default all calls to room type."/></strong>--%>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left">
                                            <asp:dropdownlist id="lstEnableRoomConference" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 27%;" class="blackblodtext" valign="top">
                                            <span style="align: left">
                                                <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                                      {%>Enable point-to-point hearing<%}
                                                      else
                                                      { %>Enable Point-to-Point Conferences<%}%><%--added for FB 1428 Start--%></span>
                                            <%--<asp:ImageButton ID="Imgppconf" valign="center" src="image/info.png" runat="server" ToolTip="Default all calls to point-to-point type."/>--%>
                                        </td>
                                        <td valign="top" style="width: 20%;" align="left">
                                            <%--FB 2430--%>
                                            <asp:dropdownlist id="p2pConfEnabled" runat="server" cssclass="alt2SelectFormat"
                                                onclick="javascript:ChangeEnableSmartP2P();">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td style="width: 25%;" align="left" class="blackblodtext">
                                            <asp:label id="lblEnableSmartP2P" runat="server" text="Enable Smart Point-to-Point"></asp:label>    <%--FB 2926--%>
                                            <%--<asp:ImageButton ID="Imgesmartp2p" valign="center"  src="image/info.png" runat="server" ToolTip="Default all calls to point-to-point when only two endpoints."/>--%>
                                        </td>
                                        <td style="width: 25%;" align="left">
                                            <asp:dropdownlist id="lstEnableSmartP2P" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td><%--ZD 100719 Start--%>
                                        <td align="left" style="width: 27%;" class="blackblodtext" valign="top">
                                            Enable Hotdesking Conferences                                            
                                        </td>
                                        <td valign="top" style="width: 20%;" align="left">
                                            <asp:dropdownlist id="lstEnableHotdeskingConference" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                            </asp:dropdownlist>
                                        </td>
                                         <td style="width: 1%;">
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>                                        
                                        <%--FB 2870 Start--%>
                                        <td align="left" style="width: 25%;" class="blackblodtext" valign="top">
                                            <span style="align: left">
                                                CTS Numeric ID </span>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left">
                                            <asp:DropDownList id="lstEnableNumericID" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem value="0">No</asp:ListItem>
                                                    <asp:ListItem value="1">Yes</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>                                        
                                        <%--FB 2870 End--%>                                       
                                        <%--ZD 100719 End--%>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7" align="left" style="height: 49px">
                                <table width="100%">
                                    <tr>
                                        <td width="100%" height="20" align="left">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:imagebutton id="img_FEAT" alternatetext="Expand/Collapse" cssclass ="0" runat="server" imageurl="image/loc/nolines_plus.gif"
                                                            height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                                    </td>
                                                    <td valign="top">
                                                        <span class="subtitleblueblodtext">Features </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trFEAT" runat="server" style="display: none">
                            <td colspan="7">
                                <table border="0" width="100%" cellpadding="5" style="margin-left:-2px" > <%-- FB 2842--%>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td style="width: 25%;" align="left" class="blackblodtext">
                                            <asp:label id="lblEnablePublicConf" runat="server" text="Enable Public Conference"></asp:label>     <%--FB 2926--%>
                                            <%--<asp:ImageButton ID="Imgenpublicconf" valign="center" src="image/info.png" runat="server" ToolTip="Public conference feature will be enabled."/>--%>
                                        </td>
                                        <td style="width: 25%;" align="left">
                                            <asp:dropdownlist id="lstEnablePublicConf" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 27%;" valign="top" class="blackblodtext">
                                            Enable Participant Registration
                                            <%--<asp:ImageButton ID="Imgeopenfreg" valign="center"  src="image/info.png" runat="server" ToolTip="All public calls will be open for registration based on this switch."/>--%>
                                        </td>
                                        <td valign="top" style="width: 20%;" align="left">
                                            <asp:dropdownlist id="DynamicInviteEnabled" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" valign="top" class="blackblodtext">
                                            <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                                      {%>Default all hearing to public<%}
                                                      else
                                                      { %>Default All Conferences to Public<%}%><%--added for FB 1428 Start--%>
                                            <%--<asp:ImageButton ID="Imgdeconfpublic" valign="center"  src="image/info.png" runat="server" ToolTip="All calls will be default to public/private"/>--%>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left">
                                            <asp:dropdownlist id="DefaultPublic" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <%--FB 2780--%>
                                        <td align="left" style="width: 27%" class="blackblodtext" valign="top">
                                            Enable Start Mode
                                        </td>
                                        <td style="width: 20%" align="left">
                                            <asp:dropdownlist id="drpEnableStartMode" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;"></td> <%-- FB 2842--%>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                            <div id="tdMaxParty" runat="server" style="display: none">
                                                Max. Public Virtual Meeting Room Parties  <%--ZD 100806--%>
                                                <%--<asp:ImageButton ID="ImageButton1" valign="center"  src="image/info.png" runat="server" ToolTip="Maximum VMR parties allowed to create conference"/>--%>
                                            </div>
                                        </td>
                                        <td id="Td1" valign="top" style="width: 25%;" align="left">
                                            <div id="tdMaxPartyCount" runat="server" style="display: none">
                                                <asp:textbox id="txtMaxVMRParty" runat="server" cssclass="altText" width="50px" maxlength="4"> </asp:textbox>
                                                <asp:regularexpressionvalidator id="RegularExpressionValidator6" validationgroup="Submit"
                                                    controltovalidate="txtMaxVMRParty" display="dynamic" runat="server" setfocusonerror="true"
                                                    errormessage="Numeric values only." validationexpression="\d+"></asp:regularexpressionvalidator>
                                            </div>
                                        </td>
                                        <%--FB 2609 Start--%>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 27%;" class="blackblodtext" rowspan="1" valign="top"> <%-- FB 2842--%>
                                            Meet & Greet Buffer
                                        </td>
                                        <td id="Td2" valign="top" style="width: 20%;" align="left"> <%-- FB 2842--%>
                                            <asp:textbox id="txtMeetandGreetBuffer" runat="server" cssclass="altText" width="50px"
                                                maxlength="4"> </asp:textbox>
                                            (hrs)
                                            <asp:regularexpressionvalidator id="regmeetandgreet" validationgroup="Submit" controltovalidate="txtMeetandGreetBuffer"
                                                display="dynamic" runat="server" setfocusonerror="true" errormessage="Numeric values only."
                                                validationexpression="\d+"></asp:regularexpressionvalidator>
                                        </td>
                                        <%--FB 2609 End--%>
                                        <td style="width: 1%;"></td> <%-- FB 2842--%>
                                    </tr>
                                    <tr id="trRecurrence" runat="server"> <%--ZD 100518--%>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" class="blackblodtext" style="width: 25%">
                                           Enable Recurring Conferences
                                            <%--<asp:ImageButton ID="Imgrecurrconf" valign="center"  src="image/info.png" runat="server" ToolTip="Recurrence feature will be enabled for calls."/>--%>
                                        </td>
                                        <td align="left" style="width: 25%">
                                            <asp:dropdownlist id="RecurEnabled" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" class="blackblodtext" style="width: 27%;" rowspan="1" valign="top">
                                            <%--FB 2052 - Start--%>
                                            Enable Special Recurrence
                                            <%--<asp:ImageButton ID="Imgesplrecurr" valign="baseline"  src="image/info.png" runat="server" ToolTip="Special recurrence will be enabled for calls."/>--%>
                                        </td>
                                        <td valign="top" style="width: 20%;" align="left">
                                            <asp:dropdownlist id="DrpDwnListSpRecur" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" class="blackblodtext" style="width: 25%;" rowspan="1" valign="top">
                                            Enable VIP Conference
                                            <%--<asp:ImageButton ID="Imgvipconf" valign="center"   src="image/info.png" runat="server" ToolTip="VIP mode will be enabled."/>--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 25%;">
                                            <asp:dropdownlist id="DrpVIP" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 27%;" class="blackblodtext" rowspan="1" valign="top">
                                            Enable "Start Now" Conferences
                                            <%--<asp:ImageButton ID="Imgimmconf" valign="center"  src="image/info.png" runat="server" ToolTip="Switch to enable start now conference features."/>--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 20%;">
                                            <asp:dropdownlist id="lstEnableImmediateConference" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td style="width: 25%;" align="left" class="blackblodtext">
                                            <asp:label id="lblEnableConfPassword" runat="server" text="Enable Conference Password"></asp:label>     <%--FB 2926--%>
                                            <%--<asp:ImageButton ID="ImgEconfpass" valign="center"  src="image/info.png" runat="server" ToolTip="Enable conference password feature for calls."/>--%>
                                        </td>
                                        <td style="width: 25%;" align="left">
                                            <asp:dropdownlist id="lstEnableConfPassword" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 27%;" class="blackblodtext" rowspan="1" valign="top">
                                            Require Unique Conference Password
                                            <%--<asp:ImageButton ID="ImgEuniquepass" valign="center" src="image/info.png" runat="server" ToolTip="Switch will allow to set unique password for calls."/>--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 20%;">
                                            <asp:dropdownlist id="DrpUniquePassword" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                            Audio/Video Rooms Shown for Room-Only
                                            <%--<asp:ImageButton ID="Imgvidroomconf" valign="center"  src="image/info.png" runat="server" ToolTip="Switch to control Video room display for room only conference."/>--%>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left">
                                            <asp:dropdownlist id="DrpDwnDedicatedVideo" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 27%;" class="blackblodtext" rowspan="1" valign="top">
                                            Telepresence Rooms Shown for Room-Only
                                            <%--<asp:ImageButton ID="Imgtelepresenceroomconf" valign="center" src="image/info.png" runat="server" ToolTip="Switch to control Telepresence room display for room only conference."/>--%>
                                        </td>
                                        <td valign="top" style="width: 20%;" align="left">
                                            <asp:dropdownlist id="DrpDwnFilterTelepresence" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                            Enable Room Service Type        <%--FB 2926--%>
                                            <%--<asp:ImageButton ID="Imgeroomser" valign="center"  src="image/info.png" runat="server" ToolTip="Room service type feature can be enabled"/>--%>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left">
                                            <asp:dropdownlist id="lstEnableRoomServiceType" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td id="trBuffer" runat="server" align="left" style="width: 27%;" class="blackblodtext" valign="top"> <%--ZD 100518--%>
                                            <span style="align: left">Enable Conference Buffer Times</span>
                                            <%--<asp:ImageButton ID="Imgbuffzone" valign="center" src="image/info.png" runat="server" ToolTip="Buffer period can be set to calls."/>--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 20%">
                                            <asp:dropdownlist id="EnableBufferZone" runat="server" cssclass="alt2SelectFormat"
                                                onclick="javascript:fnBufferOptions()">
                                                    <%-- FB 2398 --%>
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr id="trBufferOptions" runat="server">
                                        <%-- FB 2398 --%>
                                        <td style="width: 1%">
                                        </td>
                                        <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                            <span style="align: left">Setup Time</span> <%--FB 2926--%>
                                            <%--<asp:ImageButton ID="Imgsetuptime" valign="center"  src="image/info.png" runat="server" ToolTip="Default pre start time can be set for all calls."/>--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 25%">
                                            <asp:textbox id="txtSetupTime" runat="server" cssclass="altText" width="50px"> </asp:textbox>
                                            (mins)
                                            <asp:rangevalidator id="rangesetuptime" setfocusonerror="true" type="Integer" minimumvalue="0"
                                                maximumvalue="60" display="Dynamic" controltovalidate="txtSetupTime" runat="server"
                                                errormessage="Setup Time is not allowed more than 60 mins"></asp:rangevalidator> <%--FB 2926--%>
                                            <asp:regularexpressionvalidator id="RegtxtSetupTime" validationgroup="Submit" controltovalidate="txtSetupTime"
                                                display="dynamic" runat="server" setfocusonerror="true" errormessage="Numeric values only."
                                                validationexpression="\d+"></asp:regularexpressionvalidator>
                                                <asp:CustomValidator id="cussetup" Enabled="true"  Display="Dynamic"
                                            ControlToValidate="txtSetupTime"  runat="server"  ClientValidationFunction="fnCompareSetup"></asp:CustomValidator><%-- ZD 100433--%>
                                        </td>
                                        <td style="width: 1%">
                                        </td>
                                        <td align="left" class="blackblodtext" rowspan="1" valign="top" width="27%">
                                            Tear-Down Time <%--FB 2926--%>
                                            <%--<asp:ImageButton ID="Imgteardowntime" valign="center"  src="image/info.png" runat="server" ToolTip="Default post conference time can be set for all calls"/>--%>
                                        </td>
                                        <td style="height: 33px; width: 20%" align="left" valign="top">
                                            <asp:textbox id="txtTearDownTime" runat="server" cssclass="altText" width="50px"> </asp:textbox>
                                            (mins)
                                            <asp:rangevalidator id="Rangeteardowntime" setfocusonerror="true" type="Integer"
                                                minimumvalue="0" maximumvalue="60" display="Dynamic" controltovalidate="txtTearDownTime"
                                                runat="server" errormessage="Teardown Time is not allowed more than 60 mins"></asp:rangevalidator>  <%--FB  2926--%>
                                            <asp:regularexpressionvalidator id="RegtxtTearDownTime" validationgroup="Submit"
                                                controltovalidate="txtTearDownTime" display="dynamic" runat="server" setfocusonerror="true"
                                                errormessage="Numeric values only." validationexpression="\d+"></asp:regularexpressionvalidator>
                                              <asp:CustomValidator id="cusTearDown" Enabled="true" Display="Dynamic"
                                            ControlToValidate="txtTearDownTime"  runat="server"  ClientValidationFunction="fnCompareTear"></asp:CustomValidator><%-- ZD 100433--%>
                                        </td>
                                        <td width="1%">
                                        </td>
                                    </tr>
                                    <tr id="trMCUBufferOptions" runat="server">
                                        <%-- FB 2440 --%>
                                        <td style="width: 1%">
                                        </td>
                                        <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                            <span style="align: left">MCU Pre-Start Time</span>
                                            <%--<asp:ImageButton ID="Imgmcuprestart" valign="center"  src="image/info.png" runat="server" ToolTip="Default pre lauch calls to MCU"/>--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 25%" >
                                            <span style="white-space: nowrap;">
                                                <asp:TextBox id="txtMCUSetupTime" runat="server" cssclass="altText" width="50px"> </asp:TextBox>
                                                (mins)
                                                &nbsp; <%--FB 2998--%>
                                                <span class="blackblodtext"> Display</span>
                                                 <asp:dropdownlist id="MCUSetupDisplay" runat="server" cssclass="alt2SelectFormat" width="70px">
                                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                </asp:dropdownlist>            
                                            </span>                              
                                            <br /><%--ZD 100405--%>
                                           <%-- <asp:RangeValidator id="RangeValidator1" type="Integer" minimumvalue="-15"
                                                maximumvalue="15" display="Dynamic" controltovalidate="txtMCUSetupTime" runat="server"
                                                errormessage="MCU pre start time is not allowed more than 15 mins" validationgroup="Submit"></asp:RangeValidator>--%>
											<%-- ZD 100433--%>
                                            <asp:RegularExpressionValidator id="RegMCUSetupTime" validationgroup="Submit"
                                                controltovalidate="txtMCUSetupTime" display="dynamic" runat="server"
                                                errormessage="Numeric values only." validationexpression="^-{0,1}\d+$"></asp:RegularExpressionValidator>
                                            <%--FB 2998 Validation Part starts //ZD 100085--%>
											<%--<asp:CompareValidator  id="cmpNumbers" Enabled="true"  ErrorMessage="MCU Pre start time should be less than or equal to Setup Time." display="dynamic"
                                             ControlToCompare="txtSetupTime" ControlToValidate="txtMCUSetupTime" Operator="LessThanEqual" runat="server" Type="Integer"
                                              ValidationGroup="Submit" SetFocusOnError="true"  EnableClientScript="true" ></asp:CompareValidator>--%>
                                             <%--FB 2998 Validation Part Ends--%>
                                             <asp:CustomValidator id="customMCUPreStart" Enabled="true"  ErrorMessage="MCU Pre start time should be less than or equal to Setup Time." Display="Dynamic"
                                             ControlToValidate="txtMCUSetupTime"  runat="server"  ClientValidationFunction="fnCompareSetup" ValidationGroup="Submit" ></asp:CustomValidator><%-- ZD 100433--%>
                                        </td>
                                        <td style="width: 1%">
                                        </td>
                                        <td align="left" class="blackblodtext" rowspan="1" valign="top" width="27%">
                                            MCU Pre-End Time
                                            <%--<asp:ImageButton ID="Imgmcuprend" valign="center"  src="image/info.png" runat="server" ToolTip="Default post conference calls to MCU"/>--%>
                                        </td>
                                        <td style="height: 33px; width: 20%" align="left" valign="top" >
                                        <span style="white-space: nowrap;">
                                            <asp:textbox id="txtMCUTearDownTime" runat="server" cssclass="altText" width="50px"> </asp:textbox>
                                            (mins)
                                            &nbsp;<%--FB 2998--%>
                                            <span class="blackblodtext"> Display</span>
                                             <asp:dropdownlist id="MCUTearDisplay" runat="server" cssclass="alt2SelectFormat" width = "70px">
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                </asp:dropdownlist>                                         
                                             </span>
                                            <br /> <%--ZD 100405--%>
                                           <%-- <asp:rangevalidator id="RangeValidator2" type="Integer" minimumvalue="-15"
                                                maximumvalue="15" display="Dynamic" controltovalidate="txtMCUTearDownTime" runat="server"
                                                errormessage="MCU pre end time is not allowed more than 15 mins" validationgroup="Submit"></asp:rangevalidator>--%>
											<%-- ZD 100433--%>
                                            <asp:regularexpressionvalidator id="RegMCUTearDownTime" validationgroup="Submit"
                                                controltovalidate="txtMCUTearDownTime" display="dynamic" runat="server" 
                                                errormessage="Numeric values only." validationexpression="^-{0,1}\d+$"></asp:regularexpressionvalidator>
                                            <%--FB 2998 Validation Part starts //ZD 100085--%>
                                            <%--<asp:CompareValidator  id="cmpTeardown" Enabled="true"  ErrorMessage="MCU Pre-End Time should be less than or equal to Tear-Down Time." display="dynamic"
                                             ControlToCompare="txtTearDownTime" ControlToValidate="txtMCUTearDownTime" Operator="LessThanEqual" runat="server" Type="Integer"
                                              ValidationGroup="Submit" SetFocusOnError="true"  EnableClientScript="true" ></asp:CompareValidator>--%>
                                            <%--FB 2998 Validation Part Ends--%>
                                            <asp:CustomValidator id="customMCUPreEnd" Enabled="true" ErrorMessage="MCU Pre-End Time should be less than or equal to Tear-Down Time." Display="Dynamic"
                                            ControlToValidate="txtMCUTearDownTime"  runat="server" ClientValidationFunction="fnCompareTear" ValidationGroup="Submit"></asp:CustomValidator><%-- ZD 100433--%>
                                        </td>
                                        <td width="1%">
                                        </td>
                                    </tr>
                                       <%--FB 2670 START--VNOC Operator design--%>
                                    <tr>
                                        <td style="width: 1%">
                                        </td>
                                        <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                            <span style="align: left">Enable Conference Support</span> <%--FB 3023--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 25%">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <input id="chkOnsiteAV" type="checkbox" runat="server" />
                                                        <strong style="align: left">On-Site A/V Support</strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input id="chkConciergeMonitor" type="checkbox" runat="server" />
                                                        <strong style="align: left">Call Monitoring</strong> <%--FB 3023--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="width: 1%">
                                        </td>
                                        <td valign="top" align="left" style="width: 27%"> <%-- FB 2842--%>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <input id="chkMeetandGret" type="checkbox" runat="server" />
                                                        <strong style="align: left">Meet and Greet</strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input id="chkDedicatedVNOC" type="checkbox" runat="server" />
                                                        <strong style="align: left">Dedicated VNOC Operator</strong>
                                                    </td>
                                                    
                                               </tr>
                                            </table>
                                        </td>
									     <%-- FB 2842 start--%>
                                        <td style="width: 20%">
                                        </td><td style="width: 1%">
                                         </td>
										  <%-- FB 2842 end--%>
                                     </tr>
                                    <%--FB 2670 END--VNOC Operator design--%>
                                    <%--FB 2637 Starts--%>
                                    <tr>
                                        <td style="width: 1%">
                                        </td>
                                        <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                            <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                                      {%>Auto-accept modified hearing<%}
                                                      else
                                                      { %>Auto-Approve Conference Modifications<%}%><%--added for FB 1428 Start--%>
                                            <%--<asp:ImageButton ID="Imgaamodconf" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable auto-accept modified conference."/>--%>
                                        </td>
                                        <td align="left" style="width: 25%" valign="top">
                                            <asp:dropdownlist id="AutoAcpModConf" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td>
                                        </td>
                                        <td align="left" class="blackblodtext" valign="top" width="27%">
                                            Alert on Tier1 Selection
                                        </td>
                                        <td valign="top" style="width: 20%;" align="left"> <%-- FB 2842--%>
                                            <asp:listbox runat="server" focusable="false" id="lstTier1" cssclass="altSelectFormat" datatextfield="Name"
                                                datavaluefield="ID" rows="6" selectionmode="Multiple"></asp:listbox>
                                            <%--ZD 100422 Start--%>
                                            <br /><span style='color: #666666;'>* Press Ctrl + Click to Select Multiple Tiers</span>
                                        </td>
                                        <td style="width: 1%;">
                                        </td><%-- FB 2842 --%>
                                    </tr>
                                    <tr style="display: none">
                                        <td style="width: 1%;"> <%-- FB 2842 --%>
                                        </td>
                                        <td width="25%" align="left" class="blackblodtext">
                                            <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                                      {%>Enable Hearing Real-time Display?<%}
                                                      else
                                                      { %>Enable Conference Monitoring<%}%><%--added for FB 1428 Start--%>
                                        </td>
                                        <td align="left" style="width: 25%">
                                            <asp:dropdownlist id="RealtimeType" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%">
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td width="2%">
                                        </td>
                                    </tr>
                                    <%--FB 2637 Ends--%>
                                    <%--FB 2636 Starts --%>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top"> <%-- FB 2842 --%>
                                            Enable Dial Plan
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left"> <%-- FB 2842 --%>
                                            <asp:dropdownlist id="DrpEnableE164DialPlan" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <%--FB 2636 Ends --%>
                                        <%-- FB 2641 Start--%>
                                        <td align="left" class="blackblodtext" valign="top" width="27%">
                                            Enable Line Rate
                                        </td>
                                        <td valign="top" style="width: 20%" align="left">
                                            <asp:dropdownlist id="drpEnableLineRate" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
										<%-- FB 2842 --%>				
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <%-- ZD 100263 Start--%>		
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                            File White List &nbsp;&nbsp; 
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left">
                                        <asp:CheckBox id="chkWhiteList" runat="server" onclick="JavaScript:Filewhitelistshowhide()"></asp:CheckBox>
                                        <br />
                                            <asp:TextBox id="txtWhiteList" runat="server" CssClass="altText" ></asp:TextBox>
                                            <%--<button name="btnWhiteList" id="btnWhiteList" onclick="javascript:return AddRemoveWhiteList('add')" class="altMedium0BlueButtonFormat" style="width:50pt" >Add </button>--%> <%--ZD 100420--%>
                                            <asp:Button id="btnWhiteList" runat="server" Text="Add" class="altShortBlueButtonFormat" OnClientClick="javascript:return AddRemoveWhiteList('add')" />
                                        </td>
										<%--ZD 100935 Starts--%>
                                        <td style="width: 1%;">
                                        </td>
                                        <td id="tdWebExIntg" runat="server" align="left" class="blackblodtext" valign="top" width="27%">Enable WebEx Integration 
                                        </td>
                                        <td valign="top" style="width: 20%" align="left">
                                            <asp:DropDownList id="drpWebExIng" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
										<%--ZD 100935 End--%>		
                                    </tr>
                                    
                                    <tr id="trWhiteList" runat="server">
                                    <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left">
                                        <asp:ListBox runat="server" ID="lstWhiteList" CssClass="altSelectFormat" Rows="5" SelectionMode="Multiple"  onDblClick="javascript:return AddRemoveWhiteList('Rem')"  ></asp:ListBox><%-- ZD 100420 End--%>
                                        <%--<select size="5" rows="5" wrap="false" id="lstWhiteList" class="altSelectFormat" selectionmode="Multiple" ondblclick="javascript:return AddRemoveWhiteList('Rem')" runat="server" onkeydown="if(event.keyCode ==32){javascript:return AddRemoveWhiteList('Rem')}"> </select><%--ZD 100420--%>
                                            <%--<br /> * Double click on the name to <br /> remove it from the list.--%>
                                            
                                            <br /> * Double click on the name to <br /> remove it from the list.
                                        </td>
                                    </tr>
                                
                                    
                                    <%-- ZD 100263 End--%>		
                                    <%--<tr><td style="width: 1%;"></td></tr>--%> <%-- FB 2842 --%>
                                    <%-- FB 2641 End--%>
                                    <%-- ZD 100707 Start--%>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top"> 
                                            Enable Virtual Meeting Room <%--ZD 100806--%>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left"> 
                                            <asp:DropDownList id="drpEnableVMR" runat="server" onclick="JavaScript:ShowHideVMROptions();" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td id="tdPersonalVMR" runat="server" align="left" class="blackblodtext" valign="top" width="27%">
                                            Enable Personal Virtual Meeting Room  <%--ZD 100806--%>
                                        </td>
                                        <td id="tddrpEnablePersonaVMR" runat="server" valign="top" style="width: 20%" align="left">
                                            <asp:DropDownList id="drpEnablePersonaVMR" runat="server" cssclass="alt2SelectFormat">
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                            </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr id="trEnableRoomExternalVRM" runat="server" >
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top"> 
                                            Enable Room Virtual Meeting Room <%--ZD 100806--%>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left"> 
                                            <asp:DropDownList id="drpEnableRoomVMR" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" class="blackblodtext" valign="top" width="27%">
                                            Enable External Virtual Meeting Room <%--ZD 100806--%>
                                        </td>
                                        <td valign="top" style="width: 20%" align="left">
                                            <asp:DropDownList id="drpEnableExternalVMR" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <%-- ZD 700707 End--%>
                                    <%-- ZD 100704 Starts--%>
                                    <tr id="tr11" runat="server" >
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top"> 
                                            Express Form Conference Type
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left"> 
                                            <asp:DropDownList id="drpEnableExpressConfType" runat="server" cssclass="alt2SelectFormat">
                                            <asp:ListItem Value="0" >No</asp:ListItem>
                                            <asp:ListItem Selected="True" Value="1" >Yes</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" class="blackblodtext" valign="top" width="27%">
                                        </td>
                                        <td valign="top" style="width: 20%" align="left">
                                            
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                   <%-- ZD 100704 End--%>
									                                   
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7" align="left" style="height: 49px">
                                <table width="100%" border="0">
                                    <tr>
                                        <td width="100%" height="20" align="left">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:imagebutton id="img_AUD" alternatetext="Expand/Collapse" cssclass ="0" runat="server" imageurl="image/loc/nolines_plus.gif"
                                                            height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                                    </td>
                                                    <td valign="top">
                                                        <span class="subtitleblueblodtext">Audio Add-On</span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trAUD" runat="server" style="display: none">
                            <td colspan="7">
                                <table border="0" width="100%" cellpadding="5" style="margin-left:-4px"> <%-- FB 2842 --%>
                                    <tr id="trUser" runat="server">
                                        <td style="width: 1%;">
                                        </td>
                                        <td style="height: 33px; width: 25%" align="left" class="blackblodtext">
                                            <asp:label id="LblExc" runat="server" text="Enable Conference Code"></asp:label>
                                            <%--<asp:ImageButton ID="Imgeconfcode" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable conference code will be visible in Audio/Video tab for audio bridge user."/>--%>
                                        </td>
                                        <td style="width: 25%;" align="left">
                                            <asp:dropdownlist id="DrpConfcode" runat="server" width="125px" cssclass="altSelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td style="width: 27%;" align="left" class="blackblodtext">
                                            <asp:label id="LblDom" runat="server" text="Enable Leader PIN"></asp:label>
                                            <%--<asp:ImageButton ID="Imgleaderpin" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable leaderpin will be visible in Audio/Video tab for audio bridge user."/>--%>
                                        </td>
                                        <td style="width: 20%;" align="left">
                                            <asp:dropdownlist id="DrpLedpin" runat="server" width="125px" cssclass="altSelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr id="tr1" runat="server">
                                        <td style="width: 1%;">
                                        </td>
                                        <td style="width: 25%;" align="left" class="blackblodtext">
                                            <asp:label id="Label1" runat="server" text="Enable Conference Audio/Video Settings"></asp:label>
                                            <%--<asp:ImageButton ID="ImgEadvavparams"  valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable advanced Audio/video params will be visible in Audio/video tab."/>--%>
                                        </td>
                                        <td style="width: 25%;" align="left">
                                            <asp:dropdownlist id="Drpavprm" runat="server" width="125px" cssclass="altSelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td style="width: 27%;" align="left" class="blackblodtext">
                                            <asp:label id="Label2" runat="server" text="Display Audio Parameters"></asp:label>
                                            <%--<asp:ImageButton ID="Imgeaudioparams" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable audio params will be visible in Audio/Video tab."/>--%>
                                        </td>
                                        <td style="width: 20%;" align="left">
                                            <asp:dropdownlist id="DrpAudprm" runat="server" width="125px" cssclass="altSelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td style="width: 25%;" align="left" class="blackblodtext">
                                            <asp:label id="Label3" runat="server" text="Enable Audio Bridges"></asp:label>
                                            <%--<asp:ImageButton ID="Imgabridge" valign="center"  src="image/info.png" runat="server" ToolTip="Switch to enable audio bridge field visible in menu list."/>--%>
                                        </td>
                                        <td style="width: 25%;" align="left">
                                            <asp:dropdownlist id="lstEnableAudioBridges" runat="server" width="125px" cssclass="altSelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td style="width: 27%;" align="left" class="blackblodtext">
                                            <asp:label id="Label4" runat="server" text="Display Room Parameters"></asp:label>
                                            <%--<asp:ImageButton ID="ImgEroomparams" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable room params visible for a conference."/>--%>
                                        </td>
                                        <td style="width: 20%;" align="left">
                                            <asp:dropdownlist id="lstRoomprm" runat="server" width="125px" cssclass="altSelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <%--FB 2571 Start--%>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                            Enable FECC
                                            <%--<asp:ImageButton ID="ImgeFECC" valign="center"  src="image/info.png" runat="server" ToolTip="Switch to enable FECC checkbox while conference creation."/>--%>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left">
                                            <asp:dropdownlist id="lstenableFECC" runat="server" width="125px" cssclass="alt2SelectFormat"
                                                onclick="javascript:fnFeccOptions()">
                                                    <%--FB 2571--%>
                                                    <asp:ListItem Value="0">Hide</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1">Show</asp:ListItem>
                                                    <asp:ListItem Value="2">None</asp:ListItem>
                                                    <%--FB 2571--%>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td id="DefaultFECC" runat="server" align="left" style="width: 27%;" class="blackblodtext"
                                            valign="top">
                                            <%--FB 2571--%>
                                            <span style="left">Default FECC</span>
                                            <%--<asp:ImageButton ID="ImgdFECC" valign="center" src="image/info.png" runat="server" ToolTip="Switch to make default Selection for FECC while conference creation."/>--%>
                                        </td>
                                        <td id="DefaultFECCoptions" runat="server" valign="top" align="left" style="width: 20%">
                                            <%--FB 2571--%>
                                            <asp:dropdownlist id="lstdefaultFECC" runat="server" width="125px" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <%--FB 2571 End--%>
                                    <%--FB 2839 Start--%>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                            Enable Profile Selection
                                        </td>
                                        <td id="tdEnableProfile" runat="server" valign="top" align="left" style="width: 20%">
                                            <asp:DropDownList id="lstEnableProfileSelection" runat="server" width="125px" cssclass="alt2SelectFormat">
                                                <asp:ListItem selected="True" value="0">No</asp:ListItem>
                                                <asp:ListItem  value="1">Yes</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <%--FB 2839 End--%>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7" align="left" style="height: 49px">
                                <table width="100%" border="0">
                                    <tr>
                                        <td width="100%" height="20" align="left">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:imagebutton id="img_CONFMAIL" alternatetext="Expand/Collapse" cssclass ="0" runat="server" imageurl="image/loc/nolines_plus.gif"
                                                            height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                                    </td>
                                                    <td valign="top">
                                                        <span class="subtitleblueblodtext">Mail Options</span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trCONFMAIL" runat="server" style="display: none">
                            <td colspan="7">
                                <table border="0" width="100%" cellpadding="5" style="margin-left:-1px" > <%-- FB 2842 --%>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td style="width: 25%;" align="left" class="blackblodtext">
                                            <asp:label id="Label9" runat="server" text="Send Email Confirmations"></asp:label>
                                            <%--<asp:ImageButton ID="Imgsendconfirmemail" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable send confirmation emails to host only or all."/>--%>
                                        </td>
                                        <td style="width: 25%;" align="left">
                                            <asp:dropdownlist id="lstSendConfirmationEmail" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="All"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Host"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <%--FB 2817 Starts--%>
                                         <td style="width: 27%;" align="left" class="blackblodtext"> <%-- FB 2842 --%>
                                            <asp:label id="lblSigRoom" runat="server" text="Send Single Room Conference Email Confirmations"></asp:label>
                                        </td>
                                        <td style="width: 20%;" align="left"> <%-- FB 2842 --%>
                                            <asp:dropdownlist id="lstSigRoomConf" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <%--FB 2817 Ends--%>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <%--FB 2631--%>
                                    <tr>
                                        <td style="width: 1%">
                                        </td>
                                        <td align="left" class="blackblodtext" rowspan="1" valign="top" width="25%">
                                            Enable Room Admin Details
                                        </td>
                                        <td style="height: 33px; width: 25%" align="left" valign="top">
                                            <asp:dropdownlist id="lstEnableRoomAdminDetails" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td width="1%">
                                        </td> 
                                        <td style="width: 27%;" align="left" class="blackblodtext">
                                            <asp:label id="Label10" runat="server" text="Send MCU Alert Email"></asp:label>
                                            <%--<asp:ImageButton ID="Imgsendmcuemail" valign="center" src="image/info.png" runat="server" ToolTip="Switch to send mcu alert mails to host  only or to all."/>--%>
                                        </td>
                                        <td style="width: 20%;" align="left">
                                            <asp:dropdownlist id="lstMcuAlert" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="None"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Host"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="MCU Admin"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="Both"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                            Send Attachments to External Domains
                                            <%--<asp:ImageButton ID="Imgsendatteparty" valign="center" src="image/info.png" runat="server" ToolTip="Switch to control mail attachment for external party."/>--%>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left">
                                            <asp:dropdownlist id="DrpDwnAttachmnts" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%">
                                        </td>
                                        <td style="width: 27%;" align="left" class="blackblodtext">
                                            <asp:label id="Label8" runat="server" text="Display Local Time for Sites in Email"></asp:label>
                                            <%--<asp:ImageButton ID="Imgeconftimel" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable conference time in locations details in mails."/>--%>
                                        </td>
                                        <td style="width: 20%;" align="left">
                                            <asp:dropdownlist id="lstEnableConfTZinLoc" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td style="width: 25%;" align="left" class="blackblodtext">
                                            <asp:label id="Label5" runat="server" text="Display Endpoint Details in Email"></asp:label>
                                            <%--<asp:ImageButton ID="Imgeendpoint" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable endpoints details in emails."/>--%>
                                        </td>
                                        <td style="width: 25%;" align="left">
                                            <asp:dropdownlist id="lstEPinMail" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <%--FB 2610 Starts--%>
                                        <td style="width: 27%;" align="left" class="blackblodtext">
                                            <asp:label id="lblShwBrdge" runat="server" text="Show Bridge Ext # in Email"></asp:label>
                                        </td>
                                        <td style="width: 20%;" align="left">
                                            <asp:dropdownlist id="drpBrdgeExt" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <%--FB 2610 Ends--%>
                                        <td style="width: 1%;">
                                        </td> <%-- FB 2842 --%>
                                    </tr>
                                    <%--FB 2632 Starts--%>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td style="width: 25%;" align="left" class="blackblodtext">
                                            <asp:label id="lblCngSupport" runat="server" text="Enable Conference Support in Email"></asp:label> <%--FB 3023--%>
                                        </td>
                                        <td style="width: 25%;" align="left">
                                            <asp:dropdownlist id="drpCngSupport" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td>
                                        </td>
                                        <%--FB 2419--%>
                                        <td style="width: 27%;" align="left" class="blackblodtext">
                                            <asp:label id="Label6" runat="server" text="Enable Accept/Decline Links in Invitations"></asp:label>
                                            <%--<asp:ImageButton ID="Imgeaccdec" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable accept/decline link in confirmation mails."/>--%>
                                        </td>
                                        <td style="width: 20%;" align="left">
                                            <asp:dropdownlist id="lstAcceptDecline" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%">
                                        </td>
                                        <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                            <span style="align: left">Include in Email</span>
                                        </td>
                                        <td valign="top" align="left" style="width: 25%">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <input id="chkOnSiteAVSupport" type="checkbox" runat="server" />
                                                        <strong style="align: left">On-Site A/V Support</strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input id="chkConciergeMonitoring" type="checkbox" runat="server" />
                                                        <strong style="align: left">Call Monitoring</strong> <%--FB 3023--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="width: 1%">
                                        </td>
                                        <td valign="top" align="left" style="width: 27%"> <%-- FB 2842 --%>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <input id="chkMeetandGreet" type="checkbox" runat="server" />
                                                        <strong style="align: left">Meet and Greet</strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input id="chkDedicatedVNOCOperator" type="checkbox" runat="server" />
                                                        <strong style="align: left">Dedicated VNOC Operator</strong>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
											<%-- FB 2842 STARTS --%>
                                        <td style="width: 20%;">
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
											<%-- FB 2842  END--%>
                                    </tr>
                                    <%--FB 2632 Ends--%>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                            Send Invitee iCal
                                            <%--<asp:ImageButton ID="Imginviteeical" valign="center" src="image/info.png" runat="server" ToolTip="Switch to send ical to parties when conference is scheduled."/>--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 25%;">
                                            <asp:dropdownlist id="DrpListIcal" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td align="right" valign="top" style="width: 1%;">
                                            &nbsp;
                                        </td>
                                        <td align="left" style="width: 27%;" class="blackblodtext" rowspan="1" valign="top">
                                            Send Approval iCal
                                            <%--<asp:ImageButton ID="Imgapproical" valign="center" src="image/info.png" runat="server" ToolTip="Switch to send ical to application when conference is scheduled from outlook."/>--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 20%;">
                                            <asp:dropdownlist id="DrpAppIcal" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%">
                                        </td>
                                        <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                            <span style="align: left">Send Reminders</span>  <%--FB 2926--%>
                                            <%--<asp:ImageButton ID="Imgsendremain" valign="center" src="image/info.png" runat="server" ToolTip="Remainder mail options for participants."/>--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 25%">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:checkbox id="WklyChk" runat="server" />
                                                        <strong style="align: left">1 Week</strong>
                                                    </td>
                                                    <td>
                                                        <asp:checkbox id="DlyChk" runat="server" />
                                                        <strong style="align: left">1 Day</strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:checkbox id="HourlyChk" runat="server" />
                                                        <strong style="align: left">1 Hour</strong>
                                                    </td>
                                                    <td>
                                                        <asp:checkbox id="MinChk" runat="server" />
                                                        <strong style="align: left">15 mins</strong> <%--ZD 100528--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="width: 1%">
                                        </td>
                                        <td colspan="3" style="width: 48%" rowspan="1">
                                            <%-- FB 2440 --%>
                                            <table id="tblForceMCUBuffer" runat="server" width="100%" style="display: none">
                                                <tr id="trForceMCUBuffer" style="display: none;" runat="server">
                                                    <td align="left" class="blackblodtext" valign="top" style="width: 21%">
                                                        <span style="align: left">Force MCU pre start & end</span>
                                                    </td>
                                                    <td valign="top" align="left" style="width: 33%">
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:dropdownlist id="DrpForceMCUBuffer" runat="server" cssclass="alt2SelectFormat">
                                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                            </asp:dropdownlist>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%">
                                        </td>
                                        <td align="left" class="blackblodtext" rowspan="1" valign="top" width="25%">
                                            iCal Requestor Email Address
                                            <%--<asp:ImageButton ID="Imgicalreqemail" valign="center" src="image/info.png" runat="server" ToolTip="Ical requestor mail for CTP room in  ical invitations."/>--%>
                                        </td>
                                        <td style="height: 33px; width: 25%" align="left" valign="top">
                                            <asp:textbox id="txtiCalEmailId" runat="server" cssclass="altText" width="150px"
                                                maxlength="512"></asp:textbox>
                                            <br />
                                            <asp:regularexpressionvalidator id="regTestemail" runat="server" controltovalidate="txtiCalEmailId"
                                                errormessage="Invalid Email Address" display="dynamic" validationexpression="^[a-zA-Z()^][()^\w\.-]*[a-zA-Z0-9()^]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:regularexpressionvalidator>
                                            <asp:regularexpressionvalidator id="iCalReqValid2" controltovalidate="txtiCalEmailId"
                                                display="dynamic" runat="server" setfocusonerror="true" errormessage="<br>& < > ' + % \ / ; ? | = ! ` , [ ] { } :<br> # $ ~ and &#34; are invalid <br>characters."
                                                validationexpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:regularexpressionvalidator>
                                        </td>
                                        <td width="1%">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <%--FB 2595 Starts--%>
                        <tr id="trNetwork" runat="server"> <%--FB 2993--%>
                            <td colspan="7" align="left" style="height: 49px">
                                <table width="100%" border="0">
                                    <tr>
                                        <td width="100%" height="20" align="left">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:imagebutton id="img_NETSWT" alternatetext="Expand/Collapse" cssclass ="0" runat="server" imageurl="image/loc/nolines_plus.gif"
                                                            height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                                    </td>
                                                    <td valign="top">
                                                        <span class="subtitleblueblodtext">Network Features</span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trNETSWT" runat="server" style="display: none">
                            <td>
                                <table border="0" width="100%" cellpadding="5" style="margin-left:-2px" > <%-- FB 2842 --%>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" class="blackblodtext" style="width: 25%" >
                                            Enable Network State
                                        </td>
                                        <td align="left" style="width: 25%">
                                            <asp:dropdownlist id="drpSecureSwitch" runat="server" cssclass="alt2SelectFormat"
                                                onchange="JavaScript:Securedisplay()">
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td id="tdsecureadminaddress" runat="server" align="left" class="blackblodtext" valign="top"
                                            style="width: 27%;">
                                            Hardware Admin Email Address
                                        </td>
                                        <td id="txtsecureadminaddress" valign="top" align="left" style="width: 20%;">
                                            <asp:textbox id="txtHardwareAdminEmail" runat="server" cssclass="altText" width="150px"
                                                maxlength="512"></asp:textbox>
                                            <br />
                                            <asp:regularexpressionvalidator id="RegularExpressionValidator7" runat="server" controltovalidate="txtHardwareAdminEmail"
                                                errormessage="Invalid Email Address" display="dynamic" validationexpression="^[a-zA-Z()^][()^\w\.-]*[a-zA-Z0-9()^]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:regularexpressionvalidator>
                                            <asp:regularexpressionvalidator id="RegularExpressionValidator8" controltovalidate="txtHardwareAdminEmail"
                                                display="dynamic" runat="server" setfocusonerror="true" errormessage="<br>& < > ' + % \ / ; ? | = ! ` , [ ] { } :<br> # $ ~ and &#34; are invalid <br>characters."
                                                validationexpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:regularexpressionvalidator>
                                        </td>
                                        <td style="width: 1%;">
                                        </td> <%-- FB 2842 --%>
                                    </tr>
                                    <tr id="trNwtSwtiching" runat="server">
                                    <td style="width: 1%;"></td> <%-- FB 2842--%>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                            Network Switching
                                        </td>
                                        <td valign="top" align="left" style="width: 25%;">
                                            <asp:dropdownlist id="drpNwtSwtiching" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Selected="True" Value="1">None</asp:ListItem>
                                                    <asp:ListItem Value="2">NATO Secret</asp:ListItem>
                                                    <asp:ListItem Value="3">NATO Unclassified</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" valign="top" class="blackblodtext" style="width: 27%;">
                                            Call Launch
                                        </td>
                                        <td valign="top" align="left" style="width: 20%;">
                                            <asp:dropdownlist id="drpNwtCallLaunch" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="1">NATO Secret</asp:ListItem>
                                                    <asp:ListItem Value="2">NATO Unclassified</asp:ListItem>
                                                </asp:dropdownlist>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr id="trNwtCallBuffer" runat="server">
                                    <td style="width: 1%;"></td><%-- FB 2842 --%>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                            Secure Launch Buffer
                                        </td>
                                        <td valign="top" align="left" style="width: 25%">
                                            <asp:textbox id="txtSecureLaunch" runat="server" cssclass="altText" width="50px"> </asp:textbox>
                                            (secs)
                                            <asp:regularexpressionvalidator id="RegularExpressionValidator9" validationgroup="Submit"
                                                controltovalidate="txtSecureLaunch" display="dynamic" runat="server" setfocusonerror="true"
                                                errormessage="Numeric values only." validationexpression="\d+"></asp:regularexpressionvalidator>
                                            <asp:requiredfieldvalidator id="RequiredFieldValidator3" runat="server" controltovalidate="txtSecureLaunch"
                                                display="dynamic" errormessage="Please enter secure launch buffer"></asp:requiredfieldvalidator>
                                        </td>
                                         <td style="width: 1%;">
                                        </td> <%--FB 2993 start--%>
                                        <td align="left" valign="top" class="blackblodtext" style="width: 27%;" >Response Timeout</td>
                                        <td valign="top" align="left" style="width: 20%;" >
                                        <asp:TextBox id="txtResponseTimeout" runat="server" cssclass="altText" width="50px"></asp:TextBox>
                                        (Mins)
                                        <asp:regularexpressionvalidator id="RegularExpressionValidatortimeout" validationgroup="Submit"
                                                controltovalidate="txtResponseTimeout" display="dynamic" runat="server" setfocusonerror="true"
                                                errormessage="Numeric values only." validationexpression="\d+"></asp:regularexpressionvalidator>
                                        </td><%--FB 2993 End--%>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <%--FB 2595 Ends--%>
                        <%--ZD 100221 Starts--%>
                        <tr id="trWebConf" runat="server">
                            <td colspan="7" align="left" style="height: 49px">
                                <table width="100%" border="0">
                                    <tr>
                                        <td width="100%" height="20" align="left">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:imagebutton id="img_WEBCONF" alternatetext="0" runat="server" imageurl="image/loc/nolines_plus.gif"
                                                            height="25" width="25" vspace="0" hspace="0" />
                                                    </td>
                                                    <td valign="top">
                                                        <span class="subtitleblueblodtext">Web Conferencing</span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trWebCre" runat="server" style="display: none">
                            <td>
                                <table border="0" width="100%" cellpadding="5" style="margin-left:-2px" >
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" class="blackblodtext" style="width: 25%" >
                                            WebEx API URL  &nbsp;&nbsp;<asp:ImageButton id="ImgWebExAPI" src="image/info.png" runat="server" alt="Info"
                                    tooltip="Please contact your WebEx Account Administrator for your WebEx API URL, Site ID and Partner ID. This information can also be acquired by contacting WebEx Support." /> <%--ZD 100935--%>
                                        </td>
                                        <td align="left" style="width: 25%">
                                           <asp:textbox id="txtWebURL" runat="server" cssclass="altText" width="150px"
                                                maxlength="512"></asp:textbox>
                                           <asp:regularexpressionvalidator id="regWebURL" controltovalidate="txtWebURL"
                                            display="dynamic" runat="server" setfocusonerror="true" errorMessage="<br> & < and > are invalid characters."
                                            validationExpression="^[^<>&]*$"></asp:regularexpressionvalidator>
                                        
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" class="blackblodtext" valign="top"
                                            style="width: 27%;">
                                            Site ID &nbsp;&nbsp;<asp:ImageButton id="ImgWebExSiteID" src="image/info.png" runat="server" alt="Info"
                                    tooltip="Please contact your WebEx Account Administrator for your WebEx API URL, Site ID and Partner ID. This information can also be acquired by contacting WebEx Support." /><%--ZD 100935--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 20%;">
                                            <asp:textbox id="txtSiteID" runat="server" cssclass="altText" width="150px"
                                                maxlength="512"></asp:textbox>
                                            <asp:regularexpressionvalidator id="regSiteID" controltovalidate="txtSiteID"
                                            display="dynamic" runat="server" setfocusonerror="true" errorMessage="<br> & < and > are invalid characters."
                                            validationExpression="^[^<>&]*$"></asp:regularexpressionvalidator>
                                            <br />
                                        </td>
                                        <td style="width: 1%;">
                                        </td> 
                                    </tr>
                                    <tr>
                                    <td style="width: 1%;"></td> 
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                           Partner ID &nbsp;&nbsp;<asp:ImageButton id="ImgWebExPartnrID" src="image/info.png" runat="server" alt="Info"
                                    tooltip="Please contact your WebEx Account Administrator for your WebEx API URL, Site ID and Partner ID. This information can also be acquired by contacting WebEx Support." /><%--ZD 100935--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 25%;">
                                            <asp:textbox id="txtPartnrID" runat="server" cssclass="altText" width="150px"
                                                maxlength="512"></asp:textbox>
                                          <asp:regularexpressionvalidator id="RegPartnerID" controltovalidate="txtPartnrID"
                                            display="dynamic" runat="server" setfocusonerror="true" ErrorMessage="<br> & < and > are invalid characters."
                                            ValidationExpression="^[^<>&]*$"></asp:regularexpressionvalidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <%--ZD 100221 Ends--%>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%" border="0">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:imagebutton id="img_EPT" alternatetext="Expand/Collapse" cssclass ="0" runat="server" imageurl="image/loc/nolines_plus.gif"
                                                height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext">Endpoint Settings</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trEPT" runat="server" style="display: none">
                <td colspan="7">
                    <table border="0" width="100%" cellpadding="5">
                        <tr>
                            <td style="width: 1%;">
                            </td>
                            <td align="left" style="width: 25%;" class="blackblodtext"  valign="top"> <%-- FB 2842--%>
                                Enable Assigned MCU in Endpoint   <%--FB 2926--%>
                                <%--<asp:ImageButton ID="ImgeassMcu" valign="center"  src="image/info.png" runat="server" ToolTip="Switch to enable must select mcu from assigned to mcu dropdownlist to create a new endpoint."/>--%>
                            </td>
                            <td valign="top" align="left" style="width: 25%;">
                                <asp:dropdownlist id="DrpDwnListAssignedMCU" runat="server" cssclass="alt2SelectFormat">
                                        <asp:ListItem Value="0">No</asp:ListItem>
                                        <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <td style="width: 1%;">
                            </td>
                            <td align="left" valign="top" class="blackblodtext" style="width: 27%;">
                                Enable ISDN Dial-Out
                                <%--<asp:ImageButton ID="Imgedialout"  valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable dial out option in response screen."/>--%>
                            </td>
                            <td valign="top" align="left" style="width: 20%;">
                                <asp:dropdownlist id="DialoutEnabled" runat="server" cssclass="alt2SelectFormat">
                                        <asp:ListItem Value="0">No</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <td style="width: 1%;">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trOnfly" runat="server">
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%" border="0">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:imagebutton id="img_FLY" alternatetext="Expand/Collapse"  cssclass ="0"  runat="server" imageurl="image/loc/nolines_plus.gif"  name="Fly"
                                                height="25" width="25" vspace="0" hspace="0"  /> <%--FB 2841--%><%--ZD 100419--%>
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext">On-the-Fly Room Creation</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trFLY" runat="server" style="display: none">
                <td colspan="7">
                    <table border="0" width="100%" cellpadding="5">
                        <tr>
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 25%">
                                Top Tier <span class="reqfldText">*</span>
                                <%--<asp:ImageButton ID="Imgtoptier" valign="center" src="image/info.png" runat="server" ToolTip="Default top tier for on the fly rooms."/>--%>
                            </td>
                            <td style="width: 25%" align="left" valign="top">
                                <asp:dropdownlist id="lstTopTier" width="120px" datatextfield="Name" datavaluefield="ID"
                                    runat="server" cssclass="altSelectFormat" onselectedindexchanged="UpdateMiddleTiers"
                                    autopostback="true" onchange="javascript:DataLoading(1)">
                                    </asp:dropdownlist>
                                <asp:requiredfieldvalidator id="reqTopTier" runat="server" controltovalidate="lstTopTier"
                                    display="dynamic" cssclass="lblError" setfocusonerror="true" text="Required"
                                    validationgroup="Submit"></asp:requiredfieldvalidator>
                            </td>
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 27%">
                                Middle Tier<span class="reqfldText">*</span>
                                <%--<asp:ImageButton ID="Imgmiddletier" valign="center" src="image/info.png" runat="server" ToolTip="Default middle tier for on the fly rooms."/>--%>
                            </td>
                            <td style="width: 20%" align="left" valign="top">
                                <asp:dropdownlist id="lstMiddleTier" width="120px" datatextfield="Name" datavaluefield="ID"
                                    runat="server" cssclass="altSelectFormat">
                                    </asp:dropdownlist>
                                <asp:requiredfieldvalidator id="reqMiddleTier" runat="server" controltovalidate="lstMiddleTier"
                                    display="dynamic" cssclass="lblError" setfocusonerror="true" text="Required"
                                    validationgroup="Submit"></asp:requiredfieldvalidator>
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%" border="0">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:imagebutton id="img_AUTO" alternatetext="Expand/Collapse"  cssclass ="0" runat="server" imageurl="image/loc/nolines_plus.gif"
                                                height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext">Default Video Display Layout</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trAUTO" runat="server" style="display: none">
                <td colspan="7" style="padding-bottom: 0px; padding-left: 0px; padding-right: 0px;
                    padding-top: 0px;">
                    <table border="0" width="100%" cellpadding="5"> <%-- FB 2842 --%>
                        <tr>
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 25%"> 
                                Polycom MGC &nbsp;&nbsp;<asp:image id="Image6" src="image/info.png" runat="server" alt="Info"
                                    tooltip="The video display that will be used when the conference is launched. This display can be changed once the conference is ongoing." /><%--FB 2713--%> <%--ZD 100419--%>
                            </td>
                            <td align="left" style="width: 25%">
                                <asp:image id="imgLayoutMapping1" runat="server" alt="Layout" /><%--ZD 100419--%>
								<%--ZD 100420--%>
                                <%--<input style="vertical-align: 10px" id="butLayoutMapping1" type="button" runat="server"
                                    name="ConfLayoutSubmit" value="Change" class="altMedium0BlueButtonFormat" onclick="javascript: changeLayout('Poly2MGC',butLayoutMapping1,1);" />--%>
                                    <button style="vertical-align: 10px; width:100px;" id="butLayoutMapping1" runat="server"
                                    name="ConfLayoutSubmit" class="altMedium0BlueButtonFormat" onclick="javascript: changeLayout('Poly2MGC',butLayoutMapping1,1);return false;">Change</button><%--ZD 100420--%><%--ZD 100420 ZD 100393--%>
								<%--ZD 100420--%>
                            </td>
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 27%">
                                Polycom RMX &nbsp;&nbsp;<asp:image id="Image1" src="image/info.png" runat="server" alt="Info"
                                    tooltip="The video display that will be used when the conference is launched. This display can be changed once the conference is ongoing." /><%--FB 2713--%><%--ZD 100419--%>
                            </td>
                            <td align="left" style="width: 20%">
                                <asp:image id="imgLayoutMapping2" runat="server" alt="Layout" /> <%--ZD 100419--%>
								<%--ZD 100420--%>
                                <button style="vertical-align: 10px; width:100px;" id="butLayoutMapping2" runat="server"
                                    name="ConfLayoutSubmit" class="altMedium0BlueButtonFormat" onclick="javascript: changeLayout('Poly2RMX',butLayoutMapping2,2);return false;" >Change</button><%--ZD 100420--%><%--ZD 100420 ZD 100393--%>
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr>
                        <tr style="visibility: hidden">
                            <td>
                            </td>
                            <td align="left" class="blackblodtext">
                                CTMS to Cisco
                            </td>
                            <td>
                                <asp:image id="imgLayoutMapping3" runat="server" alternatetext="Video Display Layout" /> <%--ZD 100419--%>
                                <input style="vertical-align: 10px; width:100px;" id="butLayoutMapping3" type="button" runat="server"
                                    name="ConfLayoutSubmit" value="Change" class="altShortBlueButtonFormat" onclick="javascript: changeLayout('CTMS2Cisco',butLayoutMapping3,1);" /> <%--ZD 100393--%>
                            </td>
                            <td>
                            </td>
                            <td align="left" class="blackblodtext">
                                CTMS to Polycom
                            </td>
                            <td>
                                <asp:image id="imgLayoutMapping4" runat="server" alternatetext="Video Display Layout"  /> <%--ZD 100419--%>
                                <input style="vertical-align: 10px; width:100px;" id="butLayoutMapping4" type="button" runat="server"
                                    name="ConfLayoutSubmit" value="Change" class="altShortBlueButtonFormat" onclick="javascript: changeLayout('CTMS2Poly',butLayoutMapping4,2);" /> <%--ZD 100393--%>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%" border="0">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:imagebutton id="img_SYS" alternatetext="Expand/Collapse" cssclass ="0" runat="server" imageurl="image/loc/nolines_plus.gif"
                                                height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext">General Options</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trSYS" runat="server" style="display: none">
                <td colspan="7">
                    <table border="0" width="100%" cellpadding="5">
                        <tr>
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" rowspan="1" valign="top" style="width: 25%"> <%-- FB 2842--%>
                                Enable Multi-Lingual Features
                                <%--<asp:ImageButton ID="Imgmultilingual" valign="center" src="image/info.png" runat="server" ToolTip="Switch to support Multilingual all over Application (Themes/UI Text)."/>--%>
                            </td>
                            <td valign="top" align="left" style="width: 26%"> <%-- FB 2842--%>
                                <asp:dropdownlist id="DrpDwnListMultiLingual" runat="server" cssclass="alt2SelectFormat">
                                        <asp:ListItem Value="0">No</asp:ListItem>
                                        <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" valign="top" style="width: 27%"> <%-- FB 2842--%>
                                Default Calendar to Office Hours
                                <%--<asp:ImageButton ID="Imgdecaloffice" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable conference hours to office hours."/>--%>
                            </td>
                            <td valign="top" align="left" style="width: 20%">
                                <asp:dropdownlist id="lstDefaultOfficeHours" runat="server" cssclass="alt2SelectFormat">
                                        <asp:ListItem Value="0">No</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr>
                        <%--FB 2598 Starts (Switch for CallMonitor,EM7,CDR)--%>
                        <tr>
                            <td style="width: 1%">
                            </td>
                            <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                Enable Call Monitor
                            </td>
                            <td valign="top" style="width: 25%;" align="left">
                                <asp:dropdownlist id="DrpDwnEnableCallmonitor" runat="server" cssclass="alt2SelectFormat">
                                        <asp:ListItem Value="0">No</asp:ListItem>
                                        <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <td style="width: 1%;">
                            </td>
                            <td align="left" style="width: 27%;" class="blackblodtext" rowspan="1" valign="top">
                                Enable EM7
                            </td>
                            <td valign="top" style="width: 20%;" align="left">
                                <asp:dropdownlist id="DrpDwnEnableEM7" runat="server" cssclass="alt2SelectFormat">
                                        <asp:ListItem Value="0">No</asp:ListItem>
                                        <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <td style="width: 1%;"></td> <%-- FB 2842--%>
                        </tr>
                        <tr>
                            <td style="width: 1%;">
                            </td>
                            <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                Enable CDR
                            </td>
                            <td valign="top" style="width: 25%;" align="left">
                                <asp:dropdownlist id="DrpDwnEnableCDR" runat="server" cssclass="alt2SelectFormat">
                                        <asp:ListItem Value="0">No</asp:ListItem>
                                        <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <%--FB 2598 Ends (Switch for CallMonitor,EM7,CDR)--%>
                            <%--FB 2588 Starts (ZuLu Time Zone)--%>
                            <td style="width: 1%;">
                            </td>
                            <td align="left" style="width: 27%;" class="blackblodtext" rowspan="1" valign="top"> <%-- FB 2842--%>
                                Zulu System
                            </td>
                            <td valign="top" style="width: 25%;" align="left">
                                <asp:dropdownlist id="DropDownZuLu" runat="server" cssclass="alt2SelectFormat" onclick="javascript:fntimezonesOptions()">
                                        <asp:ListItem Value="0">No</asp:ListItem>
                                        <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                             <td style="width: 1%;"></td> <%-- FB 2842--%>
                        </tr>
                        <tr id="trShwCusAttr" runat="server"> <%--ZD 100151--%>
                            <td style="width: 1%;">
                            </td>
                            <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                Show Custom Attributes in Calendar
                            </td>
                            <td valign="top" style="width: 25%;" align="left">
                                <asp:dropdownlist id="DrpDwnEnableCA" runat="server" cssclass="alt2SelectFormat">
                                        <asp:ListItem Value="0">No</asp:ListItem>
                                        <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            
                            <%--ZD 100963 START--%>
                            <td style="width: 1%;">
                            </td>
                            <td align="left" style="width: 27%;" class="blackblodtext" rowspan="1" valign="top"> <%-- FB 2842--%>
                                Room Calendar View
                            </td>
                            <td valign="top" style="width: 25%;" align="left">
                                <asp:dropdownlist id="drpRoomCalView" runat="server" cssclass="alt2SelectFormat">
                                                    <asp:ListItem Value="1" Text="Private"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="Partial"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="Public"></asp:ListItem>
                                                </asp:dropdownlist>
                             </td>
                             <%--ZD 100963 END--%>
                        
                        </tr>
                        <%--FB 2588 Ends (ZuLu Time Zone)--%>
                        <tr>
                            <td style="width: 1%" rowspan="4">
                            </td>
                            <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                Open 24 Hours <%--FB 2926--%>
                                <%--<asp:ImageButton ID="Imgopen24hours" valign="center" src="image/info.png" runat="server" ToolTip="Office Timings/Conference Timings."/>--%>
                            </td>
                            <td align="left" valign="top" style="width: 25%">
                                <asp:checkbox id="Open24" runat="server" onclick="javascript:open24()" />
                            </td>
                            <td rowspan="4" style="width: 1%">
                            </td>
                            <td align="left" valign="top" style="padding-top: 6px; width: 27%;" class="blackblodtext"
                                rowspan="3">
                                Days Closed     <%--FB 2926--%>
                                <%--<asp:ImageButton ID="Imgdaysclosed" valign="center" src="image/info.png" runat="server" ToolTip="Conference can not be scheduled in closed days."/>--%>
                            </td>
                            <td rowspan="4" align="left" style="width: 20%">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="left" nowrap="nowrap">
                                            <%-- FB 2613--%>
                                            <asp:checkboxlist id="DayClosed" runat="server" cellpadding="0" cellspacing="6" repeatcolumns="2"
                                                repeatdirection="Horizontal" repeatlayout="Table" textalign="right">
                                                    <asp:ListItem Text="&lt;font class='blackblodtext'&gt;Sunday&lt;/font&gt;" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="&lt;font class='blackblodtext'&gt;Monday&lt;/font&gt;" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="&lt;font class='blackblodtext'&gt;Tuesday&lt;/font&gt;" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="&lt;font class='blackblodtext'&gt;Wednesday&lt;/font&gt;" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="&lt;font class='blackblodtext'&gt;Thursday&lt;/font&gt;" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="&lt;font class='blackblodtext'&gt;Friday&lt;/font&gt;" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="&lt;font class='blackblodtext'&gt;Saturday&lt;/font&gt;" Value="7"></asp:ListItem>
                                                </asp:checkboxlist>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td rowspan="4" style="width: 1%">
                            </td>
                        </tr>
                        <tr>
                            <td id="Open24DIV1" class="blackblodtext" align="left" style="width: 190px" valign="baseline">
                                <br />
                                Start time
                                <%--<asp:ImageButton ID="Imgstarttime" valign="center" src="image/info.png" runat="server" ToolTip="Office Timing :Start Time"/>--%>
                            </td>
                            <td id="Open24DIV2" align="left" style="width: 10px">
                                <br />
                                <mbcbb:ComboBox ID="systemStartTime" runat="server" CssClass="altSelectFormat" Rows="10"
                                    CausesValidation="True" Style="width: auto" AutoPostBack="false">
                                </mbcbb:ComboBox> <%--ZD 100284--%>
                                <asp:RegularExpressionValidator ID="regSysStartTime" runat="server" ControlToValidate="systemStartTime:Text"
                                    Display="Dynamic" ErrorMessage="Invalid Time (HH:MM AM/PM)"
                                    ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td id="Open24DIV3" class="blackblodtext" style="width: 190px" align="left" valign="baseline">
                                <br />
                                End time
                                <%--<asp:ImageButton ID="Imgendtime" valign="center" src="image/info.png" runat="server" ToolTip="Office Timing:End Time"/>--%>
                            </td>
                            <td id="Open24DIV4" align="left" style="width: 10px">
                                <br />
                                <mbcbb:ComboBox ID="systemEndTime" runat="server" CssClass="altSelectFormat" Rows="10" AutoPostBack="false"
                                    CausesValidation="True" Style="width: auto"><%-- ZD 100420--%>
                                </mbcbb:ComboBox><%--ZD 100284--%>
                                <asp:RegularExpressionValidator ID="regSysEndTime" runat="server" ControlToValidate="systemEndTime:Text"
                                    Display="Dynamic" ErrorMessage="Invalid Time (HH:MM AM/PM)"
                                    ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%" border="0">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:imagebutton id="img_CONFSECDESK" alternatetext="Expand/Collapse" cssclass ="0" runat="server" imageurl="image/loc/nolines_plus.gif"
                                                height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext">Security Desk Options</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trCONFSECDESK" runat="server" style="display: none">
                <td colspan="7">
                    <table border="0" width="100%" cellpadding="5" style="margin-left:-2px"> <%-- FB 2842--%>
                        <tr>
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" valign="top" style="width: 25%;margin-left: -3px;"> <%-- FB 2842--%>
                                <span style="align: left">Enable Security Badge</span>      <%--FB 2926--%>
                                <%--<asp:ImageButton ID="Imgesecuritybad" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable security badge functions."/>--%>
                            </td>
                            <td valign="top" align="left" style="width: 25%">
                                <asp:dropdownlist id="drpenablesecuritybadge" runat="server" width="125px" cssclass="alt2SelectFormat"
                                    onchange="JavaScript:modedisplay()">
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="0">No</asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <td style="width: 1%">
                            </td>
                            <td id="tdSecurityType" class="blackblodtext" align="left" runat="server" style="display: none; margin-left:3px;
                                width: 27%;" valign="top"> <%-- FB 2842--%>
                                Security Badge Type         <%--FB 2926--%>
                                <%--<asp:ImageButton ID="Imgsecuritybadgetype" valign="center" src="image/info.png" runat="server" ToolTip="Switch to send security Badge mails according to selections."/>--%>
                            </td>
                            <td valign="top" align="left" style="width: 20%">
                                <asp:dropdownlist id="drpsecuritybadgetype" runat="server" datatextfield="Name" datavaluefield="ID"
                                    width="125px" cssclass="alt2SelectFormat" onchange="JavaScript:emaildisplay()">
                                    </asp:dropdownlist>
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr>
                        <tr id="tdsecdeskemailid" runat="server">
                            <td style="height: 33px; width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 25%">
                                Securitydesk Email ID   <%--FB 2926--%>
                                <%--<asp:ImageButton ID="Imgsecdeskemailid" valign="center" src="image/info.png" runat="server" ToolTip="Email id to which security Badge will be sent."/>--%>
                            </td>
                            <td align="left" style="width: 25%;">
                                <asp:textbox id="txtsecdeskemailid" runat="server" cssclass="altText" width="187px"></asp:textbox>
                                <asp:regularexpressionvalidator id="regsecdeskemailid" runat="server" controltovalidate="txtsecdeskemailid"
                                    errormessage="Invalid Email Address" display="dynamic" validationexpression="^[a-zA-Z()^][()^\w\.-]*[a-zA-Z0-9()^]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:regularexpressionvalidator>
                                <asp:regularexpressionvalidator id="RegularExpressionValidator4" controltovalidate="txtiCalEmailId"
                                    display="dynamic" runat="server" setfocusonerror="true" errormessage="<br>& < > ' + % \ / ; ? | = ! ` , [ ] { } :<br> # $ ~ and &#34; are invalid <br>characters."
                                    validationexpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:regularexpressionvalidator>
                            </td>
                            <td style="width: 1%">
                            </td>
                            <td style="width: 27%">
                            </td>
                            <td style="width: 20%">
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
             <%--FB 2724 End--%>
             <tr>
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%" border="0">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:imagebutton id="img_ICP"  runat="server" alternatetext="Expand/Collapse" cssclass ="0" imageurl="image/loc/nolines_plus.gif"
                                                height="25" width="25" vspace="0" hspace="0" /> <%--ZD 100419--%>
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext">iControl Options</span><%--ZD 101019--%>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trICP" runat="server" style="display: none">
                <td colspan="7" style="padding-bottom: 0px; padding-left: 0px; padding-right: 0px;
                    padding-top: 0px;">
                    <table border="0" width="100%" cellpadding="5">
                        <tr>
                            <td style="width: 1%;">
                            </td>
                            <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                <span style="align: left">RFID Tag Value</span>
                            </td>
                            <td valign="top" align="left" style="width: 25%;">
                                <asp:DropDownList id="lstRFIDValue" runat="server" CssClass="alt2SelectFormat">
                                    <asp:ListItem selected="True" value="1">Email</asp:ListItem>
                                    <asp:ListItem value="2">Login</asp:ListItem>
                                    <asp:ListItem value="3">Custom</asp:ListItem>
                                </asp:DropDownList>
                             </td>
                             <td style="width: 1%;">
                             </td>
                             <td align="left" style="width: 27%;" class="blackblodtext" valign="top">
                                iControl Timeout
                             </td>
                             <td valign="top" style="width: 20%;" align="left">
                                <asp:TextBox id="txtiControlTimeout" runat="server" CssClass="altText" Width="50px" MaxLength="4"> </asp:TextBox>
                             </td>
                             <td style="width: 1%;">
                             </td>
                        </tr>
                        
                        <%--ZD 101019 START--%> 
                        <tr>
                            <td style="width: 1%;">
                            </td>
                            <td align="left" class="subtitleblueblodtext" valign="top" style="width: 25%">
                            <strong style="align: left">Video Streaming</strong>
                            </td>
                         </tr>
                         
                         <tr>
                            <td style="width: 1%;">
                            </td>
                            <td valign="top" class="blackblodtext" align="left" style="width: 50%" colspan="2" >
                                <table style="width:100%" border="0">
                                    <tr>
                                        <td colspan="4" style="width:50%">Screen Position</td>
                                        <td style="width:50%">Video Source URL</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="chkPosA1" runat="server" onclick="fnHandleTick(this.checked,'A2,A3,A4');" type="checkbox" />
                                        </td>
                                        <td>
                                            <input id="chkPosB1" runat="server" onclick="fnHandleTick(this.checked,'B2,B3,B4');" type="checkbox" />
                                        </td>
                                        <td>
                                            <input id="chkPosC1" runat="server" onclick="fnHandleTick(this.checked,'C2,C3,C4');" type="checkbox" />
                                        </td>
                                        <td>
                                            <input id="chkPosD1" runat="server" onclick="fnHandleTick(this.checked,'D2,D3,D4');" type="checkbox" />
                                        </td>
                                        <td>
                                            <asp:TextBox id="txtVideoSourceURL1" runat="server" CssClass="altText"></asp:TextBox>
                                             <asp:regularexpressionvalidator id="regURL1" controltovalidate="txtVideoSourceURL1"
                                            display="dynamic" runat="server" setfocusonerror="true" errorMessage="<br> & < and > are invalid characters."
                                            validationExpression="^[^<>&]*$"></asp:regularexpressionvalidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="chkPosA2" runat="server" onclick="fnHandleTick(this.checked,'A1,A3,A4');" type="checkbox" />
                                        </td>
                                        <td>
                                            <input id="chkPosB2" runat="server" onclick="fnHandleTick(this.checked,'B1,B3,B4');" type="checkbox" />
                                        </td>
                                        <td>
                                            <input id="chkPosC2" runat="server" onclick="fnHandleTick(this.checked,'C1,C3,C4');" type="checkbox" />
                                        </td>
                                        <td>
                                            <input id="chkPosD2" runat="server" onclick="fnHandleTick(this.checked,'D1,D3,D4');" type="checkbox" />
                                        </td>
                                        <td>
                                            <asp:TextBox id="txtVideoSourceURL2" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:regularexpressionvalidator id="regURL2" controltovalidate="txtVideoSourceURL2"
                                            display="dynamic" runat="server" setfocusonerror="true" errorMessage="<br> & < and > are invalid characters."
                                            validationExpression="^[^<>&]*$"></asp:regularexpressionvalidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="chkPosA3" runat="server" onclick="fnHandleTick(this.checked,'A1,A2,A4');" type="checkbox" />
                                        </td>
                                        <td>
                                            <input id="chkPosB3" runat="server" onclick="fnHandleTick(this.checked,'B1,B2,B4');" type="checkbox" />
                                        </td>
                                        <td>
                                            <input id="chkPosC3" runat="server" onclick="fnHandleTick(this.checked,'C1,C2,C4');" type="checkbox" />
                                        </td>
                                        <td>
                                            <input id="chkPosD3" runat="server" onclick="fnHandleTick(this.checked,'D1,D2,D4');" type="checkbox" />
                                        </td>
                                        <td>
                                            <asp:TextBox id="txtVideoSourceURL3" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:regularexpressionvalidator id="regURL3" controltovalidate="txtVideoSourceURL3"
                                            display="dynamic" runat="server" setfocusonerror="true" errorMessage="<br> & < and > are invalid characters."
                                            validationExpression="^[^<>&]*$"></asp:regularexpressionvalidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="chkPosA4" runat="server" onclick="fnHandleTick(this.checked,'A1,A2,A3');" type="checkbox" />
                                        </td>
                                        <td>
                                            <input id="chkPosB4" runat="server" onclick="fnHandleTick(this.checked,'B1,B2,B3');" type="checkbox" />
                                        </td>
                                        <td>
                                            <input id="chkPosC4" runat="server" onclick="fnHandleTick(this.checked,'C1,C2,C3');" type="checkbox" />
                                        </td>
                                        <td>
                                            <input id="chkPosD4" runat="server" onclick="fnHandleTick(this.checked,'D1,D2,D3');" type="checkbox" />
                                        </td>
                                        <td>
                                            <asp:TextBox id="txtVideoSourceURL4" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:regularexpressionvalidator id="regURL4" controltovalidate="txtVideoSourceURL4"
                                            display="dynamic" runat="server" setfocusonerror="true" errorMessage="<br> & < and > are invalid characters."
                                            validationExpression="^[^<>&]*$"></asp:regularexpressionvalidator>
                                        </td>
                                    </tr>
                                </table>
                             </td>
                         </tr>
                         <%--ZD 101019 END--%>        
                    
                    
                    </table>
                </td>
            </tr>
            <%--FB 2724 End--%>
            <tr>
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%" border="0">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:imagebutton id="img_PIM"  runat="server"  alternatetext="Expand/Collapse" cssclass ="0" imageurl="image/loc/nolines_plus.gif"
                                                height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext">PIM Features</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trPIM" runat="server" style="display: none">
                <td colspan="7" style="padding-bottom: 0px; padding-left: 0px; padding-right: 0px;
                    padding-top: 0px;">
                     <asp:UpdatePanel ID="UpdatePanel1" runat="server" updatemode="always"> <%--ZD 100068--%>
                     <ContentTemplate>
                     
                    <table border="0" width="100%" cellpadding="5">
                        <tr>
                            <td style="width: 1%"> <%-- FB 2842--%>
                            </td>
                            <td align="left" class="blackblodtext"  valign="top" style="width:25%"> <%-- FB 2842--%>
                                Plugin Confirmations        <%--FB 2926--%>
                                <%--<asp:ImageButton ID="Imgpluginconfirm" valign="center" src="image/info.png" runat="server" ToolTip="Switch to send confirmation mails when conference is scheduled via outlook."/>--%>
                            </td>
                            <td valign="top" align="left" style="width: 23%;"> <%-- FB 2842--%>
                                <asp:dropdownlist id="DrpPluginConfirm" runat="server" cssclass="alt2SelectFormat">
                                        <asp:ListItem Value="0">No</asp:ListItem>
                                        <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <td  style="width: 1%"><%-- FB 2842--%>
                            </td>
                            <td align="left" class="blackblodtext" valign="top" style="width: 27%"> <%-- FB 2842--%>
                                Enable Available Rooms          <%--FB 2926--%>
                                <%--<asp:ImageButton ID="Imgeavailrooms" valign="center" src="image/info.png" runat="server" ToolTip="Switch to show availble rooms  during conference creation via outlook."/>--%>
                            </td>
                            <td valign="top" align="left" style="width: 20%">
                                <asp:dropdownlist id="lstEnablePIMServiceType" runat="server" cssclass="alt2SelectFormat">
                                        <asp:ListItem Value="0">No</asp:ListItem>
                                        <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr><%--ZD 100068 --%>
                        <tr>
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 25%">
                               Tier 1 <span class="reqfldText">*</span> <%--ZD 100843--%>
                            </td>
                            <td style="width: 25%" align="left" valign="top">
                                <asp:DropDownList id="lstVMRTopTier" width="120px" datatextfield="Name" datavaluefield="ID"
                                    runat="server" cssclass="altSelectFormat" onselectedindexchanged="UpdateVMRMiddleTiers" 
                                     autopostback="true">
                                    </asp:DropDownList>
                                 <asp:requiredfieldvalidator id="reqTopTier1" runat="server" controltovalidate="lstVMRTopTier"
                                    display="dynamic" cssclass="lblError" validationgroup="Submit" errormessage="Required">
                                </asp:requiredfieldvalidator>
                                
                                
                            </td>                          
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 27%">
                                Tier 2 <span class="reqfldText">*</span> <%--ZD 100843--%>
                            </td>
                            <td style="width: 20%" align="left" valign="top">
                                <asp:DropDownList id="lstVMRMiddleTier" width="120px" datatextfield="Name" datavaluefield="ID"
                                    runat="server" cssclass="altSelectFormat" causesvalidation="true">
                                    </asp:DropDownList>                                    
                                <asp:requiredfieldvalidator id="reqMiddleTier1" runat="server" controltovalidate="lstVMRMiddleTier"
                                    display="dynamic" cssclass="lblError"  errormessage="Required" validationgroup="Submit"></asp:requiredfieldvalidator>
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr><%-- ZD 100068 end--%>
                        <tr style="display:none">
                         <td style="width: 1%;"> </td>
                            <td style="width: 25%;" align="left" class="blackblodtext"> <%-- FB 2842--%>
                                <asp:label id="lblEnableVMR" runat="server" text="VMR Type"></asp:label>
                                <%--<asp:ImageButton ID="ImgeVMR" valign="center"  src="image/info.png" runat="server" ToolTip="Enable VMR features for calls"/>--%>
                            </td>
                            <td style="width: 25%;" align="left"> <%-- FB 2842--%>
                                <asp:dropdownlist id="lstEnableVMR" runat="server" cssclass="alt2SelectFormat">
                                                    <%--FB 2448--%>
                                                    <asp:ListItem Value="0" Selected="True" Text="None"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Personal"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="Room"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="External"></asp:ListItem>
                                                    <%--FB 2481--%>
                                                </asp:dropdownlist>
                            </td>
                        </tr>
                    </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>  <%-- ZD 100068--%>      
                                   
                </td>
            </tr>
            <tr>
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%" border="0">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:imagebutton id="img_SUR" alternatetext="Expand/Collapse"  cssclass ="0" runat="server" imageurl="image/loc/nolines_plus.gif"
                                                height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext">Survey</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trSUR" runat="server" style="display: none">
                <td colspan="7" style="padding-bottom: 0px; padding-left: 0px; padding-right: 0px;
                    padding-top: 0px;">
                    <table border="0" width="100%" cellpadding="5"> <%-- FB 2842--%>
                        <tr>
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 25%">
                                Enable Survey           <%--FB 2926--%>
                                <%--<asp:ImageButton ID="Imgensurvey" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable Survey Engine field will display.Survey Engine  contain two options.There are Internal Survey,External Survey."/>--%>
                            </td>
                            <td align="left" style="width: 25%">
                                <asp:dropdownlist id="drpenablesurvey" runat="server" width="125px" cssclass="alt2SelectFormat"
                                    onchange="JavaScript:modedisplay1()">
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <td style="width: 1%">
                            </td>
                            <td colspan="2" width="47%">
                                <div id="divSurveyURL" runat="server" style="display: none">
                                    <table width="100%">
                                        <tr>
                                            <td align="left" style="font-weight: bold; width: 58%" class="blackblodtext">
                                                Survey Website URL              <%--FB 2926--%>
                                                <%--<asp:ImageButton ID="Imgsurveyweburl" valign="center" src="image/info.png" runat="server" ToolTip="Survey link to user for the organzation Options."/>--%>
                                            </td>
                                            <td align="left" style="height: 21px;" width="42%">
                                                <asp:textbox id="txtSurWebsiteURL" runat="server" cssclass="altText"></asp:textbox>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
                                                <asp:requiredfieldvalidator id="ReqSurWebsiteURL" runat="server" controltovalidate="txtSurWebsiteURL"
                                                    errormessage="Survey Website URL is required." font-names="Verdana" font-size="X-Small"         
                                                    font-bold="False"><font color="red" size="1pt"> required</font></asp:requiredfieldvalidator>  <%--FB 2926--%>
                                                <asp:regularexpressionvalidator id="RegSurWebsiteURL" controltovalidate="txtSurWebsiteURL"
                                                    display="dynamic" runat="server" setfocusonerror="true" errormessage="& < and > are invalid characters."
                                                    validationexpression="^[^<>&]*$"></asp:regularexpressionvalidator>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 1%">
                            </td>
                            <td id="tdSurveyengine" runat="server" align="left" class="blackblodtext" width="25%"
                                style="visibility: hidden">
                                Survey Engine   <%--FB 2926--%>
                                <%--<asp:ImageButton ID="Imgsurveyeng" valign="center" src="image/info.png" runat="server" ToolTip="If select External Survey Option Survey Website Url,Send Survey will display."/>--%>
                            </td>
                            <td id="tdsurveyoption" runat="server" align="left" width="25%" style="visibility: hidden">
                                <asp:dropdownlist id="drpsurveyoption" runat="server" width="125px" cssclass="alt2SelectFormat"
                                    onchange="JavaScript:modesurvey()">
                                        <asp:ListItem Value="1">Internal Survey</asp:ListItem>
                                        <asp:ListItem Value="2">External Survey</asp:ListItem>
                                    </asp:dropdownlist>
                            </td>
                            <td style="width: 1%">
                            </td>
                            <td colspan="2" width="47%">
                                <div id="divSurveytimedur" runat="server" style="display: none">
                                    <table width="100%">
                                        <tr>
                                            <td align="left" style="font-weight: bold; width: 58%" class="blackblodtext">
                                                Send Survey      <%--FB 2926--%>
                                                <%--<asp:ImageButton ID="Imgsendsurvey" valign="center" src="image/info.png" runat="server" ToolTip="How long after the completion of the Conference where the Survey would have been required."/>--%>
                                            </td>
                                            <td style="height: 21px;" align="left" width="42%">
                                                <asp:textbox id="txtTimeDur" runat="server" maxlength="9" cssclass="altText"></asp:textbox>
                                                (mins)<br />
                                                <asp:regularexpressionvalidator id="RegTimeDur" controltovalidate="txtTimeDur" validationexpression="\d+"
                                                    display="Dynamic" errormessage="Please enter number only." runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%">
                        <tr>
                            <td width="100%" height="20" align="left" class="blackblodtext">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:imagebutton id="img_ADMOPT" alternatetext="Expand/Collapse"  cssclass ="0" runat="server" imageurl="image/loc/nolines_plus.gif" 
                                                height="25" width="25" vspace="0" hspace="0" /> <%--FB 2841--%><%--ZD 100419--%>
                                        </td>
                                        <td>
                                            <span class="subtitleblueblodtext">Technical Info</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trADMOPT" runat="server" style="display: none">
                <td colspan="7">
                    <table border="0" width="100%" cellspacing="0" cellpadding="5">
                        <tr>
                            <td style="width: 1%">
                            </td>
                            <td align="left" valign="baseline" class="blackblodtext" style="width: 25%; bottom: auto">
                                Tech Support Contact        <%--FB 2926--%>
                            </td>
                            <td align="left" valign="baseline" style="width: 25%">
                                <asp:textbox id="ContactName" runat="server" cssclass="altText" maxlength="256" width="187px"></asp:textbox>
                                <asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" controltovalidate="ContactName"
                                    errormessage="Contact Name is required." font-names="Verdana" font-size="X-Small"
                                    font-bold="False"><font color="red" size="1pt"> required</font></asp:requiredfieldvalidator>
                                <asp:regularexpressionvalidator id="regContactName" controltovalidate="ContactName"
                                    display="dynamic" runat="server" setfocusonerror="true" errormessage="<br>& < > + % \ ? | ^ = ! `[ ] { } # $ and ~ are invalid characters."
                                    validationexpression="^(a-z|A-Z|0-9)*[^\\/<>^+?|!`\[\]{}\=#$%&~]*$"></asp:regularexpressionvalidator>
                                <%--FB 1888--%>
                            </td>
                            <td style="width: 1%">
                            </td>
                            <td align="left" style="height: 47px; width: 27%" valign="top" class="blackblodtext">
                                Tech Support Email          <%--FB 2926--%>
                            </td>
                            <td align="left" style="height: 47px; width: 20%" valign="top">
                                <asp:textbox id="ContactEmail" runat="server" cssclass="altText" maxlength="512"></asp:textbox>
                                <asp:regularexpressionvalidator id="RegularExpressionValidator1" runat="server" controltovalidate="ContactEmail"
                                    errormessage="Invalid Tech Support Email." validationexpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    font-names="Verdana" font-size="X-Small" font-bold="False"><font color="red" size="1pt">invalid</font></asp:regularexpressionvalidator>
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 1%">
                            </td>
                            <td align="left" valign="top" style="width: 25%" class="blackblodtext">
                                Tech Support Phone          <%--FB 2926--%>
                            </td>
                            <td align="left" valign="middle" style="width: 25%">
                                <asp:textbox id="ContactPhone" runat="server" cssclass="altText" width="187px" maxlength="250"></asp:textbox>
                                <%--FB 2498--%>
                                <asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" controltovalidate="ContactPhone"
                                    errormessage="Contact Phone is required." font-names="Verdana" font-size="X-Small"
                                    font-bold="False"><font color="red" size="1pt"> required</font></asp:requiredfieldvalidator>
                                <asp:regularexpressionvalidator id="RegularExpressionValidator2" controltovalidate="ContactPhone"
                                    display="dynamic" runat="server" setfocusonerror="true" errormessage="<br>& < > ' % \ / ; ? | ^ = ! ` [ ] { } : # $ @ ~ and &#34; are invalid characters."
                                    validationexpression="^(a-z|A-Z|0-9)*[^\\<>^;?|!`\[\]{}\x22;=@#$%&'~]*$"></asp:regularexpressionvalidator>
                                <%--FB 2319--%>
                            </td>
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" valign="top" style="width: 27%">
                                Additional Information          <%--FB 2926--%>
                            </td>
                            <td align="left" valign="top" style="width: 20%">
                                <asp:textbox id="ContactAdditionInfo" runat="server" cssclass="altText" maxlength="256"></asp:textbox>
                                <asp:regularexpressionvalidator id="regContactAdditionInfo" controltovalidate="ContactAdditionInfo"
                                    display="dynamic" runat="server" setfocusonerror="true" errormessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters."
                                    validationexpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:regularexpressionvalidator>
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
        <table width="800" border="0" cellspacing="4" cellpadding="4" align="center">
            <tr height="50">
                <td align="right">
                  <%--ZD 100263 start--%>
                    <input id="btnReset" type="Reset" name="Reset" value="Reset" runat="server" class="altMedium0BlueButtonFormat" onclick="javascript:history.go(0);"/>
					<%--ZD 100420--%>
                    <%--<asp:Button type="reset" ID="btnReset" runat="server" Text="Reset" CssClass="altMedium0BlueButtonFormat" Onclientclick="javascript:history.go(0);" />--%>
                    <%--<button ID="btnReset" runat="server" class="altMedium0BlueButtonFormat" >Reset</button>--%>
					<%--ZD 100420--%>
                    <%--ZD 100263 End--%>
                </td>
                <td align="center">
                </td>
                <td align="center">
					<%--ZD 100420--%>
                    <%--<button id="btnSubmit" onclick="javascript:return Submit();" onserverclick="btnSubmit_Click"   
                        style="width:100pt" runat="server">Submit</button>--%>
                   <asp:button runat="server" id="btnSubmit" CausesValidation="true" ValidationGroup="Submit"  onclick="btnSubmit_Click"   
                        onclientclick="javascript:return Submit();" text="Submit" width="100pt"/>
					<%--ZD 100420--%>
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <ajax:ModalPopupExtender ID="MessagePopup" runat="server" TargetControlID="btmTxtmsgPopup"
                        BackgroundCssClass="modalBackground" PopupControlID="MessagePanel" DropShadow="false"
                        Drag="true" CancelControlID="btnMsgClose">
                    </ajax:ModalPopupExtender>
                    <asp:panel id="MessagePanel" runat="server" horizontalalign="Center" width="50%"
                        cssclass="treeSelectedNode">
                            <table width="100%" align="center" border="0" bgcolor="E0E0E0"><%--ZD 100426--%>
                                <tr>
                                    <td align="center" style="width :73%">
                                        <span class="subtitleblueblodtext">Active Message Delivery</span>
                                    </td>
                                    <td align="left">
                                     <span class="subtitleblueblodtext">End By</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td id="tdTxtMsgDetails" runat="server" colspan="2">
                                        <table width="100%" border="0">
                                            <tr>
                                                <td>
                                                    <table cellpadding="3" cellspacing="0" width="100%" border="0">
                                                        <tr id="tr2" runat="server">
                                                            <td align="left" width="5%">
                                                                <asp:CheckBox ID="chkmsg1" runat="server" onclick="javascript:return fnCheck('drpdownmsgduration1');" />
                                                            </td>
                                                            <td align="left" width="68%">
                                                                <asp:DropDownList ID="drpdownconfmsg1" CssClass="altSelectFormat" runat="server"
                                                                    DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left" width="20%">
                                                                <asp:DropDownList ID="drpdownmsgduration1" CssClass="altText" runat="server" Width="55px"
                                                                    onChange="javascript:return fnCheck(this.id);">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left" width="7%">
                                                            </td>
                                                        </tr>
                                                        <tr id="tr3" runat="server">
                                                            <td align="left">
                                                                <asp:CheckBox ID="chkmsg2" runat="server" onclick="javascript:return fnCheck('drpdownmsgduration2');" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownconfmsg2" CssClass="altSelectFormat" runat="server"
                                                                    DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownmsgduration2" CssClass="altText" runat="server" Width="55px"
                                                                    onChange="javascript:return fnCheck(this.id);">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr id="tr4" runat="server">
                                                            <td align="left">
                                                                <asp:CheckBox ID="chkmsg3" runat="server" onclick="javascript:return fnCheck('drpdownmsgduration3');" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownconfmsg3" CssClass="altSelectFormat" runat="server"
                                                                    DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownmsgduration3" CssClass="altText" runat="server" Width="55px"
                                                                    onChange="javascript:return fnCheck(this.id);">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left" style="display: none;">
                                                                <a id="displayText" href="javascript:toggle();">more</a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table id="toggleText" cellpadding="3" cellspacing="0" width="100%" border="0">
                                                        <tr id="tr5" runat="server">
                                                            <td align="left" width="5%">
                                                                <asp:CheckBox ID="chkmsg4" runat="server" onclick="javascript:return fnCheck('drpdownmsgduration4');" />
                                                            </td>
                                                            <td align="left" width="68%">
                                                                <asp:DropDownList ID="drpdownconfmsg4" CssClass="altSelectFormat" runat="server"
                                                                    DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left" width="20%">
                                                                <asp:DropDownList ID="drpdownmsgduration4" CssClass="altText" runat="server" Width="55px"
                                                                    onChange="javascript:return fnCheck(this.id);">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left" width="7%">
                                                            </td>
                                                        </tr>
                                                        <tr id="tr6" runat="server">
                                                            <td align="left">
                                                                <asp:CheckBox ID="chkmsg5" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration5');" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownconfmsg5" CssClass="altSelectFormat" runat="server"
                                                                    DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownmsgduration5" CssClass="altText" runat="server" Width="55px"
                                                                    onChange="javascript:return fnCheck(this.id);">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr id="tr7" runat="server">
                                                            <td align="left">
                                                                <asp:CheckBox ID="chkmsg6" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration6');" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownconfmsg6" CssClass="altSelectFormat" runat="server"
                                                                    DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownmsgduration6" CssClass="altText" runat="server" Width="55px"
                                                                    onChange="javascript:return fnCheck(this.id);">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr id="tr8" runat="server">
                                                            <td align="left">
                                                                <asp:CheckBox ID="chkmsg7" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration7');" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownconfmsg7" CssClass="altSelectFormat" runat="server"
                                                                    DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownmsgduration7" CssClass="altText" runat="server" Width="55px"
                                                                    onChange="javascript:return fnCheck(this.id);">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr id="tr9" runat="server">
                                                            <td align="left">
                                                                <asp:CheckBox ID="chkmsg8" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration8');" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownconfmsg8" CssClass="altSelectFormat" runat="server"
                                                                    DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownmsgduration8" CssClass="altText" runat="server" Width="55px"
                                                                    onChange="javascript:return fnCheck(this.id);">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr id="tr10" runat="server">
                                                            <td align="left">
                                                                <asp:CheckBox ID="chkmsg9" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration9');" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownconfmsg9" CssClass="altSelectFormat" runat="server"
                                                                    DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownmsgduration9" CssClass="altText" runat="server" Width="55px"
                                                                    onChange="javascript:return fnCheck(this.id);">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <br />
                                        <button align="middle" runat="server" id="btnMsgClose"
                                            class="altMedium0BlueButtonFormat">Close</button><%--ZD 100420--%>
                                        <button ID="btnMsgSubmit" runat="server" onserverclick="fnOrgTxtMsgSubmit"
                                            class="altMedium0BlueButtonFormat" ValidationGroup="Submit1">Submit</button>
                                    </td>
                                </tr>
                            </table>
                        </asp:panel>
                </td>
            </tr>
        </table>
    </div>

    <script type="text/javascript">
        //Edited For FF...
        if (document.getElementById("systemStartTime_Container") != null)
            document.getElementById("systemStartTime_Container").style.width = "auto"
        if (document.getElementById("systemEndTime_Container") != null)
            document.getElementById("systemEndTime_Container").style.width = "auto"
    </script>

    <script language="javascript" type="text/javascript">
        open24();
    </script>

    </form>
</div>
</body> </html>
<%--code added for Soft Edge button--%>

<script type="text/javascript" src="inc/softedge.js"></script>

<!----------------------------------------->
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->

<script type="text/javascript">
    function fnHandleTick(stat, par) { //ZD 101019
        if (stat) {
            var ids = par.split(',');
            for (var i = 0; i < ids.length; i++) {
                document.getElementById('chkPos' + ids[i]).checked = false;
            }
        }
    }

    function fnUpdatePosVertStatus() { //ZD 101019
        var idList = ["chkPosA", "chkPosB", "chkPosC", "chkPosD"];
        for (var i = 0; i < idList.length; i++) {
            document.getElementById(idList[i] + 'Vert').value = "0";
            for(var j=1; j<5; j++)
            {
                if (document.getElementById(idList[i] + j.toString()).checked == true) {
                    document.getElementById(idList[i] + 'Vert').value = (i+1).toString();
                    break;
                }
            }
        }
        fnUpdatePosHorStatus();
    }

    function fnUpdatePosHorStatus() { //ZD 101019
        var idList = ["chkPosA", "chkPosB", "chkPosC", "chkPosD"];
        var chkd = "";
        for (var p = 1; p < 5; p++) {
            document.getElementById(idList[p - 1] + 'Hor').value = "";
            for (var q = 0; q < idList.length; q++) {
                if (document.getElementById(idList[q] + p.toString()).checked == true) {
                    chkd += "," + (q + 1).toString();
                }
            }
            chkd = chkd.replace(",", "");
            document.getElementById(idList[p - 1] + 'Hor').value = chkd;
            chkd = "";
        }
    }

    // FB 2384 Starts
    function findPos(obj) {
        var curleft = curtop = 0;

        if (obj.offsetParent) {
            curleft = obj.offsetLeft
            curtop = obj.offsetTop
            while (obj = obj.offsetParent) {
                curleft += obj.offsetLeft
                curtop += obj.offsetTop
            }
        }
        return [curleft, curtop];
    }
    //FB 2384 Ends
    // FB 2335 Starts
    var invokingObject;
    function changeLayout(invoker, myObj, posval) // FB 2384
    {
        invokingObject = invoker;
        promptpicture = "image/pen.gif";
        prompttitle = "Manage Auto Screen Layout Mapping";
        epid = "01";
        rowsize = 5;
        images = "01:02:03:04:05:06:07:08:09:10:11:12:13:14:15:16:17:18:19:20:21:22:23:24:25:26:27:28:29:30:31:32:33:34:35:36:37:38:39:40:41:42:43:44:45:46:47:48:49:50:51:52:53:54:55:56:57:58:59:";

        if (invokingObject == "Poly2MGC" || invokingObject == "Poly2RMX") {

            images = "01:02:03:04:05:06:12:13:14:15:16:17:18:19:20:24:25:33:60:61:62:63:";

        }

        imgpath = "image/displaylayout/";
        var title = new Array()
        title[0] = "Default ";
        title[1] = "Custom ";
        promptbox = document.createElement('div');
        promptbox.setAttribute('id', 'prompt');
        document.getElementsByTagName('body')[0].appendChild(promptbox);
        promptbox = document.getElementById('prompt').style; // FB 2050
        //FB 2384 starts
        var pos = findPos(myObj);
        if (posval == 1) {
            pos[0] = pos[0] - 90;
            pos[1] = pos[1] - 20;
        }
        else
            pos[0] = pos[0] - 450;//FB 2927
        pos[1] = pos[1] - 160;
        promptbox.position = 'absolute'
        promptbox.top = pos[1] + 'px'; //FB 1373 start FB 2050
        promptbox.left = pos[0] + 'px'; // FB 2050
        //FB 2384 ends
        promptbox.width = rowsize * 125 + 'px'; // FB 2050
        promptbox.border = 'outset 1 #bbbbbb';
        promptbox.height = '400px'; // FB 2050
        promptbox.overflow = 'auto'; //FB 1373 End
        promptbox.backgroundColor = '#FFFFE6'; //ZD 100426
        
        

        m = "<table cellspacing='0' cellpadding='0' border='0' width='100%'><tr valign='middle'><td width='22' height='22' style='text-indent:2;' class='tableHeader'>&nbsp;</td><td class='tableHeader'>" + prompttitle + "</td></tr></table>"
        m += "<table cellspacing='2' cellpadding='2' border='0' width='100%' class='promptbox'>";
        imagesary = images.split(":");
        rowNum = parseInt((imagesary.length + rowsize - 2) / rowsize, 10);
        m += "  <tr><td align='right' colspan='" + (rowsize * 2) + "'>"
        //Code Changed for Soft Edge Button
        //m += "    <input type='button' class='altShortBlueButtonFormat' value='Submit' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='saveOrder(epid);'>"
        //m += "    <input type='button' class='altShortBlueButtonFormat' value='Cancel' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='cancelthis();'>"
        m += "    <button id='btnCancelLayout' class='altMedium0BlueButtonFormat' style='width:80px'  onClick='cancelthis();'>Cancel</button>"
        m += "    <button id='btnSubmitLayout' class='altMedium0BlueButtonFormat' style='width:80px'  onClick='saveOrder(epid);'>Submit</button>"
        m += "  </td></tr>"
        m += "	<tr>";
        //Window Dressing
        m += "    <td colspan='" + (rowsize * 2) + "' align='left' class='blackblodtext'>Display Layout</td>";
        m += "  </tr>"
        m += "  <tr>"
        m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
        m += "  </tr>"

        imgno = 0;
        for (i = 0; i < rowNum; i++) {
            m += "  <tr>";
            for (j = 0; (j < rowsize) && (imgno < imagesary.length - 1); j++) {


                m += "    <td valign='middle'>";
                m += "      <input type='radio' name='layout' id='layout' value='" + imagesary[imgno] + "' onClick='epid=" + imagesary[imgno] + ";'>";
                m += "    </td>";
                m += "    <td valign='middle'>";
                m += "      <img src='" + imgpath + imagesary[imgno] + ".gif' width='57' height='43' alt='Layout'>"; //ZD 100419
                m += "    </td>";
                imgno++;
            }
            m += "  </tr>";
        }

        m += "  <tr>";
        m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
        m += "  </tr>"
        m += "  <tr><td align='right' colspan='" + (rowsize * 2) + "'>"
        //Code Changed for Soft Edge Button
        //m += "    <input type='button' class='altShortBlueButtonFormat' value='Submit' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='saveOrder(epid);'>"
        //m += "    <input type='button' class='altShortBlueButtonFormat' value='Cancel' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='cancelthis();'>"
        m += "    <button id='btnCancelLayout1' class='altMedium0BlueButtonFormat' style='width:80px' onClick='cancelthis();'>Cancel</button>"
        m += "    <button id='btnSubmitLayout1' class='altMedium0BlueButtonFormat' style='width:80px' onClick='saveOrder(epid);'>Submit</button>"
        m += "  </td></tr>"
        m += "</table>"

        document.getElementById('prompt').innerHTML = m;
        //ZD 100420
        if (document.getElementById('butLayoutMapping1') != null)            
            document.getElementById('butLayoutMapping1').setAttribute("onblur", "document.getElementById('btnCancelLayout').focus(); document.getElementById('btnCancelLayout').setAttribute('onfocus', '');");
        if (document.getElementById('butLayoutMapping2') != null)
            document.getElementById('butLayoutMapping2').setAttribute("onblur", "document.getElementById('btnCancelLayout').focus(); document.getElementById('btnCancelLayout').setAttribute('onfocus', '');");
        if (document.getElementById('btnSubmitLayout1') != null)
            document.getElementById('btnSubmitLayout1').setAttribute("onblur", "document.getElementById('btnCancelLayout').focus(); document.getElementById('btnCancelLayout').setAttribute('onfocus', '');");
        
    }

    function saveOrder(id) {






        if (id < 10)
            id = "0" + id;

        if (invokingObject == "Poly2MGC") {
            if (id == "001")
                id = document.getElementById("Poly2MGC").value;
            document.getElementById("Poly2MGC").value = id;
            document.getElementById("imgLayoutMapping1").src = "image/displaylayout/" + id + ".gif";
        }
        else if (invokingObject == "Poly2RMX") {
            if (id == "001")
                id = document.getElementById("Poly2RMX").value;
            document.getElementById("Poly2RMX").value = id;
            document.getElementById("imgLayoutMapping2").src = "image/displaylayout/" + id + ".gif";
        }
        else if (invokingObject == "CTMS2Cisco") {
            document.getElementById("CTMS2Cisco").value = id;
            document.getElementById("imgLayoutMapping3").src = "image/displaylayout/" + id + ".gif";
        }
        else if (invokingObject == "CTMS2Poly") {
            document.getElementById("CTMS2Poly").value = id;
            document.getElementById("imgLayoutMapping4").src = "image/displaylayout/" + id + ".gif";
        }


        cancelthis();
    }


    function cancelthis() {
        document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
        //window.resizeTo(750,450); //FB Case 536 Saima
    }

    ////    var obj = document.getElementById("dgTxtMsg_ctl03_btnEdit");
    ////    getLabel = function(elem){
    ////    if (elem.id && elem.id=="label") {
    ////    elem.id = "disabledLabel";
    ////    }
    ////    };
    ////    Dom.getElementsBy(getLabel ,'td', obj);


    // FB 2335 Ends
    ChangeEnableSmartP2P();  //FB 2430
    function ChangeEnableSmartP2P() //FB 2430
    {
        document.getElementById("lstEnableSmartP2P").disabled = false;
        if (document.getElementById("p2pConfEnabled").value == "0") {
            document.getElementById("lstEnableSmartP2P").value = "0";
            document.getElementById("lstEnableSmartP2P").disabled = true;
        }
    }
    //FB 2571 START
    function fnFeccOptions() {

        if (document.getElementById("lstenableFECC").value == "2") {

            document.getElementById("DefaultFECC").style.visibility = "hidden";
            document.getElementById("DefaultFECCoptions").style.visibility = "hidden";
        }
        else {
            document.getElementById("DefaultFECC").style.visibility = "visible";
            document.getElementById("DefaultFECCoptions").style.visibility = "visible";

        }
        if (document.getElementById("lstenableFECC").value == "0") {
            document.getElementById("lstdefaultFECC").disabled = true;
            document.getElementById("DefaultFECC").disabled = true;
            document.getElementById("lstdefaultFECC").value = "1";
        }
        else {
            document.getElementById("lstdefaultFECC").disabled = false;
        }
    }
    //FB 2571 END


</script>

<script type="text/javascript">
    function fntimezonesOptions() {

        if (document.getElementById("DropDownZuLu").value == "1") {
            document.getElementById("TimezoneSystems").disabled = true;
            document.getElementById("TimezoneSystems").value = "0";
        }
        else {
            document.getElementById("TimezoneSystems").disabled = false;
        }
    }
    //FB 2588 END
    
    //ZD 100284 - Start
    if (document.getElementById("systemStartTime_Text")) {
        var confstarttime_text = document.getElementById("systemStartTime_Text");
        confstarttime_text.onblur = function() {
        formatTimeNew('systemStartTime_Text', 'regSysStartTime',"<%=Session["timeFormat"]%>")
        };
    }
    
    if (document.getElementById("systemEndTime_Text")) {
        var confstarttime_text = document.getElementById("systemEndTime_Text");
        confstarttime_text.onblur = function() {
        formatTimeNew('systemEndTime_Text', 'regSysEndTime',"<%=Session["timeFormat"]%>")
        };
    }
    //ZD 100420 - Start
    if (document.getElementById('txtWhiteList') != null)
        document.getElementById('txtWhiteList').setAttribute("onblur", "document.getElementById('btnWhiteList').focus(); document.getElementById('btnWhiteList').setAttribute('onfocus', '');");               
    if (document.getElementById('lstWhiteList') != null)
        document.getElementById('lstWhiteList').setAttribute("onkeydown", "if(event.keyCode ==32){javascript:return AddRemoveWhiteList('Rem')}");               
    if (document.getElementById('btnReset') != null)
        document.getElementById('btnReset').setAttribute("onblur", "document.getElementById('btnSubmit').focus(); document.getElementById('btnSubmit').setAttribute('onfocus', '');");               
    
    //ZD 100420 - End
          
</script>

<%--ZD 100428 START- Close the popup window using the esc key--%>
<script language="javascript" type="text/javascript">
    document.onkeydown = function(evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 8) {
            if (document.getElementById("btnCancel") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnCancel").click();
                    return false;
                }
            }
            if (document.getElementById("btnGoBack") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnGoBack").click();
                    return false;
                }
            }
        }
        fnOnKeyDown(evt);
    };

    function EscClosePopup() {
			document.getElementById('btnMsgClose').click()
            if(document.getElementById("prompt") != null)
                cancelthis();
    }
</script>
<%--ZD 100428 END--%>

