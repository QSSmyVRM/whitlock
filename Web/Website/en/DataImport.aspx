<%--ZD 100147 start--%>
<%--/* Copyright (C) 2014 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DataImport.aspx.cs" Inherits="ns_DataImport.DataImport" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 --> <%--FB 2779--%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<script language="javascript">
//ZD 100604 start
var img = new Image();
img.src = "../en/image/wait1.gif";
//ZD 100604 End
function DataLoading()
{
    var obj = document.getElementById("tblDataImport");
    if (obj != null)
        obj.style.display="";
}
</script>
    <title>Database Import</title>
</head>
<body>
    <form id="frmDataImport" runat="server">
        <h3 style="text-align: center">
            Data Import Tool</h3>
            <br />
            <center>
                <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
            </center>
            <br />
            <center>
            <asp:GridView ID="newErrGrid" runat ="server" Width="65%" AutoGenerateColumns="false">
            <Columns>
                <asp:BoundField HeaderStyle-Width="12%" DataField="Row No" HeaderText="Row No" />
                <asp:BoundField HeaderStyle-HorizontalAlign="Left" DataField="Reason" HeaderText="Reason"/>
            </Columns>
            </asp:GridView>
            </center>
            <br /><br />
            <table id="tblDataImport" width="100%">
            <tr>
                 <td align="center">
                    <b><img border="0" src="image/wait1.gif" alt="Loading.." ><%-- FB 2742 --%> <%--ZD 100419--%>
               </td>
            </tr>
           </table>            
            <table width="100%" bgcolor="white" cellpadding="1" cellspacing="0">
                <tr align="center">
                    <td width="40%" align="center" id="tdDBType"> <%--ZD 100456--%>
                        <b>External Database Type:</b>&nbsp;
                        <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstDatabaseType" runat="server">
                            <asp:ListItem Selected="True" Text="Rendezvous" Value="1"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td width="60%">
                        <asp:FileUpload ID="fleMasterCSV" Width="60%" EnableViewState="true" runat="server" CssClass="altText" />  
                        <asp:Button ID="btnGetDataTable" runat="server" OnClick="GenerateDataTable" CssClass="altShortBlueButtonFormat" Text="Upload" />
                    </td>
                </tr>
            </table>
            <br /><br />
            <table width="100%" bgcolor="white" cellpadding="1" cellspacing="0" border="1" >
                <tr>
                    <td style="text-align: center">
                        1</td>
                    <td>
                        Import Tier1(s)</td>
                    <td width="20%" align="center">
                        <asp:Button ID="btnImportTier1" runat="server" Width="50pt"
                            Text="Submit" OnClick="ImportTier1s" OnClientClick="javascript:DataLoading()" /></td> <%--FB 2519--%>
                </tr>
                <tr bgcolor=Gainsboro>
                    <td style="text-align: center">
                        2</td>
                    <td>
                        Import Tier2(s)</td>
                    <td align="center">
                        <asp:Button ID="btnImportTier2" runat="server" Width="50pt"
                            Text="Submit" OnClick="ImportTier2s" OnClientClick="javascript:DataLoading()" /></td><%--FB 2519--%>
                </tr>
                <tr>
                    <td style="text-align: center">
                        3</td>
                    <td>
                        Import Department(s)</td>
                    <td align="center">
                        <asp:Button ID="btnImportDepartment" runat="server"  Width="50pt"
                            Text="Submit" OnClick="ImportDepartments"  OnClientClick="javascript:DataLoading()" /></td> <%--FB 2519--%>
                </tr>
                
                
                <tr bgcolor=Gainsboro>
                    <td style="text-align: center">
                        4</td>
                    <td>
                        Import MCU(s)</td>
                    <td align="center">
                        <asp:Button ID="btnImportmcu" runat="server"  Width="50pt"
                            Text="Submit" OnClick="Importmcu" OnClientClick="javascript:DataLoading()"  /></td> <%--FB 2519--%>
                </tr>
                
                <tr>
                    <td style="text-align: center">
                        5</td>
                    <td>
                        Import Endpoint(s)</td>
                    <td align="center">
                        <asp:Button ID="btnImportEndpoints" runat="server" Width="50pt"
                            Text="Submit" OnClick="ImportEndpoints" OnClientClick="javascript:DataLoading()"  /></td> <%--FB 2519--%>
                </tr>
                <tr bgcolor=Gainsboro>
                    <td style="text-align: center">
                        6</td>
                    <td>
                        Import Room(s)</td>
                    <td align="center">
                        <asp:Button ID="btnImportRooms" runat="server" Width="50pt"
                          Text="Submit" OnClick="ImportRooms" OnClientClick="javascript:DataLoading()" /></td> <%--FB 2519--%>
                </tr>
                <tr>
                    <td style="text-align: center">
                        7</td>
                    <td>
                        Import User(s)</td>
                    <td align="center">
                        <asp:Button ID="btnImportUsers" runat="server" Width="50pt"
                            Text="Submit" OnClick="ImportUsers" OnClientClick="javascript:DataLoading()" /></td> <%--FB 2519--%>
                </tr>
                <tr bgcolor=Gainsboro>
                    <td style="text-align: center">
                        8</td>
                    <td>
                        Import Conference(s)</td>
                    <td align="center">
                        <asp:Button ID="btnImportConferences" runat="server"  Text="Submit" OnClick="ImportConferences" OnClientClick="javascript:DataLoading()" Width="50pt" /></td> <%--FB 2519--%>
                </tr>
                 <tr>
                    <td style="text-align: center">
                        9</td>
                    <td>
                        Import Default CSS XML
                        
                        <asp:FileUpload ID="cssXMLFileUpload" Width="50%" EnableViewState="true" runat="server" CssClass="altText" />  
                        
                    </td>
                    <td align="center">
                        <asp:Button ID="btnImportDefaultCSSXML" runat="server" OnClick="ImportDefaultCSSXML" Text="Submit" OnClientClick="javascript:DataLoading()"  Width="50pt"/> <%--FB 2519--%>
                   </td>
                </tr>
            </table>
        <br />
        <table>
            <tr id="trNote">
                <td align="left">
                    <b>NOTE:</b> This section should only be accessed from the web server using localhost. The data file should reside on the web server it self.
                </td>
            </tr>
        </table>
        <%--ZD 100456--%>
         <table border="0" width="100%">
          <tr align="center">
                <td align="center"> 
                  <button type="button" id="btnCancel" class="altLongYellowButtonFormat" onclick="fnCancel();" style="width:100px">Cancel</button>
                </td> 
             </tr>
         </table>
        <%--<table id="tblDataImport">
            <tr>
                <td>
                    <b><font color="#FF00FF" size="2">Data loading ...</font></b>&nbsp;&nbsp;&nbsp;&nbsp;<img border="0" src="image/wait1.gif" width="100" height="12">
                </td>
            </tr>
        </table>--%>
    </form>
<br />
<br />
<p>&nbsp;</p>
<p>&nbsp;</p>
<script language="javascript">
    document.getElementById("tblDataImport").style.display = "none";

    var strRequest = "";
    strRequest = "<%=strRequest %>";
    if (strRequest == "") {
        if (document.getElementById("btnCancel")) {
            document.getElementById("btnCancel").style.display = "none";
        }
    }
    else {
        if(document.getElementById("tdDBType"))
            document.getElementById("tdDBType").style.display = "none";

        if (document.getElementById("trNote"))
            document.getElementById("trNote").style.display = "none";

        document.onkeydown = function(evt) {
            evt = evt || window.event;
            var keyCode = evt.keyCode;
            if (keyCode == 8) {
                if (document.getElementById("btnCancel") != null) { // backspace
                    var str = document.activeElement.type;
                    if (!(str == "text" || str == "textarea" || str == "password")) {
                        document.getElementById("btnCancel").click();
                        return false;
                    }
                }
            }
            fnOnKeyDown(evt);
        };
    }
    
    function fnCancel() //ZD 100456
    {
        window.location.href = 'ManageDataImport.aspx';
        return false;
    }

    //ZD 100745
    if (document.getElementById('fleMasterCSV') != null)
        document.getElementById('fleMasterCSV').setAttribute("onblur", "document.getElementById('btnGetDataTable').focus(); document.getElementById('btnGetDataTable').setAttribute('onfocus', '');");

    if (document.getElementById('btnGetDataTable') != null) {
        if (strRequest == "")
            document.getElementById('btnGetDataTable').setAttribute("onblur", "document.getElementById('btnImportTier1').focus(); document.getElementById('btnImportTier1').setAttribute('onfocus', '');");
        else
            document.getElementById('btnGetDataTable').setAttribute("onblur", "document.getElementById('btnImportmcu').focus(); document.getElementById('btnImportmcu').setAttribute('onfocus', '');");
    }
    if (document.getElementById('btnImportTier1') != null)
        document.getElementById('btnImportTier1').setAttribute("onblur", "document.getElementById('btnImportTier2').focus(); document.getElementById('btnImportTier2').setAttribute('onfocus', '');");

    if (document.getElementById('btnImportTier2') != null)
        document.getElementById('btnImportTier2').setAttribute("onblur", "document.getElementById('btnImportDepartment').focus(); document.getElementById('btnImportDepartment').setAttribute('onfocus', '');");

    if (document.getElementById('btnImportDepartment') != null)
        document.getElementById('btnImportDepartment').setAttribute("onblur", "document.getElementById('btnImportmcu').focus(); document.getElementById('btnImportmcu').setAttribute('onfocus', '');");

    if (document.getElementById('btnImportmcu') != null)
        document.getElementById('btnImportmcu').setAttribute("onblur", "document.getElementById('btnImportEndpoints').focus(); document.getElementById('btnImportEndpoints').setAttribute('onfocus', '');");

    if (document.getElementById('btnImportEndpoints') != null)
        document.getElementById('btnImportEndpoints').setAttribute("onblur", "document.getElementById('btnImportRooms').focus(); document.getElementById('btnImportRooms').setAttribute('onfocus', '');");

    if (document.getElementById('btnImportRooms') != null)
        document.getElementById('btnImportRooms').setAttribute("onblur", "document.getElementById('btnImportUsers').focus(); document.getElementById('btnImportUsers').setAttribute('onfocus', '');");

    if (document.getElementById('btnImportUsers') != null) {
        if (strRequest == "")
            document.getElementById('btnImportUsers').setAttribute("onblur", "document.getElementById('btnImportConferences').focus(); document.getElementById('btnImportConferences').setAttribute('onfocus', '');");
        else
            document.getElementById('btnImportUsers').setAttribute("onblur", "document.getElementById('btnCancel').focus(); document.getElementById('btnCancel').setAttribute('onfocus', '');");
    }

    if (document.getElementById('btnImportConferences') != null)
        document.getElementById('btnImportConferences').setAttribute("onblur", "document.getElementById('cssXMLFileUpload').focus(); document.getElementById('cssXMLFileUpload').setAttribute('onfocus', '');");

    if (document.getElementById('cssXMLFileUpload') != null)
        document.getElementById('cssXMLFileUpload').setAttribute("onblur", "document.getElementById('btnImportDefaultCSSXML').focus(); document.getElementById('btnImportDefaultCSSXML').setAttribute('onfocus', '');");

    if (document.getElementById('btnImportDefaultCSSXML') != null)
        document.getElementById('btnImportDefaultCSSXML').setAttribute("onblur", "document.getElementById('btnCancel').focus(); document.getElementById('btnCancel').setAttribute('onfocus', '');");

    
</script>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" --> 
</body>
</html>
