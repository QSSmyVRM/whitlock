<%--ZD 100147 start--%>
<%--/* Copyright (C) 2014 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" Inherits="ns_ItemsList.ItemsList" EnableSessionState="True" %><%--ZD 100170--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--Window Dressing Start-->
  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css"/>
  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css"/>
   <!--Window Dressing End-->
<script type="text/javascript">    // FB 2790
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
</script>
<script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055--%>

<script runat="server">

</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Items List</title>
    <style type="text/css">
        
        /* ZD 100637 Starts */
        .file_input_textbox {
            height:20px; 
            width:200px; 
            float:left; 
            }

        .file_input_div {
            position: relative;
            width:80px; 
            height:30px;
            overflow: hidden;
            float:left; }

        .file_input_button {
            width: 80px;
            position:absolute;
            top:0px; 
            border:1px solid #A7958B;
            padding:2px 8px 2px 8px; 
            font-weight: normal;
            height:24px;
            margin:0px; 
            margin-right:5px; 
            vertical-align:bottom; 
            background-color:#D4D0C8; 
            }

        .file_input_hidden {
            font-size :45px;
            position:absolute;
            right:0px;top:0px;
            cursor:pointer; 
            opacity:0; 
            filter:alpha(opacity=0); 
            -ms-filter:"alpha(opacity=0)";
            -khtml-opacity:0;
            -moz-opacity:0;
            }
        /* ZD 100637 End */ 
        </style>
    <script language="javascript">
        function checkFileExtension(elem) {
            var filePath = elem.value;

            if(filePath.indexOf('.') == -1)
                return false;
            
            var validExtensions = new Array();
            var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();

            validExtensions[0] = 'jpg';
            validExtensions[1] = 'jpeg';
            validExtensions[2] = 'bmp';
            validExtensions[3] = 'png';
            validExtensions[4] = 'gif';  

            for(var i = 0; i < validExtensions.length; i++) {
                if(ext == validExtensions[i])
                    return true;
            }
            alert('The file extension \"' + ext.toUpperCase() + '\" is not allowed!');
            return false;
        }
		// FB 2909 start //ZD 100583 starts //ZD 100637 starts
        function getfilename(obj) {
            var fileInputVal = obj.value;
            fileInputVal = fileInputVal.replace("C:\\fakepath\\", "");
            if (navigator.userAgent.indexOf("MSIE") > -1)
                obj.parentNode.parentNode.childNodes[0].value = fileInputVal;
            else
                obj.parentNode.parentNode.childNodes[1].value = fileInputVal;
            //if(document.getElementById('fleImage').value!="")
                //document.getElementById('browsetext').value = document.getElementById('fleImage').value
        }
        //FB 2909 End //ZD 100583 Ends //ZD 100637 Ends
    </script>
</head>
<body>
    <form id="frmItemsList" runat="server" method="post" onsubmit="return true">
        <center>
    <div>
        <table width="100%" border="0">
            <tr>
                <td align="center" colspan="3">
                    <h3>Select Items from the list below</h3>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="3">
                    <asp:Label ID="errLabel" runat="server" Font-Size="Small" Visible="False"></asp:Label><%--FB 2487--%>
                    <input type="hidden" id="txtType" value=""  />
                    <input runat="server" type="hidden" id="txtSrcID" value=""  />
            </td>
            </tr>
            <tr>
                <td align="center" colspan="3">
                    <asp:DataGrid BorderColor="blue" BorderStyle="solid" BorderWidth="1" ID="dgItems" AutoGenerateColumns="false"
                     OnItemCreated="BindRowsDeleteMessage" OnDeleteCommand="DeleteItem" OnItemDataBound="BindImages"
                     runat="server" Width="90%" GridLines="None" style="border-collapse:separate"> <%--Edited for FF--%>
                        <HeaderStyle Height="30" CssClass="tableHeader" HorizontalAlign="Center" />
                         <%--Window Dressing - Start --%>
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" HorizontalAlign="Center"/> <%--Edited for FF--%>
                        <FooterStyle CssClass="tableBody" />
                        <%--Window Dressing - End --%>
                        <Columns>
                            <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Image" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ImageId" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ImageName" Visible="false"></asp:BoundColumn>
                                                                                    
                            <asp:TemplateColumn HeaderText="Select" HeaderStyle-CssClass="tableHeader" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center">
                                <ItemTemplate>
                                    <asp:CheckBox id="chkSelectItem" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Name" Visible="true" HeaderText="Name" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                            
                            <asp:TemplateColumn HeaderText="Image" HeaderStyle-CssClass="tableHeader" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center">
                                <ItemTemplate>
                                    <%--<cc1:ImageControl id="itemImage" Width="30" Height="30" Visible="false" Runat="server"></cc1:ImageControl>--%>
                                    <%--<asp:Image ID="itemImage" visible="true" ImageUrl='<%# DataBinder.Eval(Container, "DataItem.ImagePath") %>'  Width="30" Height="30" runat="server" />--%>
                                    <asp:Image ID="itemImage" visible="true" Width="30" Height="30" runat="server" AlternateText="Work Order Item" /> <%--ZD 100419--%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Actions" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Center"> <%--Edited for FF--%>
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnDelete" Text="Delete" CommandName="Delete" runat="server"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="3">
            <table>
                <tr>
                    <td colspan="3" rowspan="3" height="40" valign="bottom">
                        <%--Code changed for Softedge button--%>
                        <input type="button" name="Cancel1" onclick="javascript:window.close()" value="Cancel" class="altMedium0BlueButtonFormat"/>&nbsp;
						<%--ZD 100420--%>
                        <%--<asp:Button ID="Button1" runat="server" CssClass="altMedium0BlueButtonFormat" OnClick="btnSubmit_Click" Text="Submit" />--%>
                        <button ID="btnSubmit" runat="server" class="altMedium0BlueButtonFormat" onserverclick="btnSubmit_Click">Submit</button>
						<%--ZD 100420--%>
                    </td>
                </tr>
                <tr>
                </tr>
            </table>
                    <asp:HiddenField ID="selItems" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <table width="90%">
                        <tr>
                            <td align="left">
                                <span class="subtitleblueblodtext">Upload New Item:</span>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table width="90%" cellspacing="5">
                                    <tr>
                                    <%--Window Dressing--%>
                                        <td align="left" class="blackblodtext">
                                            Enter Name
                                        </td>
                                        <td align="left">
                                             <%--Window Dressing--%>
                                            <asp:TextBox ID="txtItemID" Text="new" Visible="false" CssClass="altText" runat="server"></asp:TextBox>
                                            <asp:TextBox ID="txtItemName" runat="server" CssClass="altText" ></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqName" ValidationGroup="Upload" runat="server" ControlToValidate="txtItemName" ErrorMessage="Required" Display="dynamic" ></asp:RequiredFieldValidator>
                                            <!--[Vivek Issue no - 282] Regular expression validation added for ItemName -->
                                            <asp:RegularExpressionValidator ID="regItemName1" ControlToValidate="txtItemName" Display="dynamic" runat="server" ValidationGroup="Upload" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                       <%--Window Dressing--%>
                                        <td align="left" class="blackblodtext">
                                            Upload Image
                                        </td>
                                        <td align="left"> <%--FB 2909 start--%>
                                        <%--ZD 100583 starts--%><%--ZD 100637 starts--%>
                                            <div>
                                            <input type="text" class="file_input_textbox" readonly="readonly" value='No file selected' style="width:156px"/>
                                            <div class="file_input_div"><input type="button" value="Browse" class="file_input_button"  onclick="document.getElementById('fleImage').click();return false;"  /><%--FB 3055-Filter in Upload Files --%>
                                             <asp:FileUpload ID="fleImage" accept="image/*" runat="server" class="file_input_hidden" TabIndex="-1" OnChange="getfilename(this)" /><br /></div></div>
                                                                                                                                         
                                            <%--ZD 100583 ends--%>  <%--ZD 100637 Ends--%>   
                                            <%--<asp:FileUpload ID="fleImage" runat="server" CssClass="altText" ></asp:FileUpload>--%>
                                            <asp:RequiredFieldValidator ID="reqImage" ValidationGroup="Upload" runat="server" ControlToValidate="fleImage" ErrorMessage="Required" Display="dynamic" ></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="regfleimage" runat="server" Display="Dynamic" ValidationGroup="Upload" ControlToValidate="fleImage" ErrorMessage="File type is invalid." ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G))$"></asp:RegularExpressionValidator> <%--FB 2924 FB 2925--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="2">
											<%--ZD 100420--%>
                                            <%--<asp:Button ID="Button1" OnClick="AddCategoryItem" ValidationGroup="Upload" runat="server" CssClass="altMedium0BlueButtonFormat" Text="Submit"></asp:Button>--%>
                                            <button ID="btnAddItem" onserverclick="AddCategoryItem" ValidationGroup="Upload" runat="server" class="altMedium0BlueButtonFormat">Submit</button>
											<%--ZD 100420--%>
                                            <asp:CustomValidator ID="cvAdd" Text="Invalid File" OnServerValidate="ValidateInput" ValidationGroup="Upload" runat="server" ></asp:CustomValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
        <br />
            <br />
            &nbsp;
        </center>
        <script language="javascript">
            document.getElementById("txtType").value = "<%= Request.QueryString["type"].ToString() %>";
            document.getElementById("txtSrcID").value = "<%= Request.QueryString["srcID"].ToString() %>";
            document.getElementById("txtSrcID").value = document.getElementById("txtSrcID").value.substring(0,document.getElementById("txtSrcID").value.lastIndexOf("_")) + "_selCateringItems";
            
        //FB 2487 - Start
        var obj = document.getElementById("errLabel");
        if (obj != null) {
            var strInput = obj.innerHTML.toUpperCase();    
            if ((strInput.indexOf("SUCCESS") > -1) && !(strInput.indexOf("UNSUCCESS") > -1) && !(strInput.indexOf("ERROR") > -1)) {
                obj.setAttribute("class", "lblMessage");
                obj.setAttribute("className", "lblMessage");
            }
            else {
                obj.setAttribute("class", "lblError");
                obj.setAttribute("className", "lblError");
            }
        }
        //FB 2487 - End  
        </script>
        
    <%--ZD 100428 START- Close the popup window using the esc key--%>
    <script language="javascript" type="text/javascript">

    document.onkeydown = EscClosePopup;
    function EscClosePopup(e) {
        if (e == null)
            var e = window.event;
        if (e.keyCode == 27) {
            window.close();
        }
    }
    </script>
    <%--ZD 100428 END--%>

    </form>
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>