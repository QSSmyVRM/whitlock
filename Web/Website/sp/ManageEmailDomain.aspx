﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2014 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_myVRMNet.en_ManageEmailDomain" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Administrar Dominios de Correo-e</title>
</head>
<body>
    <form id="frmEmailDomain" runat="server" method="post" >
    <input type="hidden" id="hdnDomainID" runat="server" />
    <input type="hidden" id="hdnDomainStatus" runat="server" />
    <div>
        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
             <div id="dataLoadingDIV" style="display:none" align="center" >
                 <img border='0' src='image/wait1.gif'  alt='Cargando..' />
             </div> <%--ZD 100678 End--%>
            <tr>
                <td align="left">
                    <table cellspacing="5">
                        <tr>
                            <td >&nbsp;</td>
                            <td>
                                <span class=subtitleblueblodtext>Dominios de Correo-e Existentes</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgEmailDomain" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="true" OnItemDataBound="BindEmailDomain"
                          OnEditCommand="EditEmailDomain" OnUpdateCommand="UpdateEmailDomain" OnCancelCommand="CancelEmailDomain" Width="65%" Visible="true" style="border-collapse:separate">
                        <SelectedItemStyle  CssClass="tableBody"  Font-Bold="True" />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                        <FooterStyle CssClass="tableBody" />
                        <HeaderStyle CssClass="tableHeader" />
                        <Columns>
                            <asp:BoundColumn DataField="DomainID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="isActive" Visible="false"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Nombre de Empresa" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"> <%-- FB 2050 --%>
                                <ItemTemplate>
                                    <asp:Label ID="lblCompanyName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Companyname") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtCompanyName" CssClass="altText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Companyname")%>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqCompanyName" ControlToValidate="txtCompanyName" runat="server" ErrorMessage="Necesario" Display="dynamic" ValidationGroup="Update" ></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegExpCompanyName" ControlToValidate="txtCompanyName" Display="dynamic" runat="server" ValidationGroup="Update" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ y &#34; no son caracteres válidos." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Dominio de Correo-e" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"> <%-- FB 2050 --%>
                                <ItemTemplate>
                                    <asp:Label ID="lblEmailDomain" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Emaildomain") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtEmailDomain" CssClass="altText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Emaildomain")%>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqEmailDomain" ControlToValidate="txtEmailDomain" runat="server" ErrorMessage="Necesario" Display="dynamic" ValidationGroup="Update" ></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegExpEmailDomain" ControlToValidate="txtEmailDomain" Display="dynamic" runat="server" ValidationGroup="Update" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ y &#34; no son caracteres válidos." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Acciones" ItemStyle-Width="15%">
                                <HeaderStyle CssClass="tableHeader" />
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" Text="Editar" ID="btnEdit" CommandName="Edit" OnClientClick="javascript:DataLoading(1);"></asp:LinkButton> <%--ZD 100176--%> 
                                    <asp:LinkButton runat="server" ID="btnStatus" Text=""></asp:LinkButton><%--ZD 100176--%> <%--ZD 100263--%>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:LinkButton runat="server" Text="Actualizar" ID="btnUpdate" CommandName="Update" ValidationGroup="Update" OnClientClick="javascript:DataLoading(1);"></asp:LinkButton><%--ZD 100176--%> 
                                    <asp:LinkButton runat="server" Text="Cancelar" ID="btnCancel" CommandName="cancel" ValidationGroup="Update" OnClientClick="javascript:DataLoading(1);"></asp:LinkButton><%--ZD 100176--%> 
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoEmailDomain" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError">
                                No se encontró Dominio de Correo-e.
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>                    
                </td>
            </tr>
            <tr>
                <td align="left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <span class=subtitleblueblodtext><asp:Label ID="lblCreateEditDepartment" runat="server" Text="Crear Nuevo "></asp:Label> Dominio de Correo-e</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table border="0" cellpadding="5" cellspacing="5" width="90%">
                        <tr>
                            <td align="right" width="20%" class="blackblodtext">
                                Nombre de Empresa: </td>
                            <td align="left"height="21" style="font-weight:bold" width="490px">
                                <asp:TextBox ID="txtNewCompanytName" runat="server" CssClass="altText" Width="200px" ></asp:TextBox>
                                <asp:Label ID="lblRequired" CssClass="lblError" runat="server"></asp:Label>
                                <asp:RequiredFieldValidator ID="reqCompanytName" ControlToValidate="txtNewCompanytName" ErrorMessage="Necesario" ValidationGroup="Submit" runat="server" Display="dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegExpCompanytName" ControlToValidate="txtNewCompanytName" Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ y &#34; no son caracteres válidos." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td></td>
                            </tr>
                            <tr>
                            <td align="right" width="20%" class="blackblodtext">
                                Dominio de Correo-e:</td>
                            <td align="left"height="21" style="font-weight:bold" width="490px">
                                <asp:TextBox ID="txtNewEmailDomain" runat="server" CssClass="altText" Width="200px" ></asp:TextBox>
                                <asp:Label ID="lblRequired2" CssClass="lblError" runat="server"></asp:Label>
                                <asp:RequiredFieldValidator ID="ReqEmailDomain" ControlToValidate="txtNewEmailDomain" ErrorMessage="Necesario" ValidationGroup="Submit" runat="server" Display="dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegExpEmailDomain" ControlToValidate="txtNewEmailDomain" Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ y &#34; no son caracteres válidos." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td align="left"height="21" style="font-weight:bold" nowrap="nowrap" ><%--ZD 100369--%>
                                <asp:Button runat="server" ID="btnSaveDomain" Text="Entregar"  Width="100pt" OnClick="SaveDomain" ValidationGroup="Submit" /> <%--FB 2664--%>
                                &nbsp;&nbsp;
                                <input type="button" id="btnCancel" class="altMedium0BlueButtonFormat" value="Cancelar" onclick="fnCancel()" /> <%--FB 2565--%> <%--ZD 100369--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<script type="text/javascript">
    //ZD 100604 start
    var img = new Image();
    img.src = "../sp/image/wait1.gif";
    //ZD 100604 End
    document.getElementById("txtNewCompanytName").value = "";
    document.getElementById("txtNewEmailDomain").value = "";
    
    function DomainStatus(id, status) 
    {
      document.getElementById("hdnDomainID").value = id;
      var domainStatus =  document.getElementById("hdnDomainStatus");
      if (status == "1")
          domainStatus.value = "0";
      else
          domainStatus.value = "1";

      document.getElementById("txtNewCompanytName").value = ".";
      document.getElementById("txtNewEmailDomain").value = ".";
      if (document.getElementById("btnSaveDomain"))
          document.getElementById("btnSaveDomain").click();

      return false;
  }

  function fnCancel() //FB 2565
  {

      window.location='OrganisationSettings.aspx';  //CSS Project
  }
  //ZD 100176 start
  function DataLoading(val) {
      if (val == "1")
          document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
      else
          document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
  }
  //ZD 100176 End

  document.onkeydown = function(evt) {
      evt = evt || window.event;
      var keyCode = evt.keyCode;
      if (keyCode == 8) {
          if (document.getElementById("btnCancel") != null) { // backspace
              var str = document.activeElement.type;
              if (!(str == "text" || str == "textarea" || str == "password")) {
                  document.getElementById("btnCancel").click();
                  return false;
              }
          }
      }
      fnOnKeyDown(evt);
  };
</script>
