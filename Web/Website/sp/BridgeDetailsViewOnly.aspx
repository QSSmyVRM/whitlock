<%--ZD 100147 start--%>
<%--/* Copyright (C) 2014 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" Inherits="ns_Bridges.BridgeDetails" Buffer="true" CodeFile="~/sp/bridgedetailsviewonly.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 ,FB 2779 -->
<!-- #INCLUDE FILE="inc/maintop4.aspx" --> 

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Detalles MCU</title>
    <script type="text/javascript" src="inc/functions.js"></script>
    <script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055--%>
<script type="text/javascript">    // FB 2790
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
</script>
</head>
<body>
    <form id="frmMCUManagement" runat="server" method="post" onsubmit="return true">
    <input id="confPassword" runat="server" type="hidden" />
    <input id="txtApprover1_1" runat="server" type="hidden" />
    <input id="txtApprover2_1" runat="server" type="hidden" />
    <input id="txtApprover3_1" runat="server" type="hidden" />
    <input id="txtApprover4_1" runat="server" type="hidden" />
    <input id="hdnApprover1_1" runat="server" type="hidden" />
    <input id="hdnApprover2_1" runat="server" type="hidden" />
    <input id="hdnApprover3_1" runat="server" type="hidden" />
    <input id="hdnApprover4_1" runat="server" type="hidden" />

    <div>
      <input type="hidden" id="helpPage" value="65">
        
        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20"></td>
                            <td>
                                <SPAN class="subtitleblueblodtext">Configuraci�n B�sica</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%" cellspacing="3" cellpadding="2">
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Nombre MCU</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label ID="txtMCUID" Visible="false" runat="server"></asp:Label>
                                <asp:Label ID="txtMCUName"  runat="server"></asp:Label>
                            </td>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Inicio de sesi�n MCU</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label  ID="txtMCULogin" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="display:none">
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Contrase�a MCU</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label ID="txtPassword1"  runat="server"></asp:Label>
                            </td>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">vuelva a escribir la contrase�a</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label  ID="txtPassword2" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                                <%--Window Dressing--%>
                            <%--Code changed for FB 1425 QA Bug -Start--%> 
                            <td width="20%" align="left" style="font-weight:bold" id ="TzTD1" runat="server" class="blackblodtext">Zona Horaria MCU</td>
                            <td width="30%" align="left"  id ="TzTD2" runat="server">
                                <b>:</b>&nbsp;<asp:Label ID="lblTimezone" runat="server" ></asp:Label>
                                <asp:DropDownList Visible="false" ID="lstTimezone" CssClass="altSelectFormat" DataTextField="timezoneName" DataValueField="timezoneID" runat="server"></asp:DropDownList>
                            </td>
                          <%--Code changed for FB 1425 QA Bug -End--%>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Tipo MCU</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label ID="lblMCUType" runat="server" ></asp:Label>
                                <asp:DropDownList ID="lstMCUType" Visible="false" CssClass="altSelectFormat" DataTextField="name" DataValueField="ID" runat="server"></asp:DropDownList>
                                <asp:DropDownList ID="lstInterfaceType" Visible="false" DataTextField="interfaceType" DataValueField="ID" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Estado MCU</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label ID="lblStatus" runat="server" ></asp:Label>
                                <asp:DropDownList ID="lstStatus" Visible="false" CssClass="altSelectFormat" runat="server" DataTextField="name" DataValueField="ID"></asp:DropDownList>
                            </td>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">MCU virtual</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label ID="chkIsVirtual" runat="server" />
                            </td>
                        </tr>
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Administrador</td>
                            <td width="30%" align="left">
                               <b>:</b>&nbsp;<asp:Label ID="txtApprover4" EnableViewState="true"  runat="server"></asp:Label>
                                <asp:Label ID="hdnApprover4" Visible="false" runat="server" Width="0" Height="0" ForeColor="transparent" BorderColor="transparent"></asp:Label>
                            </td>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Versi�n del Firmware</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:DropDownList ID="lstFirmwareVersion" Visible="false" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Puertos s�lo-audio <br /> totales</td> <%--FB 1937--%>
                            <td width="30%" align="left" valign="middle">
                                <b>:</b>&nbsp;<asp:Label ID="txtMaxAudioCalls"  runat="server"></asp:Label>
                            </td>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Puertos Audio/V�deo <br /> totales</td> <%--FB 1937--%>
                            <td width="30%" align="left" valign="middle">
                               <b>:</b>&nbsp;<asp:Label  ID="txtMaxVideoCalls" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20"></td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Aprobadores MCU</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%" cellspacing="3" cellpadding="2">
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">aprobador primario</td>
                            <td width="30%" align="left">
                               <b>:</b>&nbsp;<asp:Label ID="txtApprover1" EnableViewState="true"  runat="server"></asp:Label>
                            </td>
                            <td width="50%" align="left">
                                <asp:Label ID="hdnApprover1" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderWidth="0" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">aprobador secundario 1</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label ID="txtApprover2"  runat="server"></asp:Label>
                            </td>
                            <td width="50%" align="left">
                                <asp:Label ID="hdnApprover2" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderWidth="0" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">aprobador secundario 2</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label ID="txtApprover3"  runat="server"></asp:Label>
                            </td>
                            <td width="50%" align="left">
                                <asp:Label ID="hdnApprover3" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderWidth="0" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>                 
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20"></td>
                            <td>
                                <asp:Label CssClass="subtitleblueblodtext" ID="lblHeader1" runat="server" Text="Configuraci�n MCU 'MGC Accord'" ></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="tr1" runat="server">
                <td align="center" >
                    <table width="90%" align="center"><%--Edited for FF--%>
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Direcci�n IP del Punto de control</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label ID="txtPortP"  runat="server"></asp:Label>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr id="trMCUCards" runat="server">
                                <%--Window Dressing--%>
                            <td colspan="4" class="subtitleblueblodtext" align="center"> <%--Edited for FF--%>
                                <h5>Tarjetas MCU</h5>
                                <asp:DataGrid ID="dgMCUCards" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="75%" AutoGenerateColumns="false" ShowFooter="false" runat="server">
                                    <HeaderStyle CssClass="tableHeader" Height="30" HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="left" />
                                    <Columns>
                                        <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Name" HeaderText="Nombre" ItemStyle-Width="40%" HeaderStyle-Width="50%" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Llamadas m�ximas" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="txtMaxCalls" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.MaximumCalls") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                </td>
                            </tr>
                        <tr id="trIPServices" runat="server">
                                <%--Window Dressing--%>
                            <td colspan="4" class="subtitleblueblodtext" align="center"> <%--Edited for FF--%>
                                <h5>Servicios IP</h5>
                                <asp:DataGrid ID="dgIPServices" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="75%" AutoGenerateColumns="false" runat="server">
                                    <HeaderStyle CssClass="tableHeader" Height="30" />
                                    <FooterStyle Height="30" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Nombre" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="txtName"  runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Tipo de Direcci�n" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddressType" runat="server"></asp:Label>
                                                <asp:DropDownList Visible="false" ID="lstAddressType" DataTextField="Name" DataValueField="ID"  runat="server" OnInit="BindAddressType" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.addressType") %>'></asp:DropDownList>  
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Direcci�n" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="txtAddress"  runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.address") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Acceso a la red" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNetworkAccess" runat="server"></asp:Label>
                                                <asp:DropDownList ID="lstNetworkAccess" visible="false" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.networkAccess") %>'>
                                                    <asp:ListItem Text="P�blico" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="privado" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Ambos" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Uso" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUsage" runat="server"></asp:Label>
                                                <asp:DropDownList ID="lstUsage"  runat="server" Visible="false" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.usage") %>'>
                                                    <asp:ListItem Text="Audio" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="V�deo" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Ambos" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                <asp:Label ID="lblNoIPServices" Text="No se encontraron Servicios IP" CssClass="lblError" runat="server" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <%--Window Dressing--%>
                                <td colspan="4" class="subtitleblueblodtext" align="center"> <%--Edited for FF--%>
                                
                                <h5>Servicios ISDN</h5>
                                <asp:DataGrid ID="dgISDNServices" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="75%" AutoGenerateColumns="false" runat="server">
                                    <HeaderStyle CssClass="tableHeader" Height="30" />
                                    <FooterStyle Height="30" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Nombre" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="txtName"  runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Prefijo" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="txtPrefix"  Width="50" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.prefix") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Rango de Inicio" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="txtStartRange"  Width="80" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.startRange") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Rango de Finalizaci�n" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="txtEndRange"  Width="80" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.endRange") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Orden de clase de Rango" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label id="lblRangeSortOrder" runat="server"></asp:Label>
                                                <asp:DropDownList ID="lstRangeSortOrder" Visible="false" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.RangeSortOrder") %>'>
                                                    <asp:ListItem Text="Inicio hasta Final" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Final hasta Inicio" Value="1"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Acceso a la red" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNetworkAccess" runat="server"></asp:Label>
                                                <asp:DropDownList ID="lstNetworkAccess" Visible="false" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.networkAccess") %>' CssClass="altText">
                                                    <asp:ListItem Text="P�blico" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="privado" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Ambos" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Uso" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUsage" runat="server"></asp:Label>
                                                <asp:DropDownList ID="lstUsage"  Visible="false" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.usage") %>' CssClass="altText">
                                                    <asp:ListItem Text="Audio" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="V�deo" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Ambos" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                <asp:Label ID="lblNoISDNServices" Text="No se encontraron Servicios ISDN" CssClass="lblError" runat="server" Visible="false"></asp:Label>
                                </td>
                            </tr>
                        <tr id="trMPIServices" runat="server">
                                <%--Window Dressing--%>
                            <td colspan="4" class="subtitleblueblodtext" align="center"> <%--Edited for FF--%>
                                <h5>Servicios MPI</h5>
                                <asp:DataGrid ID="dgMPIServices" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="75%" AutoGenerateColumns="false" runat="server">
                                    <HeaderStyle CssClass="tableHeader" Height="30" />
                                    <FooterStyle Height="30" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Nombre" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="txtName"  runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Tipo de Direcci�n" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddressType" runat="server"></asp:Label>
                                                <asp:DropDownList Visible="false" ID="lstAddressType" DataTextField="Name" DataValueField="ID"  runat="server" OnInit="BindAddressType" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.addressType") %>'></asp:DropDownList>  
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Direcci�n" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="txtAddress"  runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.address") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Acceso a la red" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNetworkAccess" runat="server"></asp:Label>
                                                <asp:DropDownList ID="lstNetworkAccess" visible="false" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.networkAccess") %>' CssClass="altText">
                                                    <asp:ListItem Text="P�blico" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="privado" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Ambos" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Uso" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUsage" runat="server"></asp:Label>
                                                <asp:DropDownList ID="lstUsage"  runat="server" Visible="false" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.usage") %>' CssClass="altText">
                                                    <asp:ListItem Text="Audio" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="V�deo" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Ambos" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                <asp:Label ID="lblNoMPIServices" Text="No se encontraron Servicios MPI" CssClass="lblError" runat="server" Visible="false"></asp:Label>
                                </td>
                            </tr>
                    </table>
                </td>
            </tr>
            <tr id="tr3" runat="server">
                <td align="center" >
                    <table width="90%">
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold">Puerto A</td>
                            <td width="30%" align="left">
                                &nbsp;<b>:</b>&nbsp;<asp:Label ID="txtPortA"  runat="server" ItemStyle-CssClass="tableBody"></asp:Label>
                            </td>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold">Puerto B</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label ID="txtPortB"  runat="server" ItemStyle-CssClass="tableBody"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="tr4" runat="server">
                <td align="center" >
                    <table width="90%">
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" class="blackblodtext" align="left" style="font-weight:bold">Puerto A</td>
                            <td width="30%" align="left">
                               <b>:</b>&nbsp;<asp:Label ID="txtPortT"  runat="server" ItemStyle-CssClass="tableBody"></asp:Label>
                            </td>
                            <td width="20%" align="left">&nbsp;</td>
                            <td width="30%" align="left">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
             <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20"></td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Alertas</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" >
                    <table width="90%">
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Alerta de Umbral ISDN</td>
                            <td width="30%" align="left"> <b>:</b>&nbsp;
<%--                              <asp:CheckBox ID="chkISDNThresholdAlert" onclick="javascript:ShowISDN(this)" runat="server" />
--%>                            </td>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">alerta defectuosa</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label ID="chkMalfunctionAlert" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trISDN" runat="server">
                <td align="center" >
                    <table width="90%">
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" class="blackblodtext" align="left">Tarifa del Puerto ISDN MCU</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label ID="txtMCUISDNPortCharge"  runat="server"></asp:Label>
                            </td>
                                <%--Window Dressing--%>
                            <td width="20%"  class="blackblodtext" align="left">Coste de la l�nea ISDN</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label ID="txtISDNLineCost"  runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" class="blackblodtext">Coste m�x. ISDN</td>
                            <td width="30%" align="left">
                               <b>:</b>&nbsp;<asp:Label ID="txtISDNMaxCost"  runat="server"></asp:Label>
                            </td>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" class="blackblodtext">Periodo del Umbral ISDN</td>
                            <td width="30%" align="left">
                               <b>:</b>&nbsp;<asp:Label ID="lblISDNThresholdTimeframe" runat="server"></asp:Label>
                                <asp:RadioButtonList ID="rdISDNThresholdTimeframe" Visible="false" runat="server">
                                    <asp:ListItem Text="mensualmente" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Anualmente" Value="2"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                         <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="left" class="blackblodtext">Porcentaje de Umbral ISDN</td>
                            <td width="30%" align="left">
                                <b>:</b>&nbsp;<asp:Label ID="txtISDNThresholdPercentage"  runat="server"></asp:Label>
                            </td>
                            <td width="20%" align="left">&nbsp;</td>
                            <td width="30%" align="left">&nbsp;</td>
                        </tr>
                   </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstAddressType" Visible="false" runat="server" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                    <asp:DropDownList runat="server" ID="lstNetworkAccess" Visible="false">
                        <asp:ListItem Text="P�blico" Value="1"></asp:ListItem>
                        <asp:ListItem Text="privado" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Ambos" Value="3"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </div>   
    </form>
</body>
</html>

<%--ZD 100428 START- Close the popup window using the esc key--%>
<script language="javascript" type="text/javascript">
document.onkeydown = EscClosePopup;
function EscClosePopup(e) {
    if (e == null)
        var e = window.event;
    if (e.keyCode == 27) {
        window.close();
    }
}
</script>
<%--ZD 100428 END--%>

<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>