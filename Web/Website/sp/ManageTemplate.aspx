<%--ZD 100147 start--%>
<%--/* Copyright (C) 2014 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" Inherits="ns_MyVRM.Template" %>
<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2779 Ends--%>


<script type="text/javascript" src="script/mousepos.js"></script>
<script type="text/javascript" src="script/managemcuorder.js"></script>

<script language="javascript">
    //ZD 100604 start
    var img = new Image();
    img.src = "../sp/image/wait1.gif";
    //ZD 100604 End
	function ManageOrder ()
	{
	    change_mcu_order_prompt('image/pen.gif', 'Administrar orden de Plantillas', document.frmManagebridge.Bridges.value, "Plantillas");
	}
	function CreateNewConference(confid)
    {
//        window.location.href = "aspToAspNet.asp?tp=ConferenceSetup.aspx&t=t&confid=" + confid; //Login Management
          window.location.href = "ConferenceSetup.aspx&t=t&confid=" + confid;
    }
	function showTemplateDetails(tid)
	{
//		popwin = window.open("dispatcher/conferencedispatcher.asp?cmd=GetTemplate&f=td&tid=" + tid,'templatedetails','status=yes,width=750,height=400,scrollbars=yes,resizable=yes')
        popwin = window.open("TemplateDetails.aspx?nt=1&f=td&tid=" + tid,'templatedetails','status=yes,width=750,height=400,scrollbars=yes,resizable=yes') //Login Management
		if (popwin)
			popwin.focus();
		else
			alert(EN_132);
	}
	function frmsubmit(save, order)
	{
	    document.getElementById("__EVENTTARGET").value="ManageOrder";
	    document.frmManagebridge.submit();
	}
	//ZD 100176 start
	function DataLoading(val) {
	    if (val == "1")
	        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
	    else
	        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
	}
	//ZD 100176 End
</script>
<head runat="server">
 
    <title>Mis Plantillas</title>
    <script type="text/javascript" src="inc/functions.js"></script>

</head>
<body>
    <form id="frmManagebridge" runat="server" method="post" onsubmit="return true">
    <div>
      <input type="hidden" id="helpPage" value="73">

        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server" Text="Administrar Plantillas"></asp:Label><!-- FB 2570 -->
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <div id="dataLoadingDIV" style="display:none" align="center" >
                <img border='0' src='image/wait1.gif'  alt='Cargando..' />
            </div> <%--ZD 100678 End--%>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20" >&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Plantillas existentes</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgTemplates" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowSorting="true" OnSortCommand="SortTemplates"
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="true" OnItemCreated="BindRowsDeleteMessage"
                        OnDeleteCommand="DeleteTemplate" OnEditCommand="EditTemplate" OnCancelCommand="CreateConference" Width="90%" Visible="true" style="border-collapse:separate"> <%--Edited for FF--%>
                        <SelectedItemStyle  CssClass="tableBody"/>
                         <AlternatingItemStyle CssClass="tableBody" />
                         <ItemStyle CssClass="tableBody"  />
                        <HeaderStyle CssClass="tableHeader" Height="30px" />
                        <EditItemStyle CssClass="tableBody" />
                         <%--Window Dressing--%>
                        <FooterStyle CssClass="tableBody" />
                        <Columns><%--ZD 100425 Starts--%>
                            <asp:BoundColumn DataField="ID" Visible="false" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="name" ItemStyle-CssClass="tableBody" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="tableHeader" HeaderText="Nombre" SortExpression="1"></asp:BoundColumn> <%-- FB 2050 --%> <%--FB 2922--%>
                            <asp:BoundColumn DataField="description" ItemStyle-CssClass="tableBody" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="tableHeader" HeaderText="Descripci�n"></asp:BoundColumn> <%-- FB 2050 --%>  <%--FB 2922--%>
                            <asp:BoundColumn DataField="administrator" ItemStyle-CssClass="tableBody" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="tableHeader" HeaderText="Due�o" SortExpression="2"></asp:BoundColumn> <%-- FB 2050 --%>  <%--FB 2922--%>
                            <asp:BoundColumn DataField="public" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderText="Privado/P�blico" SortExpression="3" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>  <%--FB 2922--%>
                            <asp:TemplateColumn HeaderText="Ver detalles" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"> <%--FB 2922--%>
                                <ItemTemplate>
                                    <asp:Button ID="btnViewDetails" Text="Ver" runat="server" CssClass="altMedium0BlueButtonFormat" Width="42%"/> 
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Acciones" ItemStyle-CssClass="tableBody"  ItemStyle-Width="25%" FooterStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="center">   <%--FB 2922--%>
                                <HeaderStyle CssClass="tableHeader" />
                                <ItemTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td style="width:145px";><%--ZD 100425--%>
                                                <%--Code Changed for FB 1428--%>
                                                <asp:LinkButton runat="server" ID="btnCreateConf" CommandName="Cancel"  OnClientClick="DataLoading(1)">
                                                    <span id="Field1">Crear Conferencia</span>
                                                </asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton runat="server" Text="Editar" ID="btnEdit" OnClientClick="DataLoading(1)" CommandName="Edit"></asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton runat="server" Text="Eliminar" ID="btnDelete" CommandName="Delete"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <span class="blackblodtext"> Plantillas totales: </span><asp:Label ID="lblTotalRecords" runat="server" Text=""></asp:Label> <%-- FB 2579 --%>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoTemplates" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError">
                                No se encontraron Plantillas.
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>                    
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%">
                        <tr>
                            <td align="center">
                                <%--<asp:Button ID="btnManageOrder" runat="server" OnClientClick="javascript:ManageOrder();return false;" Text="Manage Template Order" CssClass="altLongBlueButtonFormat" />--%><%--ZD 100420--%>
                                <button ID="btnManageOrder" runat="server" Class="altLongBlueButtonFormat" OnClick="javascript:ManageOrder();return false;" style="width:250px">Administrar orden de Plantillas</button> <%--ZD 100420--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20" >&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Buscar plantillas</SPAN>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                           <%--Removed Class for Window Dressing                      --%>
                            <td class="blackblodtext">
                                Entre al criterio de busqueda para buscar.
                                <br />Aunque usted s�lo puede editar sus plantillas privadas, puede usar una plantilla p�blica o privada como base para crear una plantilla nueva.    
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%"> 
                        <tr>
                            <td align="center">
                                <table width="100%">
                                    <tr>
                                        <td align="left" class="blackblodtext">Nombre de Plantilla</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtSTemplateName" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtSTemplateName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ y &#34; no son caracteres v�lidos." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+^;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <%if(!(Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))){%> <%--Added for FB 1425 MOJ--%>
                                        <td align="left" class="blackblodtext">Participante Incluido</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtSParticipant" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtSParticipant" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ /  ? | ^ = ! ` [ ] { } # $ @ y ~ no son caracteres v�lidos." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+^?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator> <%--FB 1888--%>
                                        </td> 
                                        <%}%> <%--Added for FB 1425 MOJ--%>                                      
                                    </tr>
                                    <tr>
                                        <td align="left" class="blackblodtext">Descripci�n</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtSDescription" TextMode="multiline" Rows="2" Width="200" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtSDescription" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ y &#34; no son caracteres v�lidos." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+^;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td align="left">&nbsp;</td>
                                        <td align="left">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <%--<asp:Button ID="btnSearch" OnClick="SearchTemplate" runat="server" CssClass="altLongBlueButtonFormat" Text="Submit"  OnClientClick="DataLoading(1)"/> <%--ZD 100176--%> <%--ZD 100420--%>
                                <button ID="btnSearch"  runat="server" Class="altLongBlueButtonFormat" onserverclick="SearchTemplate" onClientClick="DataLoading(1)">Entregar</button><%--ZD 100420--%>
                            </td>                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20" >&nbsp;</td>
                            <td>
                                <!--<SPAN class=subtitleblueblodtext></SPAN>--><%--Commented for FB 2094--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
               <td align="center">
                    <table width="90%">
                        <tr>
                            <td align="right">
                               <%--<asp:Button ID="btnCreate" OnClick="CreateNewTemplate" runat="server" CssClass="altLongBlueButtonFormat" Text="Create New Template" OnClientClick="DataLoading(1)" /><%--FB 2094--%><%-- ZD 100176--%><%--ZD 100420--%>
                               <button ID="btnCreate" runat="server"  Class="altLongBlueButtonFormat" OnClientClick="DataLoading(1)"  Onserverclick="CreateNewTemplate">Crear Nueva Plantilla</button><%--ZD 100420--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <asp:TextBox ID="Bridges" runat="server" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderColor="transparent"></asp:TextBox>
    <asp:TextBox ID="txtSortBy" runat="server" Visible="false"></asp:TextBox>
</form>
<script language="javascript">

    //ZD 100420 Start //ZD 100369
    if (document.getElementById('btnManageOrder') != null)
        document.getElementById('btnManageOrder').setAttribute("onblur", "fnManageOrderFocus();");
    //ZD 100369
    function EscClosePopup() {
            cancelthis();
    }
    //ZD 100420 End

</script>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<%--ZD 100420 start--%>
<script type="text/javascript">
    if (document.getElementById('btnDelete') != null)
        document.getElementById('btnDelete').setAttribute("onblur", "document.getElementById('btnManageOrder').focus(); document.getElementById('btnManageOrder').setAttribute('onfocus', '');");               

</script>
<%--ZD 100420 End--%>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>

