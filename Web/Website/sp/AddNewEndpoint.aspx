﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2014 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 ZD 1000886 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.en_AddNewEndpoint" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2779 -->
<% if(Request.QueryString["ifrm"] == null){%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<% }%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AddNewEndpoint</title>
    <link rel="StyleSheet" href="css/divtable.css" type="text/css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css"/>
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css"/>
    <%--<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main.css"/>--%> 
    <script type="text/javascript">         // FB 2790
         var path = '<%=Session["OrgCSSPath"]%>';
         path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
         document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
    </script> 

</head>

<%-- Access the web method through ajax --%>
<script type="text/javascript" src="script/CallMonitorJquery/jquery.1.4.2.js" ></script>
<script type="text/javascript" src="script/CallMonitorJquery/json2.js" ></script>
<%-- Jquery popup window --%>
<script type="text/javascript" src="script/CallMonitorJquery/jquery.bpopup-0.7.0.min.js"></script>
<%-- User Defined Jquery Functions  --%>
<script type="text/javascript" src="script/CallMonitorJquery/MonitorMCU.js"></script>

<body>
    <form id="frmAddNewEndpoint" runat="server" onsubmit="DataLoading(1);"> <%--ZD 100176--%>
    <input id="hdnWebAccURL" name="hdnWebAccURL" runat="server" type="hidden" />
    <input id="hdnLineRate" name="hdnLineRate" runat="server" type="hidden" />
    <input id="hdnApiPortNo" name="hdnApiPortNo" runat="server" type="hidden" />
    <input id="hdnVideoEquipment" name="hdnVideoEquipment" runat="server" type="hidden" />
    <input id="hdnMCUServiceAdd" name="hdnBridgeServiceAdd" runat="server" type="hidden" />
    <input id="hdnExchangeID" name="hdnExchangeID" runat="server" type="hidden" />
    <input id="hdnEndpointID" name="hdnEndpointID" runat="server" type="hidden" />
    <input id="hdnEndpointURL" name="hdnEndpointURL" runat="server" type="hidden" />
    <input id="hdnRoomID" name="hdnRoomID" runat="server" type="hidden" /> <%--ZD 100602--%>
    <a style="display:none"><asp:Button ID="selectEndPoint" runat="server"  onclick="BindEndpointData" /></a>
    <a style="display:none"><asp:Button ID="SelectRoom" runat="server"  onclick="BindRoomEndpointData" /></a>  <%--ZD 100602--%>
    <div>
      <center>
      <table border="0" width="100%" cellpadding="2" cellspacing="2">
            <tr>
                <td align="center" colspan="2">
                    <h3><asp:Label ID="lblHeader" runat="server" Text="Añadir punto final"></asp:Label></h3><br />
                     <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
                     <%--ZD 100678 start--%>
                     <div id="dataLoadingDIV" align="center" style="display:none">
                        <img border='0' src='image/wait1.gif' alt='Cargando..' />
                     </div><%--ZD 100176--%> <%--ZD 100678 End--%>
                </td>
            </tr>
            <tr>
                <td class="subtitleblueblodtext" align="left" colspan="2">Información básica</td>                            
            </tr>
            <tr>
                <td width="50%" valign="top">
                    <table width="100%" style="margin-left:20px">
                    <tr>
                           <td align="left"  class="blackblodtext" nowrap="nowrap">Punto final no está en la lista</td>
                           <td align="left" ><%-- ZD 100602 --%>
                                <asp:RadioButton ID="chkUnlistedEndpoint" GroupName="Same" runat="server" onclick="javascript:return OpenEndpointlist(this);" /> <%--ZD 100602--%>
                                <%--<asp:CheckBox ID="chkUnlistedEndpoint" runat="server" onclick="javascript:OpenEndpointlist(this)" />--%>
                           </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext" width="35%" nowrap="nowrap">Punto final en la lista</td>
                            <td align="left"><%-- ZD 100602 --%>
                                <asp:RadioButton ID="chkListedEndpoint" GroupName="Same" runat="server" onclick="javascript:return OpenEndpointlist(this);"  /><%--ZD 100602--%>
                                <%--<asp:CheckBox ID="chkListedEndpoint" runat="server" onclick="javascript:OpenEndpointlist(this)" />--%>
                            </td>
                        </tr>
                     </table>
                </td>
                <td valign="top">
                    <table width="100%" style="padding:0px;">
                      <tr valign="top" >
                         <td align="left" width="35%" class="blackblodtext" valign="middle">Tipo de Terminal </td>
                         <td align="left"><asp:Label ID="lblTerminalType" CssClass="subtitleblueblodtext" runat="server" Text="Invitado"/></td>
                      </tr>
                    </table>
               </td>
            </tr>
            <tr>
                <td class="subtitleblueblodtext" align="left" colspan="2">Parámetros del Punto Final</td>                            
            </tr>
            <tr>
                <td width="50%">
                    <table width="100%" style="margin-left:20px">
                        <tr>
                            <td align="left" class="blackblodtext" width="35%">
                                Nombre<span class="reqfldText">*</span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtEndpointName" runat="server" CssClass="altText"  width="50%"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqEndpointName" runat="server" ControlToValidate="txtEndpointName" ErrorMessage="Necesario"  ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regEndpointName" SetFocusOnError="true" ControlToValidate="txtEndpointName" Display="dynamic" runat="server" ValidationGroup="Submit" ErrorMessage="<br> & < > ' + % \ ; ? | ^ = ! ` [ ] { } : $ @ ~ y &#34; no son caracteres válidos." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@$%&'~]*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <%--ZD 100591 START--%>
                        <tr id="EndpointProfile" runat="server">
                            <td align="left" class="blackblodtext" width="35%">
                                Perfil del punto final<span class="reqfldText">*</span></td>
                            <td align="left">
                                <asp:DropDownList ID="lstProfileType" OnSelectedIndexChanged="ChangeEndpointProfileDetails"   runat="server" DataTextField="ProfileName" DataValueField="ProfileID" AutoPostBack="true" Width="50%" CssClass="altLong0SelectFormat"></asp:DropDownList>
                            </td>
                        </tr>
                        <%--ZD 100591 END--%>
                        <tr>
                            <td align="left" class="blackblodtext">Protocolo<span class="reqfldText">*</span></td>
                            <td align="left">
                                <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstProtocol" runat="server" DataTextField="Name" DataValueField="ID" Width="50%" onchange="javascript:ValidateSelection(this)"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqlstProtocol" ErrorMessage="Necesario" runat="server" ControlToValidate="lstProtocol" Display="dynamic" InitialValue="-1" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                            </td>
                           
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">Conexión <span class="reqfldText">*</span></td>
                            <td align="left">
                                <asp:DropDownList CssClass="altLong0SelectFormat" runat="server" ID="lstConnection" Width="50%" onchange="javascript:ValidateConnection(this);"> <%--ZD 100628--%>
                                    <asp:ListItem Text="por favor seleccione.." Value="-1"></asp:ListItem> 
                                    <asp:ListItem Text="Sólo audio" Value="1"></asp:ListItem> 
                                    <asp:ListItem Text="Audio/Vídeo" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="-1" ControlToValidate="lstConnection" ValidationGroup="Submit" ErrorMessage="Necesario"></asp:RequiredFieldValidator>                                    
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">Red exterior localizada</td>
                            <td align="left">
                                <asp:CheckBox ID="chkIsOutside" runat="server"  />
                            </td>
                            
                        </tr>
                        <tr><%-- FB 2441 --%>
                            <td align="left" class="blackblodtext" nowrap="nowrap">Silenciar el Audio</td><%-- ZD 100628 --%>
                            <td align="left">
                                <asp:CheckBox ID="chkMute" runat="server"  />
                            </td>
                            
                        </tr>
                    </table>
                </td>
                <td width="50%">
                    <table width="100%">
                        <tr>
                            <td align="left" class="blackblodtext" width="35%" nowrap="nowrap">
                                Tipo de Dirección<span class="reqfldText">*</span></td>
                            <td align="left">
                                <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstAddressType" runat="server" DataTextField="Name" DataValueField="ID" Width="50%" onchange="javascript:ValidateSelection(this);"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqAddressType" runat="server" InitialValue="-1" ControlToValidate="lstAddressType" ValidationGroup="Submit" ErrorMessage="Necesario"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">Dirección<span class="reqfldText">*</span></td>
                            <td align="left">                            
                                <asp:TextBox CssClass="altText"  ID="txtAddress" runat="server" width="50%" ></asp:TextBox>                                
                                    <asp:RequiredFieldValidator ID="reqAddress" ControlToValidate="txtAddress" ValidationGroup="Submit" runat="server" ErrorMessage="Necesario" ></asp:RequiredFieldValidator>
                                    <%--<asp:RegularExpressionValidator ID="regAddress" ControlToValidate="txtAddress" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<br>Invalid Address." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|`\[\]{}\=^$%&()~]*$"></asp:RegularExpressionValidator> --%><%--FB 2267--%>
                                
                                <%--FB 1972--%>
                                
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">Tipo de conexión<span class="reqfldText">*</span></td>
                            <td align="left"><%-- ZD 100602 --%>
                                <asp:DropDownList ID="lstConnectionType" runat="server" DataTextField="Name" DataValueField="ID" CssClass="altLong0SelectFormat" Width="50%"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqConnectionType" ErrorMessage="Necesario" runat="server" ControlToValidate="lstConnectionType" Display="dynamic" InitialValue="-1" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">encriptación</td>
                            <td align="left">
                                <asp:CheckBox ID="chkEncryption" runat="server" />
                            </td>  
                        </tr>
                        <%-- ZD 100628 Start --%>
                         <tr>
                            <td align="left" id="tdMuteVideo" class="blackblodtext">Silenciar el Vídeo</td>
                            <td align="left">
                                <asp:CheckBox ID="chkVideoMute" runat="server"  />
                            </td>
                        </tr>
                        <%-- ZD 100628 End --%>
                    </table>
                </td>
             </tr>
             <% if(Request.QueryString["ifrm"] == null){%> <%--FB 2501 Call monitoring--%>
            <tr>
                <td class="subtitleblueblodtext" align="left" colspan="2">Parámetros MCU</td>                            
            </tr>
             <tr>
                <td colspan="2">
                    <table width="100%" border="0">
                        <tr>
                            <td width="2%"></td>
                            <td align="left" width="17%" class="blackblodtext" valign="top">Asignado a MCU<span class="reqfldText">*</span></td>
                            <td align="left"  valign="bottom">
                                <asp:DropDownList CssClass="altLong0SelectFormat" OnSelectedIndexChanged="DisplayBridgeDetails" AutoPostBack="true" ID="lstBridges" runat="server" DataTextField="BridgeName" DataValueField="BridgeID" Width="20%"></asp:DropDownList>
                                <input type='submit' name='SoftEdgeTest1' tabindex="-1" style='max-height:0px;max-width:0px;height:0px;width:0px;background-color:Transparent;border:None;'/>
                                <%--<asp:Button id="btnViewMCU" Text="View" runat="server" class="altShortBlueButtonFormat" OnClientClick="javascript:return viewMCU(document.frmAddNewEndpoint.lstBridges.options[document.frmAddNewEndpoint.lstBridges.selectedIndex].value);" Width="12%"/>--%>
                                <button id="btnViewMCU" runat="server" class="altMedium0BlueButtonFormat" onclick="javascript:return viewMCU(document.frmAddNewEndpoint.lstBridges.options[document.frmAddNewEndpoint.lstBridges.selectedIndex].value);" style="width:12%" >Ver</button> <%--ZD 100420--%>
                                <% if(Request.QueryString["ifrm"] == null){%> <%--FB 2501 Call monitoring--%>
                                <asp:RequiredFieldValidator ID="reqBridges" runat="server" InitialValue="-1" ControlToValidate="lstBridges" ValidationGroup="Submit" ErrorMessage="Necesario"></asp:RequiredFieldValidator>
                                <%} %>
                            </td>
                         </tr>
                         <tr>
                            <td></td>
                            <td align="left" class="blackblodtext" valign="top">Tipo de Dirección MCU</td>
                            <td align="left" valign="top">
                                <asp:DropDownList ID="lstMCUAddressType" CssClass="altLong0SelectFormat" runat="server" DataTextField="Name" DataValueField="ID" Width="20%" onchange="javascript:ValidateSelection(this);"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqMCUAT" ErrorMessage="Necesario" runat="server" ControlToValidate="lstMCUAddressType" Display="dynamic" InitialValue="-1" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </td>
             </tr>
             <% } %> <%--FB 2501 Call monitoring--%>
             <tr style="height:20px">
               <td></td>
             </tr>  
             <tr>
                <td align="center" colspan="2">
                  <table>
                    <tr>
                     <td>
                     <%--<asp:Button ID="btnCancel" runat="server" CssClass="altShortBlueButtonFormat" Text="Cancel" OnClick="CancelEndpoint" OnClientClick="javascript:DataLoading(1);"  /></td><%--ZD 100176--%> <%--ZD 100420--%>
                     <button id="btnCancel" runat="server" class="altMedium0BlueButtonFormat" onserverclick="CancelEndpoint" onclick="if(window.page!= 'addnewendpoint.aspx'){fnCloseEndpoint(); return false;}else {DataLoading(1);}" >Cancelar</button> </td> <%--ZD 100420--%>
                     <% if(Request.QueryString["ifrm"] == null){%> <%--FB 2501 Call monitoring--%>
                     <td>
                     <asp:Button ID="btnSubmitAddNew" runat="server" CssClass="altLongBlueButtonFormat" Text="Entregar/Añadir Punto final nuevo" OnClick="SubmitEndpoint" ValidationGroup="Submit" Width="250px" />
                     <%--<button id="btnSubmitAddNew" runat="server" class="altLongBlueButtonFormat" onserverclick="SubmitEndpoint" validationgroup="Submit">Submit/Add New Endpoint</button>--%> <%--ZD 100420--%>
                     </td>
                     <%} %> <%--FB 2501 Call monitoring--%>
                     <td>
                     <asp:Button ID="btnSubmit" runat="server" CssClass="altLongBlueButtonFormat" Text="Entregar/Retroceder" OnClick="SubmitEndpoint" ValidationGroup="Submit" />
                     <%--<button id="btnSubmit" runat="server" class="altLongBlueButtonFormat" onserverclick="SubmitEndpoint" ValidationGroup="Submit">Submit/Go Back</button>--%> <%--ZD 100420--%>
                     </td>
                    </tr>
                  </table>
                </td>
             </tr>
        </table>
</center>
    </div>
    </form>
    
    <%--ZD 100602 Starts--%>
    <div id="PopupRoomList" class="rounded-corners" style="position: absolute;
    overflow:hidden; border: 0px;
    width: 98%; display: none;">    
    <iframe src="" id="RoomList" name="RoomList" style="height: 630px; border: 0px; overflow:hidden; width: 98%; overflow: hidden;"></iframe>
	</div>
	<%--ZD 100602 End--%>
   
<script language="javascript" type="text/javascript">
    //ZD 100604 start
    var img = new Image();
    img.src = "../sp/image/wait1.gif";
    //ZD 100604 End
    
    if (document.getElementById("hdnEndpointID").value == '') 
    {
        document.getElementById("hdnEndpointID").value = "new";
        var obj = document.getElementById("chkUnlistedEndpoint");
        obj.checked = true;
        OpenEndpointlist(obj);
    }

    function OpenEndpointlist(obj) {
        if (obj.id == "chkListedEndpoint" && obj.checked) {
            //ZD 100602 Starts
            var selectedRooms = '';
            var stDateTime = '';
            var enDateTime = '';
            var lblConfID = '';
            var confTimezone = 26;
            var cloudConf = 0;
            var confType = 2;
            var confServiceType = -1;
            
            var url = window.location.href;
            if (url.indexOf("confStartDate") >= 0) {    //From Call Monitor page
                stDateTime = '<%=dStart%>';
                enDateTime = '<%=dEnd%>';
                confTimezone = '<%=Request.QueryString["confTimezone"]%>';
                confType = '<%=Request.QueryString["confType"]%>';
                confServiceType = '<%=Request.QueryString["confServiceType"]%>';
            }
            else if (url.indexOf("tp") >= 0) {          //From DashBoard page
                stDateTime = '<%=Session["stdate"]%>';
                enDateTime = '<%=Session["endate"]%>';
                confTimezone = '<%=Session["conftzone"]%>'; 
                cloudConf = '<%=Session["cloudconf"]%>';
                confType = '<%=Session["conftype"]%>';
                confServiceType = '<%=Session["confservicetype"]%>';
            }
            else {                                      //From ManageConference page
                stDateTime = '<%=Session["stDate"]%>';
                enDateTime = '<%=Session["enDate"]%>';
                confTimezone = '<%=Session["tzone"]%>';
                cloudConf = '<%=Session["cloudConf"]%>';
                confType = '<%=Session["confType"]%>';
                confServiceType = '<%=Session["confServiceType"]%>';
            }
            var Openurl = window.location.href;
            
            if (Openurl.indexOf("confStartDate") >= 0) {
                var url = "RoomSearch.aspx?confID=&stDate=" + stDateTime + "&enDate=" + enDateTime + "&tzone=" + confTimezone
            	+ "&serType=" + confServiceType + "&hf=1&CloudConf=" + cloudConf + "&ConfType=" + confType + "&immediate=0&AddRoomEndpoint&hdnLnkBtnId=1";
                
				window.open(url, "RoomSearch", "width=" + screen.availWidth + ",height=600px,resizable=no,scrollbars=yes,status=no,top=0,left=0");
            }
            else {
                var url = "RoomSearch.aspx?confID=&stDate=" + stDateTime + "&enDate=" + enDateTime + "&tzone=" + confTimezone
            + "&serType=" + confServiceType + "&hf=1&CloudConf=" + cloudConf + "&ConfType=" + confType + "&immediate=0&AddRoomEndpoint&hdnLnkBtnId=1&ManageConf";
                
                $('#popupdiv').fadeIn();
                $("#RoomList").attr("src", url);
                $('#PopupRoomList').show();
                $('#PopupRoomList').bPopup({
                    fadeSpeed: 'slow',
                    followSpeed: 1500,
                    modalColor: 'gray'
                });
            }
            //url = "EndpointSearch.aspx?t=TC&hf=1&DrpValue=";
            
            //ZD 100602 End
        }
        else if (obj.id == "chkUnlistedEndpoint" && obj.checked) {
            document.getElementById("lblTerminalType").innerHTML = "Invitado"; //ZD 100631
            document.getElementById("chkListedEndpoint").checked = false;
            document.getElementById("txtEndpointName").value = "Adjuntar";
            document.getElementById("lstProtocol").value = "1";
            document.getElementById("lstAddressType").value = "1";
            document.getElementById("lstConnection").value = "2";
            document.getElementById("lstConnectionType").value = "2";
            document.getElementById("txtAddress").value = "";
            document.getElementById("chkIsOutside").checked = false;
            document.getElementById("chkEncryption").checked = false;
            if (document.getElementById("lstBridges") != null) //FB 2501 Call monitoring
                document.getElementById("lstBridges").value = "-1";
            if (document.getElementById("lstMCUAddressType") != null) //FB 2501 Call monitoring
                document.getElementById("lstMCUAddressType").value = "-1";
            document.getElementById("hdnVideoEquipment").value = "0";
            document.getElementById("hdnLineRate").value = "384";
            document.getElementById("hdnApiPortNo").value = "23";
            //ZD 100591 START
            if (document.getElementById("EndpointProfile") != null)
                document.getElementById("EndpointProfile").style.display = 'none';
            //ZD 100591 END 
        }
        return true;
		//ZD 100602 End	
    }
	//ZD 100602 Starts
    function fnTriggerFromPopup() {
        $('#popupdiv').fadeOut();
        $('#PopupRoomList').hide();
    }
	//ZD 100602 End
    function viewMCU(val)
    {
        var mcuid =  val.split("@")[0];
        
        if(mcuid != "-1" && mcuid != "")
        {
            url = "BridgeDetailsViewOnly.aspx?hf=1&bid="+ mcuid;
            window.open(url, "BrdigeDetails", "width=900,height=800,resizable=yes,scrollbars=yes,status=no");
        }
        return false;
    }

    function ValidateSelection(obj) {
        var lstProtocol = document.getElementById("lstProtocol");
        var lstMCUAddressType = document.getElementById("lstMCUAddressType");
        var lstAddressType = document.getElementById("lstAddressType");
        if (obj == lstAddressType) 
        {
            if (lstAddressType.value == 5) {
                lstProtocol.selectedIndex = 4;
                if(lstMCUAddressType != null)
                lstMCUAddressType.selectedIndex = 5;
            }
            else if (lstAddressType.value == 4) {
                lstProtocol.selectedIndex = 2;
                if(lstMCUAddressType != null)
                lstMCUAddressType.selectedIndex = 4;
            }
            else {
                if (lstProtocol.value == 4 || lstProtocol.value == 2)
                    lstProtocol.selectedIndex = 1;
                if(lstMCUAddressType != null){
                if (lstMCUAddressType.value == 5 || lstMCUAddressType.value == 4)
                    lstMCUAddressType.selectedIndex = 1;
                }
            }
        }
        if (obj == lstMCUAddressType)
         {
            if(lstMCUAddressType != null){
            if (lstMCUAddressType.value == 5) {
                lstProtocol.selectedIndex = 4;
                lstAddressType.selectedIndex = 5;
            }
            else if (lstMCUAddressType.value == 4) {
                lstProtocol.selectedIndex = 2;
                lstAddressType.selectedIndex = 4;
            }
            }
            else {
                if (lstProtocol.value == 4 || lstProtocol.value == 2)
                    lstProtocol.selectedIndex = 1;
                if (lstAddressType.value == 5 || lstAddressType.value == 4)
                    lstAddressType.selectedIndex = 1;
            }
        }
        if (obj == lstProtocol)
         {
            if (lstProtocol.value == 4) {
                if(lstMCUAddressType != null)
                lstMCUAddressType.selectedIndex = 5;
                lstAddressType.selectedIndex = 5;
            }
            else if (lstProtocol.value == 2) {
                if(lstMCUAddressType != null)
                lstMCUAddressType.selectedIndex = 4;
                lstAddressType.selectedIndex = 4;
            }
            else {
                if(lstMCUAddressType != null){
                if (lstMCUAddressType.value == 5 || lstMCUAddressType.value == 4)
                    lstMCUAddressType.selectedIndex = 1;
                }
                if (lstAddressType.value == 5 || lstAddressType.value == 4)
                    lstAddressType.selectedIndex = 1;
            }
        }
    }
    //ZD 100176 start
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
        else
            document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
    }
	//ZD 100369 Start
    function fnCloseEndpoint() {
        window.parent.document.getElementById("PopupAddUser").style.display = "none";
        window.parent.document.getElementById("popupdiv").style.display = "none";
    }
        
    if (document.getElementById('btnSubmit') != null)
        document.getElementById('btnSubmit').setAttribute("onblur", "document.getElementById('btnSubmit').focus(); document.getElementById('btnSubmit').setAttribute('onfocus', '');"); 
    
    //ZD 100369 End
    //ZD 100176 End
    
    if (document.getElementById('btnSubmitAddNew') != null)
        document.getElementById('btnSubmitAddNew').setAttribute("onblur", "document.getElementById('btnSubmit').focus(); document.getElementById('btnSubmit').setAttribute('onfocus', '');");
  //ZD 100628 Start
  function ValidateConnection(obj) 
  {
        var lstConnection = document.getElementById("lstConnection");
        if (obj == lstConnection) 
        {
            if (lstConnection.value == 2) 
            {
                document.getElementById("chkVideoMute").style.visibility = 'visible';
                document.getElementById("tdMuteVideo").style.visibility = 'visible';
            }
            else
            {
                document.getElementById("chkVideoMute").checked = false;
                document.getElementById("chkVideoMute").style.visibility = 'hidden';
                document.getElementById("tdMuteVideo").style.visibility = 'hidden'
            }
       }
  }
  //ZD 100628 End
</script>
</body>
</html>

<% if(Request.QueryString["ifrm"] == null){%>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<% }%>