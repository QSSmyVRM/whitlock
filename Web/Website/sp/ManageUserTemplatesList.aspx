<%--ZD 100147 start--%>
<%--/* Copyright (C) 2014 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" Inherits="ns_UserTemplates.UserTemplates" Buffer="true" %>
<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2779 Ends--%>

    <script type="text/javascript" src="inc/functions.js"></script>
<script language="javascript">
//ZD 100604 start
var img = new Image();
img.src = "../sp/image/wait1.gif";
//ZD 100604 End
function CheckName()
{
}
</script>
<head runat="server">
    <title>Administrar Plantillas de usuario</title>

</head>
<body>
    <form id="frmInventoryManagement" runat="server" method="post" onsubmit="return true">
      <input type="hidden" id="helpPage" value="105">

    <div>
        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        Administrar Plantillas de usuario
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                    <div id="dataLoadingDIV"  style="display:none" align="center" >
                        <img border='0' src='image/wait1.gif'  alt='Cargando..' />
                    </div> <%--ZD 100678 End--%>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td >&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Plantillas existentes de Usuarios</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgUserTemplates" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                        BorderColor="blue" BorderStyle="none" BorderWidth="0" ShowFooter="true" OnItemCreated="BindRowsDeleteMessage"
                        OnDeleteCommand="DeleteUserTempate" OnEditCommand="EditUserTemplate" Width="90%" Visible="true" >
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" HorizontalAlign="left" />
                        <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                        <Columns>
                            <asp:BoundColumn DataField="ID" Visible="false" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="Name" HeaderText="Nombre de Plantilla"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="RoleName" HeaderText="Nombre del Rol"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Acciones" ItemStyle-Width="15%" FooterStyle-Width="17%">
                                <HeaderStyle CssClass="tableHeader" />
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" Text="Editar" OnClientClick="DataLoading(1)" ID="btnEdit" CommandName="Edit"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton runat="server" Text="Eliminar" ID="btnDelete" CommandName="Delete"></asp:LinkButton>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <span class="blackblodtext">Plantillas totales:</sapn> <asp:Label ID="lblTotalRecords" runat="server" Text=""></asp:Label><%--FB 2579--%>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoUserTemplates" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError">
                                No se encontraron Plantillas.
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>                    
                </td>
            </tr>
            <%--ZD 100540 Starts--%>
         <%--   <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Crear Nueva Plantilla</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>--%>
            <%--ZD 100540 Ends--%>
            <tr>
                <td align="center">
                    <table width="90%"><tr><td align="right">
                     <%--<asp:Button ID="btnCreateNew" OnClick="CreateNewTemplate" Width="150pt"  Text="Submit" runat="server" /> <%--FB 2796--%> <%--ZD 100420--%>
                        <button ID="btnCreateNew" onserverclick="CreateNewTemplate" style="Width:150pt"  runat="server" >Crear Nueva Plantilla</button> <%--ZD 100420--%> <%--ZD 100540 --%>
                    </td></tr></table>
                </td>
            </tr>
        </table>
    </div>

<img src="keepalive.asp" alt="Sobrevivir" name="myPic" width="1px" height="1px" style="display:none"/><%--ZD 100419--%>
    </form>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>

