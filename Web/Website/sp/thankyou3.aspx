<%--ZD 100147 start--%>
<%--/* Copyright (C) 2014 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<html>
<head>
<title>Gracias</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" marginheight="0" marginwidth="0" background="image/background.gif">
  <br>
  <center>
	<br><br><br>
	<font size="4">
      <p>Permiso denegado para iniciar sesi�n dos veces desde la misma cuenta simult�neamente.<br>
      Por favor, intente iniciar sesi�n algo m�s tarde.</p><br><br>
      <p>Razones probables para este mensaje podr�an ser :<br><br>
      <li><font color="red">La cuenta est� activa actualmente.</font><br>
      <li><font color="red">La sesi�n con el servidor Web se ha cortado o desconectado bruscamente.</font></p>
      <br><br>
      <p><font size="2">Para soporte t�cnico contacte con myVRM en <%=Application["contactPhone"]%>.</font></p>
	</font>
  </center>
</body>
</html>