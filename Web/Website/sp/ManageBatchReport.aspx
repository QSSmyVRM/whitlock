<%--ZD 100147 start--%>
<%--/* Copyright (C) 2014 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" UICulture="es-US" Culture="es-US" Inherits="ns_ManageBatchReport.ManageBatchReport" Buffer="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <%--FB 2779--%>
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.2, Version=10.2.3.0, Culture=neutral,PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxAxEd" %>


<script language="javascript" type="text/javascript">
    //ZD 100604 start
    var img = new Image();
    img.src = "../sp/image/wait1.gif";
    //ZD 100604 End
    function fnChangeOption(a1) {
        if (a1 == "5") {
            document.getElementById("CustomRow").style.display = "";
            document.getElementById("tdSpace").style.display = "";
            document.getElementById("DateRow1").style.display = "None";
            document.getElementById("DateRow2").style.display = "None";
            document.getElementById("DateRowEnd1").style.display = "None";
            document.getElementById("DateRowEnd2").style.display = "None";



            ValidatorEnable(document.getElementById("reqSDate"), false);
            ValidatorEnable(document.getElementById("reqEDate"), false);
        }
        else {
            document.getElementById("CustomRow").style.display = "None";
            document.getElementById("tdSpace").style.display = "None";
            document.getElementById("DateRow1").style.display = "";
            document.getElementById("DateRow2").style.display = "";
            document.getElementById("DateRowEnd1").style.display = "";
            document.getElementById("DateRowEnd2").style.display = "";

            //ValidatorEnable(document.getElementById("reqSDate"), true); 
            //ValidatorEnable(document.getElementById("reqEDate"), true); 
        }
    }

    function fnCancel() {
        DataLoading(1); //ZD 100176
        window.location.replace('organisationsettings.aspx');
    }

    function fnShowMenu() {
        var reportsList = document.getElementById("ReportsList");
        var type = document.getElementById("DrpAllType");
        var confScheRptDivList = document.getElementById("ConfScheRptDivList");
        var resourseType = document.getElementById("lstResourseType");//FB 2410
        var rptListue;
        var lstUsageRpts;

        reportsList.value = ReportsList.GetValue();
        type = DrpAllType.GetValue();
        confScheRptDivList.value = ConfScheRptDivList.GetValue();
        lstUsageRpts = lstUsageReports.GetValue();

        if (reportsList)
            rptListValue = reportsList.value;

        //document.getElementById("lstResourseType").style.display = "Block";

        HideControls(rptListValue);
        document.getElementById("tblDate").style.display = "Block";        
        switch (rptListValue) {
            case "1":
                document.getElementById("ConfScheRptCell1").style.display = "Block";
                document.getElementById("ConfScheRptCell2").style.display = "Block";
                break;
            case "2":
                document.getElementById("UserReportsCell1").style.display = "Block";
                document.getElementById("UserReportsCell2").style.display = "Block";
                document.getElementById("tblDate").style.display = "None";
                ValidatorEnable(document.getElementById("reqDFrom"), false);
                ValidatorEnable(document.getElementById("reqDTo"), false);                  
                break;
            case "3":
                document.getElementById("UsageReportCell1").style.display = "Block";
                document.getElementById("UsageReportCell2").style.display = "Block";
                break;
            case "4":
                document.getElementById("tdAllType1").style.display = "Block";
                document.getElementById("tdAllType2").style.display = "Block";
                var drpchrtValue = DrpChrtType.GetValue();
                if (type == "1") {
                    document.getElementById("tdConfType1").style.display = "block";
                    document.getElementById("tdConfType2").style.display = "block";
                }
                break;
            case "5":
                document.getElementById("tdDaily1").style.display = "block";
                document.getElementById("tdDaily2").style.display = "block";
                document.getElementById("tdStatus1").style.display = "block";
                document.getElementById("tdStatus2").style.display = "block";
                break;
            case "6":

                resourseType.value = lstResourseType.GetValue();
                var rType = resourseType.value;
                if (rType != "5") 
                {
                    document.getElementById("tblDate").style.display = "None";
                    ValidatorEnable(document.getElementById("reqDFrom"), false);
                    ValidatorEnable(document.getElementById("reqDTo"), false);
                }
                document.getElementById("tdResourse1").style.display = "block";
                document.getElementById("tdResourse2").style.display = "block";
                if (resourseType.value == "1") {
                    document.getElementById("tdUser1").style.display = "block";
                    document.getElementById("tdUser2").style.display = "block";
                }
                else if (resourseType.value == "2") {
                    document.getElementById("tdRoom1").style.display = "block";
                    document.getElementById("tdRoom2").style.display = "block";
                }
                else if (resourseType.value == "3" || resourseType.value == "4") {
                    document.getElementById("tdRoom1").style.display = "none";
                    document.getElementById("tdRoom2").style.display = "none";
                    document.getElementById("tdUser1").style.display = "none";
                    document.getElementById("tdUser2").style.display = "none";
                }
                else if (resourseType.value == "5") {
                    document.getElementById("tdRoomConf1").style.display = "block";
                    document.getElementById("tdRoomConf2").style.display = "block";
                    //document.getElementById("DateSelRow").style.display = "Block";
                }
                break;
            case "7":
                document.getElementById("tdOrg1").style.display = "block";
                document.getElementById("tdOrg2").style.display = "block";
                document.getElementById("tdResourse1").style.display = "block";
                document.getElementById("tdResourse2").style.display = "block";
                if (resourseType.value == "1") {
                    document.getElementById("tdUser1").style.display = "block";
                    document.getElementById("tdUser2").style.display = "block";
                }
                else if (resourseType.value == "2") {
                    document.getElementById("tdRoom1").style.display = "block";
                    document.getElementById("tdRoom2").style.display = "block";
                }
                else if (resourseType.value == "3" || resourseType.value == "4") {
                    document.getElementById("tdRoom1").style.display = "none";
                    document.getElementById("tdRoom2").style.display = "none";
                    document.getElementById("tdUser1").style.display = "none";
                    document.getElementById("tdUser2").style.display = "none";
                }
                else if (resourseType.value == "5") {
                    document.getElementById("tdRoomConf1").style.display = "block";
                    document.getElementById("tdRoomConf2").style.display = "block";
                    //document.getElementById("DateSelRow").style.display = "Block";
                }
                break;
        }
        return true;
    }

    function HideControls() {
        var args = HideControls.arguments;
        if (args[0] != "4") {
            document.getElementById("tdAllType1").style.display = "none";
            document.getElementById("tdAllType2").style.display = "none";
        }

        if (args[0] != "1") {
            document.getElementById("ConfScheRptCell1").style.display = "none";
            document.getElementById("ConfScheRptCell2").style.display = "none";
        }

        document.getElementById("tdConfType1").style.display = "none";
        document.getElementById("tdConfType2").style.display = "none";
        document.getElementById("UsageReportCell1").style.display = "none";
        document.getElementById("UsageReportCell2").style.display = "none";
        document.getElementById("UserReportsCell1").style.display = "none";
        document.getElementById("UserReportsCell2").style.display = "none";
        document.getElementById("tdDaily1").style.display = "none";
        document.getElementById("tdDaily2").style.display = "none";
        document.getElementById("tdStatus1").style.display = "none";
        document.getElementById("tdStatus2").style.display = "none";
        document.getElementById("tdOrg1").style.display = "none";
        document.getElementById("tdOrg2").style.display = "none";
        document.getElementById("tdUser1").style.display = "none";
        document.getElementById("tdUser2").style.display = "none";
        document.getElementById("tdRoom1").style.display = "none";
        document.getElementById("tdRoom2").style.display = "none";
        document.getElementById("tdRoomConf1").style.display = "none";
        document.getElementById("tdRoomConf2").style.display = "none";
        //FB 2410
        document.getElementById("tdResourse1").style.display = "none";
        document.getElementById("tdResourse2").style.display = "none";
    }

    function isOverInstanceLimit(cb) {
        csl = parseInt("<%=CustomSelectedLimit%>");

        if (!isNaN(csl)) {
            if (cb.length >= csl) {
                alert(EN_211)
                return true;
            }
        }

        return false;
    }

    function SortDates() {
        var temp;
        datecb = document.frmManageReport.CustomDate;
        var dateary = new Array();

        for (var i = 0; i < datecb.length; i++) {
            dateary[i] = datecb.options[i].value;

            dateary[i] = ((parseInt(dateary[i].split("/")[0], 10) < 10) ? "0" + parseInt(dateary[i].split("/")[0], 10) : parseInt(dateary[i].split("/")[0], 10)) + "/" + ((parseInt(dateary[i].split("/")[1], 10) < 10) ? "0" + parseInt(dateary[i].split("/")[1], 10) : parseInt(dateary[i].split("/")[1], 10)) + "/" + (parseInt(dateary[i].split("/")[2], 10));
        }

        for (i = 0; i < dateary.length - 1; i++)
            for (j = i + 1; j < dateary.length; j++)
            if (mydatesort(dateary[i], dateary[j]) > 0) {
            temp = dateary[i];
            dateary[i] = dateary[j];
            dateary[j] = temp;
        }

        for (var i = 0; i < dateary.length; i++) {
            datecb.options[i].text = dateary[i];
            datecb.options[i].value = dateary[i];
        }

        return false;
    }


    function removedate(cb) {
        if (cb.selectedIndex != -1) {
            cb.options[cb.selectedIndex] = null;
        }
        cal.refresh();
    }

    //ZD 100176 start
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
        else
            document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
    }
    //ZD 100176 End
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Administrar Informe del Conjunto</title>

    <script type="text/javascript" src="inc/functions.js"></script>

    <script language="javascript" type="text/javascript" src="../sp/Organizations/Original/Javascript/RGBColorPalette.js"> </script>

    <script type="text/javascript" src="script/cal-flat.js"></script>

    <script type="text/javascript" src="script/calview.js"></script>

    <script type="text/javascript" src="lang/calendar-en.js"></script>

    <script type="text/javascript" src="script/calendar-setup.js"></script>

    <script type="text/javascript" src="script/settings2.js"></script>

    <script type="text/javascript" src="script/calendar-flat-setup.js"></script>

    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css" />
    <%--<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main.css" />--%>

    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" />
    <style type="text/css">
        .style1
        {
            font-weight: bold;
            font-size: 10pt;
            color: black;
            font-family: Arial;
            text-decoration: none;
            width: 54px;
        }
        .style2
        {
            font-weight: bold;
            font-size: 10pt;
            color: black;
            font-family: Arial;
            text-decoration: none;
            width: 19%;
        }
        .style3
        {
            font-weight: bold;
            font-size: 10pt;
            color: black;
            font-family: Arial;
            text-decoration: none;
            width: 100px;
        }
        .style4
        {
            font-weight: bold;
            font-size: 10pt;
            color: black;
            font-family: Arial;
            text-decoration: none;
            width: 208px;
        }
        .style5
        {
            font-weight: bold;
            font-size: 10pt;
            color: black;
            font-family: Arial;
            text-decoration: none;
            width: 19%;
        }
        .style6
        {
            font-weight: bold;
            font-size: 10pt;
            color: black;
            font-family: Arial;
            text-decoration: none;
            width: 208px;
            height: 31px;
        }
        .style7
        {
            height: 31px;
        }
    </style>
</head>
<body>
    <form id="frmManageReport" runat="server" method="post" onsubmit="DataLoading(1);"><%--ZD 100176--%> 
    <input type="hidden" runat="server" id="hdnDateList" />
    <input type="hidden" runat="server" id="hdnCompareDate" />
    <h3>
        <asp:Label ID="lblHeader" runat="server"></asp:Label>
    </h3>
    <table width="700px" align="center" style="border-collapse: collapse" border="0" cellspacing="0" cellpadding="0" > <%-- FB 2767 --%>
        <tr>
            <td align="center" colspan="2">
                <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                <div id="dataLoadingDIV" align="center" style="display:none">
                    <img border='0' src='image/wait1.gif'  alt='Cargando..' />
                </div> <%--ZD 100176--%> <%--ZD 100678 End--%>
            </td>
        </tr>
        <tr>
            <td align="left" class="subtitleblueblodtext" colspan="2">
                <asp:Label ID="Label1" runat="server" Text="Ajustes del Informe"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left" class="style6">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Trabajos
            </td>
            <td class="style7">
            <div>
            <table border="0" style="margin-left:-3px"><tr><td>
                <dxAxEd:ASPxComboBox ID="drpJobName" runat="server" SelectedIndex="0" Width="205"
                    AutoPostBack="true" OnSelectedIndexChanged="DisplayDetailsByID" CssClass="altText"
                    ClientInstanceName="drpJobName" TextField="JobName" ValueField="Id">
                </dxAxEd:ASPxComboBox>
            </td><td>
            <asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="delJobName" 
                    ToolTip="Eliminar Nombre del Trabajo" OnClick="DeleteBatchReportConf" Visible="false"/>
            </td></tr></table>   
            </div>             
            </td>
            <td>
             
            </td>
        </tr>
       
        <tr>
            <td align="left" class="style4">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nombre del Informe
            </td>
            <td>
                <asp:TextBox ID="txtJob" runat="server" MaxLength="256" CssClass="altText" Width="203"></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqJob" runat="server" ValidationGroup="Submit" ControlToValidate="txtJob"
                    ErrorMessage="Necesario" Font-Bold="True" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regJob" ValidationGroup="Submit" ControlToValidate="txtJob"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br> & < y > no son caracteres v�lidos."
                    ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr style ="height :10px;"></tr>
        <tr>
            <td valign="top" class="style4" align="left">
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Informes
            </td>
            <td align="left" nowrap="nowrap">
                <dxAxEd:ASPxComboBox ID="ReportsList" runat="server" SelectedIndex="0" Width="205"
                    CssClass="altText" ClientInstanceName="ReportsList">
                    <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                    <Items>
                        <dxAxEd:ListEditItem Text="Informes de Programaci�n de Conferencias" Value="1" />
                        <dxAxEd:ListEditItem Text="Informes de Usuario" Value="2" />
                        <dxAxEd:ListEditItem Text="Informes de uso" Value="3" />
                        <dxAxEd:ListEditItem Text="Informes personales" Value="5" />
                        <dxAxEd:ListEditItem Text="Informes de utilizaci�n" Value="8" />
                    </Items>
                </dxAxEd:ASPxComboBox>
             </td>
        </tr>
        
        <tr>
            <td style="width: 50%;" nowrap="nowrap">
                <div id="ConfScheRptCell1" style="display: none; padding-top:10px">
                    <b class="blackblodtext"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Informes de Programaci�n de Conferencias</b></div>
            </td>
            <td style="width: 145px;" nowrap="nowrap">
                <div id="ConfScheRptCell2" style="display: none; padding-top:10px">
                    <dxAxEd:ASPxComboBox ID="ConfScheRptDivList" runat="server" SelectedIndex="0" Width="205"
                        CssClass="altSelectFormat" ClientInstanceName="ConfScheRptDivList">
                        <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                        <Items>
                            <dxAxEd:ListEditItem Text="Programa diario" Value="2" />
                            <dxAxEd:ListEditItem Text="Programa diario" Value="1" />
                            <dxAxEd:ListEditItem Text="Asignaci�n PRI" Value="3" />
                            <dxAxEd:ListEditItem Text="Asignaci�n de Recursos" Value="4" />
                        </Items>
                    </dxAxEd:ASPxComboBox>
                </div>
            </td>
        </tr>
        <tr style ="height :5px"></tr>
        <tr>
            <td>
                <div id="UserReportsCell1" style="display: none;padding-top:20px;" runat="server">
                    <b class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Informes de Usuario</b>
                </div>
            </td>
            <td>
                <div id="UserReportsCell2" style="display: none;padding-top:20px;" runat="server">
                    <dxAxEd:ASPxComboBox ID="lstUserReports" runat="server" SelectedIndex="0" Width="205px"
                        CssClass="altSelectFormat">
                        <Items>
                            <dxAxEd:ListEditItem Text="Lista de Contactos" Value="1" />
                        </Items>
                    </dxAxEd:ASPxComboBox>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div id="UsageReportCell1" style="display: none;padding-top:10px" runat="server">
                    <b class="blackblodtext"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Informes de Uso</b>
                </div>
            </td>
            <td>
                <div id="UsageReportCell2" style="display: none;padding-top:10px" runat="server">
                    <dxAxEd:ASPxComboBox ID="lstUsageReports" runat="server" SelectedIndex="0" Width="205px"
                        CssClass="altSelectFormat" ClientInstanceName="lstUsageReports">
                        <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                    </dxAxEd:ASPxComboBox>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div id="tdAllType1" style="display: none;padding-top:10px">
                    <b class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Categor�a</b>
                </div>
            </td>
            <td>
                <div id="tdAllType2" style="display: none;padding-top:10px">
                    <dxAxEd:ASPxComboBox ID="DrpAllType" runat="server" SelectedIndex="0" Width="205px"
                        CssClass="altSelectFormat" ClientInstanceName="DrpAllType">
                        <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                    </dxAxEd:ASPxComboBox>
                    </div>
            </td>
            
        </tr>
        <tr>
            <td>
                <div id="tdConfType1" style="display: none;padding-top:10px">
                    <b class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tipo de Conferencia</b>
                </div>
            </td>
            <td>
                <div id="tdConfType2" style="display: none;padding-top:10px">
                    <dxAxEd:ASPxComboBox ID="lstConfType" runat="server" SelectedIndex="0" Width="205"
                        CssClass="altSelectFormat">
                        <Items>
                            <dxAxEd:ListEditItem Text="Todo" Value="1" />
                            <dxAxEd:ListEditItem Text="Conferencias de Audio Solamente" Value="6" />
                            <dxAxEd:ListEditItem Text="S�lo Videoconferencias" Value="2" />
                            <dxAxEd:ListEditItem Text="S�lo Punto a Punto" Value="4" />
                            <dxAxEd:ListEditItem Text="S�lo conferencias de Sal�n" Value="7" />
                        </Items>
                    </dxAxEd:ASPxComboBox>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div id="tdDaily1" style="display: none;padding-top:10px">
                    <b class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mensual</b>
                </div>
            </td>
            <td>
                <div id="tdDaily2" style="display: none;padding-top:10px">
                    <dxAxEd:ASPxComboBox ID="lstDailyMonthly" runat="server" SelectedIndex="0" Width="205"
                        CssClass="altSelectFormat" ClientInstanceName="lstDailyMonthly">
                        <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                        <Items>
                            <dxAxEd:ListEditItem Text="Mensual" Value="2" />
                        </Items>
                    </dxAxEd:ASPxComboBox>
                </div>
            </td>
        </tr>
        
        <tr>
            <td>
                <div id="tdStatus1" style="display: none;padding-top:10px">
                    <b class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Estado</b>
                </div>
            </td>
            <td>
                <div id="tdStatus2" style="display: none;padding-top:10px">
                    <dxAxEd:ASPxComboBox ID="lstStatusType" runat="server" SelectedIndex="0" Width="205"
                        CssClass="altSelectFormat" ClientInstanceName="lstStatusType">
                        <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                        <Items>
                            <dxAxEd:ListEditItem Text="En ejecuci�n" Value="2" />
                            <dxAxEd:ListEditItem Text="Pendiente" Value="3" />
                            <dxAxEd:ListEditItem Text="Reserva" Value="4" />
                            <dxAxEd:ListEditItem Text="Finalizado" Value="5" />
                            <dxAxEd:ListEditItem Text="Borrado" Value="6" />
                            <dxAxEd:ListEditItem Text="P�blico" Value="7" />
                        </Items>
                    </dxAxEd:ASPxComboBox>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div id="tdOrg1" style="display: none;padding-top:10px">
                    <b class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Seleccionar Organizaci�n</b>
                </div>
            </td>
            <td>
                <div id="tdOrg2" style="display: none;padding-top:10px">
                    <dxAxEd:ASPxComboBox ID="lstOrg" runat="server" SelectedIndex="0" Width="40%" CssClass="altSelectFormat"
                        ValueField="OrgID" TextField="OrganizationName">
                    </dxAxEd:ASPxComboBox>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div id="tdResourse1" style="display: none;padding-top:10px">
                    <b class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Informes de Recursos</b></div>
            </td>
            <td>
                <div id="tdResourse2" style="display: none;padding-top:10px">
                    <dxAxEd:ASPxComboBox ID="lstResourseType" runat="server" SelectedIndex="0" Width="205"
                        CssClass="altSelectFormat" ClientInstanceName="lstResourseType">
                        <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                        <Items>
                            <dxAxEd:ListEditItem Text="Informes de Usuario" Value="1" />
                            <dxAxEd:ListEditItem Text="Informe de sal�n" Value="2" />
                            <dxAxEd:ListEditItem Text="Informe de Punto final" Value="3" />
                            <dxAxEd:ListEditItem Text="Informe MCU" Value="4" />
                            <dxAxEd:ListEditItem Text="Informe del Programa diario" Value="5" />
                        </Items>
                    </dxAxEd:ASPxComboBox>
                </div>
            </td>
        </tr>
       
        <tr>
            <td>
                <div id="tdUser1" style="display: none;padding-top:10px">
                    <b class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Informes de Usuario</b>
                </div>
            </td>
            <td>
                <div id="tdUser2" style="display: none;padding-top:10px">
                    <dxAxEd:ASPxComboBox ID="lstUserType" runat="server" SelectedIndex="0" Width="40%"
                        CssClass="altSelectFormat" ClientInstanceName="lstUserType">
                        <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                        <Items>
                            <dxAxEd:ListEditItem Text="Todo" Value="1" />
                            <dxAxEd:ListEditItem Text="Activo" Value="2" />
                            <dxAxEd:ListEditItem Text="Inactivo" Value="3" />
                            <dxAxEd:ListEditItem Text="Bloqueado" Value="4" />
                        </Items>
                    </dxAxEd:ASPxComboBox>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div id="tdRoom1" style="display: none;padding-top:10px">
                    <b class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Informes del sal�n</b>
                </div>
            </td>
            <td>
                <div id="tdRoom2" style="display: none;padding-top:10px">
                    <dxAxEd:ASPxComboBox ID="lstRoomType" runat="server" SelectedIndex="0" Width="40%"
                        CssClass="altSelectFormat" ClientInstanceName="lstRoomType">
                        <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                        <Items>
                            <dxAxEd:ListEditItem Text="Activo" Value="1" />
                            <dxAxEd:ListEditItem Text="Desactivar" Value="2" />
                        </Items>
                    </dxAxEd:ASPxComboBox>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div id="tdRoomConf1" style="display: none;padding-top:10px">
                    <b class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Informe del Programa diario</b>
                </div>
            </td>
            <td>
                <div id="tdRoomConf2" style="display: none;padding-top:10px">
                    <dxAxEd:ASPxComboBox ID="lstConfRoomRpt" runat="server" SelectedIndex="0" Width="83%"
                        CssClass="altSelectFormat" ClientInstanceName="lstConfRoomRpt">
                        <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                        <Items>
                            <dxAxEd:ListEditItem Text="Uso por Sal�n (Programado)" Value="1" />
                            <dxAxEd:ListEditItem Text="Uso por Sal�n (Real)" Value="2" />
                            <dxAxEd:ListEditItem Text="Uso por Sal�n/Conferencia" Value="3" />
                        </Items>
                    </dxAxEd:ASPxComboBox>
                </div>
            </td>
        </tr>
        <tr>
            <td align="left" nowrap="nowrap" style="padding-top:10px" class="style5">
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Direcci�n de Correo Electr�nico Para Enviar Informe
            </td>
            <td style="padding-top:10px">
                <asp:TextBox ID="txtEmail" runat="server" MaxLength="256" CssClass="altText" Width="205"></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqEmail" runat="server" ValidationGroup="Submit"
                    ControlToValidate="txtEmail" ErrorMessage="Necesario" Font-Bold="True" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regEmail1_1" ValidationGroup="Submit" ControlToValidate="txtEmail"
                    Display="dynamic" runat="server" ErrorMessage="<br>Direcci�n de correo electr�nico Inv�lida." ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator ID="regEmail1_2" ValidationGroup="Submit" ControlToValidate="txtEmail"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ /(); ? | ^= ! ` , [ ] { } : # $ ~ y &#34;  no son caracteres v�lidos."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table width="80%" align="left">
                    <tr>
                        <td align="left" class="subtitleblueblodtext" colspan="2">
                            Frecuencia de Entrega
                            <br />
                            <dxAxEd:ASPxRadioButtonList ID="rblFrequency" ClientInstanceName="rblFrequency" RepeatDirection="Horizontal"
                                runat="server" ValueType="System.Int32" Width="110%">
                                <Border BorderWidth="0px" />
                                <Items>
                                    <dxAxEd:ListEditItem Text="Diariamente" Value="1" Selected="true" />
                                    <dxAxEd:ListEditItem Text="Semanalmente" Value="2" />
                                    <dxAxEd:ListEditItem Text="Bi-semanal" Value="3" />
                                    <dxAxEd:ListEditItem Text="Mensual" Value="4" />
                                    <dxAxEd:ListEditItem Text="Personalizado" Value="5" />
                                </Items>
                                <ClientSideEvents SelectedIndexChanged="function(s, e) {fnChangeOption(s.GetValue()); } " />
                            </dxAxEd:ASPxRadioButtonList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="style2">
                <div id="DateRow1" runat="server">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Iniciar el
                </div>
            </td>
            <td>
                <div id="DateRow2" runat="server">
                    <dxAxEd:ASPxDateEdit ID="startDateedit" ClientInstanceName="startDateedit" runat="server"
                        Width="205px" CalendarProperties-TodayButtonText="Hoy" CalendarProperties-ClearButtonText="Borrar" > 
					<%--ZD 100423--%>
                        <ClientSideEvents KeyDown="function(s, e) {
                        if (e.htmlEvent.KeyCode == ASPxKey.Enter)
                            s.HideDropDown();
                    }" GotFocus="function(s, e) {
                        s.ShowDropDown();
                    }" />
                    </dxAxEd:ASPxDateEdit>
                    <asp:RequiredFieldValidator ID="reqSDate" runat="server" ValidationGroup="Submit"
                        ControlToValidate="startDateedit" ErrorMessage="Necesario" Font-Bold="True" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
            </td>
        </tr>
       <tr style ="height :10px"></tr>
        <tr>
            <td class="style2">
                <div id="DateRowEnd1" runat="server">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Terminal el
                </div>
            </td>
            <td>
                <div id="DateRowEnd2" runat="server">
                    <dxAxEd:ASPxDateEdit ID="endDateedit" ClientInstanceName="endDateedit" runat="server"
                        Width="205" CalendarProperties-TodayButtonText="Hoy" CalendarProperties-ClearButtonText="Borrar" > 
					<%--ZD 100423--%>
                        <ClientSideEvents KeyDown="function(s, e) {
                        if (e.htmlEvent.KeyCode == ASPxKey.Enter)
                            s.HideDropDown();
                    }" GotFocus="function(s, e) {
                        s.ShowDropDown();
                    }" />
                    </dxAxEd:ASPxDateEdit>
                    <asp:RequiredFieldValidator ID="reqEDate" runat="server" ValidationGroup="Submit"
                        ControlToValidate="endDateedit" ErrorMessage="Necesario" Font-Bold="True" Display="Dynamic"></asp:RequiredFieldValidator>
                   
                </div>
            </td>
        </tr>
        <tr id="CustomRow" runat="server" style="display: none;">
            <td colspan="2">
            <div id="divCustom" style="z-index:0 ; position:absolute";>
                <table width="60%" border="0">
                    <tr valign="top">
                        <td align="left" valign="top" class="blackblodtext" width="25%" nowrap="nowrap">
                            <br />
                            &nbsp;&nbsp;&nbsp;&nbsp; Fechas Personalizadas
                        </td>
                        <td align="left">
                            <table border="0" width="100%" cellspacing="0">
                                <tr>
                                    <td style="width: 30%">
                                        <a href="#" class="name" onclick="return false"><div id="flatCalendarDisplay" style="float: right; clear: both; background-color:#E0E0E0"> <%--ZD 100426--%>
                                        </div></a>
                                        <br />
                                        <div id="preview" style="font-size: 80%; text-align: center; padding: 2px">
                                        </div>
                                    </td>
                                    <td>
                                    </td>
                                    <td style="width: 20%" valign="top">
                                        <span class="blueSText"><b>Fecha Seleccionada</b></span><br />
                                        <asp:ListBox runat="server" ID="CustomDate" Rows="8" CssClass="altSmall0SelectFormat"
                                            onChange="JavaScript: removedate(this);" onblur="javascript:document.getElementById('errLabel').innerHTML = '' "
                                            EnableViewState="true"></asp:ListBox>
                                        <br />
                                        <span class="blueSText">* haga 'clic' sobre una fecha para eliminarla de la lista.</span>
                                    </td>
                                    <td>
										<%--ZD 100423--%>
                                        <button ID="btnsortDates" runat="server"  class="altMedium0BlueButtonFormat"
                                        onclick="javascript:return SortDates();"  style="Width:90px" >Ordenar</button> <%--ZD 100420--%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
               </div> 
            </td>
            
        </tr>
        <tr id="tdSpace" style="display:none; height:211px">
        <td colspan="2" style="height:211px"></td>
        </tr>
        <tr>
            <td colspan="2"><%-- FB 2767 Starts --%>
                <table id="tblDate" border="0" >
                <tr><%-- FB 2917 Starts --%>
            <td align="left" class="subtitleblueblodtext" colspan="2">
                Par�metros Predeterminados Para ese Informe
            </td>
        </tr>
        <tr>
            <td colspan="2" width ="345px"> 
               &nbsp;&nbsp;&nbsp;&nbsp;<b>Fecha DESDE</b> <%--FB 2917--%>
            </td>
            <td id="tdDateFrom">
                <dxAxEd:ASPxDateEdit ID="dateFrom" ClientInstanceName="dateFrom" runat="server" Width="205" CalendarProperties-TodayButtonText="Hoy" CalendarProperties-ClearButtonText="Borrar" > 
				<%--ZD 100423--%>
                <ClientSideEvents KeyDown="function(s, e) {
                        if (e.htmlEvent.KeyCode == ASPxKey.Enter)
                            s.HideDropDown();
                    }" GotFocus="function(s, e) {
                        s.ShowDropDown();
                    }" />
                </dxAxEd:ASPxDateEdit>
                <asp:RequiredFieldValidator ID="reqDFrom" runat="server" ValidationGroup="Submit"
                    ControlToValidate="dateFrom" ErrorMessage="Necesario" Font-Bold="True" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr style ="height :10px"></tr>
        <tr>
            <td colspan="2">
                &nbsp;&nbsp;&nbsp;&nbsp;<b>Fecha HASTA</b> <%--FB 2917--%>
            </td>
            <td id="tdDateTo">
                <dxAxEd:ASPxDateEdit ID="dateTo" ClientInstanceName="dateTo" runat="server" Width="205" CalendarProperties-TodayButtonText="Hoy" CalendarProperties-ClearButtonText="Borrar" > 
					<%--ZD 100423--%>
                    <ClientSideEvents KeyDown="function(s, e) {
                        if (e.htmlEvent.KeyCode == ASPxKey.Enter)
                            s.HideDropDown();
                    }" GotFocus="function(s, e) {
                        s.ShowDropDown();
                    }" />
                </dxAxEd:ASPxDateEdit>
                <asp:RequiredFieldValidator ID="reqDTo" runat="server" ValidationGroup="Submit" ControlToValidate="dateTo"
                    ErrorMessage="Necesario" Font-Bold="True" Display="Dynamic"></asp:RequiredFieldValidator>
              
            </td>
        </tr>
                </table><%-- FB 2767 Ends --%>
            </td>
        </tr>
        
        <tr style="height: 10px;">
        </tr>
        <tr>
            <td align="center" colspan="2" style="padding-right:50px" > <%-- FB 2767 --%>
                
                <%--<input class="altLongBlueButtonFormat" id="btnCancel" onclick="fnCancel()" type="button" value="Cancelar"
                    name="btnCancel" style="width: 20%" />--%>
                    <button type="button" id="btnCancel" class="altLongYellowButtonFormat" onclick="fnCancel();" style="width:100px">Cancelar</button>
				<%--ZD 100423--%> <%--ZD 100420--%>
                
                <button type="button" ID="btnReset"  runat="server" class="altLongBlueButtonFormat"
                     style="width: 20%" onserverclick="ResetPage"  onclick="javascript:DataLoading(1);">Reajustar</button> <%--ZD 100176--%> 
                
                <button type="button" ID="btnCreate" runat="server" class="altLongBlueButtonFormat"
                    style="width: 20%" onclick="fnListValue();" onserverclick="SaveReportConfiguration"
                    ValidationGroup="Submit">Someter</button> <%-- FB 2917 Ends --%>
            </td>
        </tr>
    </table>
    <img src="keepalive.asp" alt="Sobrevivir" name="KeepAlive" width="1px" height="1px"/> <%--ZD 100419--%>
    </form>

    <script type="text/javascript">
        fnShowMenu();
    </script>

    <%--code added for Soft Edge button--%>

    <script type="text/javascript" src="inc/softedge.js"></script>

    <script type="text/javascript">
        document.onkeydown = function(evt) {
            evt = evt || window.event;
            var keyCode = evt.keyCode;
            if (keyCode == 8) {
                if (document.getElementById("btnCancel") != null) { // backspace
                    var str = document.activeElement.type;
                    if (!(str == "text" || str == "textarea" || str == "password")) {
                        document.getElementById("btnCancel").click();
                        return false;
                    }
                }
                if (document.getElementById("btnGoBack") != null) { // backspace
                    var str = document.activeElement.type;
                    if (!(str == "text" || str == "textarea" || str == "password")) {
                        document.getElementById("btnGoBack").click();
                        return false;
                    }
                }
            }
            fnOnKeyDown(evt);
        };
    
        // FB 2767 Starts //FB 2917 Starts
//        if (navigator.userAgent.indexOf("Trident/6.0") > -1) {
//            document.getElementById("tdDateFrom").style.paddingRight = "5px";
//            document.getElementById("tdDateTo").style.paddingRight = "5px";
//        }
//        else if (navigator.userAgent.indexOf("MSIE") > -1) {
//            document.getElementById("tdDateFrom").style.paddingRight = "42px";
//            document.getElementById("tdDateTo").style.paddingRight = "42px";
//        }
//        else if (navigator.userAgent.indexOf("Firefox") > -1) {
//            document.getElementById("tdDateFrom").style.paddingRight = "60px";
//            document.getElementById("tdDateTo").style.paddingRight = "60px";
//        }
//        else {
//            document.getElementById("tdDateFrom").style.paddingLeft = "85px";
//            document.getElementById("tdDateTo").style.paddingLeft = "85px";
//        }
//        // FB 2767 Ends //FB 2917 Ends

        var servertoday = new Date();
        var dFormat;
        dFormat = "<%=format %>";

        showFlatCalendar(1, dFormat)

        function fnListValue() {

            if (!Page_ClientValidate()) //FB 2410
                return Page_IsValid;

            var Date1 = document.getElementById("startDateedit_I").value;
            var Date2 = document.getElementById("endDateedit_I").value;
            
            var Date3 = document.getElementById("dateFrom_I").value;
            var Date4 = document.getElementById("dateTo_I").value;

            var ret = fnCompareDate2(Date1, Date2, 0);
            if (ret == false)
                return ret;
                
            var ret1 = fnCompareDate2(Date3, Date4, 1);
            if (ret1 == false)
                return ret1;
          
            if (rblFrequency.GetValue() == "5") {
                var datecb = document.frmManageReport.CustomDate;
                var datelist = document.getElementById("hdnDateList");

                if (datecb.length < 1) {
                    document.getElementById("errLabel").innerHTML = "Por favor, seleccione Fechas";
                    return false;
                }

                for (var i = 0; i < datecb.length; i++) {
                    if (datelist.value == "")
                        datelist.value = datecb.options[i].value;
                    else
                        datelist.value = datelist.value + "," + datecb.options[i].value;
                }

                ValidatorEnable(document.getElementById("reqSDate"), false);
                ValidatorEnable(document.getElementById("reqEDate"), false);

                return true;
            }
            else {
                ValidatorEnable(document.getElementById("reqSDate"), true);
                ValidatorEnable(document.getElementById("reqEDate"), true);

                if (!Page_ClientValidate())
                    return Page_IsValid;

                return true;
           }

        }
        
        
        if (document.getElementById("CustomRow").style.display != "none") {
            document.getElementById("tdSpace").style.display = "block"
        }


        function fnCompareDate2(Date1, Date2,par ) {


            dFormat = "<%=format %>";
            var str1 = Date1.toString();
            var str2 = Date2.toString();

            var dt1 = parseInt(str1.substring(0, 2), 10);
            var mon1 = parseInt(str1.substring(3, 5), 10);
            var yr1 = parseInt(str1.substring(6, 10), 10);
            var dt2 = parseInt(str2.substring(0, 2), 10);
            var mon2 = parseInt(str2.substring(3, 5), 10);
            var yr2 = parseInt(str2.substring(6, 10), 10);

            if (dFormat == "MM/dd/yyyy") {
                var temp1 = "";
                var temp2 = "";
                temp1 = mon1;
                mon1 = dt1;
                dt1 = temp1;

                temp2 = mon2;
                mon2 = dt2;
                dt2 = temp2;

                var date1 = new Date(yr1, mon1, dt1);
                var date2 = new Date(yr2, mon2, dt2);
            }
            else {
                var date1 = new Date(yr1, mon1, dt1);
                var date2 = new Date(yr2, mon2, dt2);
            }

            if (date2 < date1) {
                if (par == 0) {
                    alert("La Fecha de Inicio deber�a anterior a la Fecha de Finalizaci�n")
                }
                else {
                    alert("La Fecha DESDE deber�a ser anterior a la Fecha DESDE");
                }
                return false;
            }
//            else {
//                alert("Submitting ...");
//                document.form1.submit();
//            }
        }

  
    </script>

    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>
