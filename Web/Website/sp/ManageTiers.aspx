<%--ZD 100147 start--%>
<%--/* Copyright (C) 2014 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" Inherits="ns_Tiers.Tiers" Buffer="true" %>
<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2779 Ends--%>


<head runat="server">
    <title>Manage Tiers</title>
    <script type="text/javascript" src="inc/functions.js"></script>
    <script type="text/javascript" >
        //ZD 100604 start
        var img = new Image();
        img.src = "../sp/image/wait1.gif";
        //ZD 100604 End
    //ZD 100176 start
		function DataLoading(val) {
		    if (val == "1")
		        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
		    else
		        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
		}
		//ZD 100176 End
		</script>
</head>
<body>
    <form id="frmTierManagement" runat="server" method="post" onsubmit="return true;DataLoading(1)"> <%--ZD 100176--%> 
    <div> 
      <input type="hidden" id="helpPage" value="65">

        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
            <div id="dataLoadingDIV" style="display:none" align="center" >
                <img border='0' src='image/wait1.gif'  alt='Cargando..' />
            </div> <%--ZD 100678 End--%>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td >&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Niveles existentes</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgTier1s" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="true" OnUpdateCommand="UpdateTier1" OnCancelCommand="CancelTier1" OnItemCreated="BindRowsDeleteMessage"
                        OnDeleteCommand="DeleteTier1" OnEditCommand="EditTier1" Width="90%" Visible="true" style="border-collapse:separate" > <%--Edited for FF--%>
                        <%--Window Dressing - Start--%>
                        <SelectedItemStyle CssClass="tableBody" />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                        <FooterStyle CssClass="tableBody" /><%--Added for Window Dressing --%>                        
                        <%--Window Dressing - End--%>
                        <HeaderStyle CssClass="tableHeader" />
                        <Columns>
                            <asp:BoundColumn DataField="ID" Visible="false" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Nombre" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"> <%-- FB 2050 --%>
                                <ItemTemplate>
                                    <asp:Label ID="lblTier1Name" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtTier1Name" CssClass="altText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name")%>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqTier1Name1" ControlToValidate="txtTier1Name" runat="server" ErrorMessage="Necesario" Display="dynamic"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regName" ControlToValidate="txtTier1Name" runat="server" Display="Dynamic" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ( ) ? | ^ = ! ` [ ] { } # $ @ y ~ no son caracteres v�lidos." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=^@#$%&()~]*$"></asp:RegularExpressionValidator> <%-- fogbugz case 137 for extra junk character validation--%> <%--FB 1888--%>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="Administrar nivel 2"><%--ZD 100425--%>
                                <ItemTemplate>
                                    <asp:Button ID="btnManageTier2" runat="server" CssClass="altLongBlueButtonFormat" OnCommand="ManageTier2" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.ID") + "^" + DataBinder.Eval(Container, "DataItem.Name") %>' Text="Administrar nivel 2" CommandName="select"   OnClientClick="DataLoading(1)"/><%--ZD 100176--%> 
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Acciones" ItemStyle-Width="15%">
                                <HeaderStyle CssClass="tableHeader" />
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" Text="Editar" ID="btnEdit" CommandName="Edit" OnClientClick="DataLoading(1)"></asp:LinkButton><%--ZD 100176--%> 
                                    &nbsp;<asp:LinkButton runat="server" Text="Eliminar" ID="btnDelete" CommandName="Delete" OnClientClick="DataLoading(1)"></asp:LinkButton><%--ZD 100176--%> 
                                    <asp:LinkButton runat="server" Text="Actualizar" ID="btnUpdate" CommandName="Update" Visible="false" OnClientClick="DataLoading(1)"></asp:LinkButton><%--ZD 100176--%> 
                                    &nbsp;<asp:LinkButton runat="server" Text="Cancelar" ID="btnCancel" CommandName="Cancel" Visible="false" OnClientClick="DataLoading(1)"></asp:LinkButton><%--ZD 100176--%> 
                                </ItemTemplate>
                                <FooterTemplate>
                                <%--Window Dressing--%>
                                    <span class="blackblodtext"> Niveles totales:</span> <asp:Label ID="lblTotalRecords" runat="server" Text=""></asp:Label><%--FB 2579--%>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoTier1s" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError">
                                No se encontraron niveles 1.
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>                    
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <%--<SPAN class=subtitleblueblodtext></SPAN>--%><%--Commented for FB 2094--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trNew" runat="server"> <%--FB 2670--%>
                <%--Window Dressing--%>
                    <td align="center" class="blackblodtext">Crear Primer Nivel Nuevo<span class="reqfldstarText">*</span><%--FB 2094--%>
                    <asp:TextBox ID="txtNewTier1Name" CssClass="altText" runat="server" Text="" ></asp:TextBox>
                    <asp:Button runat="server" ID="btnCreateNewTier1" ValidationGroup="Create" Text="Entregar" CssClass="altMedium0BlueButtonFormat" OnClick="CreateNewTier1" />
                    <asp:RequiredFieldValidator ID="reqTier1Name1" ValidationGroup="Create" ControlToValidate="txtNewTier1Name" runat="server" ErrorMessage="Necesario" Display="dynamic"></asp:RequiredFieldValidator>
                    <%--& <>'+%/\"();?|^&=;!`, fogbugz case 137 for extra junk character validation--%>
                    <asp:RegularExpressionValidator ID="regName1" ValidationGroup="Create" ControlToValidate="txtNewTier1Name" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ( ) ? | ^ = ! ` [ ] { } # $ @ y ~ no son caracteres v�lidos." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=^@#$%&()~]*$"></asp:RegularExpressionValidator>  <%--FB 1888--%>
                </td>
            </tr>
        </table>
    </div>

<img src="keepalive.asp" alt="Sobrevivir" name="myPic" width="0px" height="0px" style="display:none" /> <%--ZD 100419--%> 
    </form>
<%--code added for Soft Edge button--%>
<%--ZD 100420 Start--%>
<script language="javascript">
    if (document.getElementById('txtNewTier1Name') != null)
        document.getElementById('txtNewTier1Name').setAttribute("onblur", "document.getElementById('btnCreateNewTier1').focus(); document.getElementById('btnCreateNewTier1').setAttribute('onfocus', '');");               
</script>
<%--ZD 100420 End--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>

