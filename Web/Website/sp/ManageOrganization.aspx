﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2014 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="ns_MyVRM.ManageOrganization" Buffer="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->

<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<script type="text/javascript" src="inc/functions.js"></script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script language="javascript" type="text/javascript">
        //ZD 100604 start
        var img = new Image();
        img.src = "../sp/image/wait1.gif";
        //ZD 100604 End
//        function FnCancel()
//		{
//		    DataLoading(1); // ZD 100176
//			window.location.replace('SuperAdministrator.aspx');
//		}
		//ZD 100176 start
		function DataLoading(val) {
		    if (val == "1")
		        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
		    else
		        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
		}
		//ZD 100176 End
    </script>
    <title>Manage Organization</title>
</head>
<body>
    <form id="frmManageOrganization" runat="server">
    
    <div>
        <table cellpadding="0" cellspacing="0" id="OuterTable" width="100%" border="0">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server" Text="Administrar Organizaciones"></asp:Label><!-- FB 2570 -->
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                    <div id="dataLoadingDIV" style="display:none" align="center" >
                         <img border='0' src='image/wait1.gif'  alt='Cargando..' />
                    </div> <%--ZD 100678 End--%>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table width="100%">
                        <tr>
                            <td >&nbsp;</td>
                            <td>
                                <SPAN class="subtitleblueblodtext"></SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgOrganizations" runat="server" AutoGenerateColumns="False" CellPadding="3" GridLines="None"
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" OnEditCommand="EditOrganizationProfile" OnDeleteCommand="DeleteOrganizationProfile" OnCancelCommand="PurgeNow_Click"  OnItemDataBound="BindRowsDeleteMessage" ShowFooter="false" Width="90%" Visible="true" style="border-collapse:separate"><%-- Edited for FF--%> <%--FB 1753 //FB 2074--%>
                        <%--Window Dressing - Start--%>                        
                        <SelectedItemStyle CssClass="tableBody" />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                         <%--Window Dressing - End--%> 
                        <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                        <Columns>
                            <asp:BoundColumn DataField="orgId" Visible="false" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn> 
                            <asp:BoundColumn DataField="organizationName" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderText="Nombre de la Organización"></asp:BoundColumn>
                            <asp:BoundColumn DataField="phone" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderText="Número Telefónico"></asp:BoundColumn>
                            <asp:BoundColumn DataField="emailID" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderText="ID de Correo-e"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Acciones" ItemStyle-CssClass="tableBody" HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle CssClass="tableHeader" HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <table width="50%" align="center">
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="lnkEdit" runat="server" Text="Editar" CommandName="Edit" OnClientClick="DataLoading(1)"></asp:LinkButton>  <%--ZD 100176--%> 
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkDelete" runat="server" Text="Eliminar" CommandName="Delete" Enabled='<%# !DataBinder.Eval(Container, "DataItem.orgId").ToString().Trim().Equals("11")%>' 
                                                ></asp:LinkButton> <%--ZD 100176--%> <%--ZD 100429--%> 
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPurge" runat="server" Text="Purgar" CommandName="Cancel"  Enabled='<%# !DataBinder.Eval(Container, "DataItem.orgId").ToString().Trim().Equals("11")%>' 
                                                ></asp:LinkButton><%--ZD 100176--%> <%--ZD 100429--%> 
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoOrganizations" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <%--Windows Dressing--%>
                            <asp:TableCell CssClass="lblError" HorizontalAlign="center">
                                No se encontraron Organizaciones.  <%--Edited for FB 1405--%>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <table cellspacing="0" cellpadding="0" width="90%" border="0" align="center">
                        <tr>
                            <td width="60%">&nbsp;</td>
                            <td class="blackblodtext" align="right" nowrap>Organizaciones Totales:&nbsp;
                                <span id="SpnActiveOrgs" runat="server" class="blackblodtext"></span>
                            </td>
                            <td class="blackblodtext" align="right" nowrap>Licencias Restantes:&nbsp;
                                <span id="SpnLicense" runat="server" class="blackblodtext"></span>
                            </td>
                        </tr>
                    </table>                    
                </td>
            </tr>
            <tr>
                <td align="center" width="90%"><br />
                    <table cellspacing="0" cellpadding="1" width="90%" border="0">
                        <tr>
                            <td>
                                <%--<SPAN class=subtitleblueblodtext>Crear Nueva Organización</SPAN> --%> <%--ZD 100926--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center"><br />
                    <table cellspacing="0" cellpadding="3" width="90%" border="0" align="center">
                        <tr >
                            <td align="right">
                                <%--<%--<%--<input class="altMedium0BlueButtonFormat" id="btnCancel" onclick="FnCancel()" type="button" value="Cancel" name="btnCancel"></><%--ZD 100428--%>
                                <button id="btnCancel" class="altMedium0BlueButtonFormat" runat="server" onserverclick="FunctionCancel" onclick="javascript:DataLoading('1');">Cancelar</button>
                                <%--<asp:Button ID="btnNewOrganization"  Width="100pt" runat="server"  Text="Submit" OnClick="CreateNewOrganization"  OnClientClick="javascript:DataLoading('1');"/> <%--FB 2664--%> <%--ZD 100176--%> <%--ZD 100176--%><%--ZD 100420--%>
								<button ID="btnNewOrganization"  style="width:22%" class="altLongBlueButtonFormat" runat="server" onserverclick="CreateNewOrganization" onclick="javascript:DataLoading('1');" >Crear Nueva Organización</button><%--ZD 100420--%><%--ZD 100926--%>
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
         <script type="text/javascript" src="inc/softedge.js"></script>
    </div>
    </form>
</body>
</html>

<script type="text/javascript">
    document.onkeydown = function(evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 8) {
            if (document.getElementById("btnCancel") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnCancel").click();
                    return false;
                }
            }
            if (document.getElementById("btnGoBack") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnGoBack").click();
                    return false;
                }
            }
        }
        fnOnKeyDown(evt);
    };
    //document.getElementById('btnCancel').setAttribute("onblur", "document.getElementById('btnNewOrganization').focus()"); //ZD 100420
</script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
