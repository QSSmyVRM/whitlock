﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.en_ViewUserDetails" %>
<script language="javascript" type="text/javascript">
    function ClosePopUp()
    {   
        parent.document.getElementById("viewHostDetails").style.display = 'none';
        return false;
    }
</script>

<table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">
	<tr>
		<td align="center">
            <table cellpadding="2" cellspacing="1" style="border-color:Black;border-width:1px;border-style:Solid; background-color:#E1E1E1;"  class="tableBody" align="center" width="50%"> <%--ZD 100426--%>
              <tr>
                <td class="subtitleblueblodtext" align="center" colspan="3">
                    Detalles del usuario<br />
                </td>            
              </tr>
              <tr><%--FB 2579 Start--%>
               <td align="left" style="width:40%" class="blackblodtext">Nombre </td> 
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">
                   <asp:Label ID="lblUsrName" runat="server" Text="N/D"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="left" class="blackblodtext">ID de Correo-e </td> 
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">
                   <asp:Label ID="lblUsrEmail" runat="server"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="left" class="blackblodtext">Inicio de sesión AD/LDAP </td> 
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">
                   <asp:Label ID="lblUsrLogin" runat="server" Text="N/D"></asp:Label>
               </td>
              </tr>
              <% if(Session["timezoneDisplay"].ToString() == "1") { %> 
              <tr>
               <td align="left" class="blackblodtext">Zona horaria </td> 
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">
                   <asp:Label ID="lblUsrTimeZone" runat="server"></asp:Label>
               </td>
              </tr>
              <% } else {%>
              <tr>
               <td align="left" class="blackblodtext">Mostrar Zona horaria </td>
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">Desactivado</td>
              </tr>
              <% }%>
              <tr>
               <td align="left" class="blackblodtext">Idioma preferido </td> 
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">
                   <asp:Label ID="lblUsrLang" runat="server"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="left" class="blackblodtext">Idioma de Correo-e </td> 
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">
                   <asp:Label ID="lblUsrEmailLang" runat="server" Text="N/D"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="left" class="blackblodtext">Bloquear Correos Electrónicos  </td> 
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">
                   <asp:Label ID="lblUsrBlockedEmail" runat="server"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="left" class="blackblodtext">trabajo </td> 
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">
                   <asp:Label ID="lblUsrWork" runat="server" Text="N/D"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="left" class="blackblodtext">telf. móvil </td> 
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">
                   <asp:Label ID="lblUsrCell" runat="server" Text="N/D"></asp:Label>
               </td>
              </tr><%--FB 2579 End--%>
              <tr>
               <td align="center" colspan="3"><br />
                  <button ID="BtnUsrDetailClose" class="altMedium0BlueButtonFormat"  runat="server" onclick="javascript:return ClosePopUp();">Cerrar</button>
               </td>
              </tr>
           </table>
		</td>
	</tr>
</table>
