<%--ZD 100147 start--%>
<%--/* Copyright (C) 2014 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" Inherits="ns_Tiers2.Tiers2" Buffer="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 --> <%--FB 2779--%>

<!--window Dressing-->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<script runat="server">

</script>
<script language="javascript">
    //ZD 100604 start
    var img = new Image();
    img.src = "../sp/image/wait1.gif";
    //ZD 100604 End
    //ZD 100176 start
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
        else
            document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
    }
    //ZD 100176 End
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Manage Tiers</title>
    <script type="text/javascript" src="inc/functions.js"></script>

</head>
<body>
    <form id="frmTierManagement" runat="server" method="post" onsubmit="return true">
    <div>
      <input type="hidden" id="helpPage" value="65">

        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblTier1Name" runat="server" Text="" ></asp:Label>
                        <asp:TextBox ID="txtTier1ID" Visible="false" Text="" runat="server"></asp:TextBox>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <div id="dataLoadingDIV" style="display:none" align="center" >
                <img border='0' src='image/wait1.gif'  alt='Cargando..' />
            </div> <%--ZD 100678 End--%>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Niveles existentes</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgTier2s" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="true" OnUpdateCommand="UpdateTier2" OnCancelCommand="CancelTier2" OnItemCreated="BindRowsDeleteMessage2"
                        OnDeleteCommand="DeleteTier2" OnEditCommand="EditTier2" Width="90%" Visible="true">
                         <%--Window Dressing - Start--%>
                        <SelectedItemStyle CssClass="tableBody" />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                        <FooterStyle CssClass="tableBody"  />
                        <%--Window Dressing - End--%>
                        <HeaderStyle CssClass="tableHeader" />
                        <Columns>
                            <asp:BoundColumn DataField="ID" Visible="false" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Nombre" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" > <%-- FB 2050 --%>
                                <ItemTemplate>
                                    <asp:Label ID="lblTier2Name" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtTier2Name" CssClass="altText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name")%>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqTier2Name1" ControlToValidate="txtTier2Name" ValidationGroup="Update" runat="server" ErrorMessage="Necesario" Display="dynamic"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regName" ValidationGroup="Update" ControlToValidate="txtTier2Name" runat="server" Display="dynamic" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ( ) ? | ^ = ! `[ ] { } # $ y ~ no son caracteres v�lidos." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=^@#$%&()~]*$"></asp:RegularExpressionValidator> <%-- fogbugz case 137 for extra junk character validation--%> <%--FB 1888--%> 
                                </EditItemTemplate>
                             </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Acciones" ItemStyle-Width="15%">
                                <HeaderStyle CssClass="tableHeader" />
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" Text="Editar" ID="btnEdit" CommandName="Edit" OnClientClick="DataLoading(1)"></asp:LinkButton> <%--ZD 100176--%> 
                                    &nbsp;<asp:LinkButton runat="server" Text="Eliminar" ID="btnDelete" CommandName="Delete" OnClientClick="DataLoading(1)"></asp:LinkButton><%--ZD 100176--%> 
                                    <asp:LinkButton runat="server" Text="Actualizar" ID="btnUpdate" ValidationGroup="Update" CommandName="Update" Visible="false" OnClientClick="DataLoading(1)"></asp:LinkButton><%--ZD 100176--%> 
                                    &nbsp;<asp:LinkButton runat="server" Text="Cancelar" ID="btnCancel" CommandName="Cancel" Visible="false" OnClientClick="DataLoading(1)"></asp:LinkButton><%--ZD 100176--%> 
                                </ItemTemplate>
                                <FooterTemplate>
                                <%--Window Dressing--%>
                                    <span class="blackblodtext"> Niveles totales:</span> <asp:Label ID="lblTotalRecords" runat="server" Text=""></asp:Label><%--FB 2579--%>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoTier2s" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError">
                                No se encontraron niveles Medios.
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>     
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                               <%-- <SPAN class=subtitleblueblodtext>Create New Tier2</SPAN>--%><%--Commented for FB 2094 --%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trNew" runat="server"> <%--FB 2670--%>
                <%--Window Dressing --%>            
                    <td align="center" class="blackblodtext">Crear Segundo Nivel Nuevo<span class="reqfldstarText">*</span><%--FB 2094--%>
                   <asp:TextBox ID="txtNewTier2Name" runat="server" Text="" CssClass="altText"></asp:TextBox>
                   <asp:Button runat="server" ID="btnCreateNewTier2" ValidationGroup="Create2" Text="Entregar" CssClass="altMedium0BlueButtonFormat" OnClick="CreateNewTier2" CommandName="Update" />
                   <asp:RequiredFieldValidator ID="reqTier2Name2" ValidationGroup="Create2" ControlToValidate="txtNewTier2Name" runat="server" ErrorMessage="Necesario" Display="dynamic"></asp:RequiredFieldValidator>
                   <%-- fogbugz case 137 for extra junk character validation--%>
                    <asp:RegularExpressionValidator ID="regName1" ValidationGroup="Create2" ControlToValidate="txtNewTier2Name" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ( ) ? | ^ = ! `[ ] { } # $ y ~ no son caracteres v�lidos." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=^@#$%&()~]*$"></asp:RegularExpressionValidator> <%--FB 1888--%> 
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Administrar Niveles Superior</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                   <asp:Button runat="server" ID="btnGoBack" Text="Regresar" CssClass="altLongBlueButtonFormat" OnClick="GoBack" OnClientClick="DataLoading(1)" /><%--ZD 100176--%> 
                </td>
            </tr>
        </table>
    </div>

<img src="keepalive.asp" alt="Sobrevivir" name="myPic" width="1px" height="1px" style="display:none"> <%--ZD 100419--%>
    </form>
<%--code added for Soft Edge button--%>
<%--ZD 100420 Start--%>
<script language="javascript">
    document.onkeydown = function(evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 8) {
            if (document.getElementById("btnCancel") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnCancel").click();
                    return false;
                }
            }
            if (document.getElementById("btnGoBack") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnGoBack").click();
                    return false;
                }
            }
        }
        fnOnKeyDown(evt);
    };

    if (document.getElementById('txtNewTier2Name') != null)
        document.getElementById('txtNewTier2Name').setAttribute("onblur", "document.getElementById('btnCreateNewTier2').focus(); document.getElementById('btnCreateNewTier2').setAttribute('onfocus', '');");
    if (document.getElementById('btnCreateNewTier2') != null)
        document.getElementById('btnCreateNewTier2').setAttribute("onblur", "document.getElementById('btnGoBack').focus(); document.getElementById('btnGoBack').setAttribute('onfocus', '');");               
</script>
<%--ZD 100420 End--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>

