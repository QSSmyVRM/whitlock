<%--ZD 100147 start--%>
<%--/* Copyright (C) 2014 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" Inherits="ns_EndpointList.EndpointList" %><%--ZD 100170--%>
<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2779 Ends--%>
<head id="Head1" runat="server">

<script language="javascript">
//ZD 100604 start
var img = new Image();
img.src = "../sp/image/wait1.gif";
//ZD 100604 End
function ViewBridgeDetails(bid)
{
    url = "BridgeDetailsViewOnly.aspx?hf=1&bid=" + bid;
    window.open(url, "BrdigeDetails", "width=900,height=800,resizable=yes,scrollbars=yes,status=no");
    return false;
}
function CheckSelection(obj)
{
//      alert(obj.type);
      if (obj.tagName == "INPUT" && obj.type == "radio")
            for (i=0; i<document.frmEndpoints.elements.length;i++)
            {
                var obj1 = document.frmEndpoints.elements[i];
                if (obj1.id != obj.id)
                    obj1.checked = false;
            }
}
//ZD 100176 start
function DataLoading(val) {    
    if (val == "1")
        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
    else
        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
}
//ZD 100176 End
</script>
    <title>Administrar Puntos Finales</title>
</head>
<div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display:none">
    <img border='0' src='image/wait1.gif' alt='Cargando..' />
</div><%--ZD 100678--%>
<body>
    <form id="frmEndpoints" runat="server" method="post">
        <center><table border="0" width="98%" cellpadding="2" cellspacing="2">
            <tr>
                <td align="center">
                    <h3><asp:Label ID="lblHeader" runat="server" Text="Administrar puntos finales"></asp:Label></h3><br />
                     <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <asp:Label ID="lblSubHeader1" Text="Puntos Finales Disponibles" runat="server" CssClass="subtitleblueblodtext" ></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
             </tr>
              <%--Endpoint Search--%>
             <tr>
             <td>
              <iframe id="EndpointFrame" onfocus="setTimeout('window.scrollTo(0,0);', 1000);" runat="server" width="100%" valign="top" height="625px" scrolling="no"></iframe>
             </td>
             </tr>
            <tr style="display:none">
                <td align="center">
                <asp:DataGrid ID="dgEndpointList" runat="server" AutoGenerateColumns="False" Font-Names="Verdana" Font-Size="Small"
                 Width="95%" OnItemDataBound="LoadProfiles" OnItemCreated="BindRowsDeleteMessage" CellPadding="4" GridLines="None"
                 BorderColor="blue" BorderStyle="solid" BorderWidth="1" AllowSorting="True" OnSortCommand="SortGrid"
                 OnEditCommand="EditEndpoint" OnCancelCommand="DeleteEndpoint" ShowFooter="true">
                <%--Window Dressing - Start--%>
                <SelectedItemStyle CssClass="tableBody" Font-Bold="True" />
                <EditItemStyle CssClass="tableBody" />
                <AlternatingItemStyle CssClass="tableBody" />
                <ItemStyle CssClass="tableBody" />
                <FooterStyle CssClass="tableBody" />
                <%--Window Dressing - End--%>
                    <Columns>
                        <asp:BoundColumn DataField="ID" Visible="false" HeaderText="ID" SortExpression="ID"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Seleccionar" HeaderStyle-CssClass="tableHeader" Visible="false">
                            <ItemTemplate>
                                <asp:RadioButton ID="rdSelectEndpoint" runat="server" onclick="javascript:CheckSelection(this)" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn ItemStyle-HorizontalAlign="left" ItemStyle-CssClass="tableBody" HeaderStyle-HorizontalAlign="left" HeaderStyle-CssClass="tableHeader" HeaderText="Punto final <br> Nombre" SortExpression="EndpointName">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblEPName" Text='<%#DataBinder.Eval(Container, "DataItem.EndpointName") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <br /><asp:RadioButton onclick="javascript:CheckSelection(this)" Visible="true" Checked="true" ID="rdNewEndpoint" runat="server" Text="Crear un Punto Final nuevo" />
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="DefaultProfileName" ItemStyle-CssClass="tableBody"  ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" SortExpression="DefaultProfileName" HeaderText="Perfil<br>por defecto">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="TotalProfiles" ItemStyle-CssClass="tableBody"  ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" SortExpression="TotalProfiles" HeaderText="Perfiles<br>totales">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="VideoEquipment" ItemStyle-CssClass="tableBody"  ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" SortExpression="VideoEquipment" HeaderText="Modelo de<br>Punto Final">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                         <asp:BoundColumn DataField="DefaultProtocol" ItemStyle-CssClass="tableBody"  ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" SortExpression="DefaultProtocol" HeaderText="Protocolo de<br>V�deo">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="AddressType"  ItemStyle-CssClass="tableBody" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" SortExpression="AddressType" HeaderText="Tipo de<br>Direcci�n MCU">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Address"  ItemStyle-CssClass="tableBody" SortExpression="Address" HeaderText="Direcci�n" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="IsOutside"  ItemStyle-CssClass="tableBody" SortExpression="IsOutside" HeaderText="Red<br>exterior" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ConnectionType"  ItemStyle-CssClass="tableBody"  SortExpression="ConnectionType" HeaderText="Opciones de<br>Marcaci�n" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="LineRate"  ItemStyle-CssClass="tableBody" SortExpression="LineRate" HeaderText="Ancho de banda" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="MCU<br>asignado"  ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" > 
                        <ItemTemplate>
                            <asp:Label ID="lblBridgeID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Bridge")%>' Visible="false"></asp:Label>
                            <asp:Label ID="lblBridgeName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BridgeName")%>'></asp:Label>
                            <br /><asp:LinkButton Text="Ver detalles" runat="server" ID="btnViewBridgeDetails" Visible='<%# !DataBinder.Eval(Container, "DataItem.BridgeName").ToString().Equals("") %>'></asp:LinkButton>
                        </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Perfiles" Visible="false"  ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:DropDownList CssClass="altLong4SelectFormat" runat="server" ID="lstProfiles" DataTextField="ProfileName" DataValueField="ProfileID" Visible='<%#Request.QueryString["t"].ToUpper().Equals("TC") %>'></asp:DropDownList><%-- SelectedValue='<%#DataBinder.Eval(Container, "DataItem.DefaultProfileID") %>' --%>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Button ID="btnSubmitNew" runat="server" CssClass="altLongBlueButtonFormat" Visible="true" Text="Entregar" OnClick="CreateNewEndpoint" />
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="false" ItemStyle-CssClass="tableBody" >
                            <ItemTemplate>
                                <asp:TextBox ID="txtProfilesXML" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProfilesXML") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="ProfilesXML" Visible="false" ItemStyle-CssClass="tableBody" ></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="acciones" ItemStyle-CssClass="tableBody" >
                            <HeaderStyle CssClass="tableHeader" />
                            <ItemTemplate>
                                <asp:LinkButton Text="Editar" runat="server" ID="btnEdit" CommandName="Edit" OnClientClick="DataLoading(1)"></asp:LinkButton>&nbsp;&nbsp;<%--ZD 100176--%>
                                <asp:LinkButton Text="Eliminar" runat="server" ID="btnDelete" CommandName="Cancel" Visible="true" OnClientClick="DataLoading(1)"></asp:LinkButton>&nbsp;&nbsp;<%--ZD 100176--%>
                            </ItemTemplate>
                            <FooterTemplate>
                                <b><span class="blackblodtext"> puntos finales totales: </span> <asp:Label ID="lblTotalRecords" runat="server" Text=""></asp:Label> </b>
                                <br>
				     <%--Added for License Modification START--%>                                
				    <b><span class="blackblodtext">Licencias&nbsp; restantes&nbsp;</span><asp:Label ID="lblRemaining" runat="server" Text=""></asp:Label> </b>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                    <asp:Label ID="lblNoEndpoints" runat="server" Text="No se encontraron puntos finales." Visible="False" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr style="display:none">
                <td>
                    <asp:Table ID="tblPage" Visible="false" runat="server">
                        <asp:TableRow ID="TableRow1" runat="server">
                            <asp:TableCell ID="TableCell1" Font-Bold="True" Font-Names="Verdana" Font-Size="Small" ForeColor="Blue" runat="server">p�ginas: </asp:TableCell>
                            <asp:TableCell ID="TableCell2" runat="server"></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>

                </td>
            </tr>
            <tr style="display:none">
                <td align="Left">
                    <table>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Buscar Puntos finales</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
             </tr>
             <tr style="display:none">
                <td align="Left">
                    <table width="95%">
                        <tr>
                            <%--Window Dressing - Start --%>
                            <td align=right class="blackblodtext"><b>Nombre de Punto final</b></td>
                            <td>
                                <asp:TextBox ID="txtEndpointName" runat="server" CssClass="altText" Text="" ValidationGroup="Search" ></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtEndpointName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ y &#34; no son caracteres v�lidos." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:@^#$%&()'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td align=right class="blackblodtext"><b>Tipo de Punto final</b></td>
                            <td>
                                <asp:DropDownList ID="lstAddressType" runat="server" CssClass="altText" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                                <asp:DropDownList ID="lstBridges" Visible="false" runat="server" CssClass="altText" DataTextField="BridgeName" DataValueField="BridgeID"></asp:DropDownList>
                                <asp:DropDownList ID="lstLineRate" Visible="false" runat="server" CssClass="altText" DataTextField="LineRateName" DataValueField="LineRateID"></asp:DropDownList>
                                <asp:DropDownList ID="lstVideoEquipment" Visible="false" runat="server" CssClass="altText" DataTextField="VideoEquipmentName" DataValueField="VideoEquipmentID"></asp:DropDownList>
                                <asp:DropDownList ID="lstVideoProtocol" Visible="false" runat="server" CssClass="altText" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                                <asp:DropDownList ID="lstConnectionType" runat="server" DataTextField="Name" DataValueField="ID" Visible="false"></asp:DropDownList>                                 <asp:DropDownList ID="DropDownList1" runat="server" DataTextField="Name" DataValueField="ID" Visible="false"></asp:DropDownList> <%--Fogbugz case 427--%>
                            </td>
                            <%--Window Dressing - End --%>
                        </tr>
                        <tr>
                            <td colspan="4" align="right">
                                <asp:Button runat="server" CssClass="altLongBlueButtonFormat" Text="Someter" ValidationGroup="Search" OnClick="SearchEndpoint" />
                            </td>
                        </tr>
                    </table>
                </td>
             </tr>
            <tr>
                <td align="Left">
                    <table>
                        <tr id="trNew" runat="server">
                            <td>&nbsp;</td>
                            <td>
                                <%--<SPAN class=subtitleblueblodtext>Create New Endpoint <asp:Label ID="lblAddSelection" Text=" or Choose Selected" Visible="false" runat="server" ></asp:Label></SPAN>--%><%--Commented for FB 2094--%> 
                                <SPAN class=subtitleblueblodtext><asp:Label ID="Label1" Text=" o Elegir Seleccionados" Visible="false" runat="server" ></asp:Label></SPAN><%--FB 2094--%> 
                            </td>
                        </tr>
                    </table>
                </td>
             </tr>
            <tr>
                <td align="Left">
                    <table width="95%">
                        <tr>
                            <td align="right">
                              <button ID="btnSubmit" runat="server" style="width:150pt;" onserverclick="CreateNewEndpoint">Crear Punto Final Nuevo</button><%--FB 2094--%> <%-- FB 2796--%>
                            </td>
                        </tr>

                    </table>
                </td>
             </tr>
        </table>
            <asp:TextBox Visible="false" ID="txtEndpointID" runat="server"></asp:TextBox>
        </center>
    </form>
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->