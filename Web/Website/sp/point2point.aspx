<%--ZD 100147 start--%>
<%--/* Copyright (C) 2014 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_point2point" Buffer="true"
    EnableEventValidation="false" %><%--ZD 100170--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Cache-control" content="no-cache">
    <title>Punto a Punto</title>

    <script type="text/javascript">
        //ZD 100604 start
        var img = new Image();
        img.src = "../sp/image/wait1.gif";
        //ZD 100604 End
       function showNestedGridView(obj) {
           var nestedGridView = document.getElementById(obj);
           var imageID = document.getElementById('img' + obj);
           
           var gridState = document.getElementById("hdnGridState"); 

           if (nestedGridView.style.display == "none") {
               gridState.value += nestedGridView.id + ","; 
               nestedGridView.style.display = "inline";
               imageID.src = "image/loc/nolines_minus.gif";
           } else {
               gridState.value = gridState.value.replace(nestedGridView.id + ",",""); 
               nestedGridView.style.display = "none";
               imageID.src = "image/loc/nolines_plus.gif";
           }
       }
       
       
       function showNestedGridView2(obj) {
           var nestedGridView = document.getElementById(obj);
           var imageID = document.getElementById('img' + obj);
           
           var gridState = document.getElementById("hdnGridState"); 
           

           if (nestedGridView.style.display == "none") {
               gridState.value += nestedGridView.id + ","; 
               nestedGridView.style.display = "inline";
               imageID.src = "image/loc/nolines_minus.gif";
           } else {
               gridState.value = gridState.value.replace(nestedGridView.id + ",",""); 
               nestedGridView.style.display = "none";
               imageID.src = "image/loc/nolines_plus.gif";
           }
           
           //alert(gridState.value);           
           

       }
       
       function goToCal()
       {
       if(document.getElementById("lstCalendar") != null)
       {
		           if (document.getElementById("lstCalendar").value == "4"){
                           window.location.href = "MonitorMCU.aspx";
                   } 
		           if (document.getElementById("lstCalendar").value == "5"){
                   window.location.href = "point2point.aspx";
                   }
               }
           }
           function DataLoading(val) {
               //alert(val);
               if (val == "1")
                   document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
               else
                   document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
           } 
    </script>

    <style type="text/css">
        .hidden
        {
            display: none;
        }
    </style>
    <link href="css/MonitorMCU.css" type="text/css" rel="stylesheet" />
</head>

<script type="text/javascript" src="script/CallMonitorJquery/jquery.1.4.2.js"></script>
<script type="text/javascript" src="script/CallMonitorJquery/jquery.bpopup-0.7.0.min.js"></script>
<script type="text/javascript" src="script/CallMonitorJquery/Point2Point.js" ></script>
<script type="text/javascript" src="script/CallMonitorJquery/json2.js"></script>
 <script type="text/javascript" src="script/RoomSearch.js"></script>
<body id="frmP2PMonitor"><%--ZD 100980--%>
    <div id="successbox" style="text-align: center; font-family: Verdana;" class="lblMessage"></div><br />
    <div id="errormsgbox" style="text-align: center; font-family: Verdana; font-weight: bold; color: Red;"></div><br />
    <input name="selectedloc" type="hidden" id="selectedloc" runat="server" />
   <input name="locstrname" type="hidden" id="locstrname" runat="server" />
    <form id="form1" runat="server">
    <input type="hidden" id="hdnGridState" runat="server" />
    <input type="hidden" id="communStatus" value="0" />    
    <input type="hidden" id="hdnmsg" value="" />
    <input type="hidden" id="CallmonitorPageID" value="" />    
    <input type="hidden" id="hdnconforgID" value="" /> <%--FB 2646--%>
    
    <div>
    <%--FB 2984 STARTS--%>
    <table align="center">
        <tr>
            <td>
                <table align="right" border="0" width="300"> <%--FB 2646 Starts--%>
                <tr>        
                    <td align="right">
                        <h3>Monitorizar las llamadas Punto a Punto</h3>
                    </td>
                </tr>
                </table>
            </td>
            <td>
                <table width="230">
                <tr>
                    <td align="left" style="width:200">
                        <select id="lstCalendar" name="lstCalendar" class="altText"  size="1" onchange="goToCal();javascript:DataLoading('1');" runat="server"> <%--FB 2058--%>
                        <option value="5">Llamar Monitor (P2P)</option>
                        <option value="4">Llamar Monitor</option>
          		        </select> 
                    </td>
                </tr>
                </table>
             </td>
             <td>
                <table>
                    <tr>
                        <td  style="width:100" nowrap="nowrap"> 
                            <asp:Label ID="lblchksilo" Text="Mostrar todo el silos" runat="server" CssClass="blackblodtext"/>
                        </td><%--FB 2843--%>
                        <td style="width:50">
                            <asp:CheckBox ID="chkAllSilo" runat="server" AutoPostBack="true"/> 
                        </td><%--FB 2843 --%>
                    </tr>
                </table>
            </td>
        </tr>
        </table>
        <div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display:none">
              <img border='0' src='image/wait1.gif' alt='Cargando..' />
         </div><%--ZD 100678--%>    
        <%--FB 2984 ENDS--%>
        <%--FB 2646 Ends--%>
        <br />
        <center>
        <asp:GridView ID="grdParent" runat="server" AutoGenerateColumns="false" Width="95%" HeaderStyle-BackColor="#330000" HeaderStyle-ForeColor="white"  GridLines="None" ShowHeader="false" OnRowDataBound="bindParticipant">
            <EmptyDataTemplate>
                <font style="color: Black;"><span class="lblError">No se encontraron conferencias..</span></font></EmptyDataTemplate> 
                <Columns>
                <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#000000">
                    <ItemTemplate>
                        <a href="javascript:showNestedGridView2('divid-<%# Eval("xconfName") %>');">
                            <img id="imgdivid-<%# Eval("xconfName") %>" alt="'Clic' para mostrar/ocultar detalles" border="0" src="image/loc/nolines_plus.gif" />
                        </a>
                        <asp:Image ID="Image2" runat="server" ImageUrl="~/image/MonitorMCU/confIcon.gif" AlternateText="Conf Icon" Width="25px" /><%--ZD 100419--%>
                        
                        
                        <asp:Label ID="OngoingConfUniqueID" runat="server" Visible="false" Text='<%# Bind("mcuAlias") %>'></asp:Label>
                        <asp:Label ID="OngoingconfID" runat="server" Visible="false" Text='<%# Bind("mcuAlias") %>'></asp:Label>
                        <input type="hidden" id="hdntime<%# Container.DataItemIndex +1 %>" value="<%# Eval("ConfFinishingTime")%>" /> 
                         <input type="hidden" id="confTotalCount" value="<%# Eval("confCount") %>" />
                    </ItemTemplate>
                </asp:TemplateField>                               
                <asp:BoundField DataField="confName" HeaderText="NombreConf" ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF"></asp:BoundField>                
                <%--<asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="3%"> <%--FB 2646 Starts--%>
                     <%-- <ItemTemplate>
                        <span>Silo: </span>
                    </ItemTemplate>
                </asp:TemplateField>--%> 
                <asp:BoundField DataField="siloName" HeaderText="Silo" HeaderStyle-HorizontalAlign="Left" ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="20%" ></asp:BoundField> <%--FB 2646 Ends--%>

                <asp:BoundField DataField="confDate" HeaderText="FechaConf" ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF"></asp:BoundField>
                <%--<asp:BoundField DataField="duration" HeaderText="duration" ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF"></asp:BoundField>--%> 
                <asp:TemplateField ItemStyle-BackColor="#3075AE" >
                    <ItemTemplate>
                        <label style="color:blue"  id="coundowntime<%# Container.DataItemIndex +1 %>" visible="true" value="0" ></label> <%--ZD 100670--%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#000000"> <ItemTemplate>
                
                        <input type="hidden" id="userID" value="<%=Session["UserID"]%>" />                        
                        <input type="hidden" id="conID<%# Eval("xparentId") %>" value="<%# Eval("confId") %>" />
                        <input type="hidden" id="conActualStatus<%# Eval("xparentId") %>" value="<%# Eval("confActualStatus") %>" />
                        <input type="hidden" id="conUniqID<%# Eval("xparentId") %>" value="<%# Eval("mcuAlias") %>" />
                        <input type="hidden" id="conName<%# Eval("xparentId") %>" value="<%# Eval("confName") %>" />
                        <input type="hidden" id="conType<%# Eval("xparentId") %>" value="<%# Eval("confType") %>" />
                        <input type="hidden" id="conStatus<%# Eval("xparentId") %>" value="<%# Eval("confStatus") %>" />
                        <input type="hidden" id="conStartMode<%# Eval("xparentId") %>" value="<%# Eval("connectingStatus") %>" />
                        <input type="hidden" id="conLastRun<%# Eval("xparentId") %>" value="<%# Eval("confLastRun") %>" />
                        <input type="hidden" id="conforgID<%# Eval("xparentId") %>" value='<%# Eval("conforgID") %>' /><%--FB 2646--%>                                                    
                        
                        <img src="../image/MonitorMCU/p2p.gif" title="Punto a Punto" style="padding-right: 30px;" id="Av<%# Eval("xparentId") %>" alt="p2p" width="25px" />
                        <img src="../image/MonitorMCU/call_1.gif" class="classPoint2Point" style="cursor: pointer;" title="Conectar" id="call<%# Eval("xparentId") %>" alt="Llame al 1" width="25px" />
                        <img src="../image/MonitorMCU/call_0.gif" class="classPoint2Point" style="cursor: pointer;" title="Desconectar" id="call<%# Eval("xparentId") %>" alt="" width="25px" />
                        <img src="../image/MonitorMCU/changeHost_1.gif" class="changeHost" style="cursor: pointer;" title="Cambiar Anfitri�n"  name="<%# Eval("confId") %>" id="addUser<%# Eval("xparentId") %>" alt="Cambiar Anfitri�n" width="25px" /><%--FB 2646--%>
                        <img src="../image/MonitorMCU/message_1.gif" title="Enviar mensaje" style="cursor: pointer;" alt="Mensaje"  class="classPoint2Point" id="message<%# Eval("xparentId") %>" width="25px" />                        
                        <img src="../image/MonitorMCU/time_1.gif" title="Extender la hora" class="classPoint2Point" style="cursor: pointer;" id="time<%# Eval("xparentId") %>" alt="Extender la hora" width="25px" />
                        <img src="../image/MonitorMCU/bandWidth_1.gif" title="Ancho de banda" class="classPoint2Point" style="cursor: pointer;" id="bandWidth<%# Eval("xparentId") %>" alt="Ancho de banda" width="25px" />
                        <img src="../image/MonitorMCU/eventLog_1.gif" title="Registro de Eventos" class="EventLog" style="cursor: pointer;" id="eventLog<%# Eval("xparentId") %>" alt="Registro de Eventos" width="25px" />
                        <img src="../image/MonitorMCU/graphView_1.gif" title="Vista gr�fica" class="classPoint2Point" style="cursor: pointer;" id="graphView<%# Eval("xparentId") %>" alt="Vista gr�fica" width="25px" />
                        <img src="../image/MonitorMCU/delete_1.gif" title="Finalizar" class="classPoint2Point" style="cursor: pointer;" id="delete<%# Eval("xparentId") %>" alt="Finalizar" width="25px" />
                        
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#000000">
                    <ItemTemplate>
                        <tr>
                            <td colspan="100%" id="Grand_<%# Container.DataItemIndex +1 %>_child_<%# Container.DataItemIndex +1 %>">
                                <div id="divid-<%# Eval("xconfName") %>" style="display: none; position: relative;">
                                    <table width="100%" style="border-collapse: collapse">
                                        <tr>
                                            <td>
                                                <asp:PlaceHolder ID="plhp2pgrdChild1" runat="server"></asp:PlaceHolder> <%-- ZD 101068 --%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="p2pgrdChild2" runat="server"  AutoGenerateColumns="false" Width="95%" HeaderStyle-BackColor="#003333" HeaderStyle-ForeColor="white" HorizontalAlign="Right" GridLines="None" ShowHeader="false">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-BackColor="#AED2F2" ItemStyle-Width="100px" ItemStyle-ForeColor="#000000"> <%--FB 3013--%>
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="participant" ToolTip="Participante" runat="server" ImageUrl="~/image/MonitorMCU/participant.gif"  AlternateText="Participante"
                                                                    Width="25px" OnClientClick="javascript:return false;" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="participantName" ItemStyle-Width="250px" HeaderText="Nombre" ItemStyle-BackColor="#AED2F2" ItemStyle-ForeColor="#000000"></asp:BoundField><%--FB 3013--%>

                                                        <%--<asp:BoundField DataField="partAddr" HeaderText="partAddr" ItemStyle-BackColor="#AED2F2" ItemStyle-ForeColor="#000000"></asp:BoundField>--%>
                                                        <asp:TemplateField ItemStyle-BackColor="#AED2F2" ItemStyle-ForeColor="#000000" ItemStyle-VerticalAlign="Middle" > <ItemTemplate>
                                                                
                                                                <input type="hidden" id="callerOrCalle<%# Eval("xchildId") %><%# Container.DataItemIndex %>" value="<%# Eval("CallType") %>" />
                                                                <input type="hidden" id="partEndpointID<%# Eval("xchildId") %><%# Container.DataItemIndex %>" value="<%# Eval("conPartyId") %>" />
                                                                <input type="hidden" id="partTerminalType<%# Eval("xchildId") %><%# Container.DataItemIndex %>" value="<%# Eval("conTerminalType") %>" />
                                                                <input type="hidden" id="partTerminalTypeinfo<%# Eval("xchildId") %><%# Container.DataItemIndex %>" value="<%# Eval("partyCallStatusinfo") %>" />
                                                                <input type="hidden" id="confID<%# Eval("xchildId") %><%# Container.DataItemIndex %>" value="<%# Eval("confid") %>" />
                                                                
                                                                <input type="hidden" id="hdnCallStatus<%# Eval("xchildId") %>" value="<%# Eval("partyCallStatus") %>" />
                                                                
                                                                
                                                                                                                                
                                                                <%--<img src="../image/MonitorMCU/call_<%# Eval("partyCallStatus") %>.gif" style="cursor: pointer;" title="Endpoint Connect/Disconnect status" class="classPoint2Point" id="call<%# Eval("xchildId") %>" alt="" width="25px" />--%>
                                                                
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                       <asp:TemplateField ItemStyle-BackColor="#AED2F2" ItemStyle-ForeColor="#000000" ItemStyle-Width="200px" ItemStyle-VerticalAlign="Middle" ><%--FB 3013--%>
                                                       <ItemTemplate>
                                                       <a style="padding-right:50px; vertical-align:middle" href="#" onclick="javascript:fnOpenEpt(this)" id="partIp" ><%# Eval("partAddr")%></a>
                                                       </ItemTemplate>
                                                       </asp:TemplateField>
                                                       
                                                       <asp:TemplateField ItemStyle-BackColor="#AED2F2" ItemStyle-ForeColor="#000000" ItemStyle-VerticalAlign="Middle" >
                                                       <ItemTemplate>
                                                       <%# Eval("CallType") %>
                                                       </ItemTemplate>
                                                       </asp:TemplateField>
                                                       
                                                       <asp:TemplateField ItemStyle-BackColor="#AED2F2" ItemStyle-ForeColor="#000000" ItemStyle-VerticalAlign="Middle" >
                                                       <ItemTemplate>
                                                                <img src="../image/MonitorMCU/TerminalStaus_<%# Eval("partyCallStatusinfo") %>.gif" style="padding-right: 30px;" title="Inalcanzable" class="classPoint2Point" alt="Estado Terminal" width="25px" />
                                                       </ItemTemplate>
                                                       </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:TemplateField>
                
            </Columns>
        </asp:GridView>
        </center>
    </div>
      
    <%--Bandwidth Information Popup Window--%>
    <div id="BandWidth" title="MyVRM" style="left: 425px; position: absolute; background-color: White;
        top: 420px; z-index: 9999; width: 350px; height: 150px; display: none;"><%--ZD 100671--%>
        <table width="100%" border='0' cellpadding='0' cellspacing='0' id='tblBanwidth'>
            <tr style='height: 25px;'>
                <td style='background-color: #3075AE;' colspan='2' align='center'>
                    <b style='font-size: small; margin-left: 10px; color: White; font-family: Verdana;
                        font-style: normal;'>Ancho de banda</b>
                </td>
            </tr>
            <tr style='height: 40px;' align="left">
                <td style='padding-left: 10px; width: 40%;'>
                    Velocidad m�xima de l�nea<b>: </b>
                </td>
                <td align='left'>
                    <asp:DropDownList CssClass="altSelectFormat" ID="lstLineRate" runat="server" DataTextField="LineRateName" DataValueField="LineRateID"></asp:DropDownList>
                    <br /> <span id="msglinerate" style="color:Red; font-family:Verdana;"></span>
                </td>
            </tr>            
            <tr style="height: 40px;">
                <td align="center" colspan="2">
                    <button id="Cancelbandwidth" class="altMedium0BlueButtonFormat">Cancelar</button>
                <button id="Submitbandwidth" class="altMedium0BlueButtonFormat">Someter</button>
                    
                    
                </td>
            </tr>
        </table>
    </div>
    <div style="display: none">
        <asp:Button ID="btnRefreshPage" runat="server" /></div>
    </form>
    <input type="hidden" id="msgPopupIdentificationID" />
    <input type="hidden" id="AddUserWindowRedirect" value="" />
    <br />
    <br />
    <br />
    <br />
    <br />
    <%--Message and Duration Popup window--%>
    <div id="popmsg" title="MyVRM" style="left: 425px; position: absolute; background-color: White;
        top: 420px; z-index: 9999; width: 200px; height: 320px; display: none;"><%--ZD 100671--%>
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="height: 200px;"
            id="tblpopup">
            <tr style="height: 25px;" id="smsg">
                <td style="background-color: #3075AE;" colspan="2">
                    <b style="font-size: small; margin-left: 10px; color: White; font-family: Verdana;
                        font-style: normal;">Enviar mensaje</b>
                </td>
            </tr>
            <tr style="height: 25px;" id="eTime">
                <td style="background-color: #3075AE;" colspan="2">
                    <b style="font-size: small; margin-left: 10px; color: White; font-family: Verdana;
                        font-style: normal;">Extender la hora</b>
                </td>
            </tr>
            <tr class="pMsg">
                <td style="padding-left: 10px;">
                    <label>
                        Comentarios:</label>
                </td>
                <td align="left">
                    <span id="username_warning" class="fontdisplay"></span>
                    <br />
                    <textarea id="popuptxt" class="altText" rows="5" cols="10"></textarea>
                </td>
            </tr>   
            <tr class="pMsgDuration" >
                <td style="padding-left: 10px;">
                    <label>Duraci�n:</label>
                </td>
                <td align="left">                
                    <input type="text" id="msgMinutes" class="altText" style="width:100px;"/>                    
                    <label>(mins)</label>                    
                </td>
            </tr>         
            <tr style="height: 40px;">
                <td align="center" colspan="2">
                    <div id="popupstatus" style="display: none;">
                    </div>
                    <button id="btnpopupcancel" class="altMedium0BlueButtonFormat">
                        Cancelar</button>
                    &nbsp;&nbsp;
                    <button id="btnpopupSubmit" class="altMedium0BlueButtonFormat">
                        Someter</button>
                    
                </td>
            </tr>
            <tr>
                <td height="20px;">
                </td>
            </tr>
        </table>
    </div>
    
    <%-- GraphicalView --%>
    <div id="GraphicalView" class="rounded-corners" style="left: 425px; position: absolute;
        background-color: White; top: 420px; z-index: 9999; height: 450px; overflow: hidden;
        border: 0px; width: 700px; display: none;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="gView">
            <tr style='height: 25px;'>
                <td style='background-color: #3075AE;' align='center'>
                    <b style='font-size: small; margin-left: 10px; color: White; font-family: Verdana;
                        font-style: normal;'>Vista gr�fica</b>
                </td>
             </tr>
            <tr>
                <td align="center">
                    <img src="" style="height:350px; width:500px;" id="GraphicalImgID"  alt=imagen gr�fica" /><%--ZD 100419--%>
				</td>
            </tr>
            <tr> <%--ZD 100429--%>
                <td align="center" class="blackblodtext">
                    <asp:Label ID="GraphicalTxtID" runat="server"></asp:Label>
                </td>
            </tr>
            <tr style="height: 40px;">
                <td align="center" colspan="2">
                    <button id="btnGrpview" class="altMedium0BlueButtonFormat">
                        Cerrar</button>
                </td>
            </tr>
        </table>        
    </div>
    
    <%--Event log--%>
    <div id="diveventlog" class="rounded-corners" style="left: 425px; position: absolute;
        background-color: White; top: 420px; min-height:200px; z-index: 9999; overflow: hidden;
        border: 0px; width: 700px; display: none;">
        <table align="center" border="0" cellpadding="0"  cellspacing="0" width="100%" id="Table1">
            <tr style='height: 25px;'>
                <td colspan="3" style='background-color: #3075AE;' align='center'>
                    <b style='font-size: small; margin-left: 10px; color: White; font-family: Verdana;
                        font-style: normal;'>Registro de Eventos</b>
                </td>
             </tr>                        
             <tr>
                <td colspan="3" height="20px;"></td>
             </tr>
            <tr>
                <td colspan="3" align="center">
                    <div id="EventLogHtmlContent"></div>
                </td>
            </tr>           
            <tr style="height: 40px;">
                <td align="center" colspan="3">
                    <button id="btnCancelEventLog" class="altMedium0BlueButtonFormat">
                        Cerrar</button>
                </td>
            </tr>
        </table>        
    </div>
    
       
    <%--loader gif animation--%>
    <div id="progressdiv">
        <div id="progressdivwindow">
            <table border='0' id="proceeimgid" cellpadding='0' cellspacing='0' style="padding-left: 600px;
                display: none;">
                <tr style='height: 25px;'>
                    <td align="center">
                        <img src="" alt="" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>

<script type="text/javascript">

 function fnOpenEpt(ip)
{
  
   if(ip.innerHTML != null)
   {
       window.open("http://"+ip.innerHTML, "EndPointDetails", "width=900,height=800,resizable=yes,scrollbars=yes,status=no");
       return false;
   }
}

//function OpenRoomSearchresponse(confid) {    //FB 2646
//     var url   = "RoomSearch.aspx?rmsframe=''&confID="+ confid.toString() +"&stDate=''&enDate=''&tzone=26&serType=-1&hf=1" + "&isVMR=0&immediate=0&pageID=P2P&conforgID="+document.getElementById("hdnconforgID").value; //FB 2448 //FB 2534 //FB2646

//  window.open(url, "RoomSearch", "width="+ screen. availWidth +",height=650px,resizable=no,scrollbars=yes,status=no,top=0,left=0");
//}

</script>

<script type="text/javascript">
    function refPage() { //ZD 100980
        if (document.getElementById("communStatus") != null)            
            if(document.getElementById("communStatus").value == "0")
                document.getElementById("btnRefreshPage").click();
                setTimeout("refPage()", 5000);
    }

function fnUpdateGridState()
{
var gridState = document.getElementById("hdnGridState");
var ids = gridState.value.split(",");
var i = 0;
for(i = 0; i < ids.length-1; i++)
{
    if (document.getElementById(ids[i]) != null)
       document.getElementById(ids[i]).style.display = 'inline';
    if (document.getElementById("img" + ids[i]) != null)
       document.getElementById("img" + ids[i]).src = 'image/loc/nolines_minus.gif';
}
setTimeout("refPage()", 15000); //FB 2981
}
fnUpdateGridState();
    
</script>

<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
