<%--ZD 100147 start--%>
<%--/* Copyright (C) 2014 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100886 End--%>

<%@ Page Language="C#" Inherits="en_InstantConference.InstantConference" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script type="text/javascript">

    function getYourOwnEmailList() {
        
        var url = "emaillist2main.aspx?t=e&frm=party2NET&fn=Setup&n=";

        if (!window.winrtc) {
            winrtc = window.open(url, "", "width=920,height=470px,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else if (!winrtc.closed) {
            winrtc.close();
            winrtc = window.open(url, "", "width=920,height=470px ,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else {
            winrtc = window.open(url, "", "width=920ss,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        }
    }

    function ClosePopUp() {
        window.parent.document.getElementById('modalDiv').style.display = 'none';
        window.parent.document.getElementById('contentDiv').style.display = 'none';

    }
</script>

<script type="text/javascript">
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css";
    document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
    
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf8" />
    <title>Conferencias instantáneas</title>
</head>
<body>
    <form id="frmSettings2" runat="server" method="post">
    <input runat="server" name="hdnstatic" id="hdnstatic" type="hidden" />
    <div>
        <table width="80%" align="center">
            <tr>
                <td align="center" colspan="2">
                    <b><font class="subtitleblueblodtext">Conferencia Instantánea TrueDaybook</font></b><br />
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    <b><font class="blackblodtext">Seleccione el lugar donde desea hospedar esta reunión.</font></b>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    <b><font class="blackblodtext">Seleccione los asistentes a unirse a su conferencia.</font></b><br>
                    <br>
                </td>
            </tr>
            <tr>
                <br />
                <br />
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <table width="80%" border="0">
                        <tr>
                            <td align="center">
                                <asp:Label ID="errLabel" runat="server" CssClass="lblError" Visible="false"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="25%">
                    <br />
                    <asp:Label runat="server" ID="lbltxt" CssClass="blackblodtext" Text="Sala Virtual"></asp:Label>
                </td>
                <td width="50%" align="left">
                    <asp:DropDownList ID="lstStaticDynamic" Width="400px" runat="server" DataValueField="Id"
                        DataTextField="Name">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <img id="Img5" onclick="javascript:getYourOwnEmailList()" src="image/edit.gif" style="cursor: pointer;"
                        title="myVRM Libreta de direcciones" />
                </td>
                <td align="left">
                    <asp:TextBox ID="txtInstantConferenceParticipant" TextMode="MultiLine" runat="server"
                        CssClass="altText" Width="400px"></asp:TextBox><br>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td align="right">
                    <asp:Button ID="btnConfSubmit" runat="server" 
                        Text="Bueno" OnClick="SetConference" class="altMedium0BlueButtonFormat" />
                    <asp:Button ID="btnClose" OnClientClick="ClosePopUp();" runat="server" Text="cancelar"
                        class="altMedium0BlueButtonFormat" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

<script type="text/javascript">
    document.getElementById("txtInstantConferenceParticipant").value = ""; // ZD 100167
</script>

