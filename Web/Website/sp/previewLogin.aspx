<%--ZD 100147 start--%>
<%--/* Copyright (C) 2014 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100886 End--%>
<%@ Page language="c#" CodeFile="PreviewLogin.aspx.cs" AutoEventWireup="false" Inherits="myVRMAdmin.Web.en.PreviewLogin" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
<head>
  <title>VRM</title>
  <meta name="Description" content="VRM (Videoconferencing Resource Management) is a revolutionary Web-based software application that manages any video conferencing environment.">
  <meta name="Keywords" content="VRM, Videoconferencing Resource Management, video conferencing, video bridges, video endpoints">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055-URL--%>
   <script type="text/javascript"> // FB 2815
       var path = '<%=Session["OrgCSSPath"]%>';
       path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css";
       document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
   </script>
  <script type="text/javascript" src="script/errorList.js "></script>
  <script>
		if(self.parent.name != 'Preview')
		{
			self.location.replace("genlogin.aspx");
		}
  </script>

</head>

<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" marginheight="0" marginwidth="0" bgcolor="" background="">

<!--------------------------------- CONTENT START HERE --------------->



<script language="JavaScript" src="inc/functions.js"></script>
	
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr valign="top">
      <td> <img src="../image/company-logo/StdBanner.jpg" width="100%" height="72" alt="bandera estándar" /> <%--FB 2407--%><%--ZD 100419--%>
      </td>
    </tr>
  </table>
  
  <br>

  <br><br>
  <center>
    <h3><%if (Application["Client"].ToString().ToUpper() == "MOJ"){%>VRM - Vídeo-audiencia hecha simple !<%}else{ %>VRM - Vídeo-conferencia hecha simple !<%} %></h3><%--Edited  For FB 1428--%>
  </center>



<center>
<table width="98%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="3" height="40"></td>
  </tr>
  <tr>
    <td width="20%" valign="top">
      <table width="100%" border="0" cellspacing="4" cellpadding="0">
        <tr>
          <td height="9"></td>
        </tr>
        <tr>
          <td align="right">
            <img src="../image/company-logo/SiteLogo.jpg" alt="Logo del Sitio" /><%--FB 1928--%> <%--width="122" height="44" FB 2407--%>    
          </td>
        </tr>
      </table>
    </td>

    <td width="60%" valign="top">
      <table width="100%" cellpadding="6" border="0">
        <tr>
          <td width="100%" valign="top" align="center">
			  
			  
<!--------------------------------- CONTENT GOES HERE --------------->
            
            
            <table width="450" border="0" cellspacing="4" cellpadding="0">
			  <form name="frmGenlogin" method="POST" action="#">
			  <input type="hidden" name="cmd" value="GetHome">
			  <input type="hidden" name="start" value=0>
			  <input type="hidden" name="init" value=1>
              
              <tr>
                <td style="width: 108px;"></td>
                <td style="width: 150px;"></td>
                <td style="width: 192px;"></td>
              </tr>

              <tr>
                <td align="right">
                  <label for="UserName" class="blackblodtext">ID del Correo-e</label>
                </td>
                <td colspan="2"> 
                  <input type="text" value="Admin" name="UserName" id="UserName" size="31" class="altText" onChange="JavaScript: checkAndRefresh(0);">
                </td>
              </tr>
              <tr>
                <td height="25" align="right" class="blackblodtext"> 
                  <label for="UserPassword" class="blackblodtext">Contraseña</label>
                </td>
                <td colspan="2"> 
                  <input type="password" value="Admin" name="UserPassword" id="UserPassword" size="31" class="altText" onChange="JavaScript: checkAndRefresh(0);">
                </td>
              </tr>

              <tr>
                <td height="5" colspan="3"></td>
              </tr>

              <tr>
                <td></td>
                <td colspan="2" align="left"> 
                  <input type="checkbox" name="RememberMe" id="RememberMe" value="1"> 
                  <label for="RememberMe"><font style="font-weight: normal;" class="blackblodtext">Recordarme en este PC</font></label>
                </td>
              </tr>

              <tr>
                <td height="15" colspan="3"></td>
              </tr>

              <tr> 
                <td></td>
                <td>
                  <input type="Reset" onfocus="this.blur()" name="Reset" value="Reajustar" class="altShortBlueButtonFormat" style="width: 85px;"><%--ZD 100288_5Dec2013--%>
                </td>
                <td>
                  <input type="button" onfocus="this.blur()" name="Submit" value="Entrar" class="altShortBlueButtonFormat" style="width: 85px;">
                </td>
              </tr>

               <%if (!(Application["Client"].ToString().ToUpper() == "MOJ")){%> 
              <tr>
                <td></td>
                <td colspan="2"><a href="#">¿Olvidó su Contraseña? Haga 'clic' aquí</a></td>
              </tr>

              <tr>
                <td></td>
                <td colspan="2"><a href="#">¿Necesita una cuenta de usuario myVRM? Haga 'clic' aquí</a></td>
              </tr>

              <tr>
                <td></td>
                <td colspan="2"><a href="#"><%if (Application["Client"].ToString().ToUpper() == "MOJ"){%>Ver Audiencias públicas ? Haga 'clic' aquí<%}else{ %>¿Ver conferencias públicas? Haga 'clic' aquí<%} %></a></td>
              </tr>
              <%} %>

            </form>  
          </table>
    
            
<!-------------------------------------------------------------------->


          </td>
        </tr>
      </table>
  
    </td>
    


    <td width="250" valign="top" align="left">
      <table width="90%" border="0" cellspacing="4" cellpadding="0">
        <tr>
          <td height="8"></td>
        </tr>
        <tr>
          <td>
            <iframe name="ifrmWizard" id="helpFile" height="145" width="210" src="wizard.aspx" scrolling="no">
            </iframe>     
          </td>
        </tr>
      </table>
    </td>



    <td width="5">&nbsp;&nbsp;</td>
  </tr>
</table>
</center>


<br>
<br>
<hr width="80%">

</body>
</html>
