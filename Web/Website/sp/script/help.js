/*ZD 100147 Start*/
/* Copyright (C) 2014 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147  ZD 100866 End*/
// max 143

//user info
var HLP_T_S = "Viewing_Scheduled_Conferences.htm";
var HLP_2 = "Por favor, introduzca la contrase�a. .";
var HLP_3 = "Sus entradas de contrase�a no coinciden.";
var HLP_4 = "Por favor, introduzca un Nombre.";
var HLP_5 = "Por favor, introduzca un Apellido.";
var HLP_6 = "Por favor, escriba una direcci�n de correo electr�nico v�lida.";
var HLP_51 = "Por favor, seleccione un Usuario de la Lista de Usuarios";
var HLP_81 = "Por favor, introduzca un correo-e de empresa v�lido";
var HLP_89 = "Por favor, introduzca su nombre completo"
var HLP_90 = "Por favor, introduzca un Asunto";
var HLP_91 = "Por favor, introduzca un Comentario"
