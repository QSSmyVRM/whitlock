'/*ZD 100147 Start*/
'/* Copyright (C) 2014 myVRM - All Rights Reserved
'* You may not use, distribute and/or modify this code under the
'* terms of the myVRM license.
'*
'* You should have received a copy of the myVRM license with
'* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
'*/
'/*ZD 100147 ZD 100886 End*/
'******************************************************************************
' getOutLookEmailList
'
'	Pull all the email address out of the local Outlook address book and 
'	show in a pop up window
'******************************************************************************
Function getOutLookEmailList()
	msgbox "Por favor, aseg�rese de que Outlook est� instalado y abre correctamente." & Chr(10) & "Haga clic en Aceptar para continuar y haga clic en S� si pidiera interacci�n ActiveX." & Chr(10) & "Si no ocurre nada, por favor, compruebe el ajuste de IE o el administrador de contactos.", vbExclamation, "Outlook Advertencia"

	if instr(1,ifrmMemberlist.location,"group2member.asp") <> 0 then
		willContinue = ifrmMemberlist.bfrRefresh()
		if (willContinue) then
			ifrmMemberlist.location.reload()
			document.frmManagegroup2.lotusEmails.value = ""
			document.frmManagegroup2.outlookEmails.value = ContactsAsXML()
			'Code Modified for FB 412- Start -Outlook look up program
	        'url = "preoutlookemaillist2.asp?frm=group&page=1&wintype=pop"
	        url = "preoutlookemaillist2.aspx?frm=group&page=1&wintype=pop"			
	        'Code Modified for FB 412- End -Outlook look up program
            window.open url, "EmailList", "width=700, height=450, resizable=yes, toolbar=no, menubar=no, location=no, directories=no, scrollbars=yes"

		end if
	end if
End Function



'******************************************************************************
' getLotusEmailList
'
'	Pull all the email address out of the local Outlook address book and 
'	show in a pop up window
'******************************************************************************
Function getLotusEmailList(loginName, loginPwd, dbPath)
    'Code changed for FB412 -Start
	'if instr(1,ifrmMemberlist.location,"group2member.asp") <> 0 then
	if instr(1,ifrmMemberlist.location,"group2member.aspx") <> 0 then
	'Code changed for FB412 -End
		willContinue = ifrmMemberlist.bfrRefresh()
		if (willContinue) then
			ifrmMemberlist.location.reload()

			document.frmManagegroup2.outlookEmails.value = ""
			document.frmManagegroup2.lotusEmails.value = lnContactsAsXML(loginName, loginPwd, dbPath)
			url = "prelotusemaillist2.asp?frm=group&page=1&wintype=pop"
			window.open url, "EmailList", "width=700, height=450, resizable=yes, toolbar=no, menubar=no, location=no, directories=no, scrollbars=yes"

		end if
	end if
End Function