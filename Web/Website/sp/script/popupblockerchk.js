/*ZD 100147 Start*/
/* Copyright (C) 2014 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 ZD 100866 End*/
if (navigator.appName == "Microsoft Internet Explorer") {
	popwintester = window.open("expired_page.htm",'','status=no,width=0,height=0,left=5000,top=5000,resizable=no,scrollbars=no');
	if (popwintester) {
		popwintester.close();
	} else {
		alert("Su navegador tiene habilitado el bloqueo de pop-ups. Por favor, inhabilítelo para el Sitio Web myVMR, ya que myVMR usa los pop-ups que son necesarios para que usted introduzca la información.");
		popwintester = null;
	}
}
