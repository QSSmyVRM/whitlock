/*ZD 100147 Start*/
/* Copyright (C) 2014 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 ZD 100866 End*/
menunum = 0;
menus = new Array();
_d = document;


// FB 3055 Starts
var arr = window.location.pathname.split("aspx")[0].split("/");
var page = arr[arr.length - 1] + "aspx";
page = page.toLowerCase();
if ("emailcustomization.aspx,viewblockedmails.aspx,editblockemail.aspx,conferenceorders.aspx,calendarworkorder.aspx,inventorymanagement.aspx,editinventory.aspx,editconferenceorder.aspx,viewworkorderdetails.aspx".indexOf(page) > -1) {
    var ar = window.location.href.split("/");
    var pg = ar[ar.length - 1];
    page = pg.toLowerCase();
}
//prompt("",page);

var refPage = page;
var valid = false;
var valid2 = false;


var isExpressProfile = false;
if (document.getElementById('isExpressUser').value == "1" && document.getElementById('isExpressUserAdv').value == "0" && document.getElementById('isExpressManage').value == "0")
    isExpressProfile = true;

var isExpressUsrAdv = false;
if (document.getElementById('isExpressUserAdv').value == "1")
    isExpressUsrAdv = true;

var isExpressUsrManage = false;
if (document.getElementById('isExpressManage').value == "1" && document.getElementById('isExpressUserAdv').value == "0")
    isExpressUsrManage = true;

// Page names included in menu follows
var mainPgs = "expressconference.aspx";
if (mainPgs.indexOf(page) > -1)
    var mainPage = page;
var mainPgs = "conferencelist.aspx";
if (mainPgs.indexOf(page) > -1 && isExpressProfile == false)
    var mainPage = page;

// Pages based on session variable
var sessPages = "masterchildreport.aspx";
var advRep = document.getElementById('advRep').value;

var sessPages2 = "monitormcu.aspx,point2point.aspx";
var calMon = document.getElementById('callMon').value;

var userLevel = document.getElementById('userAdminLevel').value;

if (sessPages.indexOf(page) > -1 && advRep == "1")
    valid2 = true;
//if (sessPages2.indexOf(page) > -1 && calMon == "1" )// && (userLevel == "2" || userLevel == "3")) //FB 2996
  //  valid = true;

if ("conferencelist.aspx".indexOf(page) > -1) {
    var arr = window.location.href.split("/");
    var pag = arr[arr.length - 1];
    page = pag.toLowerCase();
}
refPage = page;

if (userLevel == "0")
    if (page.indexOf("conferencelist.aspx?t=7") > -1)
    window.location.assign("thankyou.aspx");

if (page.indexOf("conferencelist.aspx?t=1&frm=1") > -1)
    valid = true;
else if (page.indexOf("hdconferencelist.aspx") > -1)
    page = "hdconferencelist.aspx";
else if (page.indexOf("conferencelist.aspx") > -1)
    page = "conferencelist.aspx";


if (isExpressProfile || isExpressUsrManage) {
    if ("conferencelist.aspx".indexOf(page) > -1) {
        var arr = window.location.href.split("/");
        var pag = arr[arr.length - 1];
        page = pag.toLowerCase();
    }
    refPage = page;
    if (page.indexOf("conferencelist.aspx?t=3") > -1 || page.indexOf("conferencelist.aspx?m=1&t=3") > -1 || page.indexOf("conferencelist.aspx?m=1&t=1") > -1)//FB 3055  //ZD 100263
        valid = true;
}
if (isExpressUsrAdv) {
    if ("conferencelist.aspx".indexOf(page) > -1 || "manageconference.aspx".indexOf(page) > -1) { // ZD 100288
        valid = true;
        var arr = window.location.href.split("/");
        var pag = arr[arr.length - 1];
        page = pag.toLowerCase();
    }
    refPage = page;
    if (page.indexOf("conferencelist.aspx?t=7") > -1)
        window.location.assign("thankyou.aspx");
}
if ("manageuser.aspx".indexOf(page) > -1) {
    var arr = window.location.href.split("/");
    var pag = arr[arr.length - 1];
    page = pag.toLowerCase();

    if (page.indexOf("manageuser.aspx?t=1") > -1)
        page = "manageuser.aspx?t=1";
    else if (page.indexOf("manageuser.aspx?t=2") > -1)
        page = "manageuser.aspx?t=2";
    else if (page.indexOf("manageuser.aspx?t=3") > -1)
        page = "manageuser.aspx?t=3";
    else
        page = "manageuser.aspx";

    refPage = page;

}


if ("manageconference.aspx".indexOf(page) > -1) {
    var arr = window.location.href.split("/");
    var pag = arr[arr.length - 1];
    page = pag.toLowerCase();
    if (page.indexOf("manageconference.aspx?t=hf") > -1)
        valid = true;
    else
        page = "manageconference.aspx";
}


// Pages depend on menu, including 1st occurance of multi dependent pages
var pgs = "searchconferenceinputparameters.aspx,hdconferencelist.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "settingselect2.aspx"; // 1

pgs = "monitormcu.aspx,point2point.aspx";//FB 2996
if (pgs.indexOf(page) > -1)
    refPage = "monitormcu.aspx";
    
pgs = "conferencelist.aspx,roomcalendar.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "personalcalendar.aspx"; // 2

pgs = "showerror.aspx,dataimport.aspx,managedataimport.aspx,searchconferenceinputparameters.aspx,manageuserprofile.aspx,responseconference.aspx,lobbymanagement.aspx,emailcustomization.aspx?tp=u,emailcustomization.aspx?m=1&tp=u,searchbridgeconference.aspx?t=1&frm=1,conferencechangerequest.aspx,,reservationhistory.aspx"; // FB 3055 //ZD 100170//ZD 100369 //ZD 100663//ZD 100718
if (pgs.indexOf(page) > -1)
    valid = true;


pgs = "addnewendpoint.aspx,addterminalendpoint.aspx,expressconference.aspx,dashboard.aspx,manageconference.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "conferencesetup.aspx"; // 3

if (isExpressProfile) {
    if (page.indexOf("lobbymanagement.aspx") > -1 || page.indexOf("emailcustomization.aspx") > -1)
        valid = false;
}

pgs = "viewblockedmails.aspx?tp=u,viewblockedmails.aspx?m=1&tp=u";
if (pgs.indexOf(page) > -1 && userLevel != "0")
    refPage = "manageuserprofile.aspx"; // 4


pgs = "editinventory.aspx?tp=au,emailcustomization.aspx?tp=au,emailcustomization.aspx?m=1&tp=au, LobbyManagement.aspx?p=1&t=au";
if (pgs.indexOf(page) > -1 && userLevel != "3")
    refPage = "manageuser.aspx?t=1";

pgs = "managereports.aspx,userreport.aspx,utilizationreport.aspx,reportdetails.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "graphicalreport.aspx"; // 5

pgs = "confirmtemplate.aspx,managetemplate2.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "managetemplate.aspx"; // 6

pgs = "managegroup2.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "managegroup.aspx"; // 7
    
pgs = "instantconferences.aspx";//ZD 100167
if (pgs.indexOf(page) > -1)
    valid = true;

pgs = "editendpoint.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "endpointlist.aspx"; // 8

pgs = "bridgedetails.aspx,mcuresourcereport.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "managebridge.aspx"; // 10

pgs = "audioaddonbridge.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "manageaudioaddonbridges.aspx"; // 11

pgs = "manageroomprofile.aspx,managevirtualmeetingroom.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "manageroom.aspx"; // 13

pgs = "managetier2.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "managetiers.aspx"; // 14

pgs = "usermenucontroller.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "manageuserroles.aspx"; // 21

pgs = "editusertemplate.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "manageusertemplateslist.aspx"; // 22

pgs = "viewcustomattributes.aspx,uisettings.aspx,editentityoption.aspx,editholidaydetails.aspx,holidaydetails.aspx,managebatchreport.aspx,managecustomattribute.aspx,manageemaildomain.aspx,manageentitycode.aspx,managemessages.aspx,workingdays.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "organisationsettings.aspx"; // 24

pgs = "manageorganization.aspx,eseventreport.aspx,manageorganizationprofile.aspx,userhistoryreport.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "superadministrator.aspx"; // 31

if (page.indexOf("editconferenceorder.aspx") > -1 || page.indexOf("viewworkorderdetails.aspx") > -1)
    refPage = "conferencesetup.aspx"; // ZD 100263

if (page.indexOf("loglist.aspx") > -1) //ZD 100263
    refPage = "eventlog.aspx";    

// Pages including 2nd occurance of multi dependent page
var refPage2;
pgs = "viewblockedmails.aspx?tp=au,emailcustomization.aspx?tp=o,emailcustomization.aspx?m=1&tp=o,viewblockedmails.aspx?tp=o,viewblockedmails.aspx?m=1&tp=o";
if (pgs.indexOf(page) > -1)
    refPage2 = "organisationsettings.aspx";

if (page.indexOf("editblockemail.aspx") > -1) {
    pgs = "&tp=u";
    if (page.indexOf(pgs) > -1 && userLevel != "0")
        refPage2 = "manageuserprofile.aspx";
    pgs = "&tp=o";
    if (page.indexOf(pgs) > -1)
        refPage2 = "organisationsettings.aspx";
    pgs = "&tp=au";
    if (page.indexOf(pgs) > -1)
        refPage2 = "organisationsettings.aspx";
}

if (page.indexOf("conferenceorders.aspx") > -1 || page.indexOf("calendarworkorder.aspx") > -1 || page.indexOf("editconferenceorder.aspx") > -1 || page.indexOf("viewworkorderdetails.aspx") > -1) {
    pgs = "t=1";
    if (page.indexOf(pgs) > -1 || page.indexOf("cmd=2") > -1) //ZD 100237
        refPage2 = "conferenceorders.aspx?t=1";
    pgs = "t=2";
    if (page.indexOf(pgs) > -1 || page.indexOf("cmd=2") > -1) //ZD 100237
        refPage2 = "conferenceorders.aspx?t=2";
    pgs = "t=3";
    if (page.indexOf(pgs) > -1 || page.indexOf("cmd=2") > -1) //ZD 100237
        refPage2 = "conferenceorders.aspx?t=3";
}

if (page.indexOf("inventorymanagement.aspx") > -1 || page.indexOf("editinventory.aspx") > -1) {
    pgs = "t=1";
    if (page.indexOf(pgs) > -1)
        refPage2 = "inventorymanagement.aspx?t=1";
    pgs = "t=2";
    if (page.indexOf(pgs) > -1)
        refPage2 = "inventorymanagement.aspx?t=2";
    pgs = "t=3";
    if (page.indexOf(pgs) > -1)
        refPage2 = "inventorymanagement.aspx?t=3";
}



function addmenu() {
    var str = menu.toString();
    var str = str.toLowerCase();
    if (str.indexOf("graphicalreport.aspx") > -1 && valid2 == true)
        valid = true;
    if (str.indexOf(refPage) > -1)
        valid = true;
    if (str.indexOf(mainPage) > -1)
        valid = true;
    if (str.indexOf(refPage2) > -1)
        valid = true;
    menunum++;
    menus[menunum] = menu;
}

function dumpmenus() {

    if (valid == false) {
        //window.stop(); // Not supportnig for IE browsers.
        window.location.assign("thankyou.aspx");
    }

    mt = "<script language=javascript>";
    for (a = 1; a < menus.length; a++) {
        mt += " menu" + a + "=menus[" + a + "];"
    }
    mt += "</script>";
    _d.write(mt)
}
// FB 3055 Ends


if (navigator.appVersion.indexOf("MSIE") > 0) {
    effect = "Fade(duration=0.2); Alpha(style=0,opacity=88); Shadow(color='#777777', Direction=135, Strength=5)"
}
else {
    effect = "Shadow(color='#777777', Direction=135, Strength=5)"
}

effect = ""			// 2785.2

timegap = 500; followspeed = 5; followrate = 40;
suboffset_top = 10; suboffset_left = 10;
style1 = ["white", "Purple", "Purple", "#FFF7CE", "lightblue", 12, "normal", "bold", "Arial, Verdana", 4, , , "66ffff", "000099", "Purple", "white", , "ffffff", "000099"]

style1_2 = ["#041433", "#ffffff", "#046380", "#E0E0E0", "lightblue", 12, "normal", "bold", "Arial, Verdana", 4, "", "", "#66ffff", "#000099", "#041433", "", "", "", "#000099", ] // FB 2791 //ZD 100156

style2 = ["black", "#FFF7CE", "#046380", "#FFF7CE", "lightblue", 120, "normal", "bold", "Arial, Verdana", 4, "image/menuarrow.gif", , "#66ffff", "#000099", "#046380", "", , "", "000099"] // FB 2791

style2_2 = ["#5E5D5E", "#E0E0E0", "#ffffff", "#7c7c7c", "transparent", 12, "normal", "bold", "Arial, Verdana", 10, "", "", "66ffff", "000099", "#041433", "#ffffff", "", "#046380", "#000099", ] // FB 2791
style2_3 = ["#5E5D5E", "#C2C2C2", "#ffffff", "#7c7c7c", "transparent", 12, "normal", "bold", "Arial, Verdana", 10, "", "", "66ffff", "000099", "#041433", "#ffffff", "", "#046380", "#000099", ] // FB 2791

var menu1Val, menu5Val;

menu1Val = ["<div id='menuHome' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>Inicio</div>", "SettingSelect2.aspx", , "Ir a la página de inicio", 0]; // FB 2719
if (defaultConfTempJS == "0")//FB 1755
    menu5Val = ["<div id='menuConf' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>Conferencia<br>Nueva</div>", "ConferenceSetup.aspx?t=n&op=1", , "Crear una nueva conferencia", 0]; // FB 2719
else
    menu5Val = ["<div id='menuConf' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>Conferencia<br>Nueva</div>", "ConferenceSetup.aspx?t=t", , "Crear una nueva conferencia", 0]; // FB 2719

if (navigator.appVersion.indexOf("MSIE") > 0) {

    menu_0 = ["mainmenu", 5, 250, 90, , , style1, 1, "center", effect, , 1, , , , , , , , , , ]			// 24 FB 2719
    menu_0_2 = ["mainmenu", 5, 250, 90, , , style1_2, 1, "center", effect, , 1, , , , , , , , , , ]		// 24//FB 1565 FB 2719
    if (isExpressUser != null)//FB 1779 FB 2827
        if (isExpressUser == 1)
        menu_0_2 = ["mainmenu", 5, 250, 90, , , style1_2, 1, "center", effect, , 1, , , , , , , , , , ]

    menu_1 = menu1Val
    menu_2 = ["<div id='menuCal' tabindex='1' onfocus='fnOnFocus(this);' onblur='fnOnBlur(this);' ><br><br><br><br>Conferencias</div>", "show-menu=conferences", , "Conferencias", 0] // FB 2719
    menu_3 = menu5Val;
    menu_4 = ["<div id='menuSet' tabindex='1' onfocus='fnOnFocus(this);' onblur='fnOnBlur(this);' ><br><br><br><br>Configuraci\u00f3n</div>", "show-menu=settings", , "Mis ajustes", 0] // FB 2719
    menu_5 = ["<div id='menuAdmin' tabindex='1' onfocus='fnOnFocus(this);' onblur='fnOnBlur(this);' ><br><br><br><br>Administraci\u00f3n </div>", "show-menu=organization", , "organizaci\u00f3n", 0] // FB 2719
    menu_6 = ["<div id='menuSite' tabindex='1' onfocus='fnOnFocus(this);' onblur='fnOnBlur(this);' ><br><br><br><br>Sitio</div>", "show-menu=site", , "Sitio", 0] // FB 2719
    menu_7 = ["<div id='menuCall' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>Programar una<br>llamada</div>", "ExpressConference.aspx?t=n", , "Programar una llamada", 0] //FB 1779 FB 2827
    //ZD 100167 START
	//menu_8 = ["<div id='menuReservation' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>Reserva</div>", "ConferenceList.aspx?t=3", , "Ver Reservas", 0] //FB 1779 FB 2827
    if (document.getElementById('isCloud').value == '1') {
        menu_8 = ["<div id='menuInstantConference' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>llamada instantánea</div>", "javascript:ModalPopUp();", , "Conferencia instantánea", 0]
        menu_9 = ["<div id='menuReservation' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>Reserva</div>", "ConferenceList.aspx?t=3", , "Ver Reservas", 0] //FB 1779 FB 2827
    }
    else {
        menu_8 = ["<div id='menuReservation' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>Reserva</div>", "ConferenceList.aspx?t=3", , "Ver Reservas", 0] //FB 1779 FB 2827
        if (document.getElementById('isExpressUser').value == '1') { // ZD 100167
            mmm_num = 8 //if we hide mainmenu we need to change then value in mmm_num and mmm_int
            mmm_int = 3
        }
    }
    //ZD 100167 END
    menu = new Array();
    menu = (if_str == "2") ? menu.concat(menu_0_2) : menu.concat(menu_0);

    for (i = 1; i <= mmm_num; i++) {
        menu = (mmm_int & (1 << (mmm_num - i))) ? (menu.concat(eval("menu_" + i))) : menu;
    }
    addmenu();

    submenu1_0 = ["conferences ", 116, , 100, 1, , style2, , "left", effect, , , , , , , , , , , , ]
    submenu1_0_2 = ["conferences", 116, , 100, 1, , style2_2, , "left", effect, , , , , , , , , , , , ]
    submenu1_1 = ["Calendario", "personalCalendar.aspx?v=1&r=1&hf=&m=&pub=&d=&comp=&f=v", , "Personal Calendar", 0]
    submenu1_2 = ["Llamar Monitor", "MonitorMCU.aspx", , "Call Monitor", 0]

    submenu2_0 = ["Ajustes", 116, , 100, 1, , style2, , "left", effect, , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu2_0_2 = ["settings", 116, , 100, 1, , style2_2, , "left", effect, , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu2_1 = ["Preferencias", "ManageUserProfile.aspx", , "Edit preferences", 0]
    submenu2_2 = ["Informes", "GraphicalReport.aspx", , "Reports", 0] //FB 2593 //FB 2885
    submenu2_3 = ["Plantillas", "ManageTemplate.aspx", , "Manage Conference Templates", 0]
    submenu2_4 = ["Grupos", "ManageGroup.aspx", , "manage group", 0]


    submenu3_0 = ["organization", 116, , 101, 1, , style2, , "left", effect, , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu3_0_2 = ["organization", 116, , 101, 1, , style2_2, , "left", effect, , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890

    submenu3_1 = ["Equipo", "show-menu=mcu", , "Manage Hardware", 0]
    submenu3_1_ = ["Hardware", "", , , 0]
    submenu3_2 =  ["Ubicaciones", "show-menu=loc", , "Manage locations", 0]
    submenu3_2_ = ["Locations", "", , , 0]
    submenu3_3 =  ["Usuarios", "show-menu=usr", , "Manage users", 0]
    submenu3_4 =  ["Opciones", "mainadministrator.aspx", , "Edit System Settings", 0]
    submenu3_5 =  ["Ajustes ", "OrganisationSettings.aspx", , "Edit Organization Settings", 0] // FB 2719
    submenu3_6 =  ["Audiovisual", "show-menu=av", , , 0]
    submenu3_6_ = ["Audiovisual", "", , , 0] // FB 2570
    submenu3_7 =  ["Catering", "show-menu=catr", , , 0]
    submenu3_7_ = ["Catering", "", , , 0]
    submenu3_8 = ["Instalaci\u00F3n", "show-menu=hk", , , 0]
    submenu3_8_ = ["Facility", "", , , 0] // FB 2570 // FB 2593 End

    submenu4_0 = ["Site", 116, , 100, 1, , style2, , "left", effect, , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu4_0_2 = ["site", 116, , 100, 1, , style2_2, , "left", effect, , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu4_1 = ["Ajustes", "SuperAdministrator.aspx", , "Edit system settings", 0]

    submenu3_1_0 = ["mcu", , , 120, 1, "", style2, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_1_0_2 = ["mcu", , , 120, 1, "", style2_3, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_1_1 = ["puntos finales", "EndpointList.aspx?t=", , "Setup Bridge", 0]
    submenu3_1_2 = ["Diagn\u00F3sticos", "EventLog.aspx", , "View hardware problem log", 0]
    submenu3_1_3 = ["MCUs", "ManageBridge.aspx", , "Edit Bridge Management", 0]
    submenu3_1_4 = ["Puentes de Audio", "ManageAudioAddOnBridges.aspx", , "Audio Bridge Management", 0] //FB 2023
    submenu3_1_5 = ["EM7", "EM7Dashboard.aspx", , "EM7", 0] //FB 2633

    submenu3_2_0 = ["loc", , , 100, 1, "", style2, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_2_0_2 = ["loc", , , 100, 1, "", style2_3, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_2_1 = ["Salones", "manageroom.aspx?hf=&m=&pub=&d=&comp=&f=&frm=", , "Edit or set up conference room", 0]
    submenu3_2_2 = ["Niveles ", "ManageTiers.aspx", , "Manage Tiers", 0]


    submenu3_3_0 = ["usr", , , 190, 1, "", style2, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_3_0_2 = ["usr", , , 190, 1, "", style2_3, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_3_1 = ["Usuarios Activos", "ManageUser.aspx?t=1", , "Edit, search or create a new user", 0]
    submenu3_3_2 = ["Herramientas por Mayor", "allocation.aspx", , "Manage Bulk User", 0]
    submenu3_3_3 = ["Departamentos", "ManageDepartment.aspx", , "Manage Department", 0]
    submenu3_3_4 = ["Invitados", "ManageUser.aspx?t=2", , "Edit, search or create a new guest", 0]
    if (sso_int)
        submenu3_3_5 = ["Restore User", "ManageUser.aspx?t=3", , "Delete or Replaced a removed user", 0]
    else
        submenu3_3_5 = ["Usuarios inactivos", "ManageUser.aspx?t=3", , "Delete or Replaced a removed user", 0]
    submenu3_3_6 = ["Importaci\u00F3n de Directorio LDAP", "LDAPImport.aspx", , "Setup Multiple Users", 0]
    submenu3_3_7 = ["Roles", "manageuserroles.aspx", , "Edit, search or create user roles", 0]
    submenu3_3_8 = ["Plantillas", "ManageUserTemplatesList.aspx", , "Edit, search or create user templates", 0]

    submenu3_6_0 = ["av", , , 170, 1, "", style2, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_6_0_2 = ["av", , , 170, 1, "", style2_3, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_6_1 = ["Inventorios audiovisuales", "InventoryManagement.aspx?t=1", , "", 0] // FB 2570
    submenu3_6_2 = ["Ordenes de Trabajo", "ConferenceOrders.aspx?t=1", , "", 0]


    submenu3_7_0 = ["catr", , , 170, 1, "", style2, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_7_0_2 = ["catr", , , 170, 1, "", style2_3, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_7_1 = ["Men\u00FA de Catering", "InventoryManagement.aspx?t=2", , "", 0] // FB 2570
    submenu3_7_2 = ["Ordenes de Trabajo", "ConferenceOrders.aspx?t=2", , "", 0]

    submenu3_8_0 = ["hk", , , 180, 1, "", style2, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_8_0_2 = ["hk", , , 180, 1, "", style2_3, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_8_1 = ["Servicios de las instalaciones", "InventoryManagement.aspx?t=3", , "", 0] // FB 2570
    submenu3_8_2 = ["Ordenes de Trabajo", "ConferenceOrders.aspx?t=3", , "", 0]
    //FB 2885 Ends

    cur_munu_no = 1; cur_munu_str = "1_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            if (EnableCallMonitor == 0 && i == 2) continue; 
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 2; cur_munu_str = "2_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
    cur_munu_no = 3; cur_munu_str = "3_";
    if (mms_int[cur_munu_no]) {
        '<%Session["SettingsMenu"] = "1";%>'
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no] - 3; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        for (i = mms_num[cur_munu_no] - 2; i <= mms_num[cur_munu_no]; i++) {
            if (rf_int & 1)
                menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
            rf_int = rf_int / 2;
        }
        addmenu();
    }

    cur_munu_no = 4; cur_munu_str = "3_1_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            if (EnableAudioBridge == 0 && i == 4) continue; //FB 2023
            if (EnableEM7Opt == 0 && i == 5) continue; // FB 2633
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
    cur_munu_no = 5; cur_munu_str = "3_2_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
    cur_munu_no = 6; cur_munu_str = "3_3_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            if ((EnableCloudInstallation == 1 && EnableAdvancedUserOption == 1) && (UsrCrossAccess == 1 || UsrCrossAccess == 0) && i == 7) continue; //ZD 100164
                menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 7; cur_munu_str = "3_6_"; //FB 2593 //FB 2885
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 8; cur_munu_str = "3_7_"; //FB 2593 //FB 2885 
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 9; cur_munu_str = "3_8_"; //FB 2593 //FB 2885
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 10; cur_munu_str = "4_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
}
else {
    menu_0 = ["mainmenu", 5, 250, 110, , , style1, 1, "center", effect, , 1, , , , , , , , , , , ]			// 24 FB 2719
    menu_0_2 = ["mainmenu", 5, 250, 110, , , style1_2, 1, "center", effect, , 1, , , , , , , , , , , ]		// 24 // FB 2050 FB 2719
    menu_1 = menu1Val
    menu_2 = ["<div id='menuCal' tabindex='1' onfocus='fnOnFocus(this);' onblur='fnOnBlur(this);'><br><br><br><br>Conferencias</div>", "show-menu=conferences", , "Conferencias", 0] // FB 2719
    menu_3 = menu5Val;
    menu_4 = ["<div id='menuSet' tabindex='1' onfocus='fnOnFocus(this);' onblur='fnOnBlur(this);'><br><br><br><br>Configuraci\u00f3n</div>", "show-menu=settings", , "Mis ajustes", 0] // FB 2719
    menu_5 = ["<div id='menuAdmin' tabindex='1' onfocus='fnOnFocus(this);' onblur='fnOnBlur(this);' ><br><br><br><br>Administraci\u00f3n</div>", "show-menu=organization", , "organizaci\u00f3n", 0] // FB 2719
    menu_6 = ["<div id='menuSite' tabindex='1' onfocus='fnOnFocus(this);' onblur='fnOnBlur(this);' ><br><br><br><br>Sitio</div>", "show-menu=site", , "Site", 0] // FB 2719
    menu_7 = ["<div id='menuCall' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>Programar una<br>llamada</div>", "ExpressConference.aspx?t=n", , "Programar una llamada", 0] //FB 1779 FB 2827
    //ZD 100167 START
	//menu_8 = ["<div id='menuReservation' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>Reserva</div>", "ConferenceList.aspx?t=3", , "Ver Reservas", 0] //FB 1779 FB 2827
    if (document.getElementById('isCloud').value == '1') {
        menu_8 = ["<div id='menuInstantConference' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>llamada instantánea</div>", "javascript:ModalPopUp();", , "Conferencia instantánea", 0]
        menu_9 = ["<div id='menuReservation' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>Reserva</div>", "ConferenceList.aspx?t=3", , "Ver Reservas", 0] //FB 1779 FB 2827
    }
    else {
        menu_8 = ["<div id='menuReservation' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>Reserva</div>", "ConferenceList.aspx?t=3", , "Ver Reservas", 0] //FB 1779 FB 2827
        if (document.getElementById('isExpressUser').value == '1') { // ZD 100167
            mmm_num = 8 //if we hide mainmenu we need to change then value in mmm_num and mmm_int
            mmm_int = 3
        }
    }
    //ZD 100167 END

    menu = new Array();
    menu = (if_str == "2") ? menu.concat(menu_0_2) : menu.concat(menu_0);

    for (i = 1; i <= mmm_num; i++) {
        menu = (mmm_int & (1 << (mmm_num - i))) ? (menu.concat(eval("menu_" + i))) : menu;
    }
    addmenu();

    submenu1_0 = ["conferences", 116, , 100, 1, , style2, , "left", effect, , , , , , , , , , , , , ]
    submenu1_0_2 = ["conferences", 116, , 100, 1, , style2_2, , "left", effect, , , , , , , , , , , , , ]
    submenu1_1 = ["Calendario", "personalCalendar.aspx?v=1&r=1&hf=&m=&pub=&d=&comp=&f=v", , "Personal Calendar", 0]
    submenu1_2 = ["Llamar Monitor", "MonitorMCU.aspx", , "Call Monitor", 0]

    submenu2_0 = ["Ajustes", 116, , 100, 1, , style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu2_0_2 = ["settings", 116, , 100, 1, , style2_2, , "left", effect, , , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu2_1 = ["Preferencias", "ManageUserProfile.aspx", , "Edit preferences", 0]
    submenu2_2 = ["Informes", "GraphicalReport.aspx", , "Reports", 0] //FB 2593 //FB 2885
    submenu2_3 = ["Plantillas", "ManageTemplate.aspx", , "Manage Conference Templates", 0]
    submenu2_4 = ["Grupos", "ManageGroup.aspx", , "manage group", 0]


    submenu3_0 = ["organization", 116, , 101, 1, , style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu3_0_2 = ["organization", 116, , 101, 1, , style2_2, , "left", effect, , , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890

    submenu3_1 = ["Equipo", "show-menu=mcu", , "Manage Hardware", 0]
    submenu3_1_ = ["Hardware", "", , , 0]
    submenu3_2 =  ["Ubicaciones", "show-menu=loc", , "Manage locations", 0]
    submenu3_2_ = ["Locations", "", , , 0]
    submenu3_3 =  ["Usuarios", "show-menu=usr", , "Manage users", 0]
    submenu3_3_ = ["Users", "", , , 0]
    submenu3_4 =  ["Opciones", "mainadministrator.aspx", , "Edit System Settings", 0]
    submenu3_5 =  ["Ajustes ", "OrganisationSettings.aspx", , "Edit Organization Settings", 0]// FB 2719
    submenu3_6 =  ["Audiovisual", "show-menu=av", , , 0]
    submenu3_6_ = ["Audiovisual", "", , , 0] // FB 2570
    submenu3_7 =  ["Catering", "show-menu=catr", , , 0]
    submenu3_7_ = ["Catering", "", , , 0]
    submenu3_8 = ["Instalaci\u00F3n", "show-menu=hk", , , 0]
    submenu3_8_ = ["Facility", "", , , 0] // FB 2570 // FB 2593 End

    submenu4_0 = ["Site", 116, , 100, 1, , style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu4_0_2 = ["site", 116, , 100, 1, , style2_2, , "left", effect, , , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu4_1 = ["Ajustes", "SuperAdministrator.aspx", , "Edit system settings", 0]

    submenu3_1_0 = ["mcu", , , 120, 1, "", style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_1_0_2 = ["mcu", , , 120, 1, "", style2_3, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_1_1 = ["puntos finales", "EndpointList.aspx?t=", , "Setup Bridge", 0]
    submenu3_1_2 = ["Diagn\u00F3sticos", "EventLog.aspx", , "View hardware problem log", 0]
    submenu3_1_3 = ["MCUs", "ManageBridge.aspx", , "Edit Bridge Management", 0]
    submenu3_1_4 = ["Puentes de Audio", "ManageAudioAddOnBridges.aspx", , "Audio Bridge Management", 0] //FB 2023
    submenu3_1_5 = ["EM7", "EM7Dashboard.aspx", , "EM7", 0] //FB 2633

    submenu3_2_0 = ["loc", , , 100, 1, "", style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_2_0_2 = ["loc", , , 100, 1, "", style2_3, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_2_1 = ["Salones", "manageroom.aspx?hf=&m=&pub=&d=&comp=&f=&frm=", , "Edit or set up conference room", 0]
    submenu3_2_2 = ["Niveles", "ManageTiers.aspx", , "Manage Tiers", 0]


    submenu3_3_0 = ["usr", , , 190, 1, "", style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_3_0_2 = ["usr", , , 190, 1, "", style2_3, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_3_1 = ["Usuarios Activos", "ManageUser.aspx?t=1", , "Edit, search or create a new user", 0]
    submenu3_3_2 = ["Herramientas por Mayor", "allocation.aspx", , "Manage Bulk User", 0]
    submenu3_3_3 = ["Departamentos", "ManageDepartment.aspx", , "Manage Department", 0]
    submenu3_3_4 = ["Invitados", "ManageUser.aspx?t=2", , "Edit, search or create a new guest", 0]
    if (sso_int)
        submenu3_3_5 = ["Recuperar usuario", "ManageUser.aspx?t=3", , "Delete or Replaced a removed user", 0]
    else
        submenu3_3_5 = ["Usuarios inactivos", "ManageUser.aspx?t=3", , "Delete or Replaced a removed user", 0]
    submenu3_3_6 = ["Importaci\u00F3n de Directorio LDAP", "LDAPImport.aspx", , "Setup Multiple Users", 0]
    submenu3_3_7 = ["Roles", "manageuserroles.aspx", , "Edit, search or create user roles", 0]
    submenu3_3_8 = ["Plantillas", "ManageUserTemplatesList.aspx", , "Edit, search or create user templates", 0]

    submenu3_6_0 = ["av", , , 170, 1, "", style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_6_0_2 = ["av", , , 170, 1, "", style2_3, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_6_1 = ["Inventorios audiovisuales", "InventoryManagement.aspx?t=1", , "", 0] // FB 2570
    submenu3_6_2 = ["Ordenes de Trabajo", "ConferenceOrders.aspx?t=1", , "", 0]

    submenu3_7_0 = ["catr", , , 170, 1, "", style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_7_0_2 = ["catr", , , 170, 1, "", style2_3, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_7_1 = ["Men\u00FA de Catering", "InventoryManagement.aspx?t=2", , "", 0] // FB 2570
    submenu3_7_2 = ["Ordenes de Trabajo", "ConferenceOrders.aspx?t=2", , "", 0]

    submenu3_8_0 = ["hk", , , 180, 1, "", style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_8_0_2 = ["hk", , , 180, 1, "", style2_3, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_8_1 = ["Servicios de las instalaciones", "InventoryManagement.aspx?t=3", , "", 0] // FB 2570
    submenu3_8_2 = ["Ordenes de Trabajo", "ConferenceOrders.aspx?t=3", , "", 0]

    cur_munu_no = 1; cur_munu_str = "1_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            if (EnableCallMonitor == 0 && i == 2) continue; 
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 2; cur_munu_str = "2_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 3; cur_munu_str = "3_";
    if (mms_int[cur_munu_no]) {
        '<%Session["SettingsMenu"] = "1";%>'
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no] - 3; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        for (i = mms_num[cur_munu_no] - 2; i <= mms_num[cur_munu_no]; i++) {
            if (rf_int & 1)
                menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
            rf_int = rf_int / 2;
        }
        addmenu();
    }

    cur_munu_no = 4; cur_munu_str = "3_1_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            if (EnableAudioBridge == 0 && i == 4) continue; //FB 2023
            if (EnableEM7Opt == 0 && i == 5) continue; // FB 2633
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
    cur_munu_no = 5; cur_munu_str = "3_2_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
    cur_munu_no = 6; cur_munu_str = "3_3_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            if ((EnableCloudInstallation == 1 && EnableAdvancedUserOption == 1) && (UsrCrossAccess == 1 || UsrCrossAccess == 0) && i == 7) continue; //ZD 100164
                menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 7; cur_munu_str = "3_6_"; //FB 2593 //FB 2885
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 8; cur_munu_str = "3_7_"; //FB 2593 //FB 2885
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 9; cur_munu_str = "3_8_"; //FB 2593 //FB 2885
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 10; cur_munu_str = "4_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
}
dumpmenus()

function fnFireEvent(cur) {
    var code = window.event.keyCode;
    if(code == 13)
        cur.parentNode.parentNode.click();
}

function fnOnFocus(cur) {
    //if (cur.id != 'menuAdmin' && document.getElementById('menuAdmin') != null)
        //fnOnBlur(document.getElementById('menuAdmin'));
    cur.parentNode.onmouseover();
}

function fnOnBlur(cur) {
    cur.parentNode.onmouseout();
}

document.onkeydown = function(evt) {
    fnOnKeyDown(evt);
};

function fnOnKeyDown(evt) {
    startTimer(); //ZD 100732
    evt = evt || window.event;
    var keyCode = evt.keyCode;
    if (keyCode == 40) {
        var str = document.activeElement.id;
        var mainMenu = ["menuCal", "menuSet", "menuAdmin", "menuSite"];
        if (str.indexOf("menu") > -1) {
            for (var i = 0; i < mainMenu.length; i++) {
                if (str == mainMenu[i]) {
                    var calSubMenu = [["Calendario", "Llamar Monitor"],
                    ["Preferencias", "Informes", "Plantillas", "Grupos"],
                    ["Equipo", "Ubicaciones", "Usuarios", "Opciones", "Ajustes", "Audiovisual", "Catering", "Instalaci\u00F3n"],
                    ["Ajustes"]];
                    for (j = 0; j < calSubMenu[i].length; j++) {
                        var obj = document.getElementById(calSubMenu[i][j]);
                        if (obj != null) {
                            if (i == 2 && j != 3 && j != 4) {
                                obj.parentNode.focus();
                            }
                            else
                                obj.parentNode.parentNode.focus();
                            break;
                        }
                    }
                }
            }
            return false;
        }
        if (document.activeElement.id == "tdtabnav1" || document.activeElement.id == "tdtabnav2") {
            var lst = "accountset,tabnav1,tabnav2,acclogout";
            var nodecount = 1;
            if (navigator.appName.indexOf("Internet Explorer") != -1) {
                nodecount = 0;
            }

            if (lst.indexOf(document.activeElement.childNodes[nodecount].id) > -1)
                tabnavigation();
        }
    }

    if (keyCode == 39) {
        var str = document.activeElement.childNodes[1].id;
        var parentId = document.activeElement.parentNode.id;
        var subMenu = ["Equipo", "Ubicaciones", "Usuarios", "Audiovisual", "Catering", "Instalaci\u00F3n"]
        if (parentId.indexOf("menu") > -1) {
            for (var i = 0; i < subMenu.length; i++) {
                if (str == subMenu[i]) {
                    var calSubItem = [["puntos finales", "Diagn\u00F3sticos", "MCUs", "Puentes de Audio", "EM7"],
                    ["Salones", "Niveles"],
                    ["Usuarios Activos", "Herramientas por Mayor", "Departamentos", "Invitados", "Restore User", "Usuarios inactivos", "Importaci\u00F3n de Directorio LDAP", "Roles", "Plantillas"],
                    ["Inventorios audiovisuales", "Ordenes de Trabajo"],
                    ["Men\u00FA de Catering", "Ordenes de Trabajo "],
                    ["Servicios de las instalaciones", "Ordenes de Trabajo  "]];
                    for (j = 0; j < calSubItem[i].length; j++) {
                        var obj = document.getElementById(calSubItem[i][j]);
                        if (obj != null) {
                            obj.parentNode.parentNode.focus();
                            break;
                        }
                    }
                }
            }
            return false;
        }
    }
    if (keyCode == 37) {
        var lst = "aswitchorgMain";

        if (lst.indexOf(document.activeElement.id) > -1)
            tabnavigationleftarrow();
    }
    if (keyCode == 27) {
        EscClosePopup();
    }
}