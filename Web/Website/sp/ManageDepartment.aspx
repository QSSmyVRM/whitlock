<%--ZD 100147 start--%>
<%--/* Copyright (C) 2014 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" Inherits="ns_Department.Department" Buffer="true" %>
<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2779 Ends--%>


<script language="javascript">
//ZD 100604 start
var img = new Image();
img.src = "../sp/image/wait1.gif";
//ZD 100604 End
function CheckName()
{
    if (document.getElementById("<%=txtNewDepartmentName.ClientID %>").value == "")
    {
        document.getElementById("<%=lblRequired.ClientID %>").innerText = "Necesario";
        return false;
    }
    else
    {
        alert("Para agregar un usario a este departamento, edite cada perfil  de usario como sea neceseario."); // this alert has been added after discussing with Bri
        document.getElementById("<%=lblRequired.ClientID %>").innerText = "";
        return true;
    }
}
//ZD 100176 start
function DataLoading(val) {
    if (val == "1")
        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
    else
        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
}
//ZD 100176 End
</script>
<head runat="server">
    <title>Administrar Departamentos</title>
    <script type="text/javascript" src="inc/functions.js"></script>

</head>
<body>
    <form id="frmInventoryManagement" runat="server" method="post" onsubmit="return true;DataLoading(1);"> <%--ZD 100176--%>
      <input type="hidden" id="helpPage" value="97">

    <div>
        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <div id="dataLoadingDIV" style="display:none" align="center" >
                 <img border='0' src='image/wait1.gif'  alt='Cargando..' />
             </div> <%--ZD 100176--%> <%--ZD 100678 End--%>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td >&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Departamentos existentes</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgDepartments" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="true" OnItemCreated="BindRowsDeleteMessage"
                        OnDeleteCommand="DeleteDepartment" OnEditCommand="EditDepartment" OnUpdateCommand="UpdateDepartment" OnCancelCommand="CancelDepartment" Width="90%" Visible="true" style="border-collapse:separate"> <%--Edited for FF--%>
                       <%--Window Dressing - Start--%>
                        <SelectedItemStyle  CssClass="tableBody"  Font-Bold="True"  />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                        <FooterStyle CssClass="tableBody" />
                        <%--Window Dressing - End--%>
                        <HeaderStyle CssClass="tableHeader" />
                        <Columns>
                            <asp:BoundColumn DataField="id" Visible="false" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Nombre" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"> <%-- FB 2050 --%>
                                <ItemTemplate>
                                    <asp:Label ID="lblDepartmentName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.name") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtDepartmentName" CssClass="altText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.name")%>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqDepartmentName1" ControlToValidate="txtDepartmentName" runat="server" ErrorMessage="Necesario" Display="dynamic" ValidationGroup="Update" ></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtDepartmentName" Display="dynamic" runat="server" ValidationGroup="Update" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ y &#34; no son caracteres v�lidos." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Acciones" ItemStyle-Width="150px"> <%-- FB 2050 --%>
                                <HeaderStyle CssClass="tableHeader" />
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" Text="Editar" ID="btnEdit" CommandName="Edit" OnClientClick="DataLoading(1)"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton runat="server" Text="Eliminar" ID="btnDelete" CommandName="Delete" OnClientClick="DataLoading(1)"></asp:LinkButton><%--ZD 100176--%> 
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:LinkButton runat="server" Text="Actualizar" ID="btnUpdate" CommandName="Update" ValidationGroup="Update" OnClientClick="DataLoading(1)"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton runat="server" Text="Cancelar" ID="btnCancel" CommandName="Cancel" OnClientClick="DataLoading(1)"></asp:LinkButton><%--ZD 100176--%> 
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <%--Window Dressing --%>
                                    <b class="blackblodtext">Departamentos &nbsp;totales:&nbsp;</b><b> <asp:Label ID="lblTotalRecords" runat="server" Text=""></asp:Label> </b>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoDepartments" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError">
                                No se encontraron Departamentos.
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>                    
                </td>
            </tr>
            <tr id="trNew" runat="server" > <%--FB 2670--%>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext><asp:Label ID="lblCreateEditDepartment" runat="server" Text="Crear Nuevo"></asp:Label> Department</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trNew1" runat="server"> <%--FB 2670--%>
                <td align="center">
                    <table border="0" cellpadding="5" cellspacing="5" width="90%">
                        <tr>
                             <%--Window Dressing --%>
                            <td align="center" width="20%" class="blackblodtext">
                                Nombre de departamento: 
                                <asp:TextBox ID="txtNewDepartmentName" CssClass="altText" runat="server" Txext="" ></asp:TextBox>
                                <asp:Label ID="lblRequired" CssClass="lblError" runat="server"></asp:Label>
                                <asp:Button runat="server" ID="btnCreateNewDepartment" Width="100pt" Text="Entregar"  OnClick="CreateNewDepartment" ValidationGroup="Submit" /> <%--FB 2796--%>
                                <asp:RequiredFieldValidator ID="reqName1" ControlToValidate="txtNewDepartmentName" ErrorMessage="Necesario" ValidationGroup="Submit" runat="server" Display="dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtNewDepartmentName" Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ y &#34; no son caracteres v�lidos." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center"> <%--fogbugz case 413--%>
                    <table border="0" cellpadding="5" cellspacing="5" width="90%">
                        <tr>
                            <td align="center" width="20%" class="blackblodtext"><%--FB 2579--%>
                                (Para a�adir usuarios existentes a este departamento, realice las modificaciones necesarias en el perfil de usuario.)
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtMultiDepartment" runat="server" Text="" Visible="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>

<img src="keepalive.asp" alt="Sobrevivir" name="myPic" width="1px" height="1px" style="display:none"/> <%--ZD 100419--%>
    </form>

<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<%--ZD 100420 Start--%>
<script language="javascript">
    if (document.getElementById('txtNewDepartmentName') != null)
        document.getElementById('txtNewDepartmentName').setAttribute("onblur", "document.getElementById('btnCreateNewDepartment').focus(); document.getElementById('btnCreateNewDepartment').setAttribute('onfocus', '');");               
</script>
<%--ZD 100420 End--%>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>

