<%--ZD 100147 start--%>
<%--/* Copyright (C) 2014 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HolidayImport.aspx.cs" Inherits="ns_HolidayImport.HolidayImport" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 --><%--FB 2779--%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<script language="javascript">
//ZD 100604 start
var img = new Image();
img.src = "../sp/image/wait1.gif";
//ZD 100604 End
function DataLoading()
{
    var obj = document.getElementById("tblDataImport");
    if (obj != null)
        obj.style.display="";
}
</script>
    <title>Importaci�n de base de datos</title>
</head>
<body>
    <form id="frmDataImport" runat="server">
    <br /><br />
    <center>
        <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
    </center>
    <br /><br /><br />
        <h3 style="text-align: center">
            Herramienta de Importaci�n dia festivo</h3>
            <br /><br />            
            <table width="100%" bgcolor="white" cellpadding="1" cellspacing="0">
                <tr>
                    <td width="40%" align="center" style="display:none;">
                        <b>Tipo de Base de datos externa:</b>&nbsp;
                        <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstDatabaseType" runat="server">
                            <asp:ListItem Selected="True" Text="Rendezvous" Value="1"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="left">
                        <asp:FileUpload ID="fleMasterCSV" Width="20%" EnableViewState="true" runat="server" CssClass="altText" />  
                        <asp:Button ID="btnGetDataTable" Width="20%" runat="server" OnClick="GenerateDataTable" CssClass="altShortBlueButtonFormat" Text="Cargar D�as Festivos" />
                    </td>
                </tr>
            </table>
            <br /><br />
            
        <br />
        <table>
            <tr>
                <td align="left">
                    <b>NOTA:</b> A esta secci�n s�lo se deber�a acceder desde el servidor Web usando un Anfitri�n local. Los archivos de datos deber�an residir en el propio servidor Web
                </td>
            </tr>
        </table>
        <table id="tblDataImport">
            <tr>
                <td>
                    <b><font color="#FF00FF" size="2">Cargando datos...</font></b>&nbsp;&nbsp;&nbsp;&nbsp;<img border="0" src="image/wait1.gif" alt="Cargando..><%-- FB2742 --%>
                </td>
            </tr>
        </table>
    </form>
<br />
<br />
<p>&nbsp;</p>
<p>&nbsp;</p>
<script language="javascript">
document.getElementById("tblDataImport").style.display="none";
</script>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" --> 
</body>
</html>
