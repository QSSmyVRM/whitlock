﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2014 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.en_ManageVirtualMeetingRoom" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->


<script type="text/javascript">

    function getYourOwnEmailList(i) {
        if (i == -2) {
            if (queryField("sb") > 0)
                url = "emaillist2.aspx?t=e&frm=roomassist&wintype=ifr&fn=frmVirtualMettingRoom&n=";
            else
                url = "emaillist2main.aspx?t=e&frm=roomassist&fn=frmVirtualMettingRoom&n=";
        }
        else {
            url = "emaillist2main.aspx?t=e&frm=approver&fn=frmVirtualMettingRoom&n=" + i;
        }
        if (!window.winrtc) {	// has not yet been defined
            winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
            winrtc.focus();
        } else // has been defined
            if (!winrtc.closed) {     // still open
            winrtc.close();
            winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
            winrtc.focus();
        } else {
        winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
            winrtc.focus();
        }
    }


    function deleteAssistant() {//FB 2448
        eval("document.frmVirtualMettingRoom.AssistantID").value = "";
        eval("document.frmVirtualMettingRoom.Assistant").value = "";
    }

    function fnClose() {
        window.location.replace("manageroom.aspx?hf=&m=&pub=&d=&comp=&f=&frm=");
        return true;
    }

    function frmMainroom_Validator() {

        if (!Page_ClientValidate())
            return Page_IsValid;

        var txtroomname = document.getElementById('<%=txtRoomName.ClientID%>');
        if (txtroomname.value == "") {
            reqName.style.display = 'block';
            txtroomname.focus();
            return false;
        }
        else if (txtroomname.value.search(/^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@$%&~]*$/) == -1) {
            regRoomName.style.display = 'block';
            txtroomname.focus();
            return false;
        }

        var hdnmultipledept = document.getElementById('<%=hdnMultipleDept.ClientID%>');
        var departmentlist = document.getElementById('<%=DepartmentList.ClientID%>');

        if (hdnmultipledept.value == 1) {
            if ((departmentlist.value == "") && (departmentlist.length > 0)) {
                isConfirm = confirm("¿Seguro de que desea configurar este salón sin departamento(s) asignados?\n")
                if (isConfirm == false) {
                    return (false);
                }
            }
        }

        return (true);
    }
    // ZD 100753 Starts
    function PasswordChange(par) {
        if (document.getElementById('reqPassword1') != null) 
        {
            ValidatorEnable(document.getElementById('reqPassword1'), true);
        }

        document.getElementById("hdnPasschange").value = true;

        if (par == 1)
            document.getElementById("hdnPW1Visit").value = true;
        else
            document.getElementById("hdnPW2Visit").value = true;

        document.getElementById("hdnVMRpwd").value = document.getElementById("txtPassword1").value; 
    } 
 function fnTextFocus(xid,par) {
     
     var obj1 = document.getElementById("txtPassword1");
     var obj2 = document.getElementById("txtPassword2");
     
     if(document.getElementById("hdnPasschange").value == "false")
     {
         if(obj1.value == "" && obj2.value == "")
         {
             document.getElementById("txtPassword1").style.backgroundImage="";
             document.getElementById("txtPassword2").style.backgroundImage="";
             document.getElementById("txtPassword1").value="";
             document.getElementById("txtPassword2").value="";
         }
     }
     return false;
     
      var obj = document.getElementById(xid);
          
    if (par == 1) {
        if(document.getElementById("hdnPW2Visit") != null)
        {
            if(document.getElementById("hdnPW2Visit").value == "false")
            { 
                document.getElementById("txtPassword1").value = "";
                document.getElementById("txtPassword1_1").value="";
                document.getElementById("txtPassword2").value = "";
                document.getElementById("txtPassword1_2").value="";
            }else
            {
                document.getElementById("txtPassword1").value = "";
                document.getElementById("txtPassword1_1").value="";
            }
        }
        else
        {
            document.getElementById("txtPassword1").value = "";
            document.getElementById("txtPassword1_1").value="";
            document.getElementById("txtPassword2").value = "";
            document.getElementById("txtPassword1_2").value="";
        }
    }
       else{
           if(document.getElementById("hdnPW1Visit") != null)
           {
            if(document.getElementById("hdnPW1Visit").value == "false")
            { 
                document.getElementById("txtPassword1").value = "";
                document.getElementById("txtPassword1_1").value="";
                document.getElementById("txtPassword2").value = "";
                document.getElementById("txtPassword1_2").value="";
            }
            else{
                document.getElementById("txtPassword2").value = "";
                document.getElementById("txtPassword1_2").value="";
                }
           
           }
           else{
                document.getElementById("txtPassword2").value = "";
                document.getElementById("txtPassword1_2").value="";
                }
        }
        
         if(document.getElementById("cmpValPassword1")!= null)
        {
            ValidatorEnable(document.getElementById('cmpValPassword1'), false);
            ValidatorEnable(document.getElementById('cmpValPassword1'), true);
        }
         if(document.getElementById("cmpValPassword2")!= null)
        {
            ValidatorEnable(document.getElementById('cmpValPassword2'), false);
            ValidatorEnable(document.getElementById('cmpValPassword2'), true);
        }
    }
    // ZD 100753 Ends
</script>

<script type="text/javascript" src="inc/functions.js"></script>
<script type="text/javascript" src="script/errorList.js"></script>
<script type="text/javascript" src="extract.js"></script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Administrar el Salón de Reuniones Virtuales</title>
    <link rel="StyleSheet" href="css/divtable.css" type="text/css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css" />
    <%--<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main.css" />--%>
    <%--ZD 100664 Start--%>
    <script src="script/CallMonitorJquery/jquery.1.4.2.js" type="text/javascript"></script> 
    <script type="text/javascript">
     $(document).ready(function() {
           $('.treeNode').click(function() {
            var target = $(this).attr('tag');
            if ($('#' + target).is(":visible")) {
                $('#' + target).hide("slow");
                $(this).attr('src', 'image/loc/nolines_plus.gif');
            } else {
                $('#' + target).show("slow");
                
                $(this).attr('src', 'image/loc/nolines_minus.gif');
            }

        });
        });
    </script>
    <%--ZD 100664 End--%>
</head>
<body>
    <form id="frmVirtualMettingRoom" runat="server">
    <input type="hidden" id="hdnMultipleDept" runat="server" />
    <input type="hidden" id="AssistantID" runat="server" />
    <input type="hidden" id="hdnRoomID" runat="server" />
    <input type="hidden" id="AssistantName" runat="server" />
    <%--ZD 100753 Starts--%>
    <input type="hidden" id="hdnPasschange" value="false" runat="server"/> 
    <input type="hidden" id="hdnPW1Visit" value="false" runat="server"/>
    <input type="hidden" id="hdnPW2Visit" value="false" runat="server"/>
    <input id="txtPassword1_1" runat="server" type="hidden" />
    <input id="txtPassword1_2" runat="server" type="hidden" />
    <%--ZD 100753 Ends--%>
    <input type="hidden" id="hdnVMRpwd" value="" runat="server"/>
    
    <div>
        <center>
            <table border="0" width="100%" cellpadding="2" cellspacing="2">
                <tr>
                    <td align="center">
                        <h3>
                            <asp:Label ID="lblTitle" runat="server"></asp:Label></h3> <%--FB 2994--%>
                        <br />
                        <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
                    </td>
                </tr>
                <%--ZD 100664 - Start --%>
                    <tr id="trHistory" runat="server">
                        <td>
                            <table width="100%" border="0">
                                <tr>
                                    <td valign="top" align="left" style="width: 2px">
                                    <span class="blackblodtext" style="vertical-align:top">Historia</span>
                                        <a href="#" onmouseup="if(event.keyCode == 13){this.childNodes[0].click();return false;}" onkeydown="if(event.keyCode == 13){document.getElementById('imgHistory').click();return false;}" >
                                        <img id="imgHistory" src="image/loc/nolines_plus.gif" class="treeNode" style="border:none;" alt="Expand/Collapse" tag="tblHistory" /></a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                    <td> 
                      <div id="tblHistory" style="display:none; width:600px; float:left">
                        <table cellpadding="4" cellspacing="0" border="0" style="border-color:Gray; border-radius:15px; background-color:#ECE9D8"  width="75%" class="tableBody" align="center" >
                            <tr>
                                <td class="subtitleblueblodtext" align="center">
                                    Detalles
                                </td>            
                            </tr>
                            <tr align="center">
                                <td align="left">
                                    
                                    <asp:DataGrid ID="dgChangedHistory" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowSorting="true" 
                                        BorderStyle="solid" BorderWidth="0" ShowFooter="False"  
                                        Width="100%" Visible="true" style="border-collapse:separate" >
                                        <SelectedItemStyle  CssClass="tableBody"/>
                                        <AlternatingItemStyle CssClass="tableBody" />
                                        <ItemStyle CssClass="tableBody"  />                        
                                        <FooterStyle CssClass="tableBody"/>
                                        <HeaderStyle CssClass="tableHeader" />
                                        <Columns>
                                            <asp:BoundColumn DataField="ModifiedUserId"  HeaderStyle-Width="0%" Visible="false" ItemStyle-BackColor="#ECE9D8" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="ModifiedDateTime" HeaderText="Fecha" ItemStyle-BackColor="#ECE9D8" ></asp:BoundColumn>
                                            <asp:BoundColumn DataField="ModifiedUserName" HeaderText="Usario"  ItemStyle-BackColor="#ECE9D8" ></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Description" HeaderText="Descripción"  ItemStyle-BackColor="#ECE9D8" ></asp:BoundColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                    
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
                
                <%--ZD 100664 - End --%>
                <%--<tr>
                    <td align="left">
                        <table id="Table4" cellpadding="2" cellspacing="2" border="0" style="width: 100%">
                            <tr align="left">
                                <td style="width: 10%" align="left" valign="top" class="blackblodtext" nowrap>
                                    Último modificado por :
                                </td>
                                <td style="width: 85%" align="left" valign="top">
                                    <asp:Label ID="lblMUser" runat="server" CssClass="active"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%" align="left" valign="top" class="blackblodtext" nowrap>
                                    Último modificado a las :
                                </td>
                                <td style="width: 85%" align="left" valign="top">
                                    <asp:Label ID="lblMdate" runat="server" CssClass="active"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>--%>
                <tr>
                    <td class="subtitleblueblodtext" align="left">
                        Configuración Básica
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" width="100%" cellpadding="2" cellspacing="2" style="margin-left:20px">
                            <tr>
                                <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                    Nombre de salon <span class="reqfldText">*</span>
                                </td>
                                <td style="width: 35%" align="left" valign="top">
                                    <asp:TextBox ID="txtRoomName" runat="server" CssClass="altText"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqName" runat="server" ControlToValidate="txtRoomName"
                                        Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="Necesario"
                                        ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regRoomName" ControlToValidate="txtRoomName"
                                        Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true"
                                        ErrorMessage="<br> & < > + % \ ? | ^ = ! ` [ ] { } $ @  y ~ no son caracteres válidos."
                                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@$%&~]*$"></asp:RegularExpressionValidator>
                                </td>
                                <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                    Asistente Encargado<span class="reqfldText">*</span><%--FB 2975--%>
                                </td>
                                <td style="width: 30%" align="left" valign="top">
                                    <asp:TextBox ID="Assistant" runat="server" CssClass="altText"></asp:TextBox>
                                    <a id="EditHref" href="javascript:getYourOwnEmailList(-2);" onmouseover="window.status='';return true;">
                                        <img border="0" src="image/edit.gif" alt="Editar" width="17" height="15" style="cursor:pointer;" title="Libreta de direcciones" /></a> <%--FB 2798--%>
                                    <a href="javascript: deleteAssistant();" onmouseover="window.status='';return true;">
                                        <img border="0" src="image/btn_delete.gif" alt="Eliminar" width="16" height="16" style="cursor:pointer;" title="Eliminar" /></a> <%--FB 2798--%>
                                    <asp:RequiredFieldValidator ID="AssistantValidator" runat="server" ControlToValidate="Assistant"
                                        Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="Necesario"
                                        ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext">
                                    Número interno
                                </td>
                                <td align="left">
                                    <asp:TextBox CssClass="altText" ID="txtInternalnum" MaxLength="25" runat="server"></asp:TextBox>
                                </td>
                                <td align="left" class="blackblodtext">
                                    Número externo
                                </td>
                                <td align="left">
                                    <asp:TextBox CssClass="altText" ID="txtExternalnum" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <%--ZD 100753 Starts--%>
                             <tr id="trVMRPass">
                                <asp:Panel ID="pnlPassword1" runat="server" >
                                <td class="blackblodtext" align="left">Salón de Reuniones Virtuales Contraseña <%--ZD 100806--%>
                                  </td>
                                <td valign="top" nowrap="nowrap" align="left">
                              
                                    <asp:TextBox ID="txtPassword1"  style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" runat="server" TextMode="Password"  onblur="PasswordChange(1)" onfocus="fnTextFocus(this.id,1)" CssClass="altText"></asp:TextBox> 
                                    <asp:CompareValidator ID="cmpValPassword1" runat="server" ControlToCompare="txtPassword2"
                                        ControlToValidate="txtPassword1" Display="Dynamic" ErrorMessage="Por favor confirmar / introducir la contraseña similares"></asp:CompareValidator>
                                    <asp:RegularExpressionValidator ID="numPassword1" runat="server" ErrorMessage="<br> & < y > son caracteres no válidos." SetFocusOnError="True" ToolTip="<br> & < y > son caracteres no válidos." ControlToValidate="txtPassword1" ValidationExpression="^[^<>&]*$" Display="Dynamic"></asp:RegularExpressionValidator>
                                </td>
                                </asp:Panel>
                                <asp:Panel ID="pnlPassword2" runat="server">
                                <td class="blackblodtext" align="left">Confirmar contraseña</td>
                                <td style="height: 20px" valign="top" align="left">
                                    <asp:TextBox ID="txtPassword2"  style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" runat="server" CssClass="altText" TextMode="Password"  onblur="PasswordChange(2)" onfocus="fnTextFocus(this.id,2)" ></asp:TextBox>
                                    <asp:CompareValidator ID="cmpValPassword2" runat="server" ControlToCompare="txtPassword1"
                                        ControlToValidate="txtPassword2" Display="Dynamic" ErrorMessage="Sus contraseñas no coinciden."></asp:CompareValidator>
                                </td>
                                </asp:Panel>
                            </tr>
                            <%--ZD 100753 Ends--%>
                            <tr>
                                <td align="left" class="blackblodtext">
                                    Enlace Salón de Reuniones Virtuales <%--ZD 100806--%>
                                </td>
                                <td align="left">
                                    <asp:TextBox CssClass="altText" ID="txtVMRLink" runat="server"  MaxLength="325" Rows="5" ></asp:TextBox>
                                </td>
                                <td align="left">
                                    &nbsp;
                                </td>
                                <td align="left" class="blackblodtext">
                                    &nbsp;
                                </td>
                                
                            </tr>
                        </table>
                    </td>
                </tr>
                
                <%--FB 2994 Start--%>
                
                <tr>
                    <td class="subtitleblueblodtext" align="left">
                        imagen
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="Table1" cellpadding="2" cellspacing="2" border="0" style="width: 90%; margin-left:20px">
                            <tr>
                                <td align="left" style="width:18%" valign="top" class="blackblodtext">
                                    Imagen del Salón
                                </td>
                                <td colspan="3" align="left" class="blackblodtext"> 
                                    <div>
                                        <input type="text" class="file_input_textbox" readonly="readonly" value='No hay archivos seleccionados' />
                                        <div class="file_input_div">
                                            <input type="button" value="Navegar" class="file_input_button" onclick="document.getElementById('roomfileimage').click();return false;"   /><%--FB 3055-Filter in Upload Files--%>
                                            <input type="file"  class="file_input_hidden" id="roomfileimage" accept="image/*" tabindex="-1" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this)"/>
                                        </div>
                                    </div>
                                    <asp:RegularExpressionValidator ID="regroomfileimage" runat="server" Display="Dynamic" ValidationGroup="Submit1" ControlToValidate="roomfileimage" CssClass="lblError" ErrorMessage="Tipo de archivo no válido." ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G)|(b|B)(m|M)(p|P))$"></asp:RegularExpressionValidator>
                                    <asp:Button ID="BtnUploadRmImg" CssClass="altLongBlueButtonFormat" runat="server"  style="margin-left:2px"
                                        Text="Cargar Imagen del salón" OnClick="UploadRoomImage" ValidationGroup="Submit1" /> <%--FB 3055-Filter in Upload Files--%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan = "4"> 
                        <div style="overflow-y: hidden; overflow-x: auto; height: auto; width: 600px; margin-left:18%">
                            <asp:DataGrid BorderColor="blue" BorderStyle="solid" BorderWidth="1" ID="dgItems"
                                AutoGenerateColumns="false" OnItemCreated="BindRowsDeleteMessage" OnDeleteCommand="RemoveImage"
                                runat="server" Width="70%" GridLines="None" Visible="false" Style="border-collapse: separate">
                                <HeaderStyle Height="30" CssClass="tableHeader" HorizontalAlign="Center" />
                                <AlternatingItemStyle CssClass="tableBody" />
                                <ItemStyle CssClass="tableBody" />
                                <FooterStyle CssClass="tableBody" />
                                <Columns>
                                    <asp:BoundColumn DataField="ImageName" Visible="true" HeaderText="Nombre" HeaderStyle-CssClass="tableHeader"
                                        ItemStyle-Width="20%"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Image" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Imagetype" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="ImagePath" Visible="false"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="imagen" HeaderStyle-CssClass="tableHeader" ItemStyle-Width="40%" ItemStyle-HorizontalAlign="center">
                                        <ItemTemplate>
                                            <asp:Image ID="itemImage" AlternateText="imagen del artículo" ImageUrl='<%# DataBinder.Eval(Container, "DataItem.ImagePath") %>'  
                                                Width="30" Height="30" runat="server" /> <%--ZD 100419--%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Acciones" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnDelete" Text="Quitar" CommandName="Delete" runat="server"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid><br />
                        </div>
                    </td>
                </tr>
                
                <%--FB 2994 End--%>
                
                <tr>
                    <td align="left">
                        <span class="subtitleblueblodtext">departamento</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="tbRoomsDept" cellpadding="2" cellspacing="2" border="0" style="width: 90%; margin-left:20px">
                            <tr>
                                <td width="16%" align="left" valign="top" class="blackblodtext">
                                    Departamentos del salón
                                </td>
                                <td style="width: 75%" align="left" valign="top">
                                    <asp:ListBox ID="DepartmentList" runat="server" CssClass="altText" DataTextField="Name"
                                        DataValueField="ID" SelectionMode="multiple"></asp:ListBox>
                                      <%--ZD 100422 Start--%>
                                     <span style='color: #666666;'>* Presione Ctrl + clic para seleccionar varios Departamentos</span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="height: 8px">
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table id="tblButtons" cellpadding="2" cellspacing="2" border="0" style="width: 90%">
                            <tr>
                                <td align="center" style="width: 20%">
                                    <asp:Button ID="btnReset" runat="server" Text="Reajustar" CssClass="altMedium0BlueButtonFormat"
                                        OnClick="ResetRoomProfile" /> <%--ZD 100288_5Dec2013--%>
                                </td>
                                <td align="center" style="width: 20%">
                                    <input name="Go" id="btnGoBack" type="button" class="altMedium0BlueButtonFormat" onclick="javascript:fnClose();"
                                        value=" Regresar " /><%--ZD 100428--%>
                                </td>
                                <td align="center" style="width: 20%">
                                    <asp:Button ID="btnSubmitAddNew" runat="server" ValidationGroup="Submit" Text="Entregar / Salón Nuevo"
                                         OnClick="SetRoomProfile" OnClientClick="javascript:return frmMainroom_Validator()" style="width:200px" />
                                </td>
                                <td align="center" style="width: 20%">
                                    <asp:Button ID="btnSubmit" ValidationGroup="Submit" runat="server" Text="Entregar" Width="150pt"
                                         OnClick="SetRoomProfile" OnClientClick="javascript:return frmMainroom_Validator()" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </center>
    </div>
    </form>
</body>
</html>

<script type="text/javascript">
    document.onkeydown = function(evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 8) {
            if (document.getElementById("btnCancel") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnCancel").click();
                    return false;
                }
            }
            if (document.getElementById("btnGoBack") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnGoBack").click();
                    return false;
                }
            }
        }
        fnOnKeyDown(evt);
    };

    if (document.getElementById("hdnMultipleDept"))
        if (document.getElementById("hdnMultipleDept").value == "0") {
        if (document.getElementById("trRoomsDept") != null)
            document.getElementById("trRoomsDept").style.display = "none";
    }

    if (document.getElementById("hdnVMRpwd").value != "") {
        var val = document.getElementById("hdnVMRpwd").value;
        document.getElementById("txtPassword1").value = val;
        document.getElementById("txtPassword2").value = val;
    }
    //        
</script>

<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
